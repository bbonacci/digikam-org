var classDigikam_1_1NamespaceEntry =
[
    [ "NamespaceType", "classDigikam_1_1NamespaceEntry.html#ad7a9ffe43a58c8f51307f3abcca82a67", [
      [ "TAGS", "classDigikam_1_1NamespaceEntry.html#ad7a9ffe43a58c8f51307f3abcca82a67a9d45e39fabb684d06de085697e4a8193", null ],
      [ "TITLE", "classDigikam_1_1NamespaceEntry.html#ad7a9ffe43a58c8f51307f3abcca82a67a01bd622322052466519191cf3f849de6", null ],
      [ "RATING", "classDigikam_1_1NamespaceEntry.html#ad7a9ffe43a58c8f51307f3abcca82a67a112e9686f8a90808a0710f0701d5a15d", null ],
      [ "COMMENT", "classDigikam_1_1NamespaceEntry.html#ad7a9ffe43a58c8f51307f3abcca82a67ac9d6b788776624fcce9e85144dacbedb", null ],
      [ "COLORLABEL", "classDigikam_1_1NamespaceEntry.html#ad7a9ffe43a58c8f51307f3abcca82a67aff4cc117d86a4603c3001f5006770c3b", null ]
    ] ],
    [ "NsSubspace", "classDigikam_1_1NamespaceEntry.html#ae868ba165b38e72650f61390013583d4", [
      [ "EXIF", "classDigikam_1_1NamespaceEntry.html#ae868ba165b38e72650f61390013583d4a31b80011ac456167f074ebbf351557e1", null ],
      [ "IPTC", "classDigikam_1_1NamespaceEntry.html#ae868ba165b38e72650f61390013583d4a76af3e900f714060ccf8046e4a3228a3", null ],
      [ "XMP", "classDigikam_1_1NamespaceEntry.html#ae868ba165b38e72650f61390013583d4ac091d8209c9501188ae4908d79be8e5e", null ]
    ] ],
    [ "SpecialOptions", "classDigikam_1_1NamespaceEntry.html#a24863503dbe3cfbb9cbdcbbccb9b8c8b", [
      [ "NO_OPTS", "classDigikam_1_1NamespaceEntry.html#a24863503dbe3cfbb9cbdcbbccb9b8c8bae1c507d1e66715ad49bdc15e7973fac7", null ],
      [ "COMMENT_ALTLANG", "classDigikam_1_1NamespaceEntry.html#a24863503dbe3cfbb9cbdcbbccb9b8c8bacdce7ef3b54aebf04f7953d0892df797", null ],
      [ "COMMENT_ATLLANGLIST", "classDigikam_1_1NamespaceEntry.html#a24863503dbe3cfbb9cbdcbbccb9b8c8ba71bb1b93b89daa6ff09c0df1281cd68b", null ],
      [ "COMMENT_XMP", "classDigikam_1_1NamespaceEntry.html#a24863503dbe3cfbb9cbdcbbccb9b8c8ba4da0bd791bf0a2118a9e8838f7c43cf2", null ],
      [ "COMMENT_JPEG", "classDigikam_1_1NamespaceEntry.html#a24863503dbe3cfbb9cbdcbbccb9b8c8ba82cc66f03ba3ed22ca95f5610675f11b", null ],
      [ "TAG_XMPBAG", "classDigikam_1_1NamespaceEntry.html#a24863503dbe3cfbb9cbdcbbccb9b8c8ba24c7d17501fbb192ea79d5fecc73bb8a", null ],
      [ "TAG_XMPSEQ", "classDigikam_1_1NamespaceEntry.html#a24863503dbe3cfbb9cbdcbbccb9b8c8bab8bf4347bf211f5dbce8c0c9bff086c6", null ],
      [ "TAG_ACDSEE", "classDigikam_1_1NamespaceEntry.html#a24863503dbe3cfbb9cbdcbbccb9b8c8ba4307087b39fb0d40e39a0995d7a8399a", null ]
    ] ],
    [ "TagType", "classDigikam_1_1NamespaceEntry.html#a82d5c3c9bc906d46d35e85eb995e7c8a", [
      [ "TAG", "classDigikam_1_1NamespaceEntry.html#a82d5c3c9bc906d46d35e85eb995e7c8aa90fb4dae9c804f534650d8f64c1f04bd", null ],
      [ "TAGPATH", "classDigikam_1_1NamespaceEntry.html#a82d5c3c9bc906d46d35e85eb995e7c8aa82e7dcd74d9bc36e144ee5535edf206b", null ]
    ] ],
    [ "NamespaceEntry", "classDigikam_1_1NamespaceEntry.html#a46f2c26cf5bd68921b83f37694bbd247", null ],
    [ "NamespaceEntry", "classDigikam_1_1NamespaceEntry.html#a47a0fc39be646036f7a97f24d97066fa", null ],
    [ "~NamespaceEntry", "classDigikam_1_1NamespaceEntry.html#aa8106d139975eae38e64f3a010eff7af", null ],
    [ "alternativeName", "classDigikam_1_1NamespaceEntry.html#a1a623291b5de86514ef25990c4653bba", null ],
    [ "convertRatio", "classDigikam_1_1NamespaceEntry.html#a1a14eea8ea194fdc1967d089a978f37f", null ],
    [ "index", "classDigikam_1_1NamespaceEntry.html#a7f754bde70b49880c37cd9de9767b634", null ],
    [ "isDefault", "classDigikam_1_1NamespaceEntry.html#af8d2681eabef7a1740d9929fc963c361", null ],
    [ "isDisabled", "classDigikam_1_1NamespaceEntry.html#a5327784a8521c715669067094d6eb22e", null ],
    [ "namespaceName", "classDigikam_1_1NamespaceEntry.html#ac46be1b43d12f1e2716bc74da6f0f933", null ],
    [ "nsType", "classDigikam_1_1NamespaceEntry.html#a45a63c40c4c416e971863ba8c4b2de66", null ],
    [ "secondNameOpts", "classDigikam_1_1NamespaceEntry.html#aa09da87c35aaf376da1333768520800c", null ],
    [ "separator", "classDigikam_1_1NamespaceEntry.html#ae256c89df0dbf354552f0b9b8d45e05c", null ],
    [ "specialOpts", "classDigikam_1_1NamespaceEntry.html#a8bf40a7e05deb9d3f6bc99bee90e279a", null ],
    [ "subspace", "classDigikam_1_1NamespaceEntry.html#a41d9f067fd3a0c9322187732f7976386", null ],
    [ "tagPaths", "classDigikam_1_1NamespaceEntry.html#a5fb755a63cbfb9a6874c9736041ba012", null ]
];