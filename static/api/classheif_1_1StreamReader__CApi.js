var classheif_1_1StreamReader__CApi =
[
    [ "grow_status", "classheif_1_1StreamReader__CApi.html#a053ecfc73f3aa86fd7124c67e07b293d", [
      [ "size_reached", "classheif_1_1StreamReader__CApi.html#a053ecfc73f3aa86fd7124c67e07b293daa01031f5674c88e9c7a7cdf725052206", null ],
      [ "timeout", "classheif_1_1StreamReader__CApi.html#a053ecfc73f3aa86fd7124c67e07b293da5c5fb607a90f14d46a00f957d246f2bb", null ],
      [ "size_beyond_eof", "classheif_1_1StreamReader__CApi.html#a053ecfc73f3aa86fd7124c67e07b293da02f265b40741b4b568a9e60285098657", null ]
    ] ],
    [ "StreamReader_CApi", "classheif_1_1StreamReader__CApi.html#a0fd46efb36b7dc40dba19b75159ffd93", null ],
    [ "get_position", "classheif_1_1StreamReader__CApi.html#a305cccdee38699ee4ca503c17c838f8c", null ],
    [ "read", "classheif_1_1StreamReader__CApi.html#a981c3ffafbe0e43ba3dd91fb1437a629", null ],
    [ "seek", "classheif_1_1StreamReader__CApi.html#a5f9a0161123272cff98255f7a5a050de", null ],
    [ "seek_cur", "classheif_1_1StreamReader__CApi.html#a5fef44e1d5988070c93b1cfdd3ad021f", null ],
    [ "wait_for_file_size", "classheif_1_1StreamReader__CApi.html#a6923861f55e9a768c5cf699d30288f26", null ]
];