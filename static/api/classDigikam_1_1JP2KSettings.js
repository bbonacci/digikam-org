var classDigikam_1_1JP2KSettings =
[
    [ "JP2KSettings", "classDigikam_1_1JP2KSettings.html#a851f3244e79969aa2db46ab39567790d", null ],
    [ "~JP2KSettings", "classDigikam_1_1JP2KSettings.html#a9bad75d3f1be2a4a8845220251278130", null ],
    [ "getCompressionValue", "classDigikam_1_1JP2KSettings.html#a9a15447d0380103acac0035b2ec56367", null ],
    [ "getLossLessCompression", "classDigikam_1_1JP2KSettings.html#a0f96d817378d4025f37526155ab3f2bc", null ],
    [ "setCompressionValue", "classDigikam_1_1JP2KSettings.html#a7a750ec89f4379a94efef07e994961a6", null ],
    [ "setLossLessCompression", "classDigikam_1_1JP2KSettings.html#a55ff00e97b39eb334d99513878ff053e", null ],
    [ "signalSettingsChanged", "classDigikam_1_1JP2KSettings.html#accbd64b2af7ab33cde34a186e501eb59", null ]
];