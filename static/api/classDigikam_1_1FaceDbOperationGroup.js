var classDigikam_1_1FaceDbOperationGroup =
[
    [ "FaceDbOperationGroup", "classDigikam_1_1FaceDbOperationGroup.html#abec993214eb0148c0e0cc3ad8034812d", null ],
    [ "FaceDbOperationGroup", "classDigikam_1_1FaceDbOperationGroup.html#a423258a898a4782931af24765734a586", null ],
    [ "~FaceDbOperationGroup", "classDigikam_1_1FaceDbOperationGroup.html#a755ff65974a3d5304f2ed1941ea14c82", null ],
    [ "allowLift", "classDigikam_1_1FaceDbOperationGroup.html#ad9191f46b6beedbb0bd48ab4dcf56565", null ],
    [ "lift", "classDigikam_1_1FaceDbOperationGroup.html#a657f2b83056ea14d8c21f2c75d124012", null ],
    [ "resetTime", "classDigikam_1_1FaceDbOperationGroup.html#ada52a53bedd2619e879109bf9dc538b8", null ],
    [ "setMaximumTime", "classDigikam_1_1FaceDbOperationGroup.html#abb3e9f9977cf71f03bbde6c70f017074", null ]
];