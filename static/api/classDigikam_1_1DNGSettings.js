var classDigikam_1_1DNGSettings =
[
    [ "DNGSettings", "classDigikam_1_1DNGSettings.html#abc60015c777442e5c696419418f37ab0", null ],
    [ "~DNGSettings", "classDigikam_1_1DNGSettings.html#ae78bd5059652050252d562f0fe3bbe92", null ],
    [ "backupOriginalRawFile", "classDigikam_1_1DNGSettings.html#afdb0751f0451cc511ec399bc214d731e", null ],
    [ "compressLossLess", "classDigikam_1_1DNGSettings.html#a4d9cb8677caaa5b457aaa7dfa8b42c08", null ],
    [ "previewMode", "classDigikam_1_1DNGSettings.html#afacf1c24f7fa9d01cc2fc42c80aab199", null ],
    [ "setBackupOriginalRawFile", "classDigikam_1_1DNGSettings.html#a8a38f6b4957455e0c79ce7b94f539b6f", null ],
    [ "setCompressLossLess", "classDigikam_1_1DNGSettings.html#a4896841b3bdce9a491d5f7ab0b17348c", null ],
    [ "setDefaultSettings", "classDigikam_1_1DNGSettings.html#a721eb13e863222d77b2400b30297ee91", null ],
    [ "setPreviewMode", "classDigikam_1_1DNGSettings.html#ac1fb010d37e83a49bdfab0e1ca6e00bf", null ],
    [ "signalSettingsChanged", "classDigikam_1_1DNGSettings.html#ae77c77ad895efe6e736425f68cefed52", null ],
    [ "signalSetupExifTool", "classDigikam_1_1DNGSettings.html#a8df62c489ff208a37c86123c7cb400fe", null ],
    [ "slotSetupChanged", "classDigikam_1_1DNGSettings.html#aa74cf5a5b3ebe14412a70c6c5f7298f4", null ]
];