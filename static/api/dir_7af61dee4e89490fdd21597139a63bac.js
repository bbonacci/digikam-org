var dir_7af61dee4e89490fdd21597139a63bac =
[
    [ "antivignetting", "dir_7c37dea6ded59e7c75b0baa7bbd8a91a.html", "dir_7c37dea6ded59e7c75b0baa7bbd8a91a" ],
    [ "blur", "dir_d0cd5c426fb6b523c5b93e316e48a432.html", "dir_d0cd5c426fb6b523c5b93e316e48a432" ],
    [ "hotpixels", "dir_7af4ae5ea1ddf14b6f6968955d41da96.html", "dir_7af4ae5ea1ddf14b6f6968955d41da96" ],
    [ "lensautofix", "dir_fb2c3f0b3b356c3dbfaf36df06c17eb3.html", "dir_fb2c3f0b3b356c3dbfaf36df06c17eb3" ],
    [ "localcontrast", "dir_3388b39ae710c14d7bc8532465f3dc87.html", "dir_3388b39ae710c14d7bc8532465f3dc87" ],
    [ "noisereduction", "dir_4426e93000a4343b833e0ad0c8cb04ca.html", "dir_4426e93000a4343b833e0ad0c8cb04ca" ],
    [ "redeyecorrection", "dir_9b87e8578f676498e1279229ee5f96df.html", "dir_9b87e8578f676498e1279229ee5f96df" ],
    [ "restoration", "dir_b4f9b75ec8b585a8e0d4d74f50b7cc8b.html", "dir_b4f9b75ec8b585a8e0d4d74f50b7cc8b" ],
    [ "sharpen", "dir_e816bf53214418285674ff5d52808cb4.html", "dir_e816bf53214418285674ff5d52808cb4" ]
];