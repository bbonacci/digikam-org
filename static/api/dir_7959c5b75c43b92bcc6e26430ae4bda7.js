var dir_7959c5b75c43b92bcc6e26430ae4bda7 =
[
    [ "itemiconview.cpp", "itemiconview_8cpp.html", null ],
    [ "itemiconview.h", "itemiconview_8h.html", [
      [ "ItemIconView", "classDigikam_1_1ItemIconView.html", "classDigikam_1_1ItemIconView" ]
    ] ],
    [ "itemiconview_albums.cpp", "itemiconview__albums_8cpp.html", null ],
    [ "itemiconview_groups.cpp", "itemiconview__groups_8cpp.html", null ],
    [ "itemiconview_iqs.cpp", "itemiconview__iqs_8cpp.html", null ],
    [ "itemiconview_items.cpp", "itemiconview__items_8cpp.html", null ],
    [ "itemiconview_p.h", "itemiconview__p_8h.html", [
      [ "Private", "classDigikam_1_1ItemIconView_1_1Private.html", "classDigikam_1_1ItemIconView_1_1Private" ]
    ] ],
    [ "itemiconview_search.cpp", "itemiconview__search_8cpp.html", null ],
    [ "itemiconview_sidebars.cpp", "itemiconview__sidebars_8cpp.html", null ],
    [ "itemiconview_tags.cpp", "itemiconview__tags_8cpp.html", null ],
    [ "itemiconview_tools.cpp", "itemiconview__tools_8cpp.html", null ],
    [ "itemiconview_views.cpp", "itemiconview__views_8cpp.html", null ],
    [ "itemiconview_zoom.cpp", "itemiconview__zoom_8cpp.html", null ],
    [ "mapwidgetview.cpp", "mapwidgetview_8cpp.html", null ],
    [ "mapwidgetview.h", "mapwidgetview_8h.html", [
      [ "MapViewModelHelper", "classDigikam_1_1MapViewModelHelper.html", "classDigikam_1_1MapViewModelHelper" ],
      [ "MapWidgetView", "classDigikam_1_1MapWidgetView.html", "classDigikam_1_1MapWidgetView" ]
    ] ],
    [ "stackedview.cpp", "stackedview_8cpp.html", null ],
    [ "stackedview.h", "stackedview_8h.html", [
      [ "StackedView", "classDigikam_1_1StackedView.html", "classDigikam_1_1StackedView" ]
    ] ],
    [ "trashview.cpp", "trashview_8cpp.html", null ],
    [ "trashview.h", "trashview_8h.html", [
      [ "ThumbnailAligningDelegate", "classDigikam_1_1ThumbnailAligningDelegate.html", "classDigikam_1_1ThumbnailAligningDelegate" ],
      [ "TrashView", "classDigikam_1_1TrashView.html", "classDigikam_1_1TrashView" ]
    ] ],
    [ "welcomepageview.cpp", "welcomepageview_8cpp.html", null ],
    [ "welcomepageview.h", "welcomepageview_8h.html", [
      [ "WelcomePageView", "classDigikam_1_1WelcomePageView.html", "classDigikam_1_1WelcomePageView" ]
    ] ]
];