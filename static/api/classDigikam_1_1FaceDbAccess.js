var classDigikam_1_1FaceDbAccess =
[
    [ "FaceDbAccess", "classDigikam_1_1FaceDbAccess.html#a902d792ad0de76be2862b3d144d7c7c8", null ],
    [ "~FaceDbAccess", "classDigikam_1_1FaceDbAccess.html#aa026c12c4fc9bc80060f1eb548e8ef1c", null ],
    [ "backend", "classDigikam_1_1FaceDbAccess.html#acfb25e296b2ccec2b32eaf2abec63c86", null ],
    [ "db", "classDigikam_1_1FaceDbAccess.html#a81095cec277c23055b72b7faa10114c7", null ],
    [ "lastError", "classDigikam_1_1FaceDbAccess.html#ae30f65c045e4d402aa8a33dcc2e0387e", null ],
    [ "setLastError", "classDigikam_1_1FaceDbAccess.html#a6053dcb68567d88997cf339d9a2aece5", null ],
    [ "FaceDbAccessUnlock", "classDigikam_1_1FaceDbAccess.html#ad70c7b93096d7fa5882015c76652acc2", null ]
];