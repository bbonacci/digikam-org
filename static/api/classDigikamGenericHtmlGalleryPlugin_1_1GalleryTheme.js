var classDigikamGenericHtmlGalleryPlugin_1_1GalleryTheme =
[
    [ "List", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryTheme.html#aa91efcc58a408946631253bc1aa156b7", null ],
    [ "ParameterList", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryTheme.html#a5499b03fb744b1bc80735e89192d88fe", null ],
    [ "Ptr", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryTheme.html#aa261c2f3b5c064108c84045a09b7ef38", null ],
    [ "~GalleryTheme", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryTheme.html#a1428d97fa9adc5ef4a323241592130fe", null ],
    [ "allowNonsquareThumbnails", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryTheme.html#a1f8f509724888926c62f73342cd36d36", null ],
    [ "authorName", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryTheme.html#a85901604fbd1e0a7a097c59b90b09172", null ],
    [ "authorUrl", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryTheme.html#ad30fa0e33209015ed5249897614bff19", null ],
    [ "comment", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryTheme.html#acc3113c5a11eb6c76d255895fc16d139", null ],
    [ "directory", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryTheme.html#a909da4cb3d4b0de86c145f9539f58011", null ],
    [ "internalName", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryTheme.html#a5da30aa9afc5847bd5142937d5fb2114", null ],
    [ "name", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryTheme.html#a2eea2db7005a82b2fda4c8306f8a972b", null ],
    [ "parameterList", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryTheme.html#a1567739a0ce86d83d973d9224d92a1d5", null ],
    [ "previewName", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryTheme.html#a36a518ed0732c2b222b9bc69e9a865e5", null ],
    [ "previewUrl", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryTheme.html#a63ddd292f135a43e5ebdd75d3b2b63bd", null ]
];