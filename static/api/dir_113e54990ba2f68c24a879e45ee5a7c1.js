var dir_113e54990ba2f68c24a879e45ee5a7c1 =
[
    [ "coredb.cpp", "coredb_8cpp.html", null ],
    [ "coredb.h", "coredb_8h.html", [
      [ "CoreDB", "classDigikam_1_1CoreDB.html", "classDigikam_1_1CoreDB" ]
    ] ],
    [ "coredbaccess.cpp", "coredbaccess_8cpp.html", null ],
    [ "coredbaccess.h", "coredbaccess_8h.html", [
      [ "CoreDbAccess", "classDigikam_1_1CoreDbAccess.html", "classDigikam_1_1CoreDbAccess" ],
      [ "CoreDbAccessUnlock", "classDigikam_1_1CoreDbAccessUnlock.html", "classDigikam_1_1CoreDbAccessUnlock" ]
    ] ],
    [ "coredbalbuminfo.h", "coredbalbuminfo_8h.html", "coredbalbuminfo_8h" ],
    [ "coredbbackend.cpp", "coredbbackend_8cpp.html", null ],
    [ "coredbbackend.h", "coredbbackend_8h.html", [
      [ "CoreDbBackend", "classDigikam_1_1CoreDbBackend.html", "classDigikam_1_1CoreDbBackend" ]
    ] ],
    [ "coredbbackend_p.h", "coredbbackend__p_8h.html", [
      [ "CoreDbBackendPrivate", "classDigikam_1_1CoreDbBackendPrivate.html", "classDigikam_1_1CoreDbBackendPrivate" ],
      [ "ChangesetContainer", "classDigikam_1_1CoreDbBackendPrivate_1_1ChangesetContainer.html", "classDigikam_1_1CoreDbBackendPrivate_1_1ChangesetContainer" ]
    ] ],
    [ "coredbchangesets.cpp", "coredbchangesets_8cpp.html", null ],
    [ "coredbchangesets.h", "coredbchangesets_8h.html", [
      [ "AlbumChangeset", "classDigikam_1_1AlbumChangeset.html", "classDigikam_1_1AlbumChangeset" ],
      [ "AlbumRootChangeset", "classDigikam_1_1AlbumRootChangeset.html", "classDigikam_1_1AlbumRootChangeset" ],
      [ "CollectionImageChangeset", "classDigikam_1_1CollectionImageChangeset.html", "classDigikam_1_1CollectionImageChangeset" ],
      [ "ImageChangeset", "classDigikam_1_1ImageChangeset.html", "classDigikam_1_1ImageChangeset" ],
      [ "ImageTagChangeset", "classDigikam_1_1ImageTagChangeset.html", "classDigikam_1_1ImageTagChangeset" ],
      [ "SearchChangeset", "classDigikam_1_1SearchChangeset.html", "classDigikam_1_1SearchChangeset" ],
      [ "TagChangeset", "classDigikam_1_1TagChangeset.html", "classDigikam_1_1TagChangeset" ]
    ] ],
    [ "coredbchecker.cpp", "coredbchecker_8cpp.html", null ],
    [ "coredbchecker.h", "coredbchecker_8h.html", [
      [ "CoreDbPrivilegesChecker", "classDigikam_1_1CoreDbPrivilegesChecker.html", "classDigikam_1_1CoreDbPrivilegesChecker" ]
    ] ],
    [ "coredbconstants.cpp", "coredbconstants_8cpp.html", null ],
    [ "coredbconstants.h", "coredbconstants_8h.html", "coredbconstants_8h" ],
    [ "coredbcopymanager.cpp", "coredbcopymanager_8cpp.html", null ],
    [ "coredbcopymanager.h", "coredbcopymanager_8h.html", [
      [ "CoreDbCopyManager", "classDigikam_1_1CoreDbCopyManager.html", "classDigikam_1_1CoreDbCopyManager" ]
    ] ],
    [ "coredbdownloadhistory.cpp", "coredbdownloadhistory_8cpp.html", null ],
    [ "coredbdownloadhistory.h", "coredbdownloadhistory_8h.html", [
      [ "CoreDbDownloadHistory", "classDigikam_1_1CoreDbDownloadHistory.html", null ]
    ] ],
    [ "coredbfields.h", "coredbfields_8h.html", "coredbfields_8h" ],
    [ "coredbinfocontainers.h", "coredbinfocontainers_8h.html", [
      [ "ImageCommonContainer", "classDigikam_1_1ImageCommonContainer.html", "classDigikam_1_1ImageCommonContainer" ],
      [ "ImageMetadataContainer", "classDigikam_1_1ImageMetadataContainer.html", "classDigikam_1_1ImageMetadataContainer" ],
      [ "VideoMetadataContainer", "classDigikam_1_1VideoMetadataContainer.html", "classDigikam_1_1VideoMetadataContainer" ]
    ] ],
    [ "coredbnamefilter.cpp", "coredbnamefilter_8cpp.html", null ],
    [ "coredbnamefilter.h", "coredbnamefilter_8h.html", [
      [ "CoreDbNameFilter", "classDigikam_1_1CoreDbNameFilter.html", "classDigikam_1_1CoreDbNameFilter" ]
    ] ],
    [ "coredboperationgroup.cpp", "coredboperationgroup_8cpp.html", null ],
    [ "coredboperationgroup.h", "coredboperationgroup_8h.html", [
      [ "CoreDbOperationGroup", "classDigikam_1_1CoreDbOperationGroup.html", "classDigikam_1_1CoreDbOperationGroup" ]
    ] ],
    [ "coredbschemaupdater.cpp", "coredbschemaupdater_8cpp.html", null ],
    [ "coredbschemaupdater.h", "coredbschemaupdater_8h.html", [
      [ "CoreDbSchemaUpdater", "classDigikam_1_1CoreDbSchemaUpdater.html", "classDigikam_1_1CoreDbSchemaUpdater" ]
    ] ],
    [ "coredbsearchxml.cpp", "coredbsearchxml_8cpp.html", null ],
    [ "coredbsearchxml.h", "coredbsearchxml_8h.html", "coredbsearchxml_8h" ],
    [ "coredbthumbinfoprovider.cpp", "coredbthumbinfoprovider_8cpp.html", null ],
    [ "coredbthumbinfoprovider.h", "coredbthumbinfoprovider_8h.html", [
      [ "DatabaseLoadSaveFileInfoProvider", "classDigikam_1_1DatabaseLoadSaveFileInfoProvider.html", "classDigikam_1_1DatabaseLoadSaveFileInfoProvider" ],
      [ "ThumbsDbInfoProvider", "classDigikam_1_1ThumbsDbInfoProvider.html", "classDigikam_1_1ThumbsDbInfoProvider" ]
    ] ],
    [ "coredbtransaction.cpp", "coredbtransaction_8cpp.html", null ],
    [ "coredbtransaction.h", "coredbtransaction_8h.html", [
      [ "CoreDbTransaction", "classDigikam_1_1CoreDbTransaction.html", "classDigikam_1_1CoreDbTransaction" ]
    ] ],
    [ "coredburl.cpp", "coredburl_8cpp.html", null ],
    [ "coredburl.h", "coredburl_8h.html", [
      [ "CoreDbUrl", "classDigikam_1_1CoreDbUrl.html", "classDigikam_1_1CoreDbUrl" ]
    ] ],
    [ "coredbwatch.cpp", "coredbwatch_8cpp.html", null ],
    [ "coredbwatch.h", "coredbwatch_8h.html", [
      [ "CoreDbWatch", "classDigikam_1_1CoreDbWatch.html", "classDigikam_1_1CoreDbWatch" ]
    ] ],
    [ "coredbwatchadaptor.cpp", "coredbwatchadaptor_8cpp.html", null ],
    [ "coredbwatchadaptor.h", "coredbwatchadaptor_8h.html", [
      [ "CoreDbWatchAdaptor", "classCoreDbWatchAdaptor.html", "classCoreDbWatchAdaptor" ]
    ] ]
];