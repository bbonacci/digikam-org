var dir_562da0a5ee0c7d161685aed09bad74a7 =
[
    [ "dbjob.cpp", "dbjob_8cpp.html", null ],
    [ "dbjob.h", "dbjob_8h.html", [
      [ "AlbumsJob", "classDigikam_1_1AlbumsJob.html", "classDigikam_1_1AlbumsJob" ],
      [ "DatesJob", "classDigikam_1_1DatesJob.html", "classDigikam_1_1DatesJob" ],
      [ "DBJob", "classDigikam_1_1DBJob.html", "classDigikam_1_1DBJob" ],
      [ "GPSJob", "classDigikam_1_1GPSJob.html", "classDigikam_1_1GPSJob" ],
      [ "SearchesJob", "classDigikam_1_1SearchesJob.html", "classDigikam_1_1SearchesJob" ],
      [ "TagsJob", "classDigikam_1_1TagsJob.html", "classDigikam_1_1TagsJob" ]
    ] ],
    [ "dbjobinfo.cpp", "dbjobinfo_8cpp.html", null ],
    [ "dbjobinfo.h", "dbjobinfo_8h.html", [
      [ "AlbumsDBJobInfo", "classDigikam_1_1AlbumsDBJobInfo.html", "classDigikam_1_1AlbumsDBJobInfo" ],
      [ "DatesDBJobInfo", "classDigikam_1_1DatesDBJobInfo.html", "classDigikam_1_1DatesDBJobInfo" ],
      [ "DBJobInfo", "classDigikam_1_1DBJobInfo.html", "classDigikam_1_1DBJobInfo" ],
      [ "GPSDBJobInfo", "classDigikam_1_1GPSDBJobInfo.html", "classDigikam_1_1GPSDBJobInfo" ],
      [ "SearchesDBJobInfo", "classDigikam_1_1SearchesDBJobInfo.html", "classDigikam_1_1SearchesDBJobInfo" ],
      [ "TagsDBJobInfo", "classDigikam_1_1TagsDBJobInfo.html", "classDigikam_1_1TagsDBJobInfo" ]
    ] ],
    [ "dbjobsmanager.cpp", "dbjobsmanager_8cpp.html", null ],
    [ "dbjobsmanager.h", "dbjobsmanager_8h.html", [
      [ "DBJobsManager", "classDigikam_1_1DBJobsManager.html", "classDigikam_1_1DBJobsManager" ]
    ] ],
    [ "dbjobsthread.cpp", "dbjobsthread_8cpp.html", null ],
    [ "dbjobsthread.h", "dbjobsthread_8h.html", [
      [ "AlbumsDBJobsThread", "classDigikam_1_1AlbumsDBJobsThread.html", "classDigikam_1_1AlbumsDBJobsThread" ],
      [ "DatesDBJobsThread", "classDigikam_1_1DatesDBJobsThread.html", "classDigikam_1_1DatesDBJobsThread" ],
      [ "DBJobsThread", "classDigikam_1_1DBJobsThread.html", "classDigikam_1_1DBJobsThread" ],
      [ "GPSDBJobsThread", "classDigikam_1_1GPSDBJobsThread.html", "classDigikam_1_1GPSDBJobsThread" ],
      [ "SearchesDBJobsThread", "classDigikam_1_1SearchesDBJobsThread.html", "classDigikam_1_1SearchesDBJobsThread" ],
      [ "TagsDBJobsThread", "classDigikam_1_1TagsDBJobsThread.html", "classDigikam_1_1TagsDBJobsThread" ]
    ] ],
    [ "duplicatesprogressobserver.cpp", "duplicatesprogressobserver_8cpp.html", null ],
    [ "duplicatesprogressobserver.h", "duplicatesprogressobserver_8h.html", [
      [ "DuplicatesProgressObserver", "classDigikam_1_1DuplicatesProgressObserver.html", "classDigikam_1_1DuplicatesProgressObserver" ]
    ] ]
];