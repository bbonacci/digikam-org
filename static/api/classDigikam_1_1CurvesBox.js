var classDigikam_1_1CurvesBox =
[
    [ "ColorPicker", "classDigikam_1_1CurvesBox.html#a97a0b2de1209e1bf736c047f08e41579", [
      [ "NoPicker", "classDigikam_1_1CurvesBox.html#a97a0b2de1209e1bf736c047f08e41579aeb59f8eba683a4e74b7fbceb091d4cf0", null ],
      [ "BlackTonal", "classDigikam_1_1CurvesBox.html#a97a0b2de1209e1bf736c047f08e41579aeb463cfb8593a92b0a1e458a37f62340", null ],
      [ "GrayTonal", "classDigikam_1_1CurvesBox.html#a97a0b2de1209e1bf736c047f08e41579a4e182501f19bf77107c8fc695df49349", null ],
      [ "WhiteTonal", "classDigikam_1_1CurvesBox.html#a97a0b2de1209e1bf736c047f08e41579aeedaa3ce5f92ebef5922dbbb79e4faaf", null ]
    ] ],
    [ "CurvesDrawingType", "classDigikam_1_1CurvesBox.html#afb3615be8f3b9240b2192244f349894d", [
      [ "SmoothDrawing", "classDigikam_1_1CurvesBox.html#afb3615be8f3b9240b2192244f349894daae24c81956e8d027fcac7ed16cce3fcb", null ],
      [ "FreeDrawing", "classDigikam_1_1CurvesBox.html#afb3615be8f3b9240b2192244f349894dabe1b0ca430d49edcfb85d356f6b7f43f", null ]
    ] ],
    [ "CurvesBox", "classDigikam_1_1CurvesBox.html#a7e37daa1b82001172b7daeb28880292c", null ],
    [ "CurvesBox", "classDigikam_1_1CurvesBox.html#a839c9c067b6e1f6c2f57a421327d2405", null ],
    [ "~CurvesBox", "classDigikam_1_1CurvesBox.html#ab1c2a3a4312c89475471fde18491c4fd", null ],
    [ "channel", "classDigikam_1_1CurvesBox.html#a3048fe9743a23f49f5ff98bd253f729f", null ],
    [ "curves", "classDigikam_1_1CurvesBox.html#a85fdbab208d0b7cf8e11333dccdefe06", null ],
    [ "curvesLeftOffset", "classDigikam_1_1CurvesBox.html#abff7eda6714e0eee209fc4d46d970b92", null ],
    [ "enableControlWidgets", "classDigikam_1_1CurvesBox.html#aeba5dd87dd3abbf3dcd21eaef523d142", null ],
    [ "enableCurveTypes", "classDigikam_1_1CurvesBox.html#a39945ef15ebaa6fda0a11d80e59f1bfc", null ],
    [ "enableGradients", "classDigikam_1_1CurvesBox.html#ab67b51c2eacb7026fb32c7a39de926f6", null ],
    [ "enableHGradient", "classDigikam_1_1CurvesBox.html#ae6cfc63b3957611d2acf6b3e3ee8c3db", null ],
    [ "enablePickers", "classDigikam_1_1CurvesBox.html#ae9afb1adb64f15e4b577f6e91024a4bd", null ],
    [ "enableResetButton", "classDigikam_1_1CurvesBox.html#a8210d74e8bdd13ab92b3d354ea6b1ed8", null ],
    [ "enableVGradient", "classDigikam_1_1CurvesBox.html#a375fa72703d0167b7f0ee62a5015674a", null ],
    [ "picker", "classDigikam_1_1CurvesBox.html#a4f55e3ad76d3d6affc690b8aec6665e1", null ],
    [ "readCurveSettings", "classDigikam_1_1CurvesBox.html#a1c41419b8bea00b45e9087cc05d16fbd", null ],
    [ "reset", "classDigikam_1_1CurvesBox.html#a55f5fc86fca1f55360c3eeff4f055088", null ],
    [ "resetChannel", "classDigikam_1_1CurvesBox.html#a9686b5e627aceb7baab69df214870e19", null ],
    [ "resetChannels", "classDigikam_1_1CurvesBox.html#a79d8d146739802439f790106c3dfe410", null ],
    [ "resetPickers", "classDigikam_1_1CurvesBox.html#aabab148bc0588f80b7f8a4faf021e224", null ],
    [ "setChannel", "classDigikam_1_1CurvesBox.html#a7f560e5cbc4181fe7094d6dff7c9436d", null ],
    [ "setCurveGuide", "classDigikam_1_1CurvesBox.html#abefbc4ec281760f9e8072d3f49b161f2", null ],
    [ "setScale", "classDigikam_1_1CurvesBox.html#a2a30a675b32031d38f62c60a849c04da", null ],
    [ "signalChannelReset", "classDigikam_1_1CurvesBox.html#aa3c2e9e454fc1ed4a006a013c374203d", null ],
    [ "signalCurvesChanged", "classDigikam_1_1CurvesBox.html#a7b89abfb3201b0df32e485c5d71e22b3", null ],
    [ "signalCurveTypeChanged", "classDigikam_1_1CurvesBox.html#ae9f0dcb50d15b6ceaaa758aae4f44f0e", null ],
    [ "signalPickerChanged", "classDigikam_1_1CurvesBox.html#a0bc6794f9e065929a536d73838a3c32c", null ],
    [ "writeCurveSettings", "classDigikam_1_1CurvesBox.html#aca26f4a9ca6d09e0008e0dbcb90fd767", null ]
];