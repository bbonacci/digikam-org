var dir_64e496cb268f2df43ea53847b0791489 =
[
    [ "addtagscombobox.cpp", "addtagscombobox_8cpp.html", null ],
    [ "addtagscombobox.h", "addtagscombobox_8h.html", [
      [ "AddTagsComboBox", "classDigikam_1_1AddTagsComboBox.html", "classDigikam_1_1AddTagsComboBox" ]
    ] ],
    [ "addtagslineedit.cpp", "addtagslineedit_8cpp.html", null ],
    [ "addtagslineedit.h", "addtagslineedit_8h.html", [
      [ "AddTagsLineEdit", "classDigikam_1_1AddTagsLineEdit.html", "classDigikam_1_1AddTagsLineEdit" ]
    ] ],
    [ "tagcheckview.cpp", "tagcheckview_8cpp.html", null ],
    [ "tagcheckview.h", "tagcheckview_8h.html", [
      [ "TagCheckView", "classDigikam_1_1TagCheckView.html", "classDigikam_1_1TagCheckView" ]
    ] ],
    [ "tageditdlg.cpp", "tageditdlg_8cpp.html", null ],
    [ "tageditdlg.h", "tageditdlg_8h.html", [
      [ "TagEditDlg", "classDigikam_1_1TagEditDlg.html", "classDigikam_1_1TagEditDlg" ]
    ] ],
    [ "tagfolderview.cpp", "tagfolderview_8cpp.html", null ],
    [ "tagfolderview.h", "tagfolderview_8h.html", [
      [ "TagFolderView", "classDigikam_1_1TagFolderView.html", "classDigikam_1_1TagFolderView" ]
    ] ],
    [ "tagslineeditoverlay.cpp", "tagslineeditoverlay_8cpp.html", null ],
    [ "tagslineeditoverlay.h", "tagslineeditoverlay_8h.html", [
      [ "TagsLineEditOverlay", "classDigikam_1_1TagsLineEditOverlay.html", "classDigikam_1_1TagsLineEditOverlay" ]
    ] ],
    [ "tagspopupmenu.cpp", "tagspopupmenu_8cpp.html", "tagspopupmenu_8cpp" ],
    [ "tagspopupmenu.h", "tagspopupmenu_8h.html", [
      [ "TagsPopupMenu", "classDigikam_1_1TagsPopupMenu.html", "classDigikam_1_1TagsPopupMenu" ]
    ] ]
];