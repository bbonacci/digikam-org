var namespaceDigikamGenericIpfsPlugin =
[
    [ "IpfsImagesList", "classDigikamGenericIpfsPlugin_1_1IpfsImagesList.html", "classDigikamGenericIpfsPlugin_1_1IpfsImagesList" ],
    [ "IpfsImagesListViewItem", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem" ],
    [ "IpfsPlugin", "classDigikamGenericIpfsPlugin_1_1IpfsPlugin.html", "classDigikamGenericIpfsPlugin_1_1IpfsPlugin" ],
    [ "IpfsTalker", "classDigikamGenericIpfsPlugin_1_1IpfsTalker.html", "classDigikamGenericIpfsPlugin_1_1IpfsTalker" ],
    [ "IpfsTalkerAction", "structDigikamGenericIpfsPlugin_1_1IpfsTalkerAction.html", "structDigikamGenericIpfsPlugin_1_1IpfsTalkerAction" ],
    [ "IpfsTalkerResult", "structDigikamGenericIpfsPlugin_1_1IpfsTalkerResult.html", "structDigikamGenericIpfsPlugin_1_1IpfsTalkerResult" ],
    [ "IpfsWindow", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html", "classDigikamGenericIpfsPlugin_1_1IpfsWindow" ],
    [ "IpfsTalkerActionType", "namespaceDigikamGenericIpfsPlugin.html#a47b24e81da9f533fd531416fc5c00c14", [
      [ "IMG_UPLOAD", "namespaceDigikamGenericIpfsPlugin.html#a47b24e81da9f533fd531416fc5c00c14a24f90f807f8e88369a150919f60e2097", null ]
    ] ]
];