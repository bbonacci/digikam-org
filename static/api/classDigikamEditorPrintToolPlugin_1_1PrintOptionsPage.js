var classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage =
[
    [ "ScaleMode", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html#a9a36b2a9ed73eba93800685bdbab67f5", [
      [ "NoScale", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html#a9a36b2a9ed73eba93800685bdbab67f5ad08c2585617d69d3e0b374223ac9b709", null ],
      [ "ScaleToPage", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html#a9a36b2a9ed73eba93800685bdbab67f5a32cb6ba3663ef189df95d8a508356d6f", null ],
      [ "ScaleToCustomSize", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html#a9a36b2a9ed73eba93800685bdbab67f5ab37d617f8de43ef1deb6b03d70cbd2c1", null ]
    ] ],
    [ "Unit", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html#af03cdce8cfe0fc802cbeb3831d142dde", [
      [ "Millimeters", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html#af03cdce8cfe0fc802cbeb3831d142ddea241a628d1bcb02d2ec696cef7636e77b", null ],
      [ "Centimeters", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html#af03cdce8cfe0fc802cbeb3831d142ddeac3e7c13f4bbbfcc298295fb41a9899ae", null ],
      [ "Inches", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html#af03cdce8cfe0fc802cbeb3831d142ddea59c15c920fbfecd6091d30fe9e25b302", null ]
    ] ],
    [ "PrintOptionsPage", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html#a476b7a313fc4258323c5989a8c33a9b1", null ],
    [ "~PrintOptionsPage", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html#a69fdf98a378781d0d6cb99d450276051", null ],
    [ "alignment", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html#a4ae9db55b909038aec583b67e54e0da1", null ],
    [ "autoRotation", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html#a0e1d46280918d00cb8bc15f6fa827cab", null ],
    [ "colorManaged", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html#ae553c3e7baa8c2d9b2944c6799c267e1", null ],
    [ "enlargeSmallerImages", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html#a781b891bfe52738c4aed00c18303be1f", null ],
    [ "loadConfig", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html#a79b74030687c699c94d634b3463c9f84", null ],
    [ "outputProfile", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html#a0eae06dd46c27c1062440a1ae86ead17", null ],
    [ "saveConfig", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html#a9fdf89997f5a68d9077ad7358ac2eae5", null ],
    [ "scaleHeight", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html#a39efda985231294f49b028876b027f51", null ],
    [ "scaleMode", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html#ae2d08a39c057920133be59e6e53300b5", null ],
    [ "scaleUnit", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html#a2b8e4a43f0f71d309921db7842c5d548", null ],
    [ "scaleWidth", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html#a61bb78b58d1dca352211cc807ecaf161", null ]
];