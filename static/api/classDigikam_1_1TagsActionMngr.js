var classDigikam_1_1TagsActionMngr =
[
    [ "TagsActionMngr", "classDigikam_1_1TagsActionMngr.html#a5671a97442693e00b19a403cd3b0671b", null ],
    [ "~TagsActionMngr", "classDigikam_1_1TagsActionMngr.html#af3b7fe6669687e4eebf447a71ad8784e", null ],
    [ "actionCollections", "classDigikam_1_1TagsActionMngr.html#ae7d99c2de7e0e57dff6800a3dacb4221", null ],
    [ "colorShortcutPrefix", "classDigikam_1_1TagsActionMngr.html#a2629c9bec0c073a0fdb89c5c3a5fa22f", null ],
    [ "pickShortcutPrefix", "classDigikam_1_1TagsActionMngr.html#a9e1aeff03299bf0a9e0564acbb272c6d", null ],
    [ "ratingShortcutPrefix", "classDigikam_1_1TagsActionMngr.html#ade741cc2ba3259899379d8d083fafab7", null ],
    [ "registerActionsToWidget", "classDigikam_1_1TagsActionMngr.html#a4025d8c3b30e3f40ceb3ffe1a7f2ef9f", null ],
    [ "registerLabelsActions", "classDigikam_1_1TagsActionMngr.html#ad620ac34bd792809061386671941b779", null ],
    [ "registerTagsActionCollections", "classDigikam_1_1TagsActionMngr.html#a5e3a3d39ef50e6934f51da91ea010c68", null ],
    [ "signalShortcutPressed", "classDigikam_1_1TagsActionMngr.html#acbe874a0f6dc2bafb97e65ebd3daa199", null ],
    [ "tagShortcutPrefix", "classDigikam_1_1TagsActionMngr.html#ab0e9d233c0337e2d944ad12bbe7ed28e", null ],
    [ "updateTagShortcut", "classDigikam_1_1TagsActionMngr.html#a8af5d59a50daffcfe71bfe2f2f490f5c", null ]
];