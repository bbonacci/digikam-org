var rajcesession_8h =
[
    [ "RajceSession", "classDigikamGenericRajcePlugin_1_1RajceSession.html", "classDigikamGenericRajcePlugin_1_1RajceSession" ],
    [ "RajceCommandType", "rajcesession_8h.html#ac9bcdac96fce90d45cdc2a308b188e89", [
      [ "Login", "rajcesession_8h.html#ac9bcdac96fce90d45cdc2a308b188e89a98cf3652c496766dfc331021ffeab2db", null ],
      [ "Logout", "rajcesession_8h.html#ac9bcdac96fce90d45cdc2a308b188e89aa81addba5f0f32b351c44934762388af", null ],
      [ "ListAlbums", "rajcesession_8h.html#ac9bcdac96fce90d45cdc2a308b188e89aa2b8965eb1316ad495a526603e02c91d", null ],
      [ "CreateAlbum", "rajcesession_8h.html#ac9bcdac96fce90d45cdc2a308b188e89a5d2cde347790dcc9e7d01b4e4c014c60", null ],
      [ "OpenAlbum", "rajcesession_8h.html#ac9bcdac96fce90d45cdc2a308b188e89aa6a03f0546fc9217cb391d569873ab9e", null ],
      [ "CloseAlbum", "rajcesession_8h.html#ac9bcdac96fce90d45cdc2a308b188e89a3d47336804ef7f55643473e0e89b1667", null ],
      [ "AddPhoto", "rajcesession_8h.html#ac9bcdac96fce90d45cdc2a308b188e89a07b95ccb63d9a5e82a329ce09c08b0c8", null ]
    ] ],
    [ "RajceErrorCode", "rajcesession_8h.html#a387dd84a90c0e86b0e60881e07581a76", [
      [ "UnknownError", "rajcesession_8h.html#a387dd84a90c0e86b0e60881e07581a76aaaf9fae5aa5ac9e422ec4a4c6452b9be", null ],
      [ "InvalidCommand", "rajcesession_8h.html#a387dd84a90c0e86b0e60881e07581a76af2cc8cf5f764f923d1bf8f39b4c5e086", null ],
      [ "InvalidCredentials", "rajcesession_8h.html#a387dd84a90c0e86b0e60881e07581a76a4b89c70bd4936ef6307bb05b1f3bd14b", null ],
      [ "InvalidSessionToken", "rajcesession_8h.html#a387dd84a90c0e86b0e60881e07581a76aacfa1e860d363c99b456ac50664f3aac", null ],
      [ "InvalidOrRepeatedColumnName", "rajcesession_8h.html#a387dd84a90c0e86b0e60881e07581a76ae4d7bb0bd7b3e7728162663d3a43b48b", null ],
      [ "InvalidAlbumId", "rajcesession_8h.html#a387dd84a90c0e86b0e60881e07581a76a76bdb5430279087287c08aa166e952c1", null ],
      [ "AlbumDoesntExistOrNoPrivileges", "rajcesession_8h.html#a387dd84a90c0e86b0e60881e07581a76ac65962418564a781fe32df7f08609571", null ],
      [ "InvalidAlbumToken", "rajcesession_8h.html#a387dd84a90c0e86b0e60881e07581a76acbd39b17d2ad4cc026515347ebe44d16", null ],
      [ "AlbumNameEmpty", "rajcesession_8h.html#a387dd84a90c0e86b0e60881e07581a76a8864077221ddc91ae5eb8d619c697e63", null ],
      [ "FailedToCreateAlbum", "rajcesession_8h.html#a387dd84a90c0e86b0e60881e07581a76a8ad96d13f75330c1f6b830139ce9a4f4", null ],
      [ "AlbumDoesntExist", "rajcesession_8h.html#a387dd84a90c0e86b0e60881e07581a76af52b30fff3208cfc6532e607e789b317", null ],
      [ "UnknownApplication", "rajcesession_8h.html#a387dd84a90c0e86b0e60881e07581a76a28a742f49f408651576f12344aa743f4", null ],
      [ "InvalidApplicationKey", "rajcesession_8h.html#a387dd84a90c0e86b0e60881e07581a76adf1e856d65b85f9263b60c74e4cc24e2", null ],
      [ "FileNotAttached", "rajcesession_8h.html#a387dd84a90c0e86b0e60881e07581a76a870238a32f4e44f31b62e3c670f4844e", null ],
      [ "NewerVersionExists", "rajcesession_8h.html#a387dd84a90c0e86b0e60881e07581a76ab759cafd9595421867801741ebc40d2d", null ],
      [ "SavingFileFailed", "rajcesession_8h.html#a387dd84a90c0e86b0e60881e07581a76ae246529b0de231f06be70bace8876b91", null ],
      [ "UnsupportedFileExtension", "rajcesession_8h.html#a387dd84a90c0e86b0e60881e07581a76a5faed831b053d871f45c35a3067ffe03", null ],
      [ "UnknownClientVersion", "rajcesession_8h.html#a387dd84a90c0e86b0e60881e07581a76aae8a7977362204a23b40d7e7d281520f", null ],
      [ "NonexistentTarget", "rajcesession_8h.html#a387dd84a90c0e86b0e60881e07581a76a6a5767f069f3ff2479da9d9c120639e7", null ]
    ] ],
    [ "operator<<", "rajcesession_8h.html#af1d4bef2251103ea496bc309fc40a007", null ]
];