var classDigikam_1_1CollectionPage =
[
    [ "CollectionPage", "classDigikam_1_1CollectionPage.html#a131e68362ebf13af0bb8e3d37206e060", null ],
    [ "~CollectionPage", "classDigikam_1_1CollectionPage.html#a03fa28be048fb7effe8eba4be9e18135", null ],
    [ "assistant", "classDigikam_1_1CollectionPage.html#a681293182dfaf2919429a4a5aa30dd04", null ],
    [ "checkSettings", "classDigikam_1_1CollectionPage.html#a059d3cfa06c87093d821a20dade49a45", null ],
    [ "firstAlbumPath", "classDigikam_1_1CollectionPage.html#adbfddc28914c410dc51ab2b3f2e17faf", null ],
    [ "id", "classDigikam_1_1CollectionPage.html#a84aa8a0e37d7ae49efc832413764ffa1", null ],
    [ "isComplete", "classDigikam_1_1CollectionPage.html#a77413f486f6d4a405359e095fb0a07a3", null ],
    [ "removePageWidget", "classDigikam_1_1CollectionPage.html#a40cc4abca8695016a1d788f3df1cdd0d", null ],
    [ "saveSettings", "classDigikam_1_1CollectionPage.html#a976a036d83eb752adfe49821879387fe", null ],
    [ "setComplete", "classDigikam_1_1CollectionPage.html#a9e97ef81648ea3f5e33c5a08ae5f0ff0", null ],
    [ "setLeftBottomPix", "classDigikam_1_1CollectionPage.html#a04095257f4a957050e91cd4666f53316", null ],
    [ "setLeftBottomPix", "classDigikam_1_1CollectionPage.html#a2e56503c5324f5551edb8fb1f1156530", null ],
    [ "setPageWidget", "classDigikam_1_1CollectionPage.html#a2815f8463990300af07fe29cbd090308", null ],
    [ "setShowLeftView", "classDigikam_1_1CollectionPage.html#a67975edf6041a574e674576a29d606a1", null ]
];