var classDigikam_1_1TIFFSettings =
[
    [ "TIFFSettings", "classDigikam_1_1TIFFSettings.html#a0dcc35e8b0175039bce8087fcfe714cc", null ],
    [ "~TIFFSettings", "classDigikam_1_1TIFFSettings.html#aa569826e8458456e2c6896176702f043", null ],
    [ "getCompression", "classDigikam_1_1TIFFSettings.html#ada3182d0c16829045d46e37a5ed5b418", null ],
    [ "setCompression", "classDigikam_1_1TIFFSettings.html#a857ed269159a73eecb8a7ddce5b026d9", null ],
    [ "signalSettingsChanged", "classDigikam_1_1TIFFSettings.html#a447f6d3ae62409a8ec4c33cc599e7df5", null ]
];