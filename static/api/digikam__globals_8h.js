var digikam__globals_8h =
[
    [ "CLAMP", "digikam__globals_8h.html#a4410a5b0fe398d071e52d3c4c48f7bb5", null ],
    [ "CLAMP0255", "digikam__globals_8h.html#a71d2460ae9b9583829c1c1056a45c918", null ],
    [ "CLAMP065535", "digikam__globals_8h.html#a5ac7eac8325c8076f259a3bc9310f64e", null ],
    [ "DEG2RAD", "digikam__globals_8h.html#af7e8592d0a634bd3642e9fd508ea8022", null ],
    [ "MAX3", "digikam__globals_8h.html#a37c6d7fcb9c177c308ef38e1d51d35e3", null ],
    [ "MIN3", "digikam__globals_8h.html#a4887ceeae88266064841cbbc11ef8b53", null ],
    [ "QT_ENDL", "digikam__globals_8h.html#ad2f1e9cccb8ff3e28fe669cbbb9ea8c2", null ],
    [ "QT_KEEP_EMPTY_PARTS", "digikam__globals_8h.html#aec0ef62bfccbf8e7f560a22f372121a6", null ],
    [ "QT_SKIP_EMPTY_PARTS", "digikam__globals_8h.html#acaf1e3b4fa52d95d144f3672e489c739", null ],
    [ "ChannelType", "digikam__globals_8h.html#a107bc68e2a353b42922bb3e983785eb3", [
      [ "LuminosityChannel", "digikam__globals_8h.html#a107bc68e2a353b42922bb3e983785eb3ac6e68baa1e405e8e84264ac5b54bcf63", null ],
      [ "RedChannel", "digikam__globals_8h.html#a107bc68e2a353b42922bb3e983785eb3a23484068e37831e80582ccaf6426fa2c", null ],
      [ "GreenChannel", "digikam__globals_8h.html#a107bc68e2a353b42922bb3e983785eb3a0414a4b541ee00c2997d9289114ccfb9", null ],
      [ "BlueChannel", "digikam__globals_8h.html#a107bc68e2a353b42922bb3e983785eb3afc5004bafb0ba62a89468749ee14348a", null ],
      [ "AlphaChannel", "digikam__globals_8h.html#a107bc68e2a353b42922bb3e983785eb3a817402b2f761fd628ee814e1bd397435", null ],
      [ "ColorChannels", "digikam__globals_8h.html#a107bc68e2a353b42922bb3e983785eb3a013bd05fd92fd2f0760f5de8decc2263", null ]
    ] ],
    [ "ColorLabel", "digikam__globals_8h.html#ac8132466ce37ded0144ffa0ea3d01b35", [
      [ "NoColorLabel", "digikam__globals_8h.html#ac8132466ce37ded0144ffa0ea3d01b35ae01e099a343d8eb7c2b323f1c86d7594", null ],
      [ "RedLabel", "digikam__globals_8h.html#ac8132466ce37ded0144ffa0ea3d01b35a1d45a922e4f92932ea21a6a1147b8fbe", null ],
      [ "OrangeLabel", "digikam__globals_8h.html#ac8132466ce37ded0144ffa0ea3d01b35a9fa8072e764921cc885643f9ff5b677f", null ],
      [ "YellowLabel", "digikam__globals_8h.html#ac8132466ce37ded0144ffa0ea3d01b35a88921a9de0083579e49d0fb78518eccd", null ],
      [ "GreenLabel", "digikam__globals_8h.html#ac8132466ce37ded0144ffa0ea3d01b35a595050f384dd7b231886c698ca7dcd40", null ],
      [ "BlueLabel", "digikam__globals_8h.html#ac8132466ce37ded0144ffa0ea3d01b35a8135460e37f16e1f6e975e9d221d837c", null ],
      [ "MagentaLabel", "digikam__globals_8h.html#ac8132466ce37ded0144ffa0ea3d01b35abeae869c7702b974c8c8f837c06d4f61", null ],
      [ "GrayLabel", "digikam__globals_8h.html#ac8132466ce37ded0144ffa0ea3d01b35a5668a997160de3b984edd8f1fcc3d70d", null ],
      [ "BlackLabel", "digikam__globals_8h.html#ac8132466ce37ded0144ffa0ea3d01b35a189733dc6ea9ac984e3794fecb12d34f", null ],
      [ "WhiteLabel", "digikam__globals_8h.html#ac8132466ce37ded0144ffa0ea3d01b35a4812164aad0793bbee94892ff00ccabd", null ],
      [ "FirstColorLabel", "digikam__globals_8h.html#ac8132466ce37ded0144ffa0ea3d01b35a646865078e91153817695be26b3e4ae5", null ],
      [ "LastColorLabel", "digikam__globals_8h.html#ac8132466ce37ded0144ffa0ea3d01b35a0e41a3e745eb1c799d8cd2130ecb0718", null ],
      [ "NumberOfColorLabels", "digikam__globals_8h.html#ac8132466ce37ded0144ffa0ea3d01b35a3d2ebc81f109e7beef71eb15cf76ae57", null ]
    ] ],
    [ "HistogramBoxType", "digikam__globals_8h.html#a3c8781f7dd8b6104606bb7d34f5f4cc6", [
      [ "RGB", "digikam__globals_8h.html#a3c8781f7dd8b6104606bb7d34f5f4cc6ab732cc6363c7cd0849d20811d1907210", null ],
      [ "RGBA", "digikam__globals_8h.html#a3c8781f7dd8b6104606bb7d34f5f4cc6a858ad8e2abf6fbf81d7401cef2d8b2cb", null ],
      [ "LRGB", "digikam__globals_8h.html#a3c8781f7dd8b6104606bb7d34f5f4cc6ad3eeb7910b9e4f47cbf16dbe37a82874", null ],
      [ "LRGBA", "digikam__globals_8h.html#a3c8781f7dd8b6104606bb7d34f5f4cc6a8557f36e4bf3ab706698092e00bbf12e", null ],
      [ "LRGBC", "digikam__globals_8h.html#a3c8781f7dd8b6104606bb7d34f5f4cc6adbb16c120b8a434d82fa214dd5bb6c1e", null ],
      [ "LRGBAC", "digikam__globals_8h.html#a3c8781f7dd8b6104606bb7d34f5f4cc6a68a69c20af1f4b005b62fe4e0e469c02", null ]
    ] ],
    [ "HistogramRenderingType", "digikam__globals_8h.html#a30f21b2ff5d9dca83cdf386f19a52228", [
      [ "FullImageHistogram", "digikam__globals_8h.html#a30f21b2ff5d9dca83cdf386f19a52228a72762ef81b0d0611ac6e17dd76c749a7", null ],
      [ "ImageSelectionHistogram", "digikam__globals_8h.html#a30f21b2ff5d9dca83cdf386f19a52228a958246ea7055c5447e26acd9ec42e917", null ]
    ] ],
    [ "HistogramScale", "digikam__globals_8h.html#a7252dea9fd7f36c45149da08748a2cb9", [
      [ "LinScaleHistogram", "digikam__globals_8h.html#a7252dea9fd7f36c45149da08748a2cb9a3d4480610fb36371f1a7393e780e18f5", null ],
      [ "LogScaleHistogram", "digikam__globals_8h.html#a7252dea9fd7f36c45149da08748a2cb9a4b51ecacce35001b47229799a27ecc83", null ]
    ] ],
    [ "PickLabel", "digikam__globals_8h.html#af3f0d4e1b18c8ca68a3f3c7d5fe7563f", [
      [ "NoPickLabel", "digikam__globals_8h.html#af3f0d4e1b18c8ca68a3f3c7d5fe7563fa11834f2966b841bb8c184ac32a60b035", null ],
      [ "RejectedLabel", "digikam__globals_8h.html#af3f0d4e1b18c8ca68a3f3c7d5fe7563fa3b812a5a96050fa9be82d5047b0894eb", null ],
      [ "PendingLabel", "digikam__globals_8h.html#af3f0d4e1b18c8ca68a3f3c7d5fe7563fa357c916be144bfcf2075f11df6a4a36a", null ],
      [ "AcceptedLabel", "digikam__globals_8h.html#af3f0d4e1b18c8ca68a3f3c7d5fe7563fa1c816b782a00c5686bbb9d81ed80cb55", null ],
      [ "FirstPickLabel", "digikam__globals_8h.html#af3f0d4e1b18c8ca68a3f3c7d5fe7563fa006147e347228cea66c9e44a9facf7e6", null ],
      [ "LastPickLabel", "digikam__globals_8h.html#af3f0d4e1b18c8ca68a3f3c7d5fe7563fa1e0eaaba4762e75eabe2dcc5ed22a9aa", null ],
      [ "NumberOfPickLabels", "digikam__globals_8h.html#af3f0d4e1b18c8ca68a3f3c7d5fe7563fa6b1ac3824b19da2b70f86e59aff70f7c", null ]
    ] ],
    [ "adjustedEnvironmentForAppImage", "digikam__globals_8h.html#abdbde7171265f488ecf60770fa654c94", null ],
    [ "defineShortcut", "digikam__globals_8h.html#a46b0931600a8cd0f7297a1644e30fa82", null ],
    [ "installQtTranslationFiles", "digikam__globals_8h.html#aef7aab1fa676680d50879a50d4487ca9", null ],
    [ "isReadableImageFile", "digikam__globals_8h.html#a51ab05c7fcd8f4f60755e69e86b26e71", null ],
    [ "isRunningInAppImageBundle", "digikam__globals_8h.html#aaa58e5ff23ca7fd3ec8e3089f199a52d", null ],
    [ "loadEcmQtTranslationFiles", "digikam__globals_8h.html#ab7938f4c91236db6a89f715edd5473db", null ],
    [ "loadStdQtTranslationFiles", "digikam__globals_8h.html#a5ca2ce891ae5f543ceeea52e1a54543b", null ],
    [ "macOSBundlePrefix", "digikam__globals_8h.html#afd8b206f3bb25f68d9472f0dc001929c", null ],
    [ "showRawCameraList", "digikam__globals_8h.html#a6df7e3734e8c28c8f52983c2d62d60fe", null ],
    [ "startOfDay", "digikam__globals_8h.html#ac7359661b2a6e534b2410d7d5bfd4690", null ],
    [ "supportedImageMimeTypes", "digikam__globals_8h.html#af7fd83602023ad1434f99b612aa15a02", null ],
    [ "toolButtonStyleSheet", "digikam__globals_8h.html#a1895e0ed4d92d6b6c5ae32ea07ad066c", null ],
    [ "tryInitDrMingw", "digikam__globals_8h.html#a4433d9e68f7da24d6a629aaf206bf913", null ],
    [ "unloadQtTranslationFiles", "digikam__globals_8h.html#aed07d6302e8ed66ede19f0256af281c4", null ]
];