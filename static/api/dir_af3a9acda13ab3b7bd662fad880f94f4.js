var dir_af3a9acda13ab3b7bd662fad880f94f4 =
[
    [ "metadatapanel.cpp", "metadatapanel_8cpp.html", null ],
    [ "metadatapanel.h", "metadatapanel_8h.html", [
      [ "MetadataPanel", "classDigikam_1_1MetadataPanel.html", "classDigikam_1_1MetadataPanel" ]
    ] ],
    [ "metadataselector.cpp", "metadataselector_8cpp.html", null ],
    [ "metadataselector.h", "metadataselector_8h.html", [
      [ "MetadataSelector", "classDigikam_1_1MetadataSelector.html", "classDigikam_1_1MetadataSelector" ],
      [ "MetadataSelectorItem", "classDigikam_1_1MetadataSelectorItem.html", "classDigikam_1_1MetadataSelectorItem" ],
      [ "MetadataSelectorView", "classDigikam_1_1MetadataSelectorView.html", "classDigikam_1_1MetadataSelectorView" ]
    ] ]
];