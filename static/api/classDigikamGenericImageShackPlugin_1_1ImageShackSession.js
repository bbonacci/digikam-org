var classDigikamGenericImageShackPlugin_1_1ImageShackSession =
[
    [ "ImageShackSession", "classDigikamGenericImageShackPlugin_1_1ImageShackSession.html#af068b6af5b002a62a554be58061fd884", null ],
    [ "~ImageShackSession", "classDigikamGenericImageShackPlugin_1_1ImageShackSession.html#a501be863c506d4f8e227472fdfd11bc9", null ],
    [ "authToken", "classDigikamGenericImageShackPlugin_1_1ImageShackSession.html#a5b9dc660e80c7d54e8d27054546cb5ab", null ],
    [ "credits", "classDigikamGenericImageShackPlugin_1_1ImageShackSession.html#aa62311a993e5aa05adec58d6af050d5e", null ],
    [ "email", "classDigikamGenericImageShackPlugin_1_1ImageShackSession.html#ac456da08aa3446ce05afd707660e4db5", null ],
    [ "loggedIn", "classDigikamGenericImageShackPlugin_1_1ImageShackSession.html#a0251722eb4faede4163e6f3a25b9803f", null ],
    [ "logOut", "classDigikamGenericImageShackPlugin_1_1ImageShackSession.html#a5068922d57d0094ce5ce9b91a4b3c3d9", null ],
    [ "password", "classDigikamGenericImageShackPlugin_1_1ImageShackSession.html#aa359caa9cf5717ef64219107d377d69c", null ],
    [ "readSettings", "classDigikamGenericImageShackPlugin_1_1ImageShackSession.html#a55d39d1a326750fc6dfd98f15326af8d", null ],
    [ "saveSettings", "classDigikamGenericImageShackPlugin_1_1ImageShackSession.html#aeac31c3bc056d0693f0fd0f7fa0de29e", null ],
    [ "setAuthToken", "classDigikamGenericImageShackPlugin_1_1ImageShackSession.html#a8ff4d1169a9501c5851be16781996e79", null ],
    [ "setCredits", "classDigikamGenericImageShackPlugin_1_1ImageShackSession.html#a6273ae4960ad2ed90e74a1f7b549b563", null ],
    [ "setEmail", "classDigikamGenericImageShackPlugin_1_1ImageShackSession.html#a4c96d060439f5a967a226746b72a2568", null ],
    [ "setLoggedIn", "classDigikamGenericImageShackPlugin_1_1ImageShackSession.html#a284d1c0fcb2b9ec81c25e711a34d67bd", null ],
    [ "setPassword", "classDigikamGenericImageShackPlugin_1_1ImageShackSession.html#a9e97792e6bc4b79f7f51ce9c49bc992f", null ],
    [ "setUsername", "classDigikamGenericImageShackPlugin_1_1ImageShackSession.html#a0504d8a0aa6d8c1ecebff05d99ad1584", null ],
    [ "username", "classDigikamGenericImageShackPlugin_1_1ImageShackSession.html#af70e7381527bdbad9b5080e2ae954814", null ]
];