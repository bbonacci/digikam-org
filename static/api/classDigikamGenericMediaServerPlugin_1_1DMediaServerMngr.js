var classDigikamGenericMediaServerPlugin_1_1DMediaServerMngr =
[
    [ "albumsShared", "classDigikamGenericMediaServerPlugin_1_1DMediaServerMngr.html#a98de9349fd405195957c0825811f6e52", null ],
    [ "cleanUp", "classDigikamGenericMediaServerPlugin_1_1DMediaServerMngr.html#a31bcb4d1b0bcc782e41774be37138bd7", null ],
    [ "collectionMap", "classDigikamGenericMediaServerPlugin_1_1DMediaServerMngr.html#af8e018e390ccb4a8e575cb00965723ff", null ],
    [ "configGroupName", "classDigikamGenericMediaServerPlugin_1_1DMediaServerMngr.html#ad1688e754ecae271b902705035c1087b", null ],
    [ "configStartServerOnStartupEntry", "classDigikamGenericMediaServerPlugin_1_1DMediaServerMngr.html#ab93082bda819ea1adb8a81ce81581118", null ],
    [ "isRunning", "classDigikamGenericMediaServerPlugin_1_1DMediaServerMngr.html#acecdf4f777749419a22873120fd48574", null ],
    [ "itemsList", "classDigikamGenericMediaServerPlugin_1_1DMediaServerMngr.html#ae0981f302344baf2131c1f6c63a571f5", null ],
    [ "itemsShared", "classDigikamGenericMediaServerPlugin_1_1DMediaServerMngr.html#ad2e9525fca47869768b4fbf4130699cd", null ],
    [ "load", "classDigikamGenericMediaServerPlugin_1_1DMediaServerMngr.html#a2f8c213be751120c14654241c25626b7", null ],
    [ "loadAtStartup", "classDigikamGenericMediaServerPlugin_1_1DMediaServerMngr.html#a03ccfffd0b0eabafa86158c1fbf0b1a2", null ],
    [ "mediaServerNotification", "classDigikamGenericMediaServerPlugin_1_1DMediaServerMngr.html#a9eed8a526ef125b9062b2090fc7415e2", null ],
    [ "save", "classDigikamGenericMediaServerPlugin_1_1DMediaServerMngr.html#a325681ae906a506bace4beadcf7c52cb", null ],
    [ "saveAtShutdown", "classDigikamGenericMediaServerPlugin_1_1DMediaServerMngr.html#afe5d5810c810f5e6c5913006f64bb875", null ],
    [ "setCollectionMap", "classDigikamGenericMediaServerPlugin_1_1DMediaServerMngr.html#ad7988440c4b82c1654b99e51dd3fea20", null ],
    [ "setItemsList", "classDigikamGenericMediaServerPlugin_1_1DMediaServerMngr.html#a9ed6f79b94b32b4d7f6d09619664ee21", null ],
    [ "startMediaServer", "classDigikamGenericMediaServerPlugin_1_1DMediaServerMngr.html#a0769c19ca275845304b7d2907ab671f3", null ],
    [ "DMediaServerMngrCreator", "classDigikamGenericMediaServerPlugin_1_1DMediaServerMngr.html#aa7799d1cc0c3bd4d6ae469a0e543ab97", null ]
];