var classDigikamGenericPiwigoPlugin_1_1PiwigoSession =
[
    [ "PiwigoSession", "classDigikamGenericPiwigoPlugin_1_1PiwigoSession.html#afefbf5bc83cf7f6d7270798fe2f087e6", null ],
    [ "~PiwigoSession", "classDigikamGenericPiwigoPlugin_1_1PiwigoSession.html#a47a275081957d8b01e0f16b02dfff2e7", null ],
    [ "password", "classDigikamGenericPiwigoPlugin_1_1PiwigoSession.html#a8eb1e9c53c57e531e17d5636af68cbaa", null ],
    [ "save", "classDigikamGenericPiwigoPlugin_1_1PiwigoSession.html#a463962853f0b17492ed038650f7d765b", null ],
    [ "setPassword", "classDigikamGenericPiwigoPlugin_1_1PiwigoSession.html#acb976bbdbff9a84fe9e423b78f939aa7", null ],
    [ "setUrl", "classDigikamGenericPiwigoPlugin_1_1PiwigoSession.html#afc9a8ca75a436df4b71fe0279350c629", null ],
    [ "setUsername", "classDigikamGenericPiwigoPlugin_1_1PiwigoSession.html#a4a6f95d7923b1b344876ccfa3d451244", null ],
    [ "url", "classDigikamGenericPiwigoPlugin_1_1PiwigoSession.html#a34728cd88f0b156f0556252d4a6066d9", null ],
    [ "username", "classDigikamGenericPiwigoPlugin_1_1PiwigoSession.html#af4b46b83c40a8fcadf0d74a3efe51c64", null ]
];