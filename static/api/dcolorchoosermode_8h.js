var dcolorchoosermode_8h =
[
    [ "DColorChooserMode", "dcolorchoosermode_8h.html#a94ef3fb31dc08962a53c4040fb71bbfb", [
      [ "ChooserClassic", "dcolorchoosermode_8h.html#a94ef3fb31dc08962a53c4040fb71bbfba3df019210baf1e510ffa0dbf7c18dd39", null ],
      [ "ChooserHue", "dcolorchoosermode_8h.html#a94ef3fb31dc08962a53c4040fb71bbfbab7b6efb7c8c19c49621e3097dd8ce9ed", null ],
      [ "ChooserSaturation", "dcolorchoosermode_8h.html#a94ef3fb31dc08962a53c4040fb71bbfbadf7934007344dd77d941463ab34fb14c", null ],
      [ "ChooserValue", "dcolorchoosermode_8h.html#a94ef3fb31dc08962a53c4040fb71bbfbae2f77094efa1bfbf0a8379024ba75466", null ],
      [ "ChooserRed", "dcolorchoosermode_8h.html#a94ef3fb31dc08962a53c4040fb71bbfba84714242bde0fd38dd28b67dfdc13f31", null ],
      [ "ChooserGreen", "dcolorchoosermode_8h.html#a94ef3fb31dc08962a53c4040fb71bbfba136df8891ed127c04bfb3ba4036cf40b", null ],
      [ "ChooserBlue", "dcolorchoosermode_8h.html#a94ef3fb31dc08962a53c4040fb71bbfba6801d45b1aaa612aeb677b927540e7f0", null ]
    ] ]
];