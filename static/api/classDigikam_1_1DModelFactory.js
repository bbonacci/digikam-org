var classDigikam_1_1DModelFactory =
[
    [ "DModelFactory", "classDigikam_1_1DModelFactory.html#ac876b6a88eefdc668f6aad9076cbb2a9", null ],
    [ "~DModelFactory", "classDigikam_1_1DModelFactory.html#aafd942592481daf0a21ecb8099061e62", null ],
    [ "getAlbumModel", "classDigikam_1_1DModelFactory.html#a13abfd0af663f39261ec3855982e4e48", null ],
    [ "getDateAlbumModel", "classDigikam_1_1DModelFactory.html#aa21c221d02167bcab374ec78b8e61729", null ],
    [ "getItemVersionsModel", "classDigikam_1_1DModelFactory.html#aa97a14b71e94b0a2098577a1041c3597", null ],
    [ "getSearchModel", "classDigikam_1_1DModelFactory.html#a00ab8e43b3d4f37831211190fbc8d36e", null ],
    [ "getTagFaceModel", "classDigikam_1_1DModelFactory.html#a4d1ba40e35d45a280334524e50b75fcc", null ],
    [ "getTagFilterModel", "classDigikam_1_1DModelFactory.html#a014bc0904dac2e193174c0b381bee5bb", null ],
    [ "getTagModel", "classDigikam_1_1DModelFactory.html#a3894162e368222a849213d05a5cda392", null ]
];