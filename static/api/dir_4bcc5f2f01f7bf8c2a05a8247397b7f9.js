var dir_4bcc5f2f01f7bf8c2a05a8247397b7f9 =
[
    [ "itemscanner.cpp", "itemscanner_8cpp.html", null ],
    [ "itemscanner.h", "itemscanner_8h.html", [
      [ "ItemScanner", "classDigikam_1_1ItemScanner.html", "classDigikam_1_1ItemScanner" ]
    ] ],
    [ "itemscanner_baloo.cpp", "itemscanner__baloo_8cpp.html", null ],
    [ "itemscanner_database.cpp", "itemscanner__database_8cpp.html", null ],
    [ "itemscanner_file.cpp", "itemscanner__file_8cpp.html", null ],
    [ "itemscanner_history.cpp", "itemscanner__history_8cpp.html", null ],
    [ "itemscanner_p.cpp", "itemscanner__p_8cpp.html", null ],
    [ "itemscanner_p.h", "itemscanner__p_8h.html", [
      [ "Private", "classDigikam_1_1ItemScanner_1_1Private.html", "classDigikam_1_1ItemScanner_1_1Private" ],
      [ "ItemScannerCommit", "classDigikam_1_1ItemScannerCommit.html", "classDigikam_1_1ItemScannerCommit" ],
      [ "LessThanByProximityToSubject", "classDigikam_1_1LessThanByProximityToSubject.html", "classDigikam_1_1LessThanByProximityToSubject" ]
    ] ],
    [ "itemscanner_photo.cpp", "itemscanner__photo_8cpp.html", null ],
    [ "itemscanner_video.cpp", "itemscanner__video_8cpp.html", null ]
];