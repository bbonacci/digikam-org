var classDigikam_1_1TaggingAction =
[
    [ "Type", "classDigikam_1_1TaggingAction.html#a8a863738b16c71cc26768f57faa944ea", [
      [ "NoAction", "classDigikam_1_1TaggingAction.html#a8a863738b16c71cc26768f57faa944eaa287fbf3b56f3abcb1b2ba1b4491858a3", null ],
      [ "AssignTag", "classDigikam_1_1TaggingAction.html#a8a863738b16c71cc26768f57faa944eaa3cfbec455429de3ce5898fdba5ecf10d", null ],
      [ "CreateNewTag", "classDigikam_1_1TaggingAction.html#a8a863738b16c71cc26768f57faa944eaafda33e443164198f0a254a330025ff2b", null ]
    ] ],
    [ "TaggingAction", "classDigikam_1_1TaggingAction.html#adddd22aac6e7172e763307117218b949", null ],
    [ "TaggingAction", "classDigikam_1_1TaggingAction.html#a6de75e821df06b9da3666a0143d4a8c8", null ],
    [ "TaggingAction", "classDigikam_1_1TaggingAction.html#a7a388b65a0c6bdadb08d7f99c58473ad", null ],
    [ "isValid", "classDigikam_1_1TaggingAction.html#a676aa8e413681887c34258c0a1902021", null ],
    [ "newTagName", "classDigikam_1_1TaggingAction.html#a5b768f12e7de7ecc27c9156c405fdad8", null ],
    [ "operator==", "classDigikam_1_1TaggingAction.html#aa2961a5722328c5e6d8433cfc7472817", null ],
    [ "parentTagId", "classDigikam_1_1TaggingAction.html#abd0c20eb3ad2889ce793fa106de90e2a", null ],
    [ "shallAssignTag", "classDigikam_1_1TaggingAction.html#a47a5fce8af20a716acc34239fbd7891d", null ],
    [ "shallCreateNewTag", "classDigikam_1_1TaggingAction.html#ae183045d1d4ed53e34084099929344e0", null ],
    [ "tagId", "classDigikam_1_1TaggingAction.html#a15658b35c1640452d0c86395e408e9c8", null ],
    [ "type", "classDigikam_1_1TaggingAction.html#aea4099414e4417d613ed3c19959f47aa", null ],
    [ "m_tagId", "classDigikam_1_1TaggingAction.html#ac961df14f90d9390174430c4d6d7a648", null ],
    [ "m_tagName", "classDigikam_1_1TaggingAction.html#a50c02a339f1b19f9c97f43697a3efe29", null ],
    [ "m_type", "classDigikam_1_1TaggingAction.html#abc037f42b49ba96cb634911bdccd9382", null ]
];