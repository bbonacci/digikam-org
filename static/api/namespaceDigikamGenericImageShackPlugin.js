var namespaceDigikamGenericImageShackPlugin =
[
    [ "ImageShackGallery", "classDigikamGenericImageShackPlugin_1_1ImageShackGallery.html", "classDigikamGenericImageShackPlugin_1_1ImageShackGallery" ],
    [ "ImageShackMPForm", "classDigikamGenericImageShackPlugin_1_1ImageShackMPForm.html", "classDigikamGenericImageShackPlugin_1_1ImageShackMPForm" ],
    [ "ImageShackNewAlbumDlg", "classDigikamGenericImageShackPlugin_1_1ImageShackNewAlbumDlg.html", "classDigikamGenericImageShackPlugin_1_1ImageShackNewAlbumDlg" ],
    [ "ImageShackPhoto", "classDigikamGenericImageShackPlugin_1_1ImageShackPhoto.html", "classDigikamGenericImageShackPlugin_1_1ImageShackPhoto" ],
    [ "ImageShackPlugin", "classDigikamGenericImageShackPlugin_1_1ImageShackPlugin.html", "classDigikamGenericImageShackPlugin_1_1ImageShackPlugin" ],
    [ "ImageShackSession", "classDigikamGenericImageShackPlugin_1_1ImageShackSession.html", "classDigikamGenericImageShackPlugin_1_1ImageShackSession" ],
    [ "ImageShackTalker", "classDigikamGenericImageShackPlugin_1_1ImageShackTalker.html", "classDigikamGenericImageShackPlugin_1_1ImageShackTalker" ],
    [ "ImageShackWidget", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget.html", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget" ],
    [ "ImageShackWindow", "classDigikamGenericImageShackPlugin_1_1ImageShackWindow.html", "classDigikamGenericImageShackPlugin_1_1ImageShackWindow" ]
];