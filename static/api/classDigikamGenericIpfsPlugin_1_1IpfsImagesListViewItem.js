var classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem =
[
    [ "State", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#a0738c425eb8ef2492e0db2e2184c0f00", [
      [ "Waiting", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#a0738c425eb8ef2492e0db2e2184c0f00ad8b6f8b9251773ead76ec18aa385d4a9", null ],
      [ "Success", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#a0738c425eb8ef2492e0db2e2184c0f00a7190ad46a44a198df532d32fa51a5138", null ],
      [ "Failed", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#a0738c425eb8ef2492e0db2e2184c0f00a69e88d7c591e2ac31e3b420e2604319d", null ]
    ] ],
    [ "IpfsImagesListViewItem", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#a9b629975a1ab9358176ef34abe24da67", null ],
    [ "~IpfsImagesListViewItem", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#a2ab7f9f1cf3f68ed1e043575bdb7e93b", null ],
    [ "comments", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#ae4091d56bd5a7136620d19e58f36df6b", null ],
    [ "Description", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#a1f4b50fc4c24fb9f63b779d0dbbb7c0b", null ],
    [ "hasValidThumbnail", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#a54fbe8eb2409f9f70df4ccad5685e476", null ],
    [ "IpfsUrl", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#ab5b04ec0408ab0e3c282c5619dfb1a24", null ],
    [ "rating", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#aaeb082f799e8983567c2af996a138a5b", null ],
    [ "setComments", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#ad892aa46c226496f5fe5ab38566ca521", null ],
    [ "setDescription", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#a38c4ae25328475dfc12b43b94f53ccdc", null ],
    [ "setIpfsUrl", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#ace39053c8b3c0acd9e125d5a1adfdd91", null ],
    [ "setIsLessThanHandler", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#a24068f26b31d6551d5c867ebb5cc5592", null ],
    [ "setProcessedIcon", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#ac95a737a72e817aeaac73c5ee285bf8d", null ],
    [ "setProgressAnimation", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#aaaa6c90b33cdbb539561019bda338442", null ],
    [ "setRating", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#a3f9e7e716209f22574b8a3f62e81ec15", null ],
    [ "setState", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#a9b1b696ccb0191e5b7d25ba59784d902", null ],
    [ "setTags", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#aa47ad759c279af82fe2f752d5b9ee7f0", null ],
    [ "setThumb", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#acadca5a7c0b7cbf90a0ab1f7f2688372", null ],
    [ "setTitle", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#a78e00fe32a6fcad12529d12134b265a7", null ],
    [ "setUrl", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#a8e1693e591371b842c878b1b2dc7f111", null ],
    [ "state", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#af4a13fa6b543e1b6af7c9665414989ec", null ],
    [ "tags", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#afc4a7455b083018334f707479a695800", null ],
    [ "Title", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#a55e7292b79a99e7fe4320ad37a5aa4e1", null ],
    [ "updateInformation", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#a69d31e335d852ce1e67f85c560c67aea", null ],
    [ "updateItemWidgets", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#a667b8e722eec6b93aefc8f1e7a60437b", null ],
    [ "url", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#ab9cc1b528f5eddbf5297c36ee34bafae", null ],
    [ "view", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html#ad4e6b75da8433c542e5fd19a56e869f4", null ]
];