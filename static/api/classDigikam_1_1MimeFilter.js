var classDigikam_1_1MimeFilter =
[
    [ "TypeMimeFilter", "classDigikam_1_1MimeFilter.html#aee10130946c8164b0e3a0a0dbbe5016f", [
      [ "AllFiles", "classDigikam_1_1MimeFilter.html#aee10130946c8164b0e3a0a0dbbe5016fa06152c9b49e5e838ba4659d73f687666", null ],
      [ "ImageFiles", "classDigikam_1_1MimeFilter.html#aee10130946c8164b0e3a0a0dbbe5016fa462275e81483ead2063fb110b949d497", null ],
      [ "NoRAWFiles", "classDigikam_1_1MimeFilter.html#aee10130946c8164b0e3a0a0dbbe5016fab19f0338b9cde7e3e4aacabc340944cf", null ],
      [ "JPGFiles", "classDigikam_1_1MimeFilter.html#aee10130946c8164b0e3a0a0dbbe5016fad8b3faff049a4e466e29225d4c6c924a", null ],
      [ "JPEG2000Files", "classDigikam_1_1MimeFilter.html#aee10130946c8164b0e3a0a0dbbe5016fa9bb71d6f3ca3355cf1ba6e05013153b3", null ],
      [ "PNGFiles", "classDigikam_1_1MimeFilter.html#aee10130946c8164b0e3a0a0dbbe5016fa12c0bd4fc8584c8afce956f101662b2c", null ],
      [ "TIFFiles", "classDigikam_1_1MimeFilter.html#aee10130946c8164b0e3a0a0dbbe5016fa2fcf0d1aa211b2d2505e25d74616585c", null ],
      [ "PGFFiles", "classDigikam_1_1MimeFilter.html#aee10130946c8164b0e3a0a0dbbe5016faf97829a93490fdf37d2447fff6d4a06c", null ],
      [ "HEIFFiles", "classDigikam_1_1MimeFilter.html#aee10130946c8164b0e3a0a0dbbe5016fa93cac708623667a12d0ac0400e22be95", null ],
      [ "DNGFiles", "classDigikam_1_1MimeFilter.html#aee10130946c8164b0e3a0a0dbbe5016fab85d7f689986d95031997bb781146970", null ],
      [ "RAWFiles", "classDigikam_1_1MimeFilter.html#aee10130946c8164b0e3a0a0dbbe5016faee0bae00017a91878aa027f8c2c5db10", null ],
      [ "MoviesFiles", "classDigikam_1_1MimeFilter.html#aee10130946c8164b0e3a0a0dbbe5016fa50b4b86b74f1e4e12254d2bce581dd23", null ],
      [ "AudioFiles", "classDigikam_1_1MimeFilter.html#aee10130946c8164b0e3a0a0dbbe5016fa76c89e599c1afbc60be7fb12440d66ce", null ],
      [ "RasterGraphics", "classDigikam_1_1MimeFilter.html#aee10130946c8164b0e3a0a0dbbe5016fa5688c6eb690477c37183c514c6eb1eef", null ]
    ] ],
    [ "MimeFilter", "classDigikam_1_1MimeFilter.html#a8b7b85e6ad81c8933897d299dede6b1f", null ],
    [ "~MimeFilter", "classDigikam_1_1MimeFilter.html#adb5c9d9d066ea8fc8f93da15ccd64c5f", null ],
    [ "mimeFilter", "classDigikam_1_1MimeFilter.html#ad1be82a49f2016b30debd2c9b35424b1", null ],
    [ "setMimeFilter", "classDigikam_1_1MimeFilter.html#a23dc1527da91bea2f362ae60c6219b46", null ]
];