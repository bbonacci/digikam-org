var classDigikam_1_1UniqueModifier =
[
    [ "IconType", "classDigikam_1_1UniqueModifier.html#ac4e4e6c482648f418e690d9ea783d6bf", [
      [ "Action", "classDigikam_1_1UniqueModifier.html#ac4e4e6c482648f418e690d9ea783d6bfa9770c3aa06a1d1603b3458687db8cc68", null ],
      [ "Dialog", "classDigikam_1_1UniqueModifier.html#ac4e4e6c482648f418e690d9ea783d6bfa5ad450d332874c24a62fd9c44de8d9d2", null ]
    ] ],
    [ "UniqueModifier", "classDigikam_1_1UniqueModifier.html#a7d83b624ac447b8ed3eba78db6667048", null ],
    [ "addToken", "classDigikam_1_1UniqueModifier.html#a0f04ae84ed73801df1834797618a1d95", null ],
    [ "description", "classDigikam_1_1UniqueModifier.html#afbaf5484ecbb3d84de892c80b4858c0a", null ],
    [ "icon", "classDigikam_1_1UniqueModifier.html#a464d899d10bf958fab9cfaf5e7a375d5", null ],
    [ "isValid", "classDigikam_1_1UniqueModifier.html#ac530d86ac9bdc95c04c1af3d39137bde", null ],
    [ "parse", "classDigikam_1_1UniqueModifier.html#ae6bff72c159cc219989b8a84724bf90c", null ],
    [ "parseOperation", "classDigikam_1_1UniqueModifier.html#adb5b0be288ce0b0d66e172f1df730d21", null ],
    [ "regExp", "classDigikam_1_1UniqueModifier.html#a8b58059ee99b053b40ef6370beb1b9a0", null ],
    [ "registerButton", "classDigikam_1_1UniqueModifier.html#ac95510178a03318696fe117067293a30", null ],
    [ "registerMenu", "classDigikam_1_1UniqueModifier.html#a43cf681665f3ae85d0536a56e806ff56", null ],
    [ "reset", "classDigikam_1_1UniqueModifier.html#ac9df704670f9233af3a936ecd19e4510", null ],
    [ "setDescription", "classDigikam_1_1UniqueModifier.html#ad7d9fd530edfeb2791e5d53a0205742d", null ],
    [ "setIcon", "classDigikam_1_1UniqueModifier.html#ad5551e615dcd27fe77777c1337ee86dc", null ],
    [ "setRegExp", "classDigikam_1_1UniqueModifier.html#a5b56e9fcbe891fb5b9b510675ba23be0", null ],
    [ "setUseTokenMenu", "classDigikam_1_1UniqueModifier.html#a27c6d3d231f9d52e2e17fe43c2b6654f", null ],
    [ "signalTokenTriggered", "classDigikam_1_1UniqueModifier.html#a1bcba596fbc4ec57ec70b856fc001c92", null ],
    [ "slotTokenTriggered", "classDigikam_1_1UniqueModifier.html#a1a2d182e7e177880ec029070bb47daff", null ],
    [ "tokens", "classDigikam_1_1UniqueModifier.html#a433727110bd5f5195e4baadbb8dc2150", null ],
    [ "useTokenMenu", "classDigikam_1_1UniqueModifier.html#ae7ff7e225d0bbfa6745266d1d89eb87c", null ]
];