var namespaceDigikamGenericGoogleServicesPlugin =
[
    [ "GDMPForm", "classDigikamGenericGoogleServicesPlugin_1_1GDMPForm.html", "classDigikamGenericGoogleServicesPlugin_1_1GDMPForm" ],
    [ "GDTalker", "classDigikamGenericGoogleServicesPlugin_1_1GDTalker.html", "classDigikamGenericGoogleServicesPlugin_1_1GDTalker" ],
    [ "GPMPForm", "classDigikamGenericGoogleServicesPlugin_1_1GPMPForm.html", "classDigikamGenericGoogleServicesPlugin_1_1GPMPForm" ],
    [ "GPTalker", "classDigikamGenericGoogleServicesPlugin_1_1GPTalker.html", "classDigikamGenericGoogleServicesPlugin_1_1GPTalker" ],
    [ "GSFolder", "classDigikamGenericGoogleServicesPlugin_1_1GSFolder.html", "classDigikamGenericGoogleServicesPlugin_1_1GSFolder" ],
    [ "GSNewAlbumDlg", "classDigikamGenericGoogleServicesPlugin_1_1GSNewAlbumDlg.html", "classDigikamGenericGoogleServicesPlugin_1_1GSNewAlbumDlg" ],
    [ "GSPhoto", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto" ],
    [ "GSPlugin", "classDigikamGenericGoogleServicesPlugin_1_1GSPlugin.html", "classDigikamGenericGoogleServicesPlugin_1_1GSPlugin" ],
    [ "GSTalkerBase", "classDigikamGenericGoogleServicesPlugin_1_1GSTalkerBase.html", "classDigikamGenericGoogleServicesPlugin_1_1GSTalkerBase" ],
    [ "GSWidget", "classDigikamGenericGoogleServicesPlugin_1_1GSWidget.html", "classDigikamGenericGoogleServicesPlugin_1_1GSWidget" ],
    [ "GSWindow", "classDigikamGenericGoogleServicesPlugin_1_1GSWindow.html", "classDigikamGenericGoogleServicesPlugin_1_1GSWindow" ],
    [ "ReplaceDialog", "classDigikamGenericGoogleServicesPlugin_1_1ReplaceDialog.html", "classDigikamGenericGoogleServicesPlugin_1_1ReplaceDialog" ],
    [ "GoogleService", "namespaceDigikamGenericGoogleServicesPlugin.html#a0771e4280da801e307777a48826af296", [
      [ "GDrive", "namespaceDigikamGenericGoogleServicesPlugin.html#a0771e4280da801e307777a48826af296ad431a13f1bdc0ce053d6bb0f1d56ad39", null ],
      [ "GPhotoExport", "namespaceDigikamGenericGoogleServicesPlugin.html#a0771e4280da801e307777a48826af296a377b5da2eb07d57046c08bd143f97ff9", null ],
      [ "GPhotoImport", "namespaceDigikamGenericGoogleServicesPlugin.html#a0771e4280da801e307777a48826af296aae0f1cc2b990bb7a68dc5737ce0409fa", null ]
    ] ],
    [ "GPhotoTagsBehaviour", "namespaceDigikamGenericGoogleServicesPlugin.html#a3267ee910bc207f9d99cb7670881b943", [
      [ "GPTagLeaf", "namespaceDigikamGenericGoogleServicesPlugin.html#a3267ee910bc207f9d99cb7670881b943abe37d07ad01b70550a20be3d9a489ecd", null ],
      [ "GPTagSplit", "namespaceDigikamGenericGoogleServicesPlugin.html#a3267ee910bc207f9d99cb7670881b943ae7895ba44b9ba18dada7c6970d21ac58", null ],
      [ "GPTagCombined", "namespaceDigikamGenericGoogleServicesPlugin.html#a3267ee910bc207f9d99cb7670881b943af8e9912514fb3fc208a6e3e848129c81", null ]
    ] ],
    [ "ReplaceDialog_Result", "namespaceDigikamGenericGoogleServicesPlugin.html#a86445e451cc62b27cdf92f8af8abdb9b", [
      [ "PWR_CANCEL", "namespaceDigikamGenericGoogleServicesPlugin.html#a86445e451cc62b27cdf92f8af8abdb9baad57562461bb7bb88e783299213a4691", null ],
      [ "PWR_ADD", "namespaceDigikamGenericGoogleServicesPlugin.html#a86445e451cc62b27cdf92f8af8abdb9ba259400ee1de50944de227b70d34b0598", null ],
      [ "PWR_ADD_ALL", "namespaceDigikamGenericGoogleServicesPlugin.html#a86445e451cc62b27cdf92f8af8abdb9ba0bc26ec8fa418b38ba41f2af2c03c973", null ],
      [ "PWR_REPLACE", "namespaceDigikamGenericGoogleServicesPlugin.html#a86445e451cc62b27cdf92f8af8abdb9ba8e6b6cb45830b68a1293c11d117090d4", null ],
      [ "PWR_REPLACE_ALL", "namespaceDigikamGenericGoogleServicesPlugin.html#a86445e451cc62b27cdf92f8af8abdb9ba0956ce1c1a4b569221e586c1190fd734", null ]
    ] ]
];