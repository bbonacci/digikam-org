var pgfutils_8h =
[
    [ "libPGFVersion", "pgfutils_8h.html#af696e50f436de7831721817787c0fcbc", null ],
    [ "loadPGFScaled", "pgfutils_8h.html#ac8a8495f72015b5f05b813f955fc43c9", null ],
    [ "readPGFImageData", "pgfutils_8h.html#a3a8c8434ca55c1014bdd47fab2adda41", null ],
    [ "writePGFImageData", "pgfutils_8h.html#aa5fae3946aa04c0e8133c951bdb53e3c", null ],
    [ "writePGFImageFile", "pgfutils_8h.html#ac969c84ec8364d0edee482a78984982e", null ]
];