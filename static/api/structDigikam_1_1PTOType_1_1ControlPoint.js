var structDigikam_1_1PTOType_1_1ControlPoint =
[
    [ "image1Id", "structDigikam_1_1PTOType_1_1ControlPoint.html#ae99f9d65dd7e71ae3047066c9200bcff", null ],
    [ "image2Id", "structDigikam_1_1PTOType_1_1ControlPoint.html#ad0edf79e67ea05ce8bf5becc04b07730", null ],
    [ "p1_x", "structDigikam_1_1PTOType_1_1ControlPoint.html#a0d202d6c23935f5c06292e81ed81060a", null ],
    [ "p1_y", "structDigikam_1_1PTOType_1_1ControlPoint.html#a5b2524f23b7aa56d01e142c69a5e0223", null ],
    [ "p2_x", "structDigikam_1_1PTOType_1_1ControlPoint.html#a6a157fb4c61dc124b6ce796cb785681c", null ],
    [ "p2_y", "structDigikam_1_1PTOType_1_1ControlPoint.html#ab4d8d9c9bafb646619b8e173352587f7", null ],
    [ "previousComments", "structDigikam_1_1PTOType_1_1ControlPoint.html#a45d6d3fa09e926d42098638dd089455d", null ],
    [ "type", "structDigikam_1_1PTOType_1_1ControlPoint.html#a12a86f721aa39aa38fe7f0f935867fd3", null ],
    [ "unmatchedParameters", "structDigikam_1_1PTOType_1_1ControlPoint.html#a1aad2bc0209f9f9bd6a9840451be3313", null ]
];