var dir_9e5bc44781b368557539b4e5dc473506 =
[
    [ "editorcore.cpp", "editorcore_8cpp.html", null ],
    [ "editorcore.h", "editorcore_8h.html", [
      [ "EditorCore", "classDigikam_1_1EditorCore.html", "classDigikam_1_1EditorCore" ]
    ] ],
    [ "editorcore_p.h", "editorcore__p_8h.html", [
      [ "Private", "classDigikam_1_1EditorCore_1_1Private.html", "classDigikam_1_1EditorCore_1_1Private" ],
      [ "FileToSave", "classDigikam_1_1EditorCore_1_1Private_1_1FileToSave.html", "classDigikam_1_1EditorCore_1_1Private_1_1FileToSave" ]
    ] ],
    [ "iccpostloadingmanager.cpp", "iccpostloadingmanager_8cpp.html", null ],
    [ "iccpostloadingmanager.h", "iccpostloadingmanager_8h.html", [
      [ "IccPostLoadingManager", "classDigikam_1_1IccPostLoadingManager.html", "classDigikam_1_1IccPostLoadingManager" ]
    ] ],
    [ "iofilesettings.h", "iofilesettings_8h.html", [
      [ "IOFileSettings", "classDigikam_1_1IOFileSettings.html", "classDigikam_1_1IOFileSettings" ]
    ] ],
    [ "savingcontext.h", "savingcontext_8h.html", [
      [ "SavingContext", "classDigikam_1_1SavingContext.html", "classDigikam_1_1SavingContext" ]
    ] ],
    [ "undoaction.cpp", "undoaction_8cpp.html", null ],
    [ "undoaction.h", "undoaction_8h.html", [
      [ "UndoAction", "classDigikam_1_1UndoAction.html", "classDigikam_1_1UndoAction" ],
      [ "UndoActionIrreversible", "classDigikam_1_1UndoActionIrreversible.html", "classDigikam_1_1UndoActionIrreversible" ],
      [ "UndoActionReversible", "classDigikam_1_1UndoActionReversible.html", "classDigikam_1_1UndoActionReversible" ],
      [ "UndoMetadataContainer", "classDigikam_1_1UndoMetadataContainer.html", "classDigikam_1_1UndoMetadataContainer" ]
    ] ],
    [ "undocache.cpp", "undocache_8cpp.html", null ],
    [ "undocache.h", "undocache_8h.html", [
      [ "UndoCache", "classDigikam_1_1UndoCache.html", "classDigikam_1_1UndoCache" ]
    ] ],
    [ "undomanager.cpp", "undomanager_8cpp.html", null ],
    [ "undomanager.h", "undomanager_8h.html", [
      [ "UndoManager", "classDigikam_1_1UndoManager.html", "classDigikam_1_1UndoManager" ]
    ] ],
    [ "undostate.h", "undostate_8h.html", [
      [ "UndoState", "classDigikam_1_1UndoState.html", "classDigikam_1_1UndoState" ]
    ] ]
];