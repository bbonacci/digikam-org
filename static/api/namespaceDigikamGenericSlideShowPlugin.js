var namespaceDigikamGenericSlideShowPlugin =
[
    [ "SetupSlideShowDialog", "classDigikamGenericSlideShowPlugin_1_1SetupSlideShowDialog.html", "classDigikamGenericSlideShowPlugin_1_1SetupSlideShowDialog" ],
    [ "SlideEnd", "classDigikamGenericSlideShowPlugin_1_1SlideEnd.html", "classDigikamGenericSlideShowPlugin_1_1SlideEnd" ],
    [ "SlideError", "classDigikamGenericSlideShowPlugin_1_1SlideError.html", "classDigikamGenericSlideShowPlugin_1_1SlideError" ],
    [ "SlideImage", "classDigikamGenericSlideShowPlugin_1_1SlideImage.html", "classDigikamGenericSlideShowPlugin_1_1SlideImage" ],
    [ "SlideOSD", "classDigikamGenericSlideShowPlugin_1_1SlideOSD.html", "classDigikamGenericSlideShowPlugin_1_1SlideOSD" ],
    [ "SlideProperties", "classDigikamGenericSlideShowPlugin_1_1SlideProperties.html", "classDigikamGenericSlideShowPlugin_1_1SlideProperties" ],
    [ "SlideShowLoader", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader" ],
    [ "SlideShowPlugin", "classDigikamGenericSlideShowPlugin_1_1SlideShowPlugin.html", "classDigikamGenericSlideShowPlugin_1_1SlideShowPlugin" ],
    [ "SlideShowSettings", "classDigikamGenericSlideShowPlugin_1_1SlideShowSettings.html", "classDigikamGenericSlideShowPlugin_1_1SlideShowSettings" ],
    [ "SlideToolBar", "classDigikamGenericSlideShowPlugin_1_1SlideToolBar.html", "classDigikamGenericSlideShowPlugin_1_1SlideToolBar" ]
];