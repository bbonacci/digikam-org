var dir_bef9ed0f20b69231f08e65b136f0599a =
[
    [ "bwsepiafilter.cpp", "bwsepiafilter_8cpp.html", null ],
    [ "bwsepiafilter.h", "bwsepiafilter_8h.html", [
      [ "BWSepiaContainer", "classDigikam_1_1BWSepiaContainer.html", "classDigikam_1_1BWSepiaContainer" ],
      [ "BWSepiaFilter", "classDigikam_1_1BWSepiaFilter.html", "classDigikam_1_1BWSepiaFilter" ]
    ] ],
    [ "bwsepiasettings.cpp", "bwsepiasettings_8cpp.html", null ],
    [ "bwsepiasettings.h", "bwsepiasettings_8h.html", [
      [ "BWSepiaSettings", "classDigikam_1_1BWSepiaSettings.html", "classDigikam_1_1BWSepiaSettings" ]
    ] ],
    [ "infraredfilter.cpp", "infraredfilter_8cpp.html", null ],
    [ "infraredfilter.h", "infraredfilter_8h.html", [
      [ "InfraredContainer", "classDigikam_1_1InfraredContainer.html", "classDigikam_1_1InfraredContainer" ],
      [ "InfraredFilter", "classDigikam_1_1InfraredFilter.html", "classDigikam_1_1InfraredFilter" ]
    ] ],
    [ "mixerfilter.cpp", "mixerfilter_8cpp.html", null ],
    [ "mixerfilter.h", "mixerfilter_8h.html", [
      [ "MixerContainer", "classDigikam_1_1MixerContainer.html", "classDigikam_1_1MixerContainer" ],
      [ "MixerFilter", "classDigikam_1_1MixerFilter.html", "classDigikam_1_1MixerFilter" ]
    ] ],
    [ "mixersettings.cpp", "mixersettings_8cpp.html", null ],
    [ "mixersettings.h", "mixersettings_8h.html", [
      [ "MixerSettings", "classDigikam_1_1MixerSettings.html", "classDigikam_1_1MixerSettings" ]
    ] ],
    [ "tonalityfilter.cpp", "tonalityfilter_8cpp.html", null ],
    [ "tonalityfilter.h", "tonalityfilter_8h.html", [
      [ "TonalityContainer", "classDigikam_1_1TonalityContainer.html", "classDigikam_1_1TonalityContainer" ],
      [ "TonalityFilter", "classDigikam_1_1TonalityFilter.html", "classDigikam_1_1TonalityFilter" ]
    ] ]
];