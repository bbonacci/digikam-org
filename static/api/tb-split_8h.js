var tb_split_8h =
[
    [ "Algo_TB_Split", "classAlgo__TB__Split.html", "classAlgo__TB__Split" ],
    [ "Algo_TB_Split_BruteForce", "classAlgo__TB__Split__BruteForce.html", "classAlgo__TB__Split__BruteForce" ],
    [ "params", "structAlgo__TB__Split__BruteForce_1_1params.html", "structAlgo__TB__Split__BruteForce_1_1params" ],
    [ "option_ALGO_TB_Split_BruteForce_ZeroBlockPrune", "classoption__ALGO__TB__Split__BruteForce__ZeroBlockPrune.html", "classoption__ALGO__TB__Split__BruteForce__ZeroBlockPrune" ],
    [ "ALGO_TB_Split_BruteForce_ZeroBlockPrune", "tb-split_8h.html#a7d68bb0bea3296cad3defc60415be2b2", [
      [ "ALGO_TB_BruteForce_ZeroBlockPrune_off", "tb-split_8h.html#a7d68bb0bea3296cad3defc60415be2b2a865408bf0c5c1bc1829665817316d1c7", null ],
      [ "ALGO_TB_BruteForce_ZeroBlockPrune_8x8", "tb-split_8h.html#a7d68bb0bea3296cad3defc60415be2b2ab16a47cb0aa802b89aa75f38ae41fa55", null ],
      [ "ALGO_TB_BruteForce_ZeroBlockPrune_8x8_16x16", "tb-split_8h.html#a7d68bb0bea3296cad3defc60415be2b2ad3333b53ea680898f77063c5355f47ba", null ],
      [ "ALGO_TB_BruteForce_ZeroBlockPrune_all", "tb-split_8h.html#a7d68bb0bea3296cad3defc60415be2b2a1c9ba43f004790712bf43db1432a8482", null ]
    ] ]
];