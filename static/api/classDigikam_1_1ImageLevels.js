var classDigikam_1_1ImageLevels =
[
    [ "ImageLevels", "classDigikam_1_1ImageLevels.html#a8f095a22a7259a75d5ca51f13fcaf34a", null ],
    [ "~ImageLevels", "classDigikam_1_1ImageLevels.html#a73832a6a5396a2f234baa6a1588377aa", null ],
    [ "getLevelGammaValue", "classDigikam_1_1ImageLevels.html#a8fb65b3f79b5916275b6f16ba6377ce2", null ],
    [ "getLevelHighInputValue", "classDigikam_1_1ImageLevels.html#afc2f556e4189257d81436f961ba6159c", null ],
    [ "getLevelHighOutputValue", "classDigikam_1_1ImageLevels.html#a922f625590f34e7692fd4ed62805e585", null ],
    [ "getLevelLowInputValue", "classDigikam_1_1ImageLevels.html#aabcd81558294b54c7639aad49287a7f1", null ],
    [ "getLevelLowOutputValue", "classDigikam_1_1ImageLevels.html#a9733601b9e5a05db1a742f3087182ac7", null ],
    [ "isDirty", "classDigikam_1_1ImageLevels.html#a807e8c75ab96ef3d5e32d40c1872ae3b", null ],
    [ "isSixteenBits", "classDigikam_1_1ImageLevels.html#ac4da958c5c9f924391e0bbac0ecd4ea7", null ],
    [ "levelsAuto", "classDigikam_1_1ImageLevels.html#a7d93ac929bb6afbf5295bcc89613fad3", null ],
    [ "levelsBlackToneAdjustByColors", "classDigikam_1_1ImageLevels.html#aa31d0baf78b7fd3ada6eaefbc70d7da2", null ],
    [ "levelsCalculateTransfers", "classDigikam_1_1ImageLevels.html#af635bc49bebb2693e6b83e0f54fe234c", null ],
    [ "levelsChannelAuto", "classDigikam_1_1ImageLevels.html#a0773f55f6e9263a053740bfe55c7acbb", null ],
    [ "levelsChannelReset", "classDigikam_1_1ImageLevels.html#acb5361a06f5fa7e44d69925ffbbcec72", null ],
    [ "levelsGrayToneAdjustByColors", "classDigikam_1_1ImageLevels.html#adb8e2c70e83fb9bd35fcb8acd4ded674", null ],
    [ "levelsInputFromColor", "classDigikam_1_1ImageLevels.html#afa0ce33e48d00ca6d21962150285e8be", null ],
    [ "levelsLutFunc", "classDigikam_1_1ImageLevels.html#ae1a1b2a32ceae5f71a50c528b8f40dd5", null ],
    [ "levelsLutProcess", "classDigikam_1_1ImageLevels.html#aa4f9db67337134e992e9da5a9e1fb8a7", null ],
    [ "levelsLutSetup", "classDigikam_1_1ImageLevels.html#a769eeaf2671f01bf77b99082fb93ce0b", null ],
    [ "levelsWhiteToneAdjustByColors", "classDigikam_1_1ImageLevels.html#aa6e52a67ba79a01af043bf414ed25d5c", null ],
    [ "loadLevelsFromGimpLevelsFile", "classDigikam_1_1ImageLevels.html#aa8f6e98c5ff3c02078a91d4433745a60", null ],
    [ "reset", "classDigikam_1_1ImageLevels.html#a6fd7fa064e55a19185ee867c9f6285b4", null ],
    [ "saveLevelsToGimpLevelsFile", "classDigikam_1_1ImageLevels.html#a702421d125bb2fb7444eddca2b09838b", null ],
    [ "setLevelGammaValue", "classDigikam_1_1ImageLevels.html#ac6c0a34abb4d3a654dc22d17cd412169", null ],
    [ "setLevelHighInputValue", "classDigikam_1_1ImageLevels.html#a3fd5d5aafd1e054c4b52a8f0924adb8d", null ],
    [ "setLevelHighOutputValue", "classDigikam_1_1ImageLevels.html#a11a847368dfb1987f2eb6b55fcc588f9", null ],
    [ "setLevelLowInputValue", "classDigikam_1_1ImageLevels.html#a8df2978a2bd55223c4d1829bd31bde6b", null ],
    [ "setLevelLowOutputValue", "classDigikam_1_1ImageLevels.html#a03fd95c2cc0a9ffebb655a11fbbf9725", null ]
];