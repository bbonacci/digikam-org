var classDigikam_1_1CustomStepsIntSpinBox =
[
    [ "CustomStepsIntSpinBox", "classDigikam_1_1CustomStepsIntSpinBox.html#abd371c6a1ad88b143afece98a614b613", null ],
    [ "~CustomStepsIntSpinBox", "classDigikam_1_1CustomStepsIntSpinBox.html#acadd4501f3c909ea5fa8e0b2b5779ed7", null ],
    [ "enableFractionMagic", "classDigikam_1_1CustomStepsIntSpinBox.html#ac18a5ac29bd865739ff9326a87ed359e", null ],
    [ "fractionMagicValue", "classDigikam_1_1CustomStepsIntSpinBox.html#a75ad956b0d68130cc8375decbac389ea", null ],
    [ "reset", "classDigikam_1_1CustomStepsIntSpinBox.html#a1a24f7e146ed888b0f85fbeec801f681", null ],
    [ "setFractionMagicValue", "classDigikam_1_1CustomStepsIntSpinBox.html#a5671c0dfc4428504df8fda5a8333cafb", null ],
    [ "setInvertStepping", "classDigikam_1_1CustomStepsIntSpinBox.html#a31c0cef7244d6c83025f2176f0553dee", null ],
    [ "setSingleSteps", "classDigikam_1_1CustomStepsIntSpinBox.html#a12e468034f9aa58883b9cb8e06fa4873", null ],
    [ "setSuggestedInitialValue", "classDigikam_1_1CustomStepsIntSpinBox.html#a65175750d6ab725b9ffff62dd3158f56", null ],
    [ "setSuggestedValues", "classDigikam_1_1CustomStepsIntSpinBox.html#ac0e6392ee650c4f849e0b4e145d6291c", null ],
    [ "stepBy", "classDigikam_1_1CustomStepsIntSpinBox.html#a172e926031152904ccb6d9817eb9720b", null ],
    [ "stepEnabled", "classDigikam_1_1CustomStepsIntSpinBox.html#a4770c2931a764dc4dddc0d333abb5a90", null ],
    [ "textFromValue", "classDigikam_1_1CustomStepsIntSpinBox.html#a1ba520f96c6964260e9fd7ad65767cf6", null ],
    [ "valueFromText", "classDigikam_1_1CustomStepsIntSpinBox.html#ad62b8d4b7d41e8b1f87e2066cc81aa71", null ]
];