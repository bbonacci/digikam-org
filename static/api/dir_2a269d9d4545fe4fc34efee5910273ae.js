var dir_2a269d9d4545fe4fc34efee5910273ae =
[
    [ "cameralist.cpp", "cameralist_8cpp.html", null ],
    [ "cameralist.h", "cameralist_8h.html", [
      [ "CameraList", "classDigikam_1_1CameraList.html", "classDigikam_1_1CameraList" ]
    ] ],
    [ "cameraselection.cpp", "cameraselection_8cpp.html", null ],
    [ "cameraselection.h", "cameraselection_8h.html", [
      [ "CameraSelection", "classDigikam_1_1CameraSelection.html", "classDigikam_1_1CameraSelection" ]
    ] ],
    [ "cameratype.cpp", "cameratype_8cpp.html", null ],
    [ "cameratype.h", "cameratype_8h.html", [
      [ "CameraType", "classDigikam_1_1CameraType.html", "classDigikam_1_1CameraType" ]
    ] ],
    [ "importfilterdlg.cpp", "importfilterdlg_8cpp.html", null ],
    [ "importfilterdlg.h", "importfilterdlg_8h.html", [
      [ "ImportFilterDlg", "classDigikam_1_1ImportFilterDlg.html", "classDigikam_1_1ImportFilterDlg" ]
    ] ],
    [ "setupcamera.cpp", "setupcamera_8cpp.html", null ],
    [ "setupcamera.h", "setupcamera_8h.html", [
      [ "CameraAutoDetectThread", "classDigikam_1_1CameraAutoDetectThread.html", "classDigikam_1_1CameraAutoDetectThread" ],
      [ "SetupCamera", "classDigikam_1_1SetupCamera.html", "classDigikam_1_1SetupCamera" ]
    ] ]
];