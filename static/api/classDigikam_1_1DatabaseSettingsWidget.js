var classDigikam_1_1DatabaseSettingsWidget =
[
    [ "Private", "classDigikam_1_1DatabaseSettingsWidget_1_1Private.html", "classDigikam_1_1DatabaseSettingsWidget_1_1Private" ],
    [ "DatabaseType", "classDigikam_1_1DatabaseSettingsWidget.html#a35ac1908f6dc04abf2c38a127f27c259", [
      [ "SQlite", "classDigikam_1_1DatabaseSettingsWidget.html#a35ac1908f6dc04abf2c38a127f27c259a7494f736807e82e2abaa69ae0eae8d8c", null ],
      [ "MysqlInternal", "classDigikam_1_1DatabaseSettingsWidget.html#a35ac1908f6dc04abf2c38a127f27c259a8ff883fc36ce0a30e976c941a23a1092", null ],
      [ "MysqlServer", "classDigikam_1_1DatabaseSettingsWidget.html#a35ac1908f6dc04abf2c38a127f27c259aecae5f66064375425ad889d4bf3f2bf7", null ]
    ] ],
    [ "DatabaseSettingsWidget", "classDigikam_1_1DatabaseSettingsWidget.html#a37d29aed431c98e8b18d7409074033a2", null ],
    [ "~DatabaseSettingsWidget", "classDigikam_1_1DatabaseSettingsWidget.html#abafe944d58ffa9ecf97a8f777f2c8a71", null ],
    [ "checkDatabaseSettings", "classDigikam_1_1DatabaseSettingsWidget.html#a0ddf7782a49ee6c632937c5a6c505148", null ],
    [ "databaseBackend", "classDigikam_1_1DatabaseSettingsWidget.html#a66bb3870882c9b1c222c221bd6d164e6", null ],
    [ "databasePath", "classDigikam_1_1DatabaseSettingsWidget.html#a0848546aa3f3f37b86eb2557690d042d", null ],
    [ "databaseType", "classDigikam_1_1DatabaseSettingsWidget.html#a5522b7f0dcf3ab72b3e40d50e1786cf1", null ],
    [ "getDbEngineParameters", "classDigikam_1_1DatabaseSettingsWidget.html#a36fff6bef5900eba1e6c4ffff9846b9c", null ],
    [ "orgDatabasePrm", "classDigikam_1_1DatabaseSettingsWidget.html#ac392449f05a1854624c9b85f9db8a28e", null ],
    [ "setDatabasePath", "classDigikam_1_1DatabaseSettingsWidget.html#a8c85a82f32b14963bef2c475d54fd32f", null ],
    [ "setDatabaseType", "classDigikam_1_1DatabaseSettingsWidget.html#a1b4b7f72cf37de174fa5f823f21c3a50", null ],
    [ "setParametersFromSettings", "classDigikam_1_1DatabaseSettingsWidget.html#a607c302e916ff62a54cf93d869676ace", null ]
];