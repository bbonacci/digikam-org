var classDigikam_1_1FaceItem =
[
    [ "Flag", "classDigikam_1_1FaceItem.html#a9609b22d4bdf37ce78c2cf9d96ba07e6", [
      [ "NoFlags", "classDigikam_1_1FaceItem.html#a9609b22d4bdf37ce78c2cf9d96ba07e6a327a0e603979e195cc71adc129395259", null ],
      [ "ShowResizeHandles", "classDigikam_1_1FaceItem.html#a9609b22d4bdf37ce78c2cf9d96ba07e6a09b0a51d52883664db1f0ab090e285f0", null ],
      [ "MoveByDrag", "classDigikam_1_1FaceItem.html#a9609b22d4bdf37ce78c2cf9d96ba07e6ac16a16e816bde10ca2eac353cad1dd72", null ],
      [ "GeometryEditable", "classDigikam_1_1FaceItem.html#a9609b22d4bdf37ce78c2cf9d96ba07e6a365af7c2ebf8a328b6263ea795974c77", null ]
    ] ],
    [ "FaceItem", "classDigikam_1_1FaceItem.html#afc6525c075072159fa79f4a5e8b381bf", null ],
    [ "~FaceItem", "classDigikam_1_1FaceItem.html#a049193b2366b32433984a4af674e2793", null ],
    [ "boundingRect", "classDigikam_1_1FaceItem.html#a51dcd87f5e387dfae3b3b87ccb60ba1f", null ],
    [ "changeFlags", "classDigikam_1_1FaceItem.html#a493b46cb5df07f52264b7653230c4973", null ],
    [ "eventFilter", "classDigikam_1_1FaceItem.html#ad8da0fb1a4f7c0e5d857ba19ebc3b627", null ],
    [ "face", "classDigikam_1_1FaceItem.html#a5f3e4cbb2f3d718e14c5db20d039a851", null ],
    [ "flags", "classDigikam_1_1FaceItem.html#aae727201e5019e6f936d475c5ffb5d4a", null ],
    [ "geometryChanged", "classDigikam_1_1FaceItem.html#a6b7756e9779bbba12982387f6f9f4399", null ],
    [ "geometryEdited", "classDigikam_1_1FaceItem.html#a8f1f0705abfbfad1b6e05e81fc4f7914", null ],
    [ "geometryOnImageChanged", "classDigikam_1_1FaceItem.html#afe3f8d8b49e876b77ffe2cd5351d4dc7", null ],
    [ "hoverEnterEvent", "classDigikam_1_1FaceItem.html#a26d1bf09aa497b38b2ee784473f80614", null ],
    [ "hoverLeaveEvent", "classDigikam_1_1FaceItem.html#a47296a5be5bc44d0b80756d21ad2cb24", null ],
    [ "hoverMoveEvent", "classDigikam_1_1FaceItem.html#afb8dc3fcc030c7165998382be4aaedb9", null ],
    [ "hudWidget", "classDigikam_1_1FaceItem.html#a2593ad8e763bda23ffd051b219bca4ea", null ],
    [ "imageSizeChanged", "classDigikam_1_1FaceItem.html#a95da4fcd8b5c9baa410009d2523a91cf", null ],
    [ "itemChange", "classDigikam_1_1FaceItem.html#a290e6153b29bcc6e826d600791b1de25", null ],
    [ "mouseMoveEvent", "classDigikam_1_1FaceItem.html#a4c946a05a88a45d8ab5a732fa12140f2", null ],
    [ "mousePressEvent", "classDigikam_1_1FaceItem.html#a857f651be41f37cbb343f021b5373326", null ],
    [ "mouseReleaseEvent", "classDigikam_1_1FaceItem.html#a4f2fd257c1a1f57a65a25684e0b38ae8", null ],
    [ "moveBy", "classDigikam_1_1FaceItem.html#ab631aa304d2aa76a58007a1ef337baa6", null ],
    [ "originalPos", "classDigikam_1_1FaceItem.html#a3822138d812cfba7304a17c773817fc5", null ],
    [ "originalRect", "classDigikam_1_1FaceItem.html#a7c32f55ca1ec52b10abee4e5ef904271", null ],
    [ "originalSize", "classDigikam_1_1FaceItem.html#a4c9efe170114709907135682aabbc5ab", null ],
    [ "paint", "classDigikam_1_1FaceItem.html#a0fab523037b1063ad7fe40e9cb7ff806", null ],
    [ "parentDImgItem", "classDigikam_1_1FaceItem.html#acd684ab88429c0dac4fa318ab461055f", null ],
    [ "positionChanged", "classDigikam_1_1FaceItem.html#a17142961a5936f872d260d8a8dc40822", null ],
    [ "positionOnImageChanged", "classDigikam_1_1FaceItem.html#ae98450f8461f0e2952f69d66da8dc747", null ],
    [ "rect", "classDigikam_1_1FaceItem.html#a32819b7f159258c8ef46b112d3abd50b", null ],
    [ "relativePos", "classDigikam_1_1FaceItem.html#a7b32c87e31584719eeb8ee469786b4f5", null ],
    [ "relativeRect", "classDigikam_1_1FaceItem.html#a93d96ecd129dce8e25e5614e49f63079", null ],
    [ "relativeSize", "classDigikam_1_1FaceItem.html#a20102a4d59e6e18d807b14cec941678b", null ],
    [ "setEditable", "classDigikam_1_1FaceItem.html#a0ff9cdb2fa4e7dc58c42d49f42e523f0", null ],
    [ "setFace", "classDigikam_1_1FaceItem.html#ac695789ddf66f08d6f8d01f68b75704f", null ],
    [ "setFixedRatio", "classDigikam_1_1FaceItem.html#a551b0da7c6ebf972707540c1c8996e1f", null ],
    [ "setFlags", "classDigikam_1_1FaceItem.html#a122aae3c89d5627991d41c9f7f607dee", null ],
    [ "setHudWidget", "classDigikam_1_1FaceItem.html#ab29fd3820b48f6114bbf8940a323a31b", null ],
    [ "setHudWidget", "classDigikam_1_1FaceItem.html#aef0f21b32ba618b06f18a78a125d0980", null ],
    [ "setHudWidget", "classDigikam_1_1FaceItem.html#af2b22dd268dd780eb9ef1998c9aa76f0", null ],
    [ "setHudWidgetVisible", "classDigikam_1_1FaceItem.html#a5674ef6524de40d808bbbdf617a1728d", null ],
    [ "setOriginalPos", "classDigikam_1_1FaceItem.html#a3789e960554a206587835dfd50f9882d", null ],
    [ "setOriginalPos", "classDigikam_1_1FaceItem.html#a47012123a5ae751f5629773ed570bb49", null ],
    [ "setOriginalRect", "classDigikam_1_1FaceItem.html#a65e3934f831f8446ecab3b389ac8840b", null ],
    [ "setOriginalRect", "classDigikam_1_1FaceItem.html#a63f61e11051448421cd9ee527d49ad7a", null ],
    [ "setOriginalSize", "classDigikam_1_1FaceItem.html#a4154c23f3186e3db54771d81d45ebd5b", null ],
    [ "setOriginalSize", "classDigikam_1_1FaceItem.html#a397fedca27df21362be6d15e4dc23788", null ],
    [ "setPos", "classDigikam_1_1FaceItem.html#ae7436629c287a825bf9346d89ea6467e", null ],
    [ "setPos", "classDigikam_1_1FaceItem.html#a072955a290be15c784d97d35b5ef91f7", null ],
    [ "setRect", "classDigikam_1_1FaceItem.html#a0652a036b7f920ab65758db4c9a31a54", null ],
    [ "setRect", "classDigikam_1_1FaceItem.html#ab82cafa31fb4262fbdea8999e9ceed4e", null ],
    [ "setRectInSceneCoordinates", "classDigikam_1_1FaceItem.html#a4e5e288bec0e7a9cd17879be5df1ce44", null ],
    [ "setRectInSceneCoordinatesAdjusted", "classDigikam_1_1FaceItem.html#aa4d22f8952425ad230545a310a0fae8a", null ],
    [ "setRelativePos", "classDigikam_1_1FaceItem.html#a0f32b0249819ad032b174d8f8a007a28", null ],
    [ "setRelativePos", "classDigikam_1_1FaceItem.html#af68c611f29f42c2806ce6005e6f9beb8", null ],
    [ "setRelativeRect", "classDigikam_1_1FaceItem.html#a120c38b4f912cf5e035ddf3b6ea454fc", null ],
    [ "setRelativeRect", "classDigikam_1_1FaceItem.html#a06f29d70c0c77c4243bbe5ace8370731", null ],
    [ "setRelativeSize", "classDigikam_1_1FaceItem.html#a5d58bf9eddc889b4efee0951aa47a1c1", null ],
    [ "setRelativeSize", "classDigikam_1_1FaceItem.html#a45b43bacc40b5b483f3dcbb1e2b6a928", null ],
    [ "setSize", "classDigikam_1_1FaceItem.html#af30a1a5aff89b12b4d8152b20a08df89", null ],
    [ "setSize", "classDigikam_1_1FaceItem.html#a39669e8ae52038908dfdbab2fe8a86ef", null ],
    [ "setViewportRect", "classDigikam_1_1FaceItem.html#ab8700bf903217f4ab157e272b6eefcde", null ],
    [ "size", "classDigikam_1_1FaceItem.html#a1431ef13751ccf3f02328e140f04cda7", null ],
    [ "sizeChanged", "classDigikam_1_1FaceItem.html#a63fde0d4d0dd6840f3b21669df1a8611", null ],
    [ "sizeOnImageChanged", "classDigikam_1_1FaceItem.html#a31651b1606bb9aa36a4357a1bec0fa41", null ],
    [ "switchMode", "classDigikam_1_1FaceItem.html#a6e413c35dd8e8547159ab680b9bc4ef5", null ],
    [ "updateCurrentTag", "classDigikam_1_1FaceItem.html#a8a14415efce7f578adf0f50ebbe2153e", null ],
    [ "widget", "classDigikam_1_1FaceItem.html#ad73e45632483e8d0e3bc2a0ebe5204b3", null ],
    [ "m_changer", "classDigikam_1_1FaceItem.html#a25e37da90509efcc41aceceee3ff0924", null ],
    [ "m_face", "classDigikam_1_1FaceItem.html#abfbff51bfff299cf4ad19a152e38d192", null ],
    [ "m_widget", "classDigikam_1_1FaceItem.html#a673f76c5ce60dda382a1dd110c0be0c5", null ]
];