var dir_90ad111ee6b302482582737c0b6fcadd =
[
    [ "auto", "dir_a96ed05feace7017e7721bb9bca74ca8.html", "dir_a96ed05feace7017e7721bb9bca74ca8" ],
    [ "bcg", "dir_c1fd2015d54d9daa4e84a256f8dca728.html", "dir_c1fd2015d54d9daa4e84a256f8dca728" ],
    [ "bw", "dir_bef9ed0f20b69231f08e65b136f0599a.html", "dir_bef9ed0f20b69231f08e65b136f0599a" ],
    [ "cb", "dir_64a8d96418b912964363146d2c0a5ebe.html", "dir_64a8d96418b912964363146d2c0a5ebe" ],
    [ "curves", "dir_9600d990ff3f4cdf9291d3f6ee5b5367.html", "dir_9600d990ff3f4cdf9291d3f6ee5b5367" ],
    [ "decorate", "dir_d4a7797cde6c784f56c558adf7618f23.html", "dir_d4a7797cde6c784f56c558adf7618f23" ],
    [ "film", "dir_0079dd0b799fd0df719a5eadbcca1758.html", "dir_0079dd0b799fd0df719a5eadbcca1758" ],
    [ "fx", "dir_ed298dcb0446a542e1f7617f5410ad00.html", "dir_ed298dcb0446a542e1f7617f5410ad00" ],
    [ "greycstoration", "dir_80b3a9c1981efd9007e79770734d5335.html", "dir_80b3a9c1981efd9007e79770734d5335" ],
    [ "hotpixels", "dir_206d22dc41f31906e956363e1694ee91.html", "dir_206d22dc41f31906e956363e1694ee91" ],
    [ "hsl", "dir_93ee4ebc3dd3378faea96817259d364d.html", "dir_93ee4ebc3dd3378faea96817259d364d" ],
    [ "icc", "dir_e5675db27fc7abe25d6b5dd638d88bd4.html", "dir_e5675db27fc7abe25d6b5dd638d88bd4" ],
    [ "imgqsort", "dir_9ca61690c6e9b726bfb84fcded18338c.html", "dir_9ca61690c6e9b726bfb84fcded18338c" ],
    [ "lc", "dir_7fed21240dce08c787f31afde160b690.html", "dir_7fed21240dce08c787f31afde160b690" ],
    [ "lens", "dir_b1d19e6cad0549c1986ce42877c3f279.html", "dir_b1d19e6cad0549c1986ce42877c3f279" ],
    [ "levels", "dir_7394af9ad2aeba2a23b236239a7f0f1c.html", "dir_7394af9ad2aeba2a23b236239a7f0f1c" ],
    [ "nr", "dir_1c03e9b57bed9171a9e9e3ffc67529b8.html", "dir_1c03e9b57bed9171a9e9e3ffc67529b8" ],
    [ "raw", "dir_17ddbe6d52fef156bdda1ef43548510b.html", "dir_17ddbe6d52fef156bdda1ef43548510b" ],
    [ "redeye", "dir_aa518d25ed052ed0fdac06a9866a9ac6.html", "dir_aa518d25ed052ed0fdac06a9866a9ac6" ],
    [ "sharp", "dir_bd94b93c261756a618b5955ed78c210e.html", "dir_bd94b93c261756a618b5955ed78c210e" ],
    [ "transform", "dir_1bb750bdc12f98410fb9b22aec15e882.html", "dir_1bb750bdc12f98410fb9b22aec15e882" ],
    [ "wb", "dir_35e304bddcbec8c8202e93164199b4b0.html", "dir_35e304bddcbec8c8202e93164199b4b0" ],
    [ "dimgbuiltinfilter.cpp", "dimgbuiltinfilter_8cpp.html", null ],
    [ "dimgbuiltinfilter.h", "dimgbuiltinfilter_8h.html", [
      [ "DImgBuiltinFilter", "classDigikam_1_1DImgBuiltinFilter.html", "classDigikam_1_1DImgBuiltinFilter" ]
    ] ],
    [ "dimgfiltergenerator.cpp", "dimgfiltergenerator_8cpp.html", null ],
    [ "dimgfiltergenerator.h", "dimgfiltergenerator_8h.html", [
      [ "BasicDImgFilterGenerator", "classDigikam_1_1BasicDImgFilterGenerator.html", "classDigikam_1_1BasicDImgFilterGenerator" ],
      [ "DImgFilterGenerator", "classDigikam_1_1DImgFilterGenerator.html", "classDigikam_1_1DImgFilterGenerator" ]
    ] ],
    [ "dimgfiltermanager.cpp", "dimgfiltermanager_8cpp.html", "dimgfiltermanager_8cpp" ],
    [ "dimgfiltermanager.h", "dimgfiltermanager_8h.html", [
      [ "DImgFilterManager", "classDigikam_1_1DImgFilterManager.html", "classDigikam_1_1DImgFilterManager" ]
    ] ],
    [ "dimgthreadedanalyser.cpp", "dimgthreadedanalyser_8cpp.html", null ],
    [ "dimgthreadedanalyser.h", "dimgthreadedanalyser_8h.html", [
      [ "DImgThreadedAnalyser", "classDigikam_1_1DImgThreadedAnalyser.html", "classDigikam_1_1DImgThreadedAnalyser" ]
    ] ],
    [ "dimgthreadedfilter.cpp", "dimgthreadedfilter_8cpp.html", null ],
    [ "dimgthreadedfilter.h", "dimgthreadedfilter_8h.html", [
      [ "DImgThreadedFilter", "classDigikam_1_1DImgThreadedFilter.html", "classDigikam_1_1DImgThreadedFilter" ],
      [ "DefaultFilterAction", "classDigikam_1_1DImgThreadedFilter_1_1DefaultFilterAction.html", "classDigikam_1_1DImgThreadedFilter_1_1DefaultFilterAction" ]
    ] ],
    [ "dpixelsaliasfilter.cpp", "dpixelsaliasfilter_8cpp.html", null ],
    [ "dpixelsaliasfilter.h", "dpixelsaliasfilter_8h.html", [
      [ "DPixelsAliasFilter", "classDigikam_1_1DPixelsAliasFilter.html", "classDigikam_1_1DPixelsAliasFilter" ]
    ] ],
    [ "filteractionfilter.cpp", "filteractionfilter_8cpp.html", null ],
    [ "filteractionfilter.h", "filteractionfilter_8h.html", [
      [ "FilterActionFilter", "classDigikam_1_1FilterActionFilter.html", "classDigikam_1_1FilterActionFilter" ]
    ] ],
    [ "randomnumbergenerator.cpp", "randomnumbergenerator_8cpp.html", null ],
    [ "randomnumbergenerator.h", "randomnumbergenerator_8h.html", [
      [ "NonDeterministicRandomData", "classDigikam_1_1NonDeterministicRandomData.html", "classDigikam_1_1NonDeterministicRandomData" ],
      [ "RandomNumberGenerator", "classDigikam_1_1RandomNumberGenerator.html", "classDigikam_1_1RandomNumberGenerator" ]
    ] ]
];