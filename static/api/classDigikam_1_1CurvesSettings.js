var classDigikam_1_1CurvesSettings =
[
    [ "CurvesSettings", "classDigikam_1_1CurvesSettings.html#aacab9e319cbb84edee8bd9b05d9954b1", null ],
    [ "~CurvesSettings", "classDigikam_1_1CurvesSettings.html#a9c3eb95332699dd8d06aa7da8f18b115", null ],
    [ "curvesLeftOffset", "classDigikam_1_1CurvesSettings.html#a3f6bae7dbebbafffe26356030730e6ed", null ],
    [ "defaultSettings", "classDigikam_1_1CurvesSettings.html#a0910ae7b1ff4a574e335b27d581ad018", null ],
    [ "loadSettings", "classDigikam_1_1CurvesSettings.html#aa88c33286b89d3d397856794d9275e07", null ],
    [ "readSettings", "classDigikam_1_1CurvesSettings.html#ab6380e7da165849bbe6ca588a6ce8f84", null ],
    [ "resetToDefault", "classDigikam_1_1CurvesSettings.html#abec5dd1244394ae2b1c00911d2505fb9", null ],
    [ "saveAsSettings", "classDigikam_1_1CurvesSettings.html#a5dd17085dc791d785cbad17e6f9be259", null ],
    [ "setCurrentChannel", "classDigikam_1_1CurvesSettings.html#a0ceeb4d44d7284adc5dc725c12eaca2a", null ],
    [ "setScale", "classDigikam_1_1CurvesSettings.html#ac0b7995175a30651ada1d0a10494a076", null ],
    [ "setSettings", "classDigikam_1_1CurvesSettings.html#af939de6cfcd669137b511d4b37b650f6", null ],
    [ "settings", "classDigikam_1_1CurvesSettings.html#a83c6a9c66a5a6b5a0c581ea9e56c2ab5", null ],
    [ "signalChannelReset", "classDigikam_1_1CurvesSettings.html#aa281fdec185cba27a4bc0023b37ea9ef", null ],
    [ "signalPickerChanged", "classDigikam_1_1CurvesSettings.html#a44c6154781d0a2c95247742a950c52aa", null ],
    [ "signalSettingsChanged", "classDigikam_1_1CurvesSettings.html#a5e66e0333a922272b909bd7c25769ef3", null ],
    [ "signalSpotColorChanged", "classDigikam_1_1CurvesSettings.html#a94f02eb18ad5eb6e7420194468c9aba8", null ],
    [ "slotSpotColorChanged", "classDigikam_1_1CurvesSettings.html#abd98e9a7a67ef962eeb2016585b04ad1", null ],
    [ "writeSettings", "classDigikam_1_1CurvesSettings.html#ad458c90e9602368ad65c2dfb1c59a955", null ]
];