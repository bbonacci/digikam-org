var structen265__packet =
[
    [ "complete_picture", "structen265__packet.html#a7e778d7d7a8ec7f55d9da5157a8003db", null ],
    [ "content_type", "structen265__packet.html#ae3d9fb26563f3489f87c401314651100", null ],
    [ "data", "structen265__packet.html#a65207049564881d11cf89fa8d09470a9", null ],
    [ "dependent_slice", "structen265__packet.html#ab84c69bbbbf874dca9b112b1fb62a6d4", null ],
    [ "encoder_context", "structen265__packet.html#aaa1ddf6d09251246aa2e869320de4940", null ],
    [ "final_slice", "structen265__packet.html#ae16f25815d42539ee1b911a735f0fdf4", null ],
    [ "frame_number", "structen265__packet.html#aa46ed600262211d6b0c95b9e40988094", null ],
    [ "input_image", "structen265__packet.html#a6e45df0ef584f59ceb62b5695a894606", null ],
    [ "length", "structen265__packet.html#a6cbe18072b3672e3c8a548d74aa6fc41", null ],
    [ "nal_unit_type", "structen265__packet.html#a919c49bf2f5f2fbf4ba234eab5772d3b", null ],
    [ "nuh_layer_id", "structen265__packet.html#a52c5cb840932d3f5b14ce25846c8aaf3", null ],
    [ "nuh_temporal_id", "structen265__packet.html#ae18a63a1e4250abeb393df9756986129", null ],
    [ "reconstruction", "structen265__packet.html#a61fce9da10040b8c93993243aef96fcc", null ],
    [ "version", "structen265__packet.html#a6bc571c8cb17623b94f7c1b7340c01d7", null ]
];