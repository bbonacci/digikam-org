var classDigikam_1_1DNNFaceDetectorSSD =
[
    [ "DNNFaceDetectorSSD", "classDigikam_1_1DNNFaceDetectorSSD.html#ae40d1c516805ae83be93f42cc74630ff", null ],
    [ "~DNNFaceDetectorSSD", "classDigikam_1_1DNNFaceDetectorSSD.html#a236ce94488b6ddb3512961500359b40d", null ],
    [ "correctBbox", "classDigikam_1_1DNNFaceDetectorSSD.html#a0fef3b769989f7e1f752dbf7074697c4", null ],
    [ "detectFaces", "classDigikam_1_1DNNFaceDetectorSSD.html#a7652d1e4036d2b44f3b326fd6c5ad9e9", null ],
    [ "loadModels", "classDigikam_1_1DNNFaceDetectorSSD.html#ad7dda8d733a436d2ebaa3ae59ff8cb8f", null ],
    [ "nnInputSizeRequired", "classDigikam_1_1DNNFaceDetectorSSD.html#a10c2f7b450ebc0a6b0b631e333f4f10d", null ],
    [ "selectBbox", "classDigikam_1_1DNNFaceDetectorSSD.html#ac162bff0abe3bb46f644651ae04d7026", null ],
    [ "inputImageSize", "classDigikam_1_1DNNFaceDetectorSSD.html#a57327c3e3bc01d71b2c155cb51f10488", null ],
    [ "meanValToSubtract", "classDigikam_1_1DNNFaceDetectorSSD.html#a800befeba470c89ae6e2b9cad13bf6f3", null ],
    [ "mutex", "classDigikam_1_1DNNFaceDetectorSSD.html#add749bb2513feabbf51fbf7a8d368fbf", null ],
    [ "net", "classDigikam_1_1DNNFaceDetectorSSD.html#a05c55414cc0784bebc3925a6547f8fef", null ],
    [ "scaleFactor", "classDigikam_1_1DNNFaceDetectorSSD.html#a3c902bace9d612c1990c1eeeec51b22f", null ]
];