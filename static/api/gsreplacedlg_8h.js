var gsreplacedlg_8h =
[
    [ "ReplaceDialog", "classDigikamGenericGoogleServicesPlugin_1_1ReplaceDialog.html", "classDigikamGenericGoogleServicesPlugin_1_1ReplaceDialog" ],
    [ "ReplaceDialog_Result", "gsreplacedlg_8h.html#a86445e451cc62b27cdf92f8af8abdb9b", [
      [ "PWR_CANCEL", "gsreplacedlg_8h.html#a86445e451cc62b27cdf92f8af8abdb9baad57562461bb7bb88e783299213a4691", null ],
      [ "PWR_ADD", "gsreplacedlg_8h.html#a86445e451cc62b27cdf92f8af8abdb9ba259400ee1de50944de227b70d34b0598", null ],
      [ "PWR_ADD_ALL", "gsreplacedlg_8h.html#a86445e451cc62b27cdf92f8af8abdb9ba0bc26ec8fa418b38ba41f2af2c03c973", null ],
      [ "PWR_REPLACE", "gsreplacedlg_8h.html#a86445e451cc62b27cdf92f8af8abdb9ba8e6b6cb45830b68a1293c11d117090d4", null ],
      [ "PWR_REPLACE_ALL", "gsreplacedlg_8h.html#a86445e451cc62b27cdf92f8af8abdb9ba0956ce1c1a4b569221e586c1190fd734", null ]
    ] ]
];