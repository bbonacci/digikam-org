var classDigikam_1_1ImageQualitySettings =
[
    [ "ImageQualitySettings", "classDigikam_1_1ImageQualitySettings.html#a9fa467865161e5dc6cb41b60b51ad5bb", null ],
    [ "~ImageQualitySettings", "classDigikam_1_1ImageQualitySettings.html#a5e63448fc7f6d95fbd7a27ceec8f4569", null ],
    [ "applySettings", "classDigikam_1_1ImageQualitySettings.html#aecfb9d16f109d3615bf61dc47fe54bff", null ],
    [ "getImageQualityContainer", "classDigikam_1_1ImageQualitySettings.html#a5e9278f86378529b7aabf262cec1deeb", null ],
    [ "readSettings", "classDigikam_1_1ImageQualitySettings.html#a9b79f35b529088cc2a6f46b772a3903c", null ]
];