var classDigikam_1_1PreviewLoadingTask =
[
    [ "LoadingTaskStatus", "classDigikam_1_1PreviewLoadingTask.html#aa4131ed076cc79578e21922f88d08255", [
      [ "LoadingTaskStatusLoading", "classDigikam_1_1PreviewLoadingTask.html#aa4131ed076cc79578e21922f88d08255a9fb917335571fcf5a3c92c4929800b17", null ],
      [ "LoadingTaskStatusPreloading", "classDigikam_1_1PreviewLoadingTask.html#aa4131ed076cc79578e21922f88d08255a28d9aec69c9254aabc1a0494204b64d4", null ],
      [ "LoadingTaskStatusStopping", "classDigikam_1_1PreviewLoadingTask.html#aa4131ed076cc79578e21922f88d08255ad8070c2c8f54d077b89a3b211c964b82", null ]
    ] ],
    [ "TaskType", "classDigikam_1_1PreviewLoadingTask.html#a31124b0a1702f3922b442b8efecd4490", [
      [ "TaskTypeLoading", "classDigikam_1_1PreviewLoadingTask.html#a31124b0a1702f3922b442b8efecd4490a36456327a8b7bf2e3cd7dee8ddfc949c", null ],
      [ "TaskTypeSaving", "classDigikam_1_1PreviewLoadingTask.html#a31124b0a1702f3922b442b8efecd4490a4e4323e60ccc3a9c630058150c411b3b", null ]
    ] ],
    [ "PreviewLoadingTask", "classDigikam_1_1PreviewLoadingTask.html#ab0fb97a1ad2a33518835708e949f56e6", null ],
    [ "~PreviewLoadingTask", "classDigikam_1_1PreviewLoadingTask.html#a4c01c3783726a084b8d058ae0bbc0cf8", null ],
    [ "accessMode", "classDigikam_1_1PreviewLoadingTask.html#a3f31c38b7606b40c749340487d9a5b55", null ],
    [ "addListener", "classDigikam_1_1PreviewLoadingTask.html#abb5fd83653837f547d8c1b88c2b7c18b", null ],
    [ "cacheKey", "classDigikam_1_1PreviewLoadingTask.html#a1067dfc3fa8804b03eeb3d1e3ba9bac2", null ],
    [ "completed", "classDigikam_1_1PreviewLoadingTask.html#aab624e9a16511a2c6fafafb4b1e1e6db", null ],
    [ "continueQuery", "classDigikam_1_1PreviewLoadingTask.html#a33ffedea47be3d1f9e097ad2ef8c39da", null ],
    [ "execute", "classDigikam_1_1PreviewLoadingTask.html#abd747a6c6834d9cb1e4f01fae11795a5", null ],
    [ "filePath", "classDigikam_1_1PreviewLoadingTask.html#a31c24110e86375e6fda784927f1e57e7", null ],
    [ "granularity", "classDigikam_1_1PreviewLoadingTask.html#ab976f06eb18822b0da8e481fd2ff55d0", null ],
    [ "img", "classDigikam_1_1PreviewLoadingTask.html#ab8e4672d7f66b5aca48dfc430ced2569", null ],
    [ "loadingDescription", "classDigikam_1_1PreviewLoadingTask.html#a3e8772984b42901b441e9d878e396ad7", null ],
    [ "loadSaveNotifier", "classDigikam_1_1PreviewLoadingTask.html#a1013b97be358eb54782a8bcf893a6f45", null ],
    [ "needsPostProcessing", "classDigikam_1_1PreviewLoadingTask.html#ab1fc0033e7b7d446e9e8f365c23c8c34", null ],
    [ "notifyNewLoadingProcess", "classDigikam_1_1PreviewLoadingTask.html#ae66da407c26ab9618675825c30532bf6", null ],
    [ "postProcess", "classDigikam_1_1PreviewLoadingTask.html#ad97c67c25d44b7138fc0f9c67fa0dff0", null ],
    [ "progressInfo", "classDigikam_1_1PreviewLoadingTask.html#a1907a7fb429679088e27a2e5d062a5ca", null ],
    [ "querySendNotifyEvent", "classDigikam_1_1PreviewLoadingTask.html#a2c55410c0b046f4b5e9d81e516bc303a", null ],
    [ "removeListener", "classDigikam_1_1PreviewLoadingTask.html#a2b5af90f37dcd5ccec5f2d5fe6b58003", null ],
    [ "setResult", "classDigikam_1_1PreviewLoadingTask.html#ad94b16dd3cae56ef7973a78d3dfa6997", null ],
    [ "setStatus", "classDigikam_1_1PreviewLoadingTask.html#a52051344cc4589d6d0e8335a938f1b75", null ],
    [ "status", "classDigikam_1_1PreviewLoadingTask.html#a1a797a9d75eac08782a879a4955fbdcb", null ],
    [ "type", "classDigikam_1_1PreviewLoadingTask.html#a5fee5b84b321dfd50f3a054078ea4cd4", null ],
    [ "m_accessMode", "classDigikam_1_1PreviewLoadingTask.html#aae251f49ca1b13d688c9c19957bbc809", null ],
    [ "m_completed", "classDigikam_1_1PreviewLoadingTask.html#ae3825bd82ce4ceeb2e8604cfc7521389", null ],
    [ "m_img", "classDigikam_1_1PreviewLoadingTask.html#a6a634b1b30dc2d7095b8c583e19f8fb3", null ],
    [ "m_listeners", "classDigikam_1_1PreviewLoadingTask.html#ab62925a9c1419e3518ae1ccf42c2f37e", null ],
    [ "m_loadingDescription", "classDigikam_1_1PreviewLoadingTask.html#ab16d4a9ce3d15d4148a711218bcecf4e", null ],
    [ "m_loadingTaskStatus", "classDigikam_1_1PreviewLoadingTask.html#aac204d0b11985a2f5c04ab0087c5fe66", null ],
    [ "m_thread", "classDigikam_1_1PreviewLoadingTask.html#abfe53f7642fca9a8e3ed0f7d5438ae7d", null ]
];