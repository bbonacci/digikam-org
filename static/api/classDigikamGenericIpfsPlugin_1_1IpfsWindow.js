var classDigikamGenericIpfsPlugin_1_1IpfsWindow =
[
    [ "IpfsWindow", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html#a575e4341c26480a11e2dc9f0fe73e9f3", null ],
    [ "~IpfsWindow", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html#a126aad5ed9cb4d6acae5449318d0f0fa", null ],
    [ "addButton", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html#a338ad1ca1d39489c83775e3286302c59", null ],
    [ "apiBusy", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html#a9ccd843026977da0b62d78bb402fb437", null ],
    [ "apiError", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html#a0da4d00cefa83bdb9ae22bfcaa961493", null ],
    [ "apiProgress", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html#a970060a6efeef80fdc0c7e416ef7e11d", null ],
    [ "apiRequestPin", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html#a30952f9f8b0bfa81e033006cf25446fe", null ],
    [ "apiSuccess", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html#a21aadf0d52c80c408b47cd0e675344c1", null ],
    [ "cancelClicked", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html#a6f9919249e2e8aa237fabb2ed72e75a2", null ],
    [ "reactivate", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html#a4cc7c1d28924f171fc82f8eeb122e846", null ],
    [ "restoreDialogSize", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html#a5da66f5eb64468e9524230138e09c2a2", null ],
    [ "saveDialogSize", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html#a5496bc5fd80e57edd35e652dbb8361ae", null ],
    [ "setMainWidget", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html#ae956aceb6ae24079791e2f491957d446", null ],
    [ "setPlugin", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html#a8f9473a19990a8001d98cde6286d1059", null ],
    [ "setRejectButtonMode", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html#a32667efc4f6a02e644886729952fd354", null ],
    [ "slotCancel", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html#a119114527a55ef69e73aa75f5ff3d310", null ],
    [ "slotFinished", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html#a661fa5e5c9631b649c49dc68b475cece", null ],
    [ "slotUpload", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html#a9f510c452c25df3958a276c4745e8e41", null ],
    [ "startButton", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html#a34e7c6deb1458c37066862375a487600", null ],
    [ "m_buttons", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html#a06170b9f45856493f30cb0edd368a3c0", null ]
];