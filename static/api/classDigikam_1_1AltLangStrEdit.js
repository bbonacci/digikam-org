var classDigikam_1_1AltLangStrEdit =
[
    [ "AltLangStrEdit", "classDigikam_1_1AltLangStrEdit.html#a91cdcefd56d9eb5455fc00b038ae22e6", null ],
    [ "~AltLangStrEdit", "classDigikam_1_1AltLangStrEdit.html#ad91ddc2c98031a955b4bb8b557aab185", null ],
    [ "addCurrent", "classDigikam_1_1AltLangStrEdit.html#a9e026b5244a4febb2180623f64e9fbe1", null ],
    [ "asDefaultAltLang", "classDigikam_1_1AltLangStrEdit.html#a34e0c711792ba680d1bf0ef19c45550c", null ],
    [ "changeEvent", "classDigikam_1_1AltLangStrEdit.html#a6dfdcf090bc852ec5fb801691aa98e18", null ],
    [ "currentLanguageCode", "classDigikam_1_1AltLangStrEdit.html#a3f10d7d72066fd1ceed3cf567d5e596e", null ],
    [ "defaultAltLang", "classDigikam_1_1AltLangStrEdit.html#aaa3e1a88f9f612b6fedc34c8c1b111f3", null ],
    [ "languageCode", "classDigikam_1_1AltLangStrEdit.html#a2bf236d327e6cde71ede5d93e8e5b48f", null ],
    [ "linesVisible", "classDigikam_1_1AltLangStrEdit.html#a6db87f00367da418342f3509842c9f15", null ],
    [ "loadLangAltListEntries", "classDigikam_1_1AltLangStrEdit.html#a77d3f3a656a2978dbe4d8b2da93c1cfa", null ],
    [ "reset", "classDigikam_1_1AltLangStrEdit.html#ac464808aa99dba9690c72f8fd542e9fb", null ],
    [ "setCurrentLanguageCode", "classDigikam_1_1AltLangStrEdit.html#a128fad90e298ea1315ae791a250951da", null ],
    [ "setLinesVisible", "classDigikam_1_1AltLangStrEdit.html#a059d9a6215579eded85de02641d78456", null ],
    [ "setPlaceholderText", "classDigikam_1_1AltLangStrEdit.html#a9a22bb0814c0a777462650bf76f44342", null ],
    [ "setTitle", "classDigikam_1_1AltLangStrEdit.html#a8891490e42ddd6a5c5097968e7b425bc", null ],
    [ "setValues", "classDigikam_1_1AltLangStrEdit.html#a578ff41894996429046372ba04093062", null ],
    [ "signalModified", "classDigikam_1_1AltLangStrEdit.html#a87abd4280db5706621e73fc01ae8d555", null ],
    [ "signalSelectionChanged", "classDigikam_1_1AltLangStrEdit.html#a3822ca16d195b4c63923ce3ba5ffe696", null ],
    [ "signalValueAdded", "classDigikam_1_1AltLangStrEdit.html#a7c0936b58168fb772c02db1b45bd1eaa", null ],
    [ "signalValueDeleted", "classDigikam_1_1AltLangStrEdit.html#ae78734a79854d3388c29bfbb3c30ccda", null ],
    [ "slotDeleteValue", "classDigikam_1_1AltLangStrEdit.html#ab96ca2d280cb7fca1df3f10230726cdf", null ],
    [ "slotSelectionChanged", "classDigikam_1_1AltLangStrEdit.html#a18561e34a4099d7a8e5d75c145048044", null ],
    [ "slotTextChanged", "classDigikam_1_1AltLangStrEdit.html#a8b5bb09bde5fd385281ad80f4690ee8b", null ],
    [ "textEdit", "classDigikam_1_1AltLangStrEdit.html#ab84e77b46a8bf8661b0bbc4222cca20d", null ],
    [ "values", "classDigikam_1_1AltLangStrEdit.html#ab61373fd007dcd7f52c778f6b3c27d8e", null ],
    [ "Private", "classDigikam_1_1AltLangStrEdit.html#ac96b60d37bd806132da680e187dc2288", null ]
];