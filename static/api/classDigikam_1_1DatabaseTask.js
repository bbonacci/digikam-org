var classDigikam_1_1DatabaseTask =
[
    [ "Mode", "classDigikam_1_1DatabaseTask.html#aa6f177d9af9239b74a53d2a68c757727", [
      [ "Unknown", "classDigikam_1_1DatabaseTask.html#aa6f177d9af9239b74a53d2a68c757727af249cdebe7f3a3bb94ae285b013a139d", null ],
      [ "ComputeDatabaseJunk", "classDigikam_1_1DatabaseTask.html#aa6f177d9af9239b74a53d2a68c757727ac4cf524921f9111889b611cd79634196", null ],
      [ "CleanCoreDb", "classDigikam_1_1DatabaseTask.html#aa6f177d9af9239b74a53d2a68c757727ab6e75e35af85af630f8be71c81794ee1", null ],
      [ "CleanThumbsDb", "classDigikam_1_1DatabaseTask.html#aa6f177d9af9239b74a53d2a68c757727afcdc335de56763378411e3503bffc285", null ],
      [ "CleanRecognitionDb", "classDigikam_1_1DatabaseTask.html#aa6f177d9af9239b74a53d2a68c757727aa905c0a3d5f2219fecdec203928aeef4", null ],
      [ "CleanSimilarityDb", "classDigikam_1_1DatabaseTask.html#aa6f177d9af9239b74a53d2a68c757727ab78d146f2b71f5b40d0e31bc5e1f1746", null ],
      [ "ShrinkDatabases", "classDigikam_1_1DatabaseTask.html#aa6f177d9af9239b74a53d2a68c757727aa813a980f0ad93d1cca1406a62108cc0", null ]
    ] ],
    [ "DatabaseTask", "classDigikam_1_1DatabaseTask.html#a8f2f55b41ffb20160ef5b2aea41aed0e", null ],
    [ "~DatabaseTask", "classDigikam_1_1DatabaseTask.html#adf9f3819fb5430f73b74a0b2fe583215", null ],
    [ "cancel", "classDigikam_1_1DatabaseTask.html#ad801a5f7a879375239c3acc000cb91a4", null ],
    [ "computeDatabaseJunk", "classDigikam_1_1DatabaseTask.html#a7aa28b2a81eef70e8fac856f02abc63c", null ],
    [ "run", "classDigikam_1_1DatabaseTask.html#a66f783dfdab099212dbe961015dfb80c", null ],
    [ "setMaintenanceData", "classDigikam_1_1DatabaseTask.html#a5030411d01e5b5f397770e616c1ef441", null ],
    [ "setMode", "classDigikam_1_1DatabaseTask.html#af6aecfb728c6afc53792cffa76426141", null ],
    [ "signalAddItemsToProcess", "classDigikam_1_1DatabaseTask.html#a7099cac824698f9689cd15985e501529", null ],
    [ "signalData", "classDigikam_1_1DatabaseTask.html#a7b57b7a64dc22b20e8e4bdb6028d25a1", null ],
    [ "signalDone", "classDigikam_1_1DatabaseTask.html#aa34d76e1cd1e839039f1b67add81920d", null ],
    [ "signalFinished", "classDigikam_1_1DatabaseTask.html#ae1de5f0853e2e4239c112bace294e27f", null ],
    [ "signalFinished", "classDigikam_1_1DatabaseTask.html#a83774562d5db896db7127cf5c5afe2a0", null ],
    [ "signalProgress", "classDigikam_1_1DatabaseTask.html#a736bc91a70d7892ae9fe8d1bd76b795e", null ],
    [ "signalStarted", "classDigikam_1_1DatabaseTask.html#afed71b5e01ec11f40bcb99c9f24b9048", null ],
    [ "m_cancel", "classDigikam_1_1DatabaseTask.html#a7b2f25a3878a3a2bcdde09ef10c4345e", null ]
];