var classDigikamGenericMetadataEditPlugin_1_1MultiStringsEdit =
[
    [ "MultiStringsEdit", "classDigikamGenericMetadataEditPlugin_1_1MultiStringsEdit.html#a713c604539576a014e038d65035d18c3", null ],
    [ "~MultiStringsEdit", "classDigikamGenericMetadataEditPlugin_1_1MultiStringsEdit.html#aa04daf4bd7a7d2411e02e463080697cf", null ],
    [ "getValues", "classDigikamGenericMetadataEditPlugin_1_1MultiStringsEdit.html#af2b68e614a8c30bb948d96c2218b8383", null ],
    [ "setValues", "classDigikamGenericMetadataEditPlugin_1_1MultiStringsEdit.html#aa3fa9b73915f444fe2b05678ee3f41b8", null ],
    [ "signalModified", "classDigikamGenericMetadataEditPlugin_1_1MultiStringsEdit.html#a0a04af219faa4560233bcdb5a24ce850", null ],
    [ "valueEdit", "classDigikamGenericMetadataEditPlugin_1_1MultiStringsEdit.html#ac7e545b6415473bea7d93dbbb885db12", null ]
];