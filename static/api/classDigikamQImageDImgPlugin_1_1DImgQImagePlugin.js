var classDigikamQImageDImgPlugin_1_1DImgQImagePlugin =
[
    [ "DImgQImagePlugin", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#a979d6db2bdce903e92020373d8c0f179", null ],
    [ "~DImgQImagePlugin", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#a375b71d8dfd0ee72137b22a1eaeecf09", null ],
    [ "authors", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#a9112a60ef450ec35c8f58290c5a6a3de", null ],
    [ "canRead", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#a9aa183f6067ca99424c7c6f412898571", null ],
    [ "canWrite", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#a1be5d72c5c44c70aa93d1fb46e76b969", null ],
    [ "categories", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#a9d4aa695c6b7f2ece3d716151bf8ffce", null ],
    [ "cleanUp", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#a586144ad9625ffa1f503ad74e341c639", null ],
    [ "count", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#ae117d5b70e52b5a0bbfde8f6edf752bc", null ],
    [ "description", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#a44d3d2aa0c60514a7e7e79fcb489717b", null ],
    [ "details", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#a71ab3602bd734db66e69fe952e7475e7", null ],
    [ "extraAboutData", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#a68e9602a084c8454a54f99b6c1892e23", null ],
    [ "extraAboutDataTitle", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#a7f051d6b86f3a102d64b3c686a4ad340", null ],
    [ "hasVisibilityProperty", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#ac65695cb5503241969125bb672d61f25", null ],
    [ "icon", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#aae5d70352b0f40537db5d132a56a8df0", null ],
    [ "ifaceIid", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#a0d5e052f75a658df3e868294a7074de7", null ],
    [ "iid", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#aa749e5f709ddd402f656bda9771d618d", null ],
    [ "libraryFileName", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#a149a7b768dc09157dc9cb2dc118d2285", null ],
    [ "loader", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#a9831546d33e54d8a340fd01f068d5e1e", null ],
    [ "loaderName", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#a2984e49ea3e1961866122a56bd6276b9", null ],
    [ "name", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#ad1a975ea8e3b6a885f9e01a1d01f5989", null ],
    [ "pluginAuthors", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#a98546b3aa26a43b6695a25643b73725e", null ],
    [ "previewSupported", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#a29fa82020cdad64e5f8f70236dde9ff6", null ],
    [ "setLibraryFileName", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#afde82d7d92a1d75dbae094a66c669057", null ],
    [ "setShouldLoaded", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#a27e50fb1f2756122f15cc289f7e02c7b", null ],
    [ "setup", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#a6b2274a704776c838a2ee2ad9da9cd17", null ],
    [ "setVisible", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#a9b221082072c61e54c5b5154b5caf29e", null ],
    [ "shouldLoaded", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#a0806aedffe128e70253d910c1dbf5690", null ],
    [ "typeMimes", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#adf587a98d7b1ce7d90a9a4da49d7530b", null ],
    [ "version", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html#a71a6f035204fb005960edf5d285884a9", null ]
];