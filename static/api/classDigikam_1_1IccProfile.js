var classDigikam_1_1IccProfile =
[
    [ "ProfileType", "classDigikam_1_1IccProfile.html#a32bb775defd3d7169273c8c02bd5b52b", [
      [ "InvalidType", "classDigikam_1_1IccProfile.html#a32bb775defd3d7169273c8c02bd5b52ba9fb1e108c178d9aa9d829af22e63b8e1", null ],
      [ "Input", "classDigikam_1_1IccProfile.html#a32bb775defd3d7169273c8c02bd5b52ba9ca72d660c7e4a2019ca2dd0cc5276a6", null ],
      [ "Output", "classDigikam_1_1IccProfile.html#a32bb775defd3d7169273c8c02bd5b52ba4ce6afb14ae9e560ab0ca0d3f4e5fda1", null ],
      [ "Display", "classDigikam_1_1IccProfile.html#a32bb775defd3d7169273c8c02bd5b52ba44db8b7b281dc93435884f5f196f7bb9", null ],
      [ "Abstract", "classDigikam_1_1IccProfile.html#a32bb775defd3d7169273c8c02bd5b52bae0f877e04039d4af221fba1fb310894b", null ],
      [ "ColorSpace", "classDigikam_1_1IccProfile.html#a32bb775defd3d7169273c8c02bd5b52bab5107e19d9db787b7bee4833dadb1796", null ],
      [ "DeviceLink", "classDigikam_1_1IccProfile.html#a32bb775defd3d7169273c8c02bd5b52ba103ad8a7dffbd3b26127b692a3f6abfa", null ],
      [ "NamedColor", "classDigikam_1_1IccProfile.html#a32bb775defd3d7169273c8c02bd5b52bae5f8140fdff96b961475f194196cc9f7", null ]
    ] ],
    [ "IccProfile", "classDigikam_1_1IccProfile.html#a9ee1af352261000bff701c0330e101d3", null ],
    [ "IccProfile", "classDigikam_1_1IccProfile.html#a3de62e198ac2d0625e69217e6ec91ec9", null ],
    [ "IccProfile", "classDigikam_1_1IccProfile.html#a171b59c5d47587ddd2e66152a5561738", null ],
    [ "IccProfile", "classDigikam_1_1IccProfile.html#aab6745882ce234c18c9db53748e61f59", null ],
    [ "~IccProfile", "classDigikam_1_1IccProfile.html#aa1e3283829f4286b5e21b13146c4fcd6", null ],
    [ "close", "classDigikam_1_1IccProfile.html#aedb41b9bba85d1e87e052cf6be218313", null ],
    [ "data", "classDigikam_1_1IccProfile.html#a2a0033c301bf9d81b892ab28301cc335", null ],
    [ "description", "classDigikam_1_1IccProfile.html#a51aa2e28b386ec6d51338235f1ce6c5b", null ],
    [ "filePath", "classDigikam_1_1IccProfile.html#aede1a091edf66c07b49f904e13caeeea", null ],
    [ "handle", "classDigikam_1_1IccProfile.html#a17ffde1be657e136100e678969bf9156", null ],
    [ "isNull", "classDigikam_1_1IccProfile.html#a40100103685c2da8f0a935e0bbc3b6e3", null ],
    [ "isOpen", "classDigikam_1_1IccProfile.html#ac4e8c863438e88aa686b74d6bb30144d", null ],
    [ "isSameProfileAs", "classDigikam_1_1IccProfile.html#a0c8a1b9d50c5d050f89218d63f0bfedd", null ],
    [ "open", "classDigikam_1_1IccProfile.html#ae2028401b2e23a3a7a61b7a5673a262d", null ],
    [ "operator void *", "classDigikam_1_1IccProfile.html#afb423dcafb1423bf871568eb53182619", null ],
    [ "operator!=", "classDigikam_1_1IccProfile.html#af0109148e11e144dcfc6fe9e595a23ab", null ],
    [ "operator=", "classDigikam_1_1IccProfile.html#aa39e6925fb707f8625c074f657f1f5bc", null ],
    [ "operator==", "classDigikam_1_1IccProfile.html#a0ca72bb410bd577717e70fee921d1f8b", null ],
    [ "type", "classDigikam_1_1IccProfile.html#a9a35e0e5cd6683a4f1be5d82daabc5b5", null ],
    [ "writeToFile", "classDigikam_1_1IccProfile.html#aaea33c96f8c00aac76bc47418409782a", null ]
];