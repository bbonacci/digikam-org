var classDigikam_1_1FaceScanWidget =
[
    [ "Private", "classDigikam_1_1FaceScanWidget_1_1Private.html", "classDigikam_1_1FaceScanWidget_1_1Private" ],
    [ "StateSavingDepth", "classDigikam_1_1FaceScanWidget.html#abdf8ad03070bc560242e56575909d6b6", [
      [ "INSTANCE", "classDigikam_1_1FaceScanWidget.html#abdf8ad03070bc560242e56575909d6b6a535f22ead553f373020dce094f0ae735", null ],
      [ "DIRECT_CHILDREN", "classDigikam_1_1FaceScanWidget.html#abdf8ad03070bc560242e56575909d6b6a402f3f077dcfc1835196020f1986fb55", null ],
      [ "RECURSIVE", "classDigikam_1_1FaceScanWidget.html#abdf8ad03070bc560242e56575909d6b6a1d31a55830067a26a4c25eabb48f6d4f", null ]
    ] ],
    [ "FaceScanWidget", "classDigikam_1_1FaceScanWidget.html#a0594311c4244f19299e19615d6523750", null ],
    [ "~FaceScanWidget", "classDigikam_1_1FaceScanWidget.html#aaa308189cf12ad1b25679f8af0601de7", null ],
    [ "doLoadState", "classDigikam_1_1FaceScanWidget.html#ab969b649c3fae088574c0de26944bcb9", null ],
    [ "doSaveState", "classDigikam_1_1FaceScanWidget.html#afc33c2bbaeea9f708a51bb77a5921fd6", null ],
    [ "entryName", "classDigikam_1_1FaceScanWidget.html#a226ce9b9faa055120ba386b85509bcdf", null ],
    [ "getConfigGroup", "classDigikam_1_1FaceScanWidget.html#a3dce4d65e29a07ea09d2e7b4746f8350", null ],
    [ "getStateSavingDepth", "classDigikam_1_1FaceScanWidget.html#a340090725416a1b9030c67d7d6a77d6e", null ],
    [ "loadState", "classDigikam_1_1FaceScanWidget.html#a884a7c5ec2c82c6b61519a6c035577c2", null ],
    [ "saveState", "classDigikam_1_1FaceScanWidget.html#a0db6b9c2daff1b521bce63f78d8f1652", null ],
    [ "setConfigGroup", "classDigikam_1_1FaceScanWidget.html#aa37c59ac6e91ba60c3c0dd14cc6fa71e", null ],
    [ "setEntryPrefix", "classDigikam_1_1FaceScanWidget.html#ac289161ea3144b78c348c283cbb7777d", null ],
    [ "setStateSavingDepth", "classDigikam_1_1FaceScanWidget.html#a1023eaabd6d22bcfcd83b0c7f5c47e9b", null ],
    [ "settings", "classDigikam_1_1FaceScanWidget.html#a0cfff619623263aa10ba35e51d079005", null ],
    [ "settingsConflicted", "classDigikam_1_1FaceScanWidget.html#ad1b6fc8f8c98df93d3f66d5e2f4ccff1", null ]
];