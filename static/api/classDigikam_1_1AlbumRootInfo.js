var classDigikam_1_1AlbumRootInfo =
[
    [ "AlbumRootInfo", "classDigikam_1_1AlbumRootInfo.html#ab94790821a6ee84d2b1411f954325d69", null ],
    [ "id", "classDigikam_1_1AlbumRootInfo.html#a9374367a52023c19a8ad8fb9af91a4af", null ],
    [ "identifier", "classDigikam_1_1AlbumRootInfo.html#a01c0513aa72b232f0f1c868097047fb7", null ],
    [ "label", "classDigikam_1_1AlbumRootInfo.html#a4b31547d76a7afb5ccb810804fb53e02", null ],
    [ "specificPath", "classDigikam_1_1AlbumRootInfo.html#ab846891879636447511960197bf19662", null ],
    [ "status", "classDigikam_1_1AlbumRootInfo.html#aba1f859d96cb354dd090dc77a0191b3c", null ],
    [ "type", "classDigikam_1_1AlbumRootInfo.html#a4610760ca32dc916bed4ed16b2b18942", null ]
];