var heif__plugin_8h =
[
    [ "heif_decoder_plugin", "structheif__decoder__plugin.html", "structheif__decoder__plugin" ],
    [ "heif_encoder_parameter", "structheif__encoder__parameter.html", "structheif__encoder__parameter" ],
    [ "heif_encoder_plugin", "structheif__encoder__plugin.html", "structheif__encoder__plugin" ],
    [ "heif_encoder_parameter_name_lossless", "heif__plugin_8h.html#abd7490354006015fcf1d87cdc5d7d606", null ],
    [ "heif_encoder_parameter_name_quality", "heif__plugin_8h.html#a2dd033395e00959a6929b42ce7a36091", null ],
    [ "heif_encoded_data_type", "heif__plugin_8h.html#a8cc25f345ee674dcdcf9654ac5a7bbd6", [
      [ "heif_encoded_data_type_HEVC_header", "heif__plugin_8h.html#a8cc25f345ee674dcdcf9654ac5a7bbd6a125686dc7ee6e61492c7cbd358546c32", null ],
      [ "heif_encoded_data_type_HEVC_image", "heif__plugin_8h.html#a8cc25f345ee674dcdcf9654ac5a7bbd6a6ac68a90da5e4f42324f668e4de49b6a", null ],
      [ "heif_encoded_data_type_HEVC_depth_SEI", "heif__plugin_8h.html#a8cc25f345ee674dcdcf9654ac5a7bbd6a87343a102bd24eeb2f61b04648e163e5", null ]
    ] ],
    [ "heif_image_input_class", "heif__plugin_8h.html#a04a1f0424555eaead92180f8124a7808", [
      [ "heif_image_input_class_normal", "heif__plugin_8h.html#a04a1f0424555eaead92180f8124a7808afc6b6807d33767711af0b70c34ff4471", null ],
      [ "heif_image_input_class_alpha", "heif__plugin_8h.html#a04a1f0424555eaead92180f8124a7808a922100b18c586ce7e225092e2777a770", null ],
      [ "heif_image_input_class_depth", "heif__plugin_8h.html#a04a1f0424555eaead92180f8124a7808afad605ad2b576c445adb28d90b693148", null ],
      [ "heif_image_input_class_thumbnail", "heif__plugin_8h.html#a04a1f0424555eaead92180f8124a7808a98951d7c54621d837c67ed7a5ce133a9", null ]
    ] ],
    [ "heif_error_invalid_parameter_value", "heif__plugin_8h.html#abfef3643eae7b0cde7609db186841289", null ],
    [ "heif_error_ok", "heif__plugin_8h.html#a69abab56cede4623f7348b4aa049086c", null ],
    [ "heif_error_unsupported_parameter", "heif__plugin_8h.html#a691faa3c2318a9ee7e6747c22f8da250", null ]
];