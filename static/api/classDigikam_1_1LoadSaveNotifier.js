var classDigikam_1_1LoadSaveNotifier =
[
    [ "LoadSaveNotifier", "classDigikam_1_1LoadSaveNotifier.html#a13857f526b7481eb03329d143a2d18a4", null ],
    [ "~LoadSaveNotifier", "classDigikam_1_1LoadSaveNotifier.html#a71f7e91bdfa6e74f1284918eb016ef04", null ],
    [ "imageLoaded", "classDigikam_1_1LoadSaveNotifier.html#aaa6b3e67905b806ad9069bceb2c3e1bd", null ],
    [ "imageSaved", "classDigikam_1_1LoadSaveNotifier.html#a44c6f948a5a9c739a17a39187a766218", null ],
    [ "imageStartedLoading", "classDigikam_1_1LoadSaveNotifier.html#aaf02b52c3c220c8e96e6f5a71079b352", null ],
    [ "imageStartedSaving", "classDigikam_1_1LoadSaveNotifier.html#a9090f47bbb374116b7a24005153f4c39", null ],
    [ "loadingProgress", "classDigikam_1_1LoadSaveNotifier.html#a54ad34a676740c43eb221c420234a00a", null ],
    [ "moreCompleteLoadingAvailable", "classDigikam_1_1LoadSaveNotifier.html#a36d013e98dec0d8b1ba329fecff603e4", null ],
    [ "savingProgress", "classDigikam_1_1LoadSaveNotifier.html#a181a7a23b3bac20edb61d464d9973ad9", null ],
    [ "thumbnailLoaded", "classDigikam_1_1LoadSaveNotifier.html#aacf933075b6117b7716406a86022978e", null ]
];