var dir_7283eb0263dbaa4b1d4aa8586af8f3c9 =
[
    [ "inatbrowserdlg.cpp", "inatbrowserdlg_8cpp.html", null ],
    [ "inatbrowserdlg.h", "inatbrowserdlg_8h.html", [
      [ "INatBrowserDlg", "classDigikamGenericINatPlugin_1_1INatBrowserDlg.html", "classDigikamGenericINatPlugin_1_1INatBrowserDlg" ]
    ] ],
    [ "inatplugin.cpp", "inatplugin_8cpp.html", null ],
    [ "inatplugin.h", "inatplugin_8h.html", "inatplugin_8h" ],
    [ "inatscore.cpp", "inatscore_8cpp.html", null ],
    [ "inatscore.h", "inatscore_8h.html", "inatscore_8h" ],
    [ "inatsuggest.cpp", "inatsuggest_8cpp.html", null ],
    [ "inatsuggest.h", "inatsuggest_8h.html", "inatsuggest_8h" ],
    [ "inattalker.cpp", "inattalker_8cpp.html", null ],
    [ "inattalker.h", "inattalker_8h.html", [
      [ "INatTalker", "classDigikamGenericINatPlugin_1_1INatTalker.html", "classDigikamGenericINatPlugin_1_1INatTalker" ],
      [ "NearbyObservation", "structDigikamGenericINatPlugin_1_1INatTalker_1_1NearbyObservation.html", "structDigikamGenericINatPlugin_1_1INatTalker_1_1NearbyObservation" ],
      [ "PhotoUploadRequest", "structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadRequest.html", "structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadRequest" ],
      [ "PhotoUploadResult", "structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadResult.html", "structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadResult" ]
    ] ],
    [ "inattaxon.cpp", "inattaxon_8cpp.html", null ],
    [ "inattaxon.h", "inattaxon_8h.html", [
      [ "Taxon", "classDigikamGenericINatPlugin_1_1Taxon.html", "classDigikamGenericINatPlugin_1_1Taxon" ]
    ] ],
    [ "inattaxonedit.cpp", "inattaxonedit_8cpp.html", null ],
    [ "inattaxonedit.h", "inattaxonedit_8h.html", [
      [ "TaxonEdit", "classDigikamGenericINatPlugin_1_1TaxonEdit.html", "classDigikamGenericINatPlugin_1_1TaxonEdit" ]
    ] ],
    [ "inatutils.cpp", "inatutils_8cpp.html", "inatutils_8cpp" ],
    [ "inatutils.h", "inatutils_8h.html", "inatutils_8h" ],
    [ "inatwidget.cpp", "inatwidget_8cpp.html", "inatwidget_8cpp" ],
    [ "inatwidget.h", "inatwidget_8h.html", [
      [ "INatWidget", "classDigikamGenericINatPlugin_1_1INatWidget.html", "classDigikamGenericINatPlugin_1_1INatWidget" ]
    ] ],
    [ "inatwidget_p.h", "inatwidget__p_8h.html", [
      [ "Private", "classDigikamGenericINatPlugin_1_1INatWidget_1_1Private.html", "classDigikamGenericINatPlugin_1_1INatWidget_1_1Private" ]
    ] ],
    [ "inatwindow.cpp", "inatwindow_8cpp.html", null ],
    [ "inatwindow.h", "inatwindow_8h.html", [
      [ "INatWindow", "classDigikamGenericINatPlugin_1_1INatWindow.html", "classDigikamGenericINatPlugin_1_1INatWindow" ]
    ] ]
];