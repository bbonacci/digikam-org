var dir_2f5e5d58764afb87aa754d16bab28eb1 =
[
    [ "dimagehistory.cpp", "dimagehistory_8cpp.html", null ],
    [ "dimagehistory.h", "dimagehistory_8h.html", [
      [ "DImageHistory", "classDigikam_1_1DImageHistory.html", "classDigikam_1_1DImageHistory" ],
      [ "Entry", "classDigikam_1_1DImageHistory_1_1Entry.html", "classDigikam_1_1DImageHistory_1_1Entry" ]
    ] ],
    [ "filteraction.cpp", "filteraction_8cpp.html", null ],
    [ "filteraction.h", "filteraction_8h.html", [
      [ "FilterAction", "classDigikam_1_1FilterAction.html", "classDigikam_1_1FilterAction" ]
    ] ],
    [ "historyimageid.cpp", "historyimageid_8cpp.html", null ],
    [ "historyimageid.h", "historyimageid_8h.html", [
      [ "HistoryImageId", "classDigikam_1_1HistoryImageId.html", "classDigikam_1_1HistoryImageId" ]
    ] ]
];