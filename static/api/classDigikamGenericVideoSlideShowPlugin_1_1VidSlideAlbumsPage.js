var classDigikamGenericVideoSlideShowPlugin_1_1VidSlideAlbumsPage =
[
    [ "VidSlideAlbumsPage", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideAlbumsPage.html#adbb24e4251a04c55a3f9f4ecd254c305", null ],
    [ "~VidSlideAlbumsPage", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideAlbumsPage.html#afbd4c28edab7f3a1498173952e2ee10f", null ],
    [ "assistant", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideAlbumsPage.html#a681293182dfaf2919429a4a5aa30dd04", null ],
    [ "id", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideAlbumsPage.html#a84aa8a0e37d7ae49efc832413764ffa1", null ],
    [ "isComplete", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideAlbumsPage.html#a05baa031e70c7617ebc87427356fe8a0", null ],
    [ "removePageWidget", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideAlbumsPage.html#a40cc4abca8695016a1d788f3df1cdd0d", null ],
    [ "setComplete", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideAlbumsPage.html#a9e97ef81648ea3f5e33c5a08ae5f0ff0", null ],
    [ "setLeftBottomPix", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideAlbumsPage.html#a04095257f4a957050e91cd4666f53316", null ],
    [ "setLeftBottomPix", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideAlbumsPage.html#a2e56503c5324f5551edb8fb1f1156530", null ],
    [ "setPageWidget", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideAlbumsPage.html#a2815f8463990300af07fe29cbd090308", null ],
    [ "setShowLeftView", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideAlbumsPage.html#a67975edf6041a574e674576a29d606a1", null ],
    [ "validatePage", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideAlbumsPage.html#acbffd8effe809cb27b67f29c5e4c2034", null ]
];