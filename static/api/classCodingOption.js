var classCodingOption =
[
    [ "CodingOption", "classCodingOption.html#a12e353a616464bdc5edbe5bccf335de6", null ],
    [ "begin", "classCodingOption.html#a01064081bc0c0b05e6fb932645b7bf21", null ],
    [ "end", "classCodingOption.html#a7e1b220da819aea4f93aa716528a1bb5", null ],
    [ "get_cabac", "classCodingOption.html#ac82b14d5b4ed0ab1721ff6420bcc3e18", null ],
    [ "get_cabac_rate", "classCodingOption.html#a900ea817ba809527bb261f4f674bb9d8", null ],
    [ "get_context", "classCodingOption.html#a228353f47facd3f1be2f57b13d913cdf", null ],
    [ "get_node", "classCodingOption.html#affe5259b37b5f8ac66f7ab656383ff7b", null ],
    [ "operator bool", "classCodingOption.html#a09f06df3434f95ff3a6fdb116d4d93fb", null ],
    [ "set_node", "classCodingOption.html#aa2a1aa80ce8a69c6ff82fbb3479cf2a4", null ],
    [ "set_rdo_cost", "classCodingOption.html#af96e32e5ef6188ed35af69722a95d668", null ],
    [ "CodingOptions< node >", "classCodingOption.html#a5921f82bbdfaf5068ff58bd3e86e70ce", null ]
];