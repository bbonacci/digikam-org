var dir_48a0acc72de186e6f0ca16537214e47a =
[
    [ "exifwidget.cpp", "exifwidget_8cpp.html", null ],
    [ "exifwidget.h", "exifwidget_8h.html", [
      [ "ExifWidget", "classDigikam_1_1ExifWidget.html", "classDigikam_1_1ExifWidget" ]
    ] ],
    [ "iptcwidget.cpp", "iptcwidget_8cpp.html", null ],
    [ "iptcwidget.h", "iptcwidget_8h.html", [
      [ "IptcWidget", "classDigikam_1_1IptcWidget.html", "classDigikam_1_1IptcWidget" ]
    ] ],
    [ "makernotewidget.cpp", "makernotewidget_8cpp.html", null ],
    [ "makernotewidget.h", "makernotewidget_8h.html", [
      [ "MakerNoteWidget", "classDigikam_1_1MakerNoteWidget.html", "classDigikam_1_1MakerNoteWidget" ]
    ] ],
    [ "mdkeylistviewitem.cpp", "mdkeylistviewitem_8cpp.html", null ],
    [ "mdkeylistviewitem.h", "mdkeylistviewitem_8h.html", [
      [ "MdKeyListViewItem", "classDigikam_1_1MdKeyListViewItem.html", "classDigikam_1_1MdKeyListViewItem" ]
    ] ],
    [ "metadatalistview.cpp", "metadatalistview_8cpp.html", null ],
    [ "metadatalistview.h", "metadatalistview_8h.html", [
      [ "MetadataListView", "classDigikam_1_1MetadataListView.html", "classDigikam_1_1MetadataListView" ]
    ] ],
    [ "metadatalistviewitem.cpp", "metadatalistviewitem_8cpp.html", null ],
    [ "metadatalistviewitem.h", "metadatalistviewitem_8h.html", [
      [ "MetadataListViewItem", "classDigikam_1_1MetadataListViewItem.html", "classDigikam_1_1MetadataListViewItem" ]
    ] ],
    [ "metadatawidget.cpp", "metadatawidget_8cpp.html", null ],
    [ "metadatawidget.h", "metadatawidget_8h.html", [
      [ "MetadataWidget", "classDigikam_1_1MetadataWidget.html", "classDigikam_1_1MetadataWidget" ]
    ] ],
    [ "xmpwidget.cpp", "xmpwidget_8cpp.html", null ],
    [ "xmpwidget.h", "xmpwidget_8h.html", [
      [ "XmpWidget", "classDigikam_1_1XmpWidget.html", "classDigikam_1_1XmpWidget" ]
    ] ]
];