var namespaceDigikamGenericTimeAdjustPlugin =
[
    [ "TimeAdjustDialog", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustDialog.html", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustDialog" ],
    [ "TimeAdjustList", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustList.html", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustList" ],
    [ "TimeAdjustPlugin", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustPlugin.html", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustPlugin" ],
    [ "TimeAdjustTask", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustTask.html", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustTask" ],
    [ "TimeAdjustThread", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread" ],
    [ "TimePreviewTask", "classDigikamGenericTimeAdjustPlugin_1_1TimePreviewTask.html", "classDigikamGenericTimeAdjustPlugin_1_1TimePreviewTask" ]
];