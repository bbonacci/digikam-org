var classDigikam_1_1LoadingDescription =
[
    [ "PostProcessingParameters", "classDigikam_1_1LoadingDescription_1_1PostProcessingParameters.html", "classDigikam_1_1LoadingDescription_1_1PostProcessingParameters" ],
    [ "PreviewParameters", "classDigikam_1_1LoadingDescription_1_1PreviewParameters.html", "classDigikam_1_1LoadingDescription_1_1PreviewParameters" ],
    [ "ColorManagementSettings", "classDigikam_1_1LoadingDescription.html#a81901ca9ee7af448927d26611b25abeb", [
      [ "NoColorConversion", "classDigikam_1_1LoadingDescription.html#a81901ca9ee7af448927d26611b25abebaba3d1c6c057e3d34860361f6c3d63804", null ],
      [ "ApplyTransform", "classDigikam_1_1LoadingDescription.html#a81901ca9ee7af448927d26611b25abebab3d00f74e7719c0ffb98199e9aec8832", null ],
      [ "ConvertForEditor", "classDigikam_1_1LoadingDescription.html#a81901ca9ee7af448927d26611b25abeba69847aebf62ab0177f6dc391f06dae5c", null ],
      [ "ConvertToSRGB", "classDigikam_1_1LoadingDescription.html#a81901ca9ee7af448927d26611b25abebadde5931f7b4a0fd55776c61e460c6f94", null ],
      [ "ConvertForDisplay", "classDigikam_1_1LoadingDescription.html#a81901ca9ee7af448927d26611b25abebaacd571b15cb8adf500ee03507603e4c5", null ],
      [ "ConvertForOutput", "classDigikam_1_1LoadingDescription.html#a81901ca9ee7af448927d26611b25abeba9fe8e37aaac874725cddad162df966e1", null ]
    ] ],
    [ "RawDecodingHint", "classDigikam_1_1LoadingDescription.html#a28f17d83d4862f442e3f83bb8ef6a4bd", [
      [ "RawDecodingDefaultSettings", "classDigikam_1_1LoadingDescription.html#a28f17d83d4862f442e3f83bb8ef6a4bda109dbece406a191a36d9a6d9a5ef023f", null ],
      [ "RawDecodingGlobalSettings", "classDigikam_1_1LoadingDescription.html#a28f17d83d4862f442e3f83bb8ef6a4bdab4bb32438473cc6be3c88e8865e600ed", null ],
      [ "RawDecodingCustomSettings", "classDigikam_1_1LoadingDescription.html#a28f17d83d4862f442e3f83bb8ef6a4bda2891c244aa2eebc2ce0e36f72a36b270", null ],
      [ "RawDecodingTimeOptimized", "classDigikam_1_1LoadingDescription.html#a28f17d83d4862f442e3f83bb8ef6a4bda21e581ffde96d88e127dda662b156d3d", null ]
    ] ],
    [ "LoadingDescription", "classDigikam_1_1LoadingDescription.html#ad296992801b851011e32c1064b86dda1", null ],
    [ "LoadingDescription", "classDigikam_1_1LoadingDescription.html#a0c5ed4d5a7fbb280681cc1099d91a17a", null ],
    [ "LoadingDescription", "classDigikam_1_1LoadingDescription.html#a3b0632c76ee95febbece121c456b74c5", null ],
    [ "LoadingDescription", "classDigikam_1_1LoadingDescription.html#a09b1cdeedad0bc61cbe031e36cd0eb41", null ],
    [ "cacheKey", "classDigikam_1_1LoadingDescription.html#aa684f1d2b25917c2e46fc7e1fb47568e", null ],
    [ "equalsIgnoreReducedVersion", "classDigikam_1_1LoadingDescription.html#a803d3b622db8540ef04fec7b6c4e4952", null ],
    [ "equalsOrBetterThan", "classDigikam_1_1LoadingDescription.html#a960d9730f9467c513047f167a5e5500e", null ],
    [ "isPreviewImage", "classDigikam_1_1LoadingDescription.html#ae5176ecd9509226f139e6643480e5120", null ],
    [ "isReducedVersion", "classDigikam_1_1LoadingDescription.html#a1248ff3e0d650705ffa06b2db855b877", null ],
    [ "isThumbnail", "classDigikam_1_1LoadingDescription.html#a093a88a7a42d7bff1d389337101f481b", null ],
    [ "lookupCacheKeys", "classDigikam_1_1LoadingDescription.html#a5f9b7f8619a55fa57a39ce6e66a77483", null ],
    [ "needCheckRawDecoding", "classDigikam_1_1LoadingDescription.html#a4fd33f00cd1ab07b33bfd1320bb43752", null ],
    [ "operator!=", "classDigikam_1_1LoadingDescription.html#a0e6e9a39600e3f4a2602a6481ce2d717", null ],
    [ "operator==", "classDigikam_1_1LoadingDescription.html#a4dfd05e7725aeee0719f6fcad7529594", null ],
    [ "thumbnailIdentifier", "classDigikam_1_1LoadingDescription.html#a9e6aa94846f81b750baa5d6cb7c524da", null ],
    [ "filePath", "classDigikam_1_1LoadingDescription.html#a1389e4b77eeb65173ea7535d596d2252", null ],
    [ "postProcessingParameters", "classDigikam_1_1LoadingDescription.html#a45f092ca7346395bdff3c7ac9a144de6", null ],
    [ "previewParameters", "classDigikam_1_1LoadingDescription.html#acdc4238553eb1eb5d6a5a7ce71316f51", null ],
    [ "rawDecodingHint", "classDigikam_1_1LoadingDescription.html#a9585292e57607715fa7b08c066d3d96c", null ],
    [ "rawDecodingSettings", "classDigikam_1_1LoadingDescription.html#aceb9d46650b03a5f170f2d94f93bab0b", null ]
];