var dir_a8bd6122b03f9d2a2cd96651339a87ac =
[
    [ "album", "dir_ed26fe0bb44d45ccb5664f2574eec32a.html", "dir_ed26fe0bb44d45ccb5664f2574eec32a" ],
    [ "camera", "dir_2a269d9d4545fe4fc34efee5910273ae.html", "dir_2a269d9d4545fe4fc34efee5910273ae" ],
    [ "collections", "dir_abd7eef6879d410766debba7191e7852.html", "dir_abd7eef6879d410766debba7191e7852" ],
    [ "downloader", "dir_e0dee8de3ccc37d5e30d507bad34fcae.html", "dir_e0dee8de3ccc37d5e30d507bad34fcae" ],
    [ "editor", "dir_3a5e3fc9ed6716a4419641047ac85a81.html", "dir_3a5e3fc9ed6716a4419641047ac85a81" ],
    [ "metadata", "dir_0ac36df7177f6a00d051fd7dcc1e45db.html", "dir_0ac36df7177f6a00d051fd7dcc1e45db" ],
    [ "system", "dir_6199426c87f7498015d8743bc6d90494.html", "dir_6199426c87f7498015d8743bc6d90494" ],
    [ "setup.cpp", "setup_8cpp.html", null ],
    [ "setup.h", "setup_8h.html", [
      [ "Setup", "classDigikam_1_1Setup.html", "classDigikam_1_1Setup" ]
    ] ],
    [ "setupdatabase.cpp", "setupdatabase_8cpp.html", null ],
    [ "setupdatabase.h", "setupdatabase_8h.html", [
      [ "SetupDatabase", "classDigikam_1_1SetupDatabase.html", "classDigikam_1_1SetupDatabase" ]
    ] ],
    [ "setupicc.cpp", "setupicc_8cpp.html", null ],
    [ "setupicc.h", "setupicc_8h.html", [
      [ "SetupICC", "classDigikam_1_1SetupICC.html", "classDigikam_1_1SetupICC" ]
    ] ],
    [ "setupimagequalitysorter.cpp", "setupimagequalitysorter_8cpp.html", null ],
    [ "setupimagequalitysorter.h", "setupimagequalitysorter_8h.html", [
      [ "SetupImageQualitySorter", "classDigikam_1_1SetupImageQualitySorter.html", "classDigikam_1_1SetupImageQualitySorter" ]
    ] ],
    [ "setuplighttable.cpp", "setuplighttable_8cpp.html", null ],
    [ "setuplighttable.h", "setuplighttable_8h.html", [
      [ "SetupLightTable", "classDigikam_1_1SetupLightTable.html", "classDigikam_1_1SetupLightTable" ]
    ] ],
    [ "setupmisc.cpp", "setupmisc_8cpp.html", null ],
    [ "setupmisc.h", "setupmisc_8h.html", [
      [ "SetupMisc", "classDigikam_1_1SetupMisc.html", "classDigikam_1_1SetupMisc" ]
    ] ],
    [ "setupplugins.cpp", "setupplugins_8cpp.html", null ],
    [ "setupplugins.h", "setupplugins_8h.html", [
      [ "SetupPlugins", "classDigikam_1_1SetupPlugins.html", "classDigikam_1_1SetupPlugins" ]
    ] ],
    [ "setuptemplate.cpp", "setuptemplate_8cpp.html", null ],
    [ "setuptemplate.h", "setuptemplate_8h.html", [
      [ "SetupTemplate", "classDigikam_1_1SetupTemplate.html", "classDigikam_1_1SetupTemplate" ]
    ] ],
    [ "setuptooltip.cpp", "setuptooltip_8cpp.html", null ],
    [ "setuptooltip.h", "setuptooltip_8h.html", [
      [ "SetupToolTip", "classDigikam_1_1SetupToolTip.html", "classDigikam_1_1SetupToolTip" ]
    ] ],
    [ "setuputils.cpp", "setuputils_8cpp.html", "setuputils_8cpp" ],
    [ "setuputils.h", "setuputils_8h.html", "setuputils_8h" ]
];