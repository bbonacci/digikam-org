var classDigikamGenericMetadataEditPlugin_1_1IPTCCredits =
[
    [ "IPTCCredits", "classDigikamGenericMetadataEditPlugin_1_1IPTCCredits.html#a49defd5a491eab4ec5f3c66fef68a328", null ],
    [ "~IPTCCredits", "classDigikamGenericMetadataEditPlugin_1_1IPTCCredits.html#a79222705a6f2a621fbdf815088d274fb", null ],
    [ "applyMetadata", "classDigikamGenericMetadataEditPlugin_1_1IPTCCredits.html#a5ec5c65c5d97316ab721ce740eb672ad", null ],
    [ "readMetadata", "classDigikamGenericMetadataEditPlugin_1_1IPTCCredits.html#ad4c64c37a5e5c157b52315eb52941a9b", null ],
    [ "signalModified", "classDigikamGenericMetadataEditPlugin_1_1IPTCCredits.html#acee57f8a5d0eb86c394bb200886b9fb5", null ]
];