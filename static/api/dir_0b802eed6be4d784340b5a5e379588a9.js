var dir_0b802eed6be4d784340b5a5e379588a9 =
[
    [ "facetags.cpp", "facetags_8cpp.html", null ],
    [ "facetags.h", "facetags_8h.html", [
      [ "FaceTags", "classDigikam_1_1FaceTags.html", null ]
    ] ],
    [ "facetagseditor.cpp", "facetagseditor_8cpp.html", null ],
    [ "facetagseditor.h", "facetagseditor_8h.html", [
      [ "FaceTagsEditor", "classDigikam_1_1FaceTagsEditor.html", "classDigikam_1_1FaceTagsEditor" ]
    ] ],
    [ "facetagsiface.cpp", "facetagsiface_8cpp.html", "facetagsiface_8cpp" ],
    [ "facetagsiface.h", "facetagsiface_8h.html", "facetagsiface_8h" ],
    [ "tagproperties.cpp", "tagproperties_8cpp.html", "tagproperties_8cpp" ],
    [ "tagproperties.h", "tagproperties_8h.html", [
      [ "TagProperties", "classDigikam_1_1TagProperties.html", "classDigikam_1_1TagProperties" ]
    ] ],
    [ "tagregion.cpp", "tagregion_8cpp.html", "tagregion_8cpp" ],
    [ "tagregion.h", "tagregion_8h.html", "tagregion_8h" ],
    [ "tagscache.cpp", "tagscache_8cpp.html", "tagscache_8cpp" ],
    [ "tagscache.h", "tagscache_8h.html", [
      [ "TagsCache", "classDigikam_1_1TagsCache.html", "classDigikam_1_1TagsCache" ]
    ] ]
];