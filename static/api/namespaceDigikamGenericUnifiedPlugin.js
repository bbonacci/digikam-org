var namespaceDigikamGenericUnifiedPlugin =
[
    [ "UnifiedPlugin", "classDigikamGenericUnifiedPlugin_1_1UnifiedPlugin.html", "classDigikamGenericUnifiedPlugin_1_1UnifiedPlugin" ],
    [ "WSAlbumsPage", "classDigikamGenericUnifiedPlugin_1_1WSAlbumsPage.html", "classDigikamGenericUnifiedPlugin_1_1WSAlbumsPage" ],
    [ "WSAuthentication", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication" ],
    [ "WSAuthenticationPage", "classDigikamGenericUnifiedPlugin_1_1WSAuthenticationPage.html", "classDigikamGenericUnifiedPlugin_1_1WSAuthenticationPage" ],
    [ "WSAuthenticationPageView", "classDigikamGenericUnifiedPlugin_1_1WSAuthenticationPageView.html", "classDigikamGenericUnifiedPlugin_1_1WSAuthenticationPageView" ],
    [ "WSAuthenticationWizard", "classDigikamGenericUnifiedPlugin_1_1WSAuthenticationWizard.html", "classDigikamGenericUnifiedPlugin_1_1WSAuthenticationWizard" ],
    [ "WSFinalPage", "classDigikamGenericUnifiedPlugin_1_1WSFinalPage.html", "classDigikamGenericUnifiedPlugin_1_1WSFinalPage" ],
    [ "WSImagesPage", "classDigikamGenericUnifiedPlugin_1_1WSImagesPage.html", "classDigikamGenericUnifiedPlugin_1_1WSImagesPage" ],
    [ "WSIntroPage", "classDigikamGenericUnifiedPlugin_1_1WSIntroPage.html", "classDigikamGenericUnifiedPlugin_1_1WSIntroPage" ],
    [ "WSSettingsPage", "classDigikamGenericUnifiedPlugin_1_1WSSettingsPage.html", "classDigikamGenericUnifiedPlugin_1_1WSSettingsPage" ],
    [ "WSTalker", "classDigikamGenericUnifiedPlugin_1_1WSTalker.html", "classDigikamGenericUnifiedPlugin_1_1WSTalker" ],
    [ "WSWizard", "classDigikamGenericUnifiedPlugin_1_1WSWizard.html", "classDigikamGenericUnifiedPlugin_1_1WSWizard" ]
];