var classDigikamGenericIpfsPlugin_1_1IpfsTalker =
[
    [ "IpfsTalker", "classDigikamGenericIpfsPlugin_1_1IpfsTalker.html#a5ad3a1db6614b0b9ddc342726c9466f0", null ],
    [ "~IpfsTalker", "classDigikamGenericIpfsPlugin_1_1IpfsTalker.html#a6baa77d69492f03ec2a7789432a1b87b", null ],
    [ "busy", "classDigikamGenericIpfsPlugin_1_1IpfsTalker.html#ad758a9b88263a4016855ad3970e67a3b", null ],
    [ "cancelAllWork", "classDigikamGenericIpfsPlugin_1_1IpfsTalker.html#ab79a93a5f422e89a613cb5d02b98fdee", null ],
    [ "error", "classDigikamGenericIpfsPlugin_1_1IpfsTalker.html#afce77c0a24424a90b94949cf2e821d49", null ],
    [ "progress", "classDigikamGenericIpfsPlugin_1_1IpfsTalker.html#a147f4c47dad541cca7eb1550ecb58c88", null ],
    [ "queueWork", "classDigikamGenericIpfsPlugin_1_1IpfsTalker.html#a4aecd1e824fffea40839b4230a6e4787", null ],
    [ "replyFinished", "classDigikamGenericIpfsPlugin_1_1IpfsTalker.html#a7ea5d90ea33ebcec50b89dc448634e97", null ],
    [ "success", "classDigikamGenericIpfsPlugin_1_1IpfsTalker.html#a4a14fc59bac4d1d81b61a2ae3074308c", null ],
    [ "timerEvent", "classDigikamGenericIpfsPlugin_1_1IpfsTalker.html#a6c7f823d0ec6825aaa2518e94c30e6da", null ],
    [ "uploadProgress", "classDigikamGenericIpfsPlugin_1_1IpfsTalker.html#a66c32df03c8ce2dd631f57b68afda657", null ],
    [ "workQueueLength", "classDigikamGenericIpfsPlugin_1_1IpfsTalker.html#a1d92063a1e6eb5d9baf1db0753bfe4a9", null ]
];