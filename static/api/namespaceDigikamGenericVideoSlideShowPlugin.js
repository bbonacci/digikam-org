var namespaceDigikamGenericVideoSlideShowPlugin =
[
    [ "VideoSlideShowPlugin", "classDigikamGenericVideoSlideShowPlugin_1_1VideoSlideShowPlugin.html", "classDigikamGenericVideoSlideShowPlugin_1_1VideoSlideShowPlugin" ],
    [ "VidSlideAlbumsPage", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideAlbumsPage.html", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideAlbumsPage" ],
    [ "VidSlideFinalPage", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideFinalPage.html", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideFinalPage" ],
    [ "VidSlideImagesPage", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideImagesPage.html", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideImagesPage" ],
    [ "VidSlideIntroPage", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideIntroPage.html", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideIntroPage" ],
    [ "VidSlideOutputPage", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideOutputPage.html", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideOutputPage" ],
    [ "VidSlideVideoPage", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideVideoPage.html", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideVideoPage" ],
    [ "VidSlideWizard", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideWizard.html", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideWizard" ]
];