var dir_5d2b890dc7b6594b2e7b75a1caa82770 =
[
    [ "htmlwidget_qwebengine.cpp", "htmlwidget__qwebengine_8cpp.html", null ],
    [ "htmlwidget_qwebengine.h", "htmlwidget__qwebengine_8h.html", [
      [ "HTMLWidget", "classDigikam_1_1HTMLWidget.html", "classDigikam_1_1HTMLWidget" ],
      [ "HTMLWidgetPage", "classDigikam_1_1HTMLWidgetPage.html", "classDigikam_1_1HTMLWidgetPage" ]
    ] ],
    [ "htmlwidget_qwebkit.cpp", "htmlwidget__qwebkit_8cpp.html", null ],
    [ "htmlwidget_qwebkit.h", "htmlwidget__qwebkit_8h.html", [
      [ "HTMLWidget", "classDigikam_1_1HTMLWidget.html", "classDigikam_1_1HTMLWidget" ]
    ] ],
    [ "mapwidget.cpp", "mapwidget_8cpp.html", null ],
    [ "mapwidget.h", "mapwidget_8h.html", [
      [ "MapWidget", "classDigikam_1_1MapWidget.html", "classDigikam_1_1MapWidget" ]
    ] ],
    [ "placeholderwidget.cpp", "placeholderwidget_8cpp.html", null ],
    [ "placeholderwidget.h", "placeholderwidget_8h.html", [
      [ "PlaceholderWidget", "classDigikam_1_1PlaceholderWidget.html", "classDigikam_1_1PlaceholderWidget" ]
    ] ]
];