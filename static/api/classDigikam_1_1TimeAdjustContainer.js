var classDigikam_1_1TimeAdjustContainer =
[
    [ "AdjType", "classDigikam_1_1TimeAdjustContainer.html#afabcca3c147155f4bbccafab33b24f7e", [
      [ "COPYVALUE", "classDigikam_1_1TimeAdjustContainer.html#afabcca3c147155f4bbccafab33b24f7ea3f0b30aa9c3b5f6b9929c545755daf1b", null ],
      [ "ADDVALUE", "classDigikam_1_1TimeAdjustContainer.html#afabcca3c147155f4bbccafab33b24f7ea50cdec79fa8c45ba9aa63c20c13c18be", null ],
      [ "SUBVALUE", "classDigikam_1_1TimeAdjustContainer.html#afabcca3c147155f4bbccafab33b24f7eaa4971f565ce2fdb8ec3a296313593653", null ],
      [ "INTERVAL", "classDigikam_1_1TimeAdjustContainer.html#afabcca3c147155f4bbccafab33b24f7eac7a88d580aaf71de3c5692f4dd3c5994", null ]
    ] ],
    [ "UseDateSource", "classDigikam_1_1TimeAdjustContainer.html#a8c2bf57f4c68a992c34679eea9e9d499", [
      [ "APPDATE", "classDigikam_1_1TimeAdjustContainer.html#a8c2bf57f4c68a992c34679eea9e9d499a6039205a4f5ef227e416ba5f168dbde7", null ],
      [ "FILENAME", "classDigikam_1_1TimeAdjustContainer.html#a8c2bf57f4c68a992c34679eea9e9d499a39364b7ef5251358655d833a7f19c059", null ],
      [ "FILEDATE", "classDigikam_1_1TimeAdjustContainer.html#a8c2bf57f4c68a992c34679eea9e9d499a189d9879fba17c950f9dd6727aa9884a", null ],
      [ "METADATADATE", "classDigikam_1_1TimeAdjustContainer.html#a8c2bf57f4c68a992c34679eea9e9d499ad2638c047f6dd4db6141df79ea2a482e", null ],
      [ "CUSTOMDATE", "classDigikam_1_1TimeAdjustContainer.html#a8c2bf57f4c68a992c34679eea9e9d499a4b6d6b0167ab041dcd0c3cb7375abeb0", null ]
    ] ],
    [ "UseFileDateType", "classDigikam_1_1TimeAdjustContainer.html#a287def56780f73c26a9187ca340ecbe2", [
      [ "FILELASTMOD", "classDigikam_1_1TimeAdjustContainer.html#a287def56780f73c26a9187ca340ecbe2a25dc8603e8b7c0a4fd0ca880cf924a29", null ],
      [ "FILECREATED", "classDigikam_1_1TimeAdjustContainer.html#a287def56780f73c26a9187ca340ecbe2a2ae75b764a933361ec83c074ff6438d2", null ]
    ] ],
    [ "UseMetaDateType", "classDigikam_1_1TimeAdjustContainer.html#a73ae77924a46a2bddda3d335c82b2278", [
      [ "EXIFIPTCXMP", "classDigikam_1_1TimeAdjustContainer.html#a73ae77924a46a2bddda3d335c82b2278a404b3fe864f80990a97d59fefdfed7d2", null ],
      [ "EXIFCREATED", "classDigikam_1_1TimeAdjustContainer.html#a73ae77924a46a2bddda3d335c82b2278af62d8906465fb3b219a816b8fd55ecf4", null ],
      [ "EXIFORIGINAL", "classDigikam_1_1TimeAdjustContainer.html#a73ae77924a46a2bddda3d335c82b2278ab685d2ad7d5ca888888a55d0d8e79edc", null ],
      [ "EXIFDIGITIZED", "classDigikam_1_1TimeAdjustContainer.html#a73ae77924a46a2bddda3d335c82b2278ada859de51fb7bc9980f527fed506a20a", null ],
      [ "IPTCCREATED", "classDigikam_1_1TimeAdjustContainer.html#a73ae77924a46a2bddda3d335c82b2278ac1457444154c914ccc4036c0d236a405", null ],
      [ "XMPCREATED", "classDigikam_1_1TimeAdjustContainer.html#a73ae77924a46a2bddda3d335c82b2278a2ca7296612f13063921addda00ec92e0", null ]
    ] ],
    [ "TimeAdjustContainer", "classDigikam_1_1TimeAdjustContainer.html#a8c394d8b534e1b71ee87574fdf889b1b", null ],
    [ "~TimeAdjustContainer", "classDigikam_1_1TimeAdjustContainer.html#a31afc6f203f7a480b84d2eb3c1d69966", null ],
    [ "atLeastOneUpdateToProcess", "classDigikam_1_1TimeAdjustContainer.html#a8f44cd8cf2e9cfa9a8ef57bed67371a7", null ],
    [ "calculateAdjustedDate", "classDigikam_1_1TimeAdjustContainer.html#aba90605f1048acc30536afda7ef26875", null ],
    [ "getDateTimeFromUrl", "classDigikam_1_1TimeAdjustContainer.html#aa6fe51a09ee1764da2b823b49ffdd817", null ],
    [ "adjustmentDays", "classDigikam_1_1TimeAdjustContainer.html#a78d28c7aaf9e8dd38a4dc71a2d732c34", null ],
    [ "adjustmentTime", "classDigikam_1_1TimeAdjustContainer.html#a2453aa9c326e771e5fa27c522cd7c42b", null ],
    [ "adjustmentType", "classDigikam_1_1TimeAdjustContainer.html#a8d0c76a32abed5e68ed82fd75e4964f5", null ],
    [ "customDate", "classDigikam_1_1TimeAdjustContainer.html#a11cc91819aa615b2f66af503a17bad05", null ],
    [ "customTime", "classDigikam_1_1TimeAdjustContainer.html#a684fb5e6bcb8de2ede247982081e620d", null ],
    [ "dateSource", "classDigikam_1_1TimeAdjustContainer.html#af4947f9cc974b3432ea99a1ac34d0622", null ],
    [ "fileDateSource", "classDigikam_1_1TimeAdjustContainer.html#ae993596619739367116fe0929cfe054c", null ],
    [ "metadataSource", "classDigikam_1_1TimeAdjustContainer.html#a53738486fef139221fc08a87366729b7", null ],
    [ "updEXIFDigDate", "classDigikam_1_1TimeAdjustContainer.html#adc315cdb2fdf8b1ba132879e70283ae4", null ],
    [ "updEXIFModDate", "classDigikam_1_1TimeAdjustContainer.html#a99b06896aae50df1fd1c12e8d84ed506", null ],
    [ "updEXIFOriDate", "classDigikam_1_1TimeAdjustContainer.html#acf9de48796d47745c54f324d32fec300", null ],
    [ "updEXIFThmDate", "classDigikam_1_1TimeAdjustContainer.html#a850c3d76ace707194301865ec4a67a73", null ],
    [ "updFileModDate", "classDigikam_1_1TimeAdjustContainer.html#a5e8108aba61d82d25436345144db61a8", null ],
    [ "updIfAvailable", "classDigikam_1_1TimeAdjustContainer.html#a87a9377529b8bb848187ec00383500a6", null ],
    [ "updIPTCDate", "classDigikam_1_1TimeAdjustContainer.html#a65ecc473b1c269bd75fc223d4461e120", null ],
    [ "updXMPDate", "classDigikam_1_1TimeAdjustContainer.html#a879103668ad048ac46ebea2de4ad18f8", null ],
    [ "updXMPVideo", "classDigikam_1_1TimeAdjustContainer.html#a2fbc2001949d7c6e5af5027cc549f93d", null ]
];