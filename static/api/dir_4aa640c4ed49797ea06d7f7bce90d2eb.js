var dir_4aa640c4ed49797ea06d7f7bce90d2eb =
[
    [ "drawdecoder.cpp", "drawdecoder_8cpp.html", null ],
    [ "drawdecoder.h", "drawdecoder_8h.html", [
      [ "DRawDecoder", "classDigikam_1_1DRawDecoder.html", "classDigikam_1_1DRawDecoder" ]
    ] ],
    [ "drawdecoder_p.cpp", "drawdecoder__p_8cpp.html", "drawdecoder__p_8cpp" ],
    [ "drawdecoder_p.h", "drawdecoder__p_8h.html", "drawdecoder__p_8h" ],
    [ "drawdecodersettings.cpp", "drawdecodersettings_8cpp.html", "drawdecodersettings_8cpp" ],
    [ "drawdecodersettings.h", "drawdecodersettings_8h.html", "drawdecodersettings_8h" ],
    [ "drawdecoderwidget.cpp", "drawdecoderwidget_8cpp.html", "drawdecoderwidget_8cpp" ],
    [ "drawdecoderwidget.h", "drawdecoderwidget_8h.html", [
      [ "DRawDecoderWidget", "classDigikam_1_1DRawDecoderWidget.html", "classDigikam_1_1DRawDecoderWidget" ]
    ] ],
    [ "drawfiles.cpp", "drawfiles_8cpp.html", "drawfiles_8cpp" ],
    [ "drawfiles.h", "drawfiles_8h.html", "drawfiles_8h" ],
    [ "drawinfo.cpp", "drawinfo_8cpp.html", "drawinfo_8cpp" ],
    [ "drawinfo.h", "drawinfo_8h.html", "drawinfo_8h" ]
];