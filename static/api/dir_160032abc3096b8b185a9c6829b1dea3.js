var dir_160032abc3096b8b185a9c6829b1dea3 =
[
    [ "audio", "dir_e38047d7c7da44dd3ff7414967cf6fe0.html", "dir_e38047d7c7da44dd3ff7414967cf6fe0" ],
    [ "common", "dir_dcb1fd0fb92ac342cf204dd6719de61a.html", "dir_dcb1fd0fb92ac342cf204dd6719de61a" ],
    [ "dialogs", "dir_ca80a7bd7fc1cc7bd8c57574fd22c913.html", "dir_ca80a7bd7fc1cc7bd8c57574fd22c913" ],
    [ "opengl", "dir_49f59ef4b301d55ed5bfdd776d7c9433.html", "dir_49f59ef4b301d55ed5bfdd776d7c9433" ],
    [ "widgets", "dir_c01633ba32d027dcc7f1e37b6f827096.html", "dir_c01633ba32d027dcc7f1e37b6f827096" ],
    [ "presentationmngr.cpp", "presentationmngr_8cpp.html", null ],
    [ "presentationmngr.h", "presentationmngr_8h.html", [
      [ "PresentationMngr", "classDigikamGenericPresentationPlugin_1_1PresentationMngr.html", "classDigikamGenericPresentationPlugin_1_1PresentationMngr" ]
    ] ],
    [ "presentationplugin.cpp", "presentationplugin_8cpp.html", null ],
    [ "presentationplugin.h", "presentationplugin_8h.html", "presentationplugin_8h" ]
];