var classDigikam_1_1SaveProperties =
[
    [ "SaveProperties", "classDigikam_1_1SaveProperties.html#abf30027819259d4b84d0aab8bea0b0c0", null ],
    [ "altitude", "classDigikam_1_1SaveProperties.html#a2f6ccef39278505c8d551d8097a1dc0d", null ],
    [ "latitude", "classDigikam_1_1SaveProperties.html#ad59c6b099e62ccbbf9cc1556beb804a4", null ],
    [ "longitude", "classDigikam_1_1SaveProperties.html#a82145504210d65ca87a8066b4d646e07", null ],
    [ "shouldRemoveAltitude", "classDigikam_1_1SaveProperties.html#ad12381841b8c7da781ee7213734ab19b", null ],
    [ "shouldRemoveCoordinates", "classDigikam_1_1SaveProperties.html#a984095ffc3fdeb78c2813ea206585ba4", null ],
    [ "shouldWriteAltitude", "classDigikam_1_1SaveProperties.html#a0e588f56ff159abfba784d17106238ca", null ],
    [ "shouldWriteCoordinates", "classDigikam_1_1SaveProperties.html#ae169a366c21cc9de8a283b07cae5895f", null ]
];