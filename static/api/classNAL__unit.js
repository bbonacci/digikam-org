var classNAL__unit =
[
    [ "NAL_unit", "classNAL__unit.html#afa0b5ac2198b31ca86841fbe890fe01a", null ],
    [ "~NAL_unit", "classNAL__unit.html#a9be726825718c43977c8168557573c7e", null ],
    [ "append", "classNAL__unit.html#a5676fb4a6ed39c7d72bd27963425ba5e", null ],
    [ "clear", "classNAL__unit.html#a07d4e805f0aa59d3d3819844162be539", null ],
    [ "data", "classNAL__unit.html#a79842c2d2795be09ed10cedc5d6706ae", null ],
    [ "data", "classNAL__unit.html#a1a2902b153b3dff589baa4f7cbfc899a", null ],
    [ "insert_skipped_byte", "classNAL__unit.html#acc3b8fc5893f04fd8ec00ec7121273fd", null ],
    [ "num_skipped_bytes", "classNAL__unit.html#a956ba14f2ccab749439c7715f24abf81", null ],
    [ "num_skipped_bytes_before", "classNAL__unit.html#ad64f151509539826cb3459aae0045ffd", null ],
    [ "remove_stuffing_bytes", "classNAL__unit.html#ae24f896f1cd4722d7a1993bf70e1e112", null ],
    [ "resize", "classNAL__unit.html#a7e9c6662509e769c1fe7b5a94e0153aa", null ],
    [ "set_data", "classNAL__unit.html#a8e1b0eb35d1c18c6ed19be01ba65630e", null ],
    [ "set_size", "classNAL__unit.html#af61eef990e15183f2c867ff38fdab5c2", null ],
    [ "size", "classNAL__unit.html#a3ed5c11ffe25cb409a2dfdc4e9299d85", null ],
    [ "header", "classNAL__unit.html#ab2242b9150d9140f66329dcdc77bfefc", null ],
    [ "pts", "classNAL__unit.html#abc84e6d16c823e081e4de95c3792a22e", null ],
    [ "user_data", "classNAL__unit.html#adcb149aaaa136e21105425ba051a00dc", null ]
];