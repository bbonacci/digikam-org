var dplugin_8h =
[
    [ "DPlugin", "classDigikam_1_1DPlugin.html", "classDigikam_1_1DPlugin" ],
    [ "DIGIKAM_DPLUGIN_BQM_IID", "dplugin_8h.html#a08ab2ae00a247b7776497273cc2e28fd", null ],
    [ "DIGIKAM_DPLUGIN_DIMG_IID", "dplugin_8h.html#a60936dfceeadf6fe4bb227f9ff0eccd4", null ],
    [ "DIGIKAM_DPLUGIN_EDITOR_IID", "dplugin_8h.html#a039aa2d3a3e3697689fd0fd37ccc2764", null ],
    [ "DIGIKAM_DPLUGIN_GENERIC_IID", "dplugin_8h.html#a4ef7404c95e06f79c9957ccac54232c9", null ],
    [ "DIGIKAM_DPLUGIN_RAWIMPORT_IID", "dplugin_8h.html#a259abcc12f69df9ee0af954b74ba0e14", null ],
    [ "Q_DECLARE_TYPEINFO", "dplugin_8h.html#ab8f0a09953c14d05eb638d10c6bc4a08", null ]
];