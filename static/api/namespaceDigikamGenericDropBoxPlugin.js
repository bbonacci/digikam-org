var namespaceDigikamGenericDropBoxPlugin =
[
    [ "DBFolder", "classDigikamGenericDropBoxPlugin_1_1DBFolder.html", "classDigikamGenericDropBoxPlugin_1_1DBFolder" ],
    [ "DBMPForm", "classDigikamGenericDropBoxPlugin_1_1DBMPForm.html", "classDigikamGenericDropBoxPlugin_1_1DBMPForm" ],
    [ "DBNewAlbumDlg", "classDigikamGenericDropBoxPlugin_1_1DBNewAlbumDlg.html", "classDigikamGenericDropBoxPlugin_1_1DBNewAlbumDlg" ],
    [ "DBPhoto", "classDigikamGenericDropBoxPlugin_1_1DBPhoto.html", "classDigikamGenericDropBoxPlugin_1_1DBPhoto" ],
    [ "DBPlugin", "classDigikamGenericDropBoxPlugin_1_1DBPlugin.html", "classDigikamGenericDropBoxPlugin_1_1DBPlugin" ],
    [ "DBTalker", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html", "classDigikamGenericDropBoxPlugin_1_1DBTalker" ],
    [ "DBWidget", "classDigikamGenericDropBoxPlugin_1_1DBWidget.html", "classDigikamGenericDropBoxPlugin_1_1DBWidget" ],
    [ "DBWindow", "classDigikamGenericDropBoxPlugin_1_1DBWindow.html", "classDigikamGenericDropBoxPlugin_1_1DBWindow" ]
];