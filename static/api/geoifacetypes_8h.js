var geoifacetypes_8h =
[
    [ "QIntList", "geoifacetypes_8h.html#a8a9cb74cc4ca9ba80e183bd0cdfb16c7", null ],
    [ "GeoExtraAction", "geoifacetypes_8h.html#a8971aac0d05c55ccb67b2c4d65439fa2", [
      [ "ExtraActionSticky", "geoifacetypes_8h.html#a8971aac0d05c55ccb67b2c4d65439fa2a5c1351815eb9246dc90b75fae3120641", null ]
    ] ],
    [ "GeoMouseMode", "geoifacetypes_8h.html#a96da3b44cd6c57508b7ae09f105fe014", [
      [ "MouseModePan", "geoifacetypes_8h.html#a96da3b44cd6c57508b7ae09f105fe014af017d0791e5a3b92a5d6057d17c356f3", null ],
      [ "MouseModeRegionSelection", "geoifacetypes_8h.html#a96da3b44cd6c57508b7ae09f105fe014aa7ae2e51f96e224b331456707281b58d", null ],
      [ "MouseModeRegionSelectionFromIcon", "geoifacetypes_8h.html#a96da3b44cd6c57508b7ae09f105fe014a0779e47f2d455e08e67694cb1d0c1aba", null ],
      [ "MouseModeFilter", "geoifacetypes_8h.html#a96da3b44cd6c57508b7ae09f105fe014a15e6d9df94920e9d7d5eeb94404ead3a", null ],
      [ "MouseModeSelectThumbnail", "geoifacetypes_8h.html#a96da3b44cd6c57508b7ae09f105fe014a41a4ef4e6810bf05a17925ab95fa729e", null ],
      [ "MouseModeZoomIntoGroup", "geoifacetypes_8h.html#a96da3b44cd6c57508b7ae09f105fe014adcf54aaa06ab88c8acadbd8eddd2e824", null ],
      [ "MouseModeLast", "geoifacetypes_8h.html#a96da3b44cd6c57508b7ae09f105fe014a50d50cd86592b409d22b0e083cabab64", null ]
    ] ]
];