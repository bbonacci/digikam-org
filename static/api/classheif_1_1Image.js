var classheif_1_1Image =
[
    [ "ScalingOptions", "classheif_1_1Image_1_1ScalingOptions.html", null ],
    [ "Image", "classheif_1_1Image.html#a9e4aec3e34a3b19775e8a3c3dd376b0d", null ],
    [ "Image", "classheif_1_1Image.html#a3caecd4e4ac229150e271cc7a0b4bf74", null ],
    [ "add_plane", "classheif_1_1Image.html#a22eb4f7ded904180a4cbee885c7d77db", null ],
    [ "create", "classheif_1_1Image.html#a4ebcd10bba014a45e3a07a99649cac2b", null ],
    [ "get_bits_per_pixel", "classheif_1_1Image.html#ad1bcbc10a5716580616c58e8a186a3f4", null ],
    [ "get_chroma_format", "classheif_1_1Image.html#aaaa7ac877c35442ecb7e4fcc04dc8b40", null ],
    [ "get_colorspace", "classheif_1_1Image.html#ab7be552efc2861c7b80f5ebc661bbcf0", null ],
    [ "get_height", "classheif_1_1Image.html#a1274e8f0487639517a3844c4aceaa6f9", null ],
    [ "get_plane", "classheif_1_1Image.html#a9c167d725d9b905cf1a3d4028ad23a16", null ],
    [ "get_plane", "classheif_1_1Image.html#a2e07c92530dbf15ef4f3bd1a733ea2cb", null ],
    [ "get_width", "classheif_1_1Image.html#aca0d19c6f4f6cf7a1e5e80f720bf319e", null ],
    [ "has_channel", "classheif_1_1Image.html#a18f817193f2f433b2c2c9cc556ba3cfa", null ],
    [ "scale_image", "classheif_1_1Image.html#ad7234de6d586991d0b9a9af69211b87d", null ],
    [ "Context", "classheif_1_1Image.html#ac26c806e60ca4a0547680edb68f6e39b", null ]
];