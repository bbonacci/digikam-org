var thumbsdb_8h =
[
    [ "ThumbsDb", "classDigikam_1_1ThumbsDb.html", "classDigikam_1_1ThumbsDb" ],
    [ "ThumbsDbInfo", "classDigikam_1_1ThumbsDbInfo.html", "classDigikam_1_1ThumbsDbInfo" ],
    [ "Type", "thumbsdb_8h.html#ac2a59d7ead8d312339686a08a916e533", [
      [ "UndefinedType", "thumbsdb_8h.html#ac2a59d7ead8d312339686a08a916e533a9aca6ca2d8c8bb7b85dfb785fa45b7f1", null ],
      [ "NoThumbnail", "thumbsdb_8h.html#ac2a59d7ead8d312339686a08a916e533ad9f8fe0ae524388da5fa9d95d06ce87b", null ],
      [ "PGF", "thumbsdb_8h.html#ac2a59d7ead8d312339686a08a916e533a0d53b7c204cc9d3a1a3f33dd07dfaaaf", null ],
      [ "JPEG", "thumbsdb_8h.html#ac2a59d7ead8d312339686a08a916e533a0193d43ed2ccf9b5c96fd20b9f5d447e", null ],
      [ "JPEG2000", "thumbsdb_8h.html#ac2a59d7ead8d312339686a08a916e533acd2e0faec304186c1553b08b0158db4b", null ],
      [ "PNG", "thumbsdb_8h.html#ac2a59d7ead8d312339686a08a916e533a06807ecdecb4f53f90e7f7cd8fe4fc4b", null ]
    ] ]
];