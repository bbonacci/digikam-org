var classDigikam_1_1FiltersHistoryWidget =
[
    [ "FiltersHistoryWidget", "classDigikam_1_1FiltersHistoryWidget.html#a5bd3778a92eee8b945b83ab1011aef9d", null ],
    [ "~FiltersHistoryWidget", "classDigikam_1_1FiltersHistoryWidget.html#aefebb055a696ae310dc99d3b3c66929d", null ],
    [ "clearData", "classDigikam_1_1FiltersHistoryWidget.html#ae69446fb39206fd6d332b1760fc47430", null ],
    [ "disableEntries", "classDigikam_1_1FiltersHistoryWidget.html#a44bd50b5217691800acb46fef15d0e21", null ],
    [ "enableEntries", "classDigikam_1_1FiltersHistoryWidget.html#a92617a4c9f8236e2698893eb9220ffc2", null ],
    [ "setCurrentURL", "classDigikam_1_1FiltersHistoryWidget.html#a7dc3e4d347b228bc0135bcd46701ee93", null ],
    [ "setEnabledEntries", "classDigikam_1_1FiltersHistoryWidget.html#ad1152b478bad3beca01775153dc5455f", null ],
    [ "setHistory", "classDigikam_1_1FiltersHistoryWidget.html#a07da626435d99b9cfea6e569fcea7809", null ],
    [ "showCustomContextMenu", "classDigikam_1_1FiltersHistoryWidget.html#a09836f0217e0be4daa5da1f83c230670", null ]
];