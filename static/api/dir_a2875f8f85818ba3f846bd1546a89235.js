var dir_a2875f8f85818ba3f846bd1546a89235 =
[
    [ "assignnamewidget.cpp", "assignnamewidget_8cpp.html", null ],
    [ "assignnamewidget.h", "assignnamewidget_8h.html", [
      [ "AssignNameWidget", "classDigikam_1_1AssignNameWidget.html", "classDigikam_1_1AssignNameWidget" ]
    ] ],
    [ "assignnamewidget_p.cpp", "assignnamewidget__p_8cpp.html", null ],
    [ "assignnamewidget_p.h", "assignnamewidget__p_8h.html", [
      [ "Private", "classDigikam_1_1AssignNameWidget_1_1Private.html", "classDigikam_1_1AssignNameWidget_1_1Private" ]
    ] ],
    [ "assignnamewidgetstates.cpp", "assignnamewidgetstates_8cpp.html", null ],
    [ "assignnamewidgetstates.h", "assignnamewidgetstates_8h.html", [
      [ "AssignNameWidgetStates", "classDigikam_1_1AssignNameWidgetStates.html", "classDigikam_1_1AssignNameWidgetStates" ]
    ] ],
    [ "facemanagementhelpdlg.cpp", "facemanagementhelpdlg_8cpp.html", null ],
    [ "facemanagementhelpdlg.h", "facemanagementhelpdlg_8h.html", [
      [ "FaceManagementHelpDlg", "classDigikam_1_1FaceManagementHelpDlg.html", "classDigikam_1_1FaceManagementHelpDlg" ]
    ] ],
    [ "facescanwidget.cpp", "facescanwidget_8cpp.html", null ],
    [ "facescanwidget.h", "facescanwidget_8h.html", [
      [ "FaceScanWidget", "classDigikam_1_1FaceScanWidget.html", "classDigikam_1_1FaceScanWidget" ]
    ] ],
    [ "facescanwidget_p.h", "facescanwidget__p_8h.html", [
      [ "Private", "classDigikam_1_1FaceScanWidget_1_1Private.html", "classDigikam_1_1FaceScanWidget_1_1Private" ]
    ] ]
];