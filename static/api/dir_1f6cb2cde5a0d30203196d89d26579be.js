var dir_1f6cb2cde5a0d30203196d89d26579be =
[
    [ "smugitem.h", "smugitem_8h.html", [
      [ "SmugAlbum", "classDigikamGenericSmugPlugin_1_1SmugAlbum.html", "classDigikamGenericSmugPlugin_1_1SmugAlbum" ],
      [ "SmugAlbumTmpl", "classDigikamGenericSmugPlugin_1_1SmugAlbumTmpl.html", "classDigikamGenericSmugPlugin_1_1SmugAlbumTmpl" ],
      [ "SmugCategory", "classDigikamGenericSmugPlugin_1_1SmugCategory.html", "classDigikamGenericSmugPlugin_1_1SmugCategory" ],
      [ "SmugPhoto", "classDigikamGenericSmugPlugin_1_1SmugPhoto.html", "classDigikamGenericSmugPlugin_1_1SmugPhoto" ],
      [ "SmugUser", "classDigikamGenericSmugPlugin_1_1SmugUser.html", "classDigikamGenericSmugPlugin_1_1SmugUser" ]
    ] ],
    [ "smugmpform.cpp", "smugmpform_8cpp.html", null ],
    [ "smugmpform.h", "smugmpform_8h.html", [
      [ "SmugMPForm", "classDigikamGenericSmugPlugin_1_1SmugMPForm.html", "classDigikamGenericSmugPlugin_1_1SmugMPForm" ]
    ] ],
    [ "smugnewalbumdlg.cpp", "smugnewalbumdlg_8cpp.html", null ],
    [ "smugnewalbumdlg.h", "smugnewalbumdlg_8h.html", [
      [ "SmugNewAlbumDlg", "classDigikamGenericSmugPlugin_1_1SmugNewAlbumDlg.html", "classDigikamGenericSmugPlugin_1_1SmugNewAlbumDlg" ]
    ] ],
    [ "smugplugin.cpp", "smugplugin_8cpp.html", null ],
    [ "smugplugin.h", "smugplugin_8h.html", "smugplugin_8h" ],
    [ "smugtalker.cpp", "smugtalker_8cpp.html", null ],
    [ "smugtalker.h", "smugtalker_8h.html", [
      [ "SmugTalker", "classDigikamGenericSmugPlugin_1_1SmugTalker.html", "classDigikamGenericSmugPlugin_1_1SmugTalker" ]
    ] ],
    [ "smugwidget.cpp", "smugwidget_8cpp.html", null ],
    [ "smugwidget.h", "smugwidget_8h.html", [
      [ "SmugWidget", "classDigikamGenericSmugPlugin_1_1SmugWidget.html", "classDigikamGenericSmugPlugin_1_1SmugWidget" ]
    ] ],
    [ "smugwindow.cpp", "smugwindow_8cpp.html", null ],
    [ "smugwindow.h", "smugwindow_8h.html", [
      [ "SmugWindow", "classDigikamGenericSmugPlugin_1_1SmugWindow.html", "classDigikamGenericSmugPlugin_1_1SmugWindow" ]
    ] ]
];