var dir_0da513334ea3403daea584c13d503d14 =
[
    [ "filtershistorywidget.cpp", "filtershistorywidget_8cpp.html", null ],
    [ "filtershistorywidget.h", "filtershistorywidget_8h.html", [
      [ "FiltersHistoryWidget", "classDigikam_1_1FiltersHistoryWidget.html", "classDigikam_1_1FiltersHistoryWidget" ],
      [ "RemoveFilterAction", "classDigikam_1_1RemoveFilterAction.html", "classDigikam_1_1RemoveFilterAction" ]
    ] ],
    [ "itemfiltershistoryitemdelegate.cpp", "itemfiltershistoryitemdelegate_8cpp.html", null ],
    [ "itemfiltershistoryitemdelegate.h", "itemfiltershistoryitemdelegate_8h.html", [
      [ "ItemFiltersHistoryItemDelegate", "classDigikam_1_1ItemFiltersHistoryItemDelegate.html", "classDigikam_1_1ItemFiltersHistoryItemDelegate" ]
    ] ],
    [ "itemfiltershistorymodel.cpp", "itemfiltershistorymodel_8cpp.html", null ],
    [ "itemfiltershistorymodel.h", "itemfiltershistorymodel_8h.html", [
      [ "ItemFiltersHistoryModel", "classDigikam_1_1ItemFiltersHistoryModel.html", "classDigikam_1_1ItemFiltersHistoryModel" ]
    ] ],
    [ "itemfiltershistorytreeitem.cpp", "itemfiltershistorytreeitem_8cpp.html", null ],
    [ "itemfiltershistorytreeitem.h", "itemfiltershistorytreeitem_8h.html", [
      [ "ItemFiltersHistoryTreeItem", "classDigikam_1_1ItemFiltersHistoryTreeItem.html", "classDigikam_1_1ItemFiltersHistoryTreeItem" ]
    ] ],
    [ "versionswidget.cpp", "versionswidget_8cpp.html", null ],
    [ "versionswidget.h", "versionswidget_8h.html", [
      [ "VersionsWidget", "classDigikam_1_1VersionsWidget.html", "classDigikam_1_1VersionsWidget" ]
    ] ]
];