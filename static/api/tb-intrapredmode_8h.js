var tb_intrapredmode_8h =
[
    [ "Algo_TB_IntraPredMode", "classAlgo__TB__IntraPredMode.html", "classAlgo__TB__IntraPredMode" ],
    [ "Algo_TB_IntraPredMode_BruteForce", "classAlgo__TB__IntraPredMode__BruteForce.html", "classAlgo__TB__IntraPredMode__BruteForce" ],
    [ "Algo_TB_IntraPredMode_FastBrute", "classAlgo__TB__IntraPredMode__FastBrute.html", "classAlgo__TB__IntraPredMode__FastBrute" ],
    [ "params", "structAlgo__TB__IntraPredMode__FastBrute_1_1params.html", "structAlgo__TB__IntraPredMode__FastBrute_1_1params" ],
    [ "Algo_TB_IntraPredMode_MinResidual", "classAlgo__TB__IntraPredMode__MinResidual.html", "classAlgo__TB__IntraPredMode__MinResidual" ],
    [ "params", "structAlgo__TB__IntraPredMode__MinResidual_1_1params.html", "structAlgo__TB__IntraPredMode__MinResidual_1_1params" ],
    [ "Algo_TB_IntraPredMode_ModeSubset", "classAlgo__TB__IntraPredMode__ModeSubset.html", "classAlgo__TB__IntraPredMode__ModeSubset" ],
    [ "option_ALGO_TB_IntraPredMode", "classoption__ALGO__TB__IntraPredMode.html", "classoption__ALGO__TB__IntraPredMode" ],
    [ "option_ALGO_TB_IntraPredMode_Subset", "classoption__ALGO__TB__IntraPredMode__Subset.html", "classoption__ALGO__TB__IntraPredMode__Subset" ],
    [ "option_TBBitrateEstimMethod", "classoption__TBBitrateEstimMethod.html", "classoption__TBBitrateEstimMethod" ],
    [ "ALGO_TB_IntraPredMode", "tb-intrapredmode_8h.html#ab32ee079a9bce803780d5c5466f421bc", [
      [ "ALGO_TB_IntraPredMode_BruteForce", "tb-intrapredmode_8h.html#ab32ee079a9bce803780d5c5466f421bca6122f83126c23a2f1cc85483ef1c007a", null ],
      [ "ALGO_TB_IntraPredMode_FastBrute", "tb-intrapredmode_8h.html#ab32ee079a9bce803780d5c5466f421bca9c4e32b14ba205261b73cd94f0a7a85f", null ],
      [ "ALGO_TB_IntraPredMode_MinResidual", "tb-intrapredmode_8h.html#ab32ee079a9bce803780d5c5466f421bcabfe2a95a515770677508b4387aea3838", null ]
    ] ],
    [ "ALGO_TB_IntraPredMode_Subset", "tb-intrapredmode_8h.html#afad9a9a1e3c9b349bc9d9b89e3c5d190", [
      [ "ALGO_TB_IntraPredMode_Subset_All", "tb-intrapredmode_8h.html#afad9a9a1e3c9b349bc9d9b89e3c5d190ab8e18a8b236c8865912436a1097d7e15", null ],
      [ "ALGO_TB_IntraPredMode_Subset_HVPlus", "tb-intrapredmode_8h.html#afad9a9a1e3c9b349bc9d9b89e3c5d190a0ec465c68bea87eb1fcf7fee0e8249ab", null ],
      [ "ALGO_TB_IntraPredMode_Subset_DC", "tb-intrapredmode_8h.html#afad9a9a1e3c9b349bc9d9b89e3c5d190a1cbb0e0632870928dcf723b7e30c1465", null ],
      [ "ALGO_TB_IntraPredMode_Subset_Planar", "tb-intrapredmode_8h.html#afad9a9a1e3c9b349bc9d9b89e3c5d190a2d300e8aa3a8c0371a61374a42eccd4d", null ]
    ] ],
    [ "TBBitrateEstimMethod", "tb-intrapredmode_8h.html#a7788684612503870ab79f1458503f6e4", [
      [ "TBBitrateEstim_SSD", "tb-intrapredmode_8h.html#a7788684612503870ab79f1458503f6e4acb3d64c59faea52e37e745d2f33da840", null ],
      [ "TBBitrateEstim_SAD", "tb-intrapredmode_8h.html#a7788684612503870ab79f1458503f6e4ade635b09844f93fa2216ca1b8042649c", null ],
      [ "TBBitrateEstim_SATD_DCT", "tb-intrapredmode_8h.html#a7788684612503870ab79f1458503f6e4a0dc8f2e7b8a19c99f73951e83e810ccf", null ],
      [ "TBBitrateEstim_SATD_Hadamard", "tb-intrapredmode_8h.html#a7788684612503870ab79f1458503f6e4abc59e126a1e82a3fe39fc6c0217536cb", null ]
    ] ]
];