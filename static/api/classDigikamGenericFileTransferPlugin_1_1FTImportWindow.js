var classDigikamGenericFileTransferPlugin_1_1FTImportWindow =
[
    [ "FTImportWindow", "classDigikamGenericFileTransferPlugin_1_1FTImportWindow.html#ac6056bd2119c9de862aa46dc59323819", null ],
    [ "~FTImportWindow", "classDigikamGenericFileTransferPlugin_1_1FTImportWindow.html#aa9a8026f27d3351f669c39628cb20433", null ],
    [ "addButton", "classDigikamGenericFileTransferPlugin_1_1FTImportWindow.html#a338ad1ca1d39489c83775e3286302c59", null ],
    [ "cancelClicked", "classDigikamGenericFileTransferPlugin_1_1FTImportWindow.html#a6f9919249e2e8aa237fabb2ed72e75a2", null ],
    [ "restoreDialogSize", "classDigikamGenericFileTransferPlugin_1_1FTImportWindow.html#a5da66f5eb64468e9524230138e09c2a2", null ],
    [ "saveDialogSize", "classDigikamGenericFileTransferPlugin_1_1FTImportWindow.html#a5496bc5fd80e57edd35e652dbb8361ae", null ],
    [ "setMainWidget", "classDigikamGenericFileTransferPlugin_1_1FTImportWindow.html#ae956aceb6ae24079791e2f491957d446", null ],
    [ "setPlugin", "classDigikamGenericFileTransferPlugin_1_1FTImportWindow.html#a8f9473a19990a8001d98cde6286d1059", null ],
    [ "setRejectButtonMode", "classDigikamGenericFileTransferPlugin_1_1FTImportWindow.html#a32667efc4f6a02e644886729952fd354", null ],
    [ "startButton", "classDigikamGenericFileTransferPlugin_1_1FTImportWindow.html#a34e7c6deb1458c37066862375a487600", null ],
    [ "m_buttons", "classDigikamGenericFileTransferPlugin_1_1FTImportWindow.html#a06170b9f45856493f30cb0edd368a3c0", null ]
];