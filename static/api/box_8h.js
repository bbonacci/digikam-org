var box_8h =
[
    [ "Box", "classheif_1_1Box.html", "classheif_1_1Box" ],
    [ "Box_auxC", "classheif_1_1Box__auxC.html", "classheif_1_1Box__auxC" ],
    [ "Box_clap", "classheif_1_1Box__clap.html", "classheif_1_1Box__clap" ],
    [ "Box_colr", "classheif_1_1Box__colr.html", "classheif_1_1Box__colr" ],
    [ "Box_dinf", "classheif_1_1Box__dinf.html", "classheif_1_1Box__dinf" ],
    [ "Box_dref", "classheif_1_1Box__dref.html", "classheif_1_1Box__dref" ],
    [ "Box_ftyp", "classheif_1_1Box__ftyp.html", "classheif_1_1Box__ftyp" ],
    [ "Box_grpl", "classheif_1_1Box__grpl.html", "classheif_1_1Box__grpl" ],
    [ "EntityGroup", "structheif_1_1Box__grpl_1_1EntityGroup.html", "structheif_1_1Box__grpl_1_1EntityGroup" ],
    [ "Box_hdlr", "classheif_1_1Box__hdlr.html", "classheif_1_1Box__hdlr" ],
    [ "Box_hvcC", "classheif_1_1Box__hvcC.html", "classheif_1_1Box__hvcC" ],
    [ "configuration", "structheif_1_1Box__hvcC_1_1configuration.html", "structheif_1_1Box__hvcC_1_1configuration" ],
    [ "Box_idat", "classheif_1_1Box__idat.html", "classheif_1_1Box__idat" ],
    [ "Box_iinf", "classheif_1_1Box__iinf.html", "classheif_1_1Box__iinf" ],
    [ "Box_iloc", "classheif_1_1Box__iloc.html", "classheif_1_1Box__iloc" ],
    [ "Extent", "structheif_1_1Box__iloc_1_1Extent.html", "structheif_1_1Box__iloc_1_1Extent" ],
    [ "Item", "structheif_1_1Box__iloc_1_1Item.html", "structheif_1_1Box__iloc_1_1Item" ],
    [ "Box_imir", "classheif_1_1Box__imir.html", "classheif_1_1Box__imir" ],
    [ "Box_infe", "classheif_1_1Box__infe.html", "classheif_1_1Box__infe" ],
    [ "Box_ipco", "classheif_1_1Box__ipco.html", "classheif_1_1Box__ipco" ],
    [ "Property", "structheif_1_1Box__ipco_1_1Property.html", "structheif_1_1Box__ipco_1_1Property" ],
    [ "Box_ipma", "classheif_1_1Box__ipma.html", "classheif_1_1Box__ipma" ],
    [ "Entry", "structheif_1_1Box__ipma_1_1Entry.html", "structheif_1_1Box__ipma_1_1Entry" ],
    [ "PropertyAssociation", "structheif_1_1Box__ipma_1_1PropertyAssociation.html", "structheif_1_1Box__ipma_1_1PropertyAssociation" ],
    [ "Box_iprp", "classheif_1_1Box__iprp.html", "classheif_1_1Box__iprp" ],
    [ "Box_iref", "classheif_1_1Box__iref.html", "classheif_1_1Box__iref" ],
    [ "Reference", "structheif_1_1Box__iref_1_1Reference.html", "structheif_1_1Box__iref_1_1Reference" ],
    [ "Box_irot", "classheif_1_1Box__irot.html", "classheif_1_1Box__irot" ],
    [ "Box_ispe", "classheif_1_1Box__ispe.html", "classheif_1_1Box__ispe" ],
    [ "Box_meta", "classheif_1_1Box__meta.html", "classheif_1_1Box__meta" ],
    [ "Box_pitm", "classheif_1_1Box__pitm.html", "classheif_1_1Box__pitm" ],
    [ "Box_pixi", "classheif_1_1Box__pixi.html", "classheif_1_1Box__pixi" ],
    [ "Box_url", "classheif_1_1Box__url.html", "classheif_1_1Box__url" ],
    [ "BoxHeader", "classheif_1_1BoxHeader.html", "classheif_1_1BoxHeader" ],
    [ "color_profile", "classheif_1_1color__profile.html", "classheif_1_1color__profile" ],
    [ "color_profile_nclx", "classheif_1_1color__profile__nclx.html", "classheif_1_1color__profile__nclx" ],
    [ "color_profile_raw", "classheif_1_1color__profile__raw.html", "classheif_1_1color__profile__raw" ],
    [ "Fraction", "classheif_1_1Fraction.html", "classheif_1_1Fraction" ],
    [ "fourcc", "box_8h.html#afb131ceeb6fa0b1e34bb9a5f738bd7ea", null ],
    [ "HAS_BOOL_ARRAY", "box_8h.html#ad76465503af1a67204dbaddc8c7320f2", null ]
];