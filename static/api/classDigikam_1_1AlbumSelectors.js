var classDigikam_1_1AlbumSelectors =
[
    [ "AlbumType", "classDigikam_1_1AlbumSelectors.html#a4f96775d9092edab0f77978409c9afb7", [
      [ "PhysAlbum", "classDigikam_1_1AlbumSelectors.html#a4f96775d9092edab0f77978409c9afb7a54bc59d8d19d8ad7dcaa3bdcf7f7cb58", null ],
      [ "TagsAlbum", "classDigikam_1_1AlbumSelectors.html#a4f96775d9092edab0f77978409c9afb7aeefdedf9411029cb80d6a6583afc4f80", null ],
      [ "All", "classDigikam_1_1AlbumSelectors.html#a4f96775d9092edab0f77978409c9afb7aa887d1ca6cee74627982c618882bf9ea", null ]
    ] ],
    [ "SelectionType", "classDigikam_1_1AlbumSelectors.html#aafbd857ed998016cb2123e2a69ee4721", [
      [ "SingleSelection", "classDigikam_1_1AlbumSelectors.html#aafbd857ed998016cb2123e2a69ee4721a2df9e2e423495c20ba5abfa5cb0ef110", null ],
      [ "MultipleSelection", "classDigikam_1_1AlbumSelectors.html#aafbd857ed998016cb2123e2a69ee4721a4c3967e82389ccf46656d990fe9a3024", null ]
    ] ],
    [ "AlbumSelectors", "classDigikam_1_1AlbumSelectors.html#a8ce1f8574c146aedf0765991dbba5e17", null ],
    [ "~AlbumSelectors", "classDigikam_1_1AlbumSelectors.html#a06ac2ccb31d0503ea716359b7b518aca", null ],
    [ "loadState", "classDigikam_1_1AlbumSelectors.html#ae2b442d63c91974f8425c72ef294e02e", null ],
    [ "resetPAlbumSelection", "classDigikam_1_1AlbumSelectors.html#a4af904aa27f4f53a4074dcb1632cb834", null ],
    [ "resetSelection", "classDigikam_1_1AlbumSelectors.html#aafbf2fc383a4efd561825095eaffaaa5", null ],
    [ "resetTAlbumSelection", "classDigikam_1_1AlbumSelectors.html#adbb3ebeb4b01a2c045355e1942eb1e94", null ],
    [ "saveState", "classDigikam_1_1AlbumSelectors.html#a0d2233eaf9b56285fda338acfbce6af1", null ],
    [ "selectedAlbumIds", "classDigikam_1_1AlbumSelectors.html#a3493be8216497e9eede23492b86e3b4f", null ],
    [ "selectedAlbums", "classDigikam_1_1AlbumSelectors.html#a713601b92f15823094fd59f9907ccd97", null ],
    [ "selectedAlbumsAndTags", "classDigikam_1_1AlbumSelectors.html#aecfff66758948e66b413749f77acc416", null ],
    [ "selectedTagIds", "classDigikam_1_1AlbumSelectors.html#aeb9439848bfbefbd6488263cf73e7f3d", null ],
    [ "selectedTags", "classDigikam_1_1AlbumSelectors.html#aff55b23443fe07116069aa8d47f83b8c", null ],
    [ "setAlbumSelected", "classDigikam_1_1AlbumSelectors.html#a1d5f226a135716cdddd5cee86945fa8f", null ],
    [ "setTagSelected", "classDigikam_1_1AlbumSelectors.html#a3d18a80e7ded8881b3248875c143d160", null ],
    [ "setTypeSelection", "classDigikam_1_1AlbumSelectors.html#a526b03b0d6bf748c4899702be31cde6a", null ],
    [ "signalSelectionChanged", "classDigikam_1_1AlbumSelectors.html#ad907d381c758aadf60e7e2d51ffd4b14", null ],
    [ "typeSelection", "classDigikam_1_1AlbumSelectors.html#a82a551ab534d88c54c6b58657ee7aec2", null ],
    [ "wholeAlbumsChecked", "classDigikam_1_1AlbumSelectors.html#a3626aa44c40261c48f0a8f7e6dd9b3f7", null ],
    [ "wholeTagsChecked", "classDigikam_1_1AlbumSelectors.html#a3405676eca7161c91e52afa1588091f9", null ]
];