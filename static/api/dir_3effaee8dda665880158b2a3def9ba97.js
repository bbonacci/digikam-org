var dir_3effaee8dda665880158b2a3def9ba97 =
[
    [ "showfotosetup.cpp", "showfotosetup_8cpp.html", null ],
    [ "showfotosetup.h", "showfotosetup_8h.html", [
      [ "ShowfotoSetup", "classShowFoto_1_1ShowfotoSetup.html", "classShowFoto_1_1ShowfotoSetup" ]
    ] ],
    [ "showfotosetupmetadata.cpp", "showfotosetupmetadata_8cpp.html", null ],
    [ "showfotosetupmetadata.h", "showfotosetupmetadata_8h.html", [
      [ "ShowfotoSetupMetadata", "classShowFoto_1_1ShowfotoSetupMetadata.html", "classShowFoto_1_1ShowfotoSetupMetadata" ]
    ] ],
    [ "showfotosetupmisc.cpp", "showfotosetupmisc_8cpp.html", null ],
    [ "showfotosetupmisc.h", "showfotosetupmisc_8h.html", [
      [ "ShowfotoSetupMisc", "classShowFoto_1_1ShowfotoSetupMisc.html", "classShowFoto_1_1ShowfotoSetupMisc" ]
    ] ],
    [ "showfotosetupplugins.cpp", "showfotosetupplugins_8cpp.html", null ],
    [ "showfotosetupplugins.h", "showfotosetupplugins_8h.html", [
      [ "ShowfotoSetupPlugins", "classShowFoto_1_1ShowfotoSetupPlugins.html", "classShowFoto_1_1ShowfotoSetupPlugins" ]
    ] ],
    [ "showfotosetupraw.cpp", "showfotosetupraw_8cpp.html", null ],
    [ "showfotosetupraw.h", "showfotosetupraw_8h.html", [
      [ "ShowfotoSetupRaw", "classShowFoto_1_1ShowfotoSetupRaw.html", "classShowFoto_1_1ShowfotoSetupRaw" ]
    ] ],
    [ "showfotosetuptooltip.cpp", "showfotosetuptooltip_8cpp.html", null ],
    [ "showfotosetuptooltip.h", "showfotosetuptooltip_8h.html", [
      [ "ShowfotoSetupToolTip", "classShowFoto_1_1ShowfotoSetupToolTip.html", "classShowFoto_1_1ShowfotoSetupToolTip" ]
    ] ]
];