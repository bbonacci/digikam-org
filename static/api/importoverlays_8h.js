var importoverlays_8h =
[
    [ "ImportCoordinatesOverlay", "classDigikam_1_1ImportCoordinatesOverlay.html", "classDigikam_1_1ImportCoordinatesOverlay" ],
    [ "ImportDownloadOverlay", "classDigikam_1_1ImportDownloadOverlay.html", "classDigikam_1_1ImportDownloadOverlay" ],
    [ "ImportLockOverlay", "classDigikam_1_1ImportLockOverlay.html", "classDigikam_1_1ImportLockOverlay" ],
    [ "ImportOverlayWidget", "classDigikam_1_1ImportOverlayWidget.html", "classDigikam_1_1ImportOverlayWidget" ],
    [ "ImportRatingOverlay", "classDigikam_1_1ImportRatingOverlay.html", "classDigikam_1_1ImportRatingOverlay" ],
    [ "ImportRotateOverlay", "classDigikam_1_1ImportRotateOverlay.html", "classDigikam_1_1ImportRotateOverlay" ],
    [ "ImportRotateOverlayButton", "classDigikam_1_1ImportRotateOverlayButton.html", "classDigikam_1_1ImportRotateOverlayButton" ],
    [ "ImportRotateOverlayDirection", "importoverlays_8h.html#a8f43ea2a032a4dbe5ac49fbe00da017d", [
      [ "ImportRotateOverlayLeft", "importoverlays_8h.html#a8f43ea2a032a4dbe5ac49fbe00da017da208a5f843db2778c697feb0acf981bcb", null ],
      [ "ImportRotateOverlayRight", "importoverlays_8h.html#a8f43ea2a032a4dbe5ac49fbe00da017da9b35ba15d2234542da14b3c4efa3207c", null ]
    ] ]
];