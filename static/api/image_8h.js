var image_8h =
[
    [ "CB_ref_info", "structCB__ref__info.html", "structCB__ref__info" ],
    [ "CTB_info", "structCTB__info.html", "structCTB__info" ],
    [ "de265_image", "structde265__image.html", "structde265__image" ],
    [ "MetaDataArray", "classMetaDataArray.html", "classMetaDataArray" ],
    [ "CLEAR_TB_BLK", "image_8h.html#ad98584cd4c0f100cef9eae0522f4beb8", null ],
    [ "CTB_PROGRESS_DEBLK_H", "image_8h.html#a67a83e12ca47273bdc4e6e967672ed1c", null ],
    [ "CTB_PROGRESS_DEBLK_V", "image_8h.html#acd88b441193badedf262c612be704de2", null ],
    [ "CTB_PROGRESS_NONE", "image_8h.html#ab0f9d5e97aca7c6df636b43ed53d2c00", null ],
    [ "CTB_PROGRESS_PREFILTER", "image_8h.html#ad685f728f99e304cdbb231cb4e31961d", null ],
    [ "CTB_PROGRESS_SAO", "image_8h.html#a6a7e51047d08b3b26bafb7975889e168", null ],
    [ "DEBLOCK_BS_MASK", "image_8h.html#ac429b470a1bf7d84e1f73ff2df2555cc", null ],
    [ "DEBLOCK_FLAG_HORIZ", "image_8h.html#a699fe5cef8baf1bab5dfd6cfee10c1db", null ],
    [ "DEBLOCK_FLAG_VERTI", "image_8h.html#a1b467ae4a580e0596bac86dd9d0c8ce6", null ],
    [ "DEBLOCK_PB_EDGE_HORIZ", "image_8h.html#ab68c3b4661284b80b185c2318dc5dbd9", null ],
    [ "DEBLOCK_PB_EDGE_VERTI", "image_8h.html#afb70c0eeedbf8c683fd3785e2c3c0a18", null ],
    [ "INTEGRITY_CORRECT", "image_8h.html#a17bb7ff0a9b91ca5e53a7d6fa13674cc", null ],
    [ "INTEGRITY_DECODING_ERRORS", "image_8h.html#a5f815edbebfb35a510600cb6773e08ac", null ],
    [ "INTEGRITY_DERIVED_FROM_FAULTY_REFERENCE", "image_8h.html#ad72a7650d2a049eee98ab63d0d94bdcc", null ],
    [ "INTEGRITY_NOT_DECODED", "image_8h.html#ab234e95e7d94a169b8d7559c3520d09d", null ],
    [ "INTEGRITY_UNAVAILABLE_REFERENCE", "image_8h.html#a25990e95c72e79ec3ba348fe2612ffa8", null ],
    [ "SEI_HASH_CORRECT", "image_8h.html#a147cfea6cbd99a32e170473b23712558", null ],
    [ "SEI_HASH_INCORRECT", "image_8h.html#a7fc0437c9b0167d0c9bdc7f53cb31e91", null ],
    [ "SEI_HASH_UNCHECKED", "image_8h.html#ad5cc78a9c535a7c8eb1e2d6a28ac6e16", null ],
    [ "SET_CB_BLK", "image_8h.html#ae9be2a7910ab2d37ff2974c89989ed81", null ],
    [ "TU_FLAG_NONZERO_COEFF", "image_8h.html#ab250c7e3a3894b31f19711787e28f9cd", null ],
    [ "TU_FLAG_SPLIT_TRANSFORM_MASK", "image_8h.html#aa6a99144753db9fd55a6730fab0d4159", null ],
    [ "PictureState", "image_8h.html#a2a42960d8df5674378c8b8425747ee62", [
      [ "UnusedForReference", "image_8h.html#a2a42960d8df5674378c8b8425747ee62a053fb927896adf1ac95d0c407099e3fb", null ],
      [ "UsedForShortTermReference", "image_8h.html#a2a42960d8df5674378c8b8425747ee62aad6d5dafe78a1b52c7140ba1e4fd46a4", null ],
      [ "UsedForLongTermReference", "image_8h.html#a2a42960d8df5674378c8b8425747ee62a9b46b6828f90be2f77b4101cf6946888", null ]
    ] ]
];