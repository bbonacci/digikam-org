var classShowFoto_1_1ShowfotoFolderViewBar =
[
    [ "FolderViewTypeMime", "classShowFoto_1_1ShowfotoFolderViewBar.html#a7019d96cbdc416f3cabe949f6f64d1af", [
      [ "TYPE_MIME_JPEG", "classShowFoto_1_1ShowfotoFolderViewBar.html#a7019d96cbdc416f3cabe949f6f64d1afa9bf2e178a18626652251b0e71ee2ed4e", null ],
      [ "TYPE_MIME_TIFF", "classShowFoto_1_1ShowfotoFolderViewBar.html#a7019d96cbdc416f3cabe949f6f64d1afa86dcf841d9f06e2b92e4ad67f703e6ea", null ],
      [ "TYPE_MIME_PNG", "classShowFoto_1_1ShowfotoFolderViewBar.html#a7019d96cbdc416f3cabe949f6f64d1afa1370310f489b1ff10859327a034208b8", null ],
      [ "TYPE_MIME_PGF", "classShowFoto_1_1ShowfotoFolderViewBar.html#a7019d96cbdc416f3cabe949f6f64d1afa19b2fbfe4dd0173071eee7cb3a6d3cea", null ],
      [ "TYPE_MIME_HEIF", "classShowFoto_1_1ShowfotoFolderViewBar.html#a7019d96cbdc416f3cabe949f6f64d1afabac6cc5c8aca7df00762b3456554ee89", null ],
      [ "TYPE_MIME_DNG", "classShowFoto_1_1ShowfotoFolderViewBar.html#a7019d96cbdc416f3cabe949f6f64d1afa47280cfac83334dbc2fca1eb276f596a", null ],
      [ "TYPE_MIME_RAW", "classShowFoto_1_1ShowfotoFolderViewBar.html#a7019d96cbdc416f3cabe949f6f64d1afa07944e79bcc1547240538ba795595c1f", null ],
      [ "TYPE_MIME_NORAW", "classShowFoto_1_1ShowfotoFolderViewBar.html#a7019d96cbdc416f3cabe949f6f64d1afaac4ed4d30c088b3290a66e402734e876", null ],
      [ "TYPE_MIME_ALL", "classShowFoto_1_1ShowfotoFolderViewBar.html#a7019d96cbdc416f3cabe949f6f64d1afaee31e0bfafcbdefe9a60f22a244bc3db", null ]
    ] ],
    [ "ShowfotoFolderViewBar", "classShowFoto_1_1ShowfotoFolderViewBar.html#a16265dd0c95e83ab88884d47fe9fa22c", null ],
    [ "~ShowfotoFolderViewBar", "classShowFoto_1_1ShowfotoFolderViewBar.html#a0fd1cc3138d2c1899e80e0eaff146747", null ],
    [ "bookmarksVisible", "classShowFoto_1_1ShowfotoFolderViewBar.html#af29d562094087d52aaa6dbf8f8a65bc2", null ],
    [ "childEvent", "classShowFoto_1_1ShowfotoFolderViewBar.html#a579b04e3ec4519b0ebbaafb6256cdf15", null ],
    [ "currentPath", "classShowFoto_1_1ShowfotoFolderViewBar.html#aadf2c809223afc6ee1b49dffac6f9344", null ],
    [ "folderViewMode", "classShowFoto_1_1ShowfotoFolderViewBar.html#ac81fda4ff662dc2071d240c008137572", null ],
    [ "folderViewTypeMime", "classShowFoto_1_1ShowfotoFolderViewBar.html#a8c89fb3de52ba5ca7df2bb9dbac30528", null ],
    [ "iconSize", "classShowFoto_1_1ShowfotoFolderViewBar.html#a1bf59cc7ef83d1739a199dbd11f22ede", null ],
    [ "minimumSizeHint", "classShowFoto_1_1ShowfotoFolderViewBar.html#a25789a423f4bb2a6458949f1135a5b84", null ],
    [ "pluginActions", "classShowFoto_1_1ShowfotoFolderViewBar.html#add1fcc08979ac5ed40eb00261073ff1d", null ],
    [ "registerPluginActions", "classShowFoto_1_1ShowfotoFolderViewBar.html#a8ef3bd6fbeaac170d40d37b45a3ec01f", null ],
    [ "setBookmarksVisible", "classShowFoto_1_1ShowfotoFolderViewBar.html#ad8312f8b1869918732ff2775bd0c5eb3", null ],
    [ "setContentsMargins", "classShowFoto_1_1ShowfotoFolderViewBar.html#ae1da4ee33a0f131ea0f1855813d4fb86", null ],
    [ "setContentsMargins", "classShowFoto_1_1ShowfotoFolderViewBar.html#ae5fcb8ec12518ec7a9dff86dcf027e84", null ],
    [ "setCurrentPath", "classShowFoto_1_1ShowfotoFolderViewBar.html#a7a98f3dce7dadc3e6b67423d935eacb8", null ],
    [ "setFolderViewMode", "classShowFoto_1_1ShowfotoFolderViewBar.html#aa7920c92316ffe3918a4383acad0ec49", null ],
    [ "setFolderViewTypeMime", "classShowFoto_1_1ShowfotoFolderViewBar.html#acb49f098e8b46f9c24c2b665fb8e5257", null ],
    [ "setIconSize", "classShowFoto_1_1ShowfotoFolderViewBar.html#a48b0889ea63b1c3a33896fd7bb29bb51", null ],
    [ "setSpacing", "classShowFoto_1_1ShowfotoFolderViewBar.html#a7c13f7e941510af04789d6097c878ee1", null ],
    [ "setStretchFactor", "classShowFoto_1_1ShowfotoFolderViewBar.html#a6a79fbab0ad275840da007964ea8b5a0", null ],
    [ "signalAppendContents", "classShowFoto_1_1ShowfotoFolderViewBar.html#af3676cf26b458a6733152ff3b3110a89", null ],
    [ "signalCustomPathChanged", "classShowFoto_1_1ShowfotoFolderViewBar.html#a13e97442f899d8820efd2be7ab5a5899", null ],
    [ "signalGoHome", "classShowFoto_1_1ShowfotoFolderViewBar.html#a635046cba5fd25e1251b8f9dc6626fb8", null ],
    [ "signalGoNext", "classShowFoto_1_1ShowfotoFolderViewBar.html#a46bc8083b6ba4081d55b2ea1adb1a704", null ],
    [ "signalGoPrevious", "classShowFoto_1_1ShowfotoFolderViewBar.html#ac9bdf4540790c853eff3e6413351eeb5", null ],
    [ "signalGoUp", "classShowFoto_1_1ShowfotoFolderViewBar.html#af3eaba08a2ed78e60fd326b62cf60ac7", null ],
    [ "signalIconSizeChanged", "classShowFoto_1_1ShowfotoFolderViewBar.html#a9441d66489d8019fc2f28e90f7f76fa7", null ],
    [ "signalLoadContents", "classShowFoto_1_1ShowfotoFolderViewBar.html#ab1b5aaf07398b127ae6d72af2ba98050", null ],
    [ "signalPluginActionTriggered", "classShowFoto_1_1ShowfotoFolderViewBar.html#aba9c4864d6cbbdec686d3e84d265999a", null ],
    [ "signalSetup", "classShowFoto_1_1ShowfotoFolderViewBar.html#ad57ce22b46d6db378b351bc69187780c", null ],
    [ "signalShowBookmarks", "classShowFoto_1_1ShowfotoFolderViewBar.html#af2c7c020abac0ef029f88395fd121671", null ],
    [ "signalTypeMimesChanged", "classShowFoto_1_1ShowfotoFolderViewBar.html#a17af7a80555d36a026d8e18c09f78550", null ],
    [ "signalViewModeChanged", "classShowFoto_1_1ShowfotoFolderViewBar.html#ac5481503ecca2afd80a411f6b8e8f0c0", null ],
    [ "sizeHint", "classShowFoto_1_1ShowfotoFolderViewBar.html#adfd68279bc71f4b8e91011a8ed733f96", null ],
    [ "slotNextEnabled", "classShowFoto_1_1ShowfotoFolderViewBar.html#ad09241d659c008308d7c825db45c86ee", null ],
    [ "slotPreviousEnabled", "classShowFoto_1_1ShowfotoFolderViewBar.html#a65733f4bc8a7aa864a26b6733a6d038b", null ],
    [ "toolBarAction", "classShowFoto_1_1ShowfotoFolderViewBar.html#a7c3d7ff8efb491c2a43784901877f2f3", null ]
];