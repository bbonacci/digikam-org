var itemquerybuilder__p_8h =
[
    [ "RuleType", "classDigikam_1_1RuleType.html", "classDigikam_1_1RuleType" ],
    [ "RuleTypeForConversion", "classDigikam_1_1RuleTypeForConversion.html", "classDigikam_1_1RuleTypeForConversion" ],
    [ "SubQueryBuilder", "classDigikam_1_1SubQueryBuilder.html", "classDigikam_1_1SubQueryBuilder" ],
    [ "SKey", "itemquerybuilder__p_8h.html#a8e2839c58fa689cdb5a080caa3dbff1f", [
      [ "ALBUM", "itemquerybuilder__p_8h.html#a8e2839c58fa689cdb5a080caa3dbff1fa65eeb414d28874cd4d43f01cb8992fc3", null ],
      [ "ALBUMNAME", "itemquerybuilder__p_8h.html#a8e2839c58fa689cdb5a080caa3dbff1fac58435c9e51d98d4007fb8aa8cbfb8e6", null ],
      [ "ALBUMCAPTION", "itemquerybuilder__p_8h.html#a8e2839c58fa689cdb5a080caa3dbff1fab9274a86a008de36d1b26ddf4e33f40e", null ],
      [ "ALBUMCOLLECTION", "itemquerybuilder__p_8h.html#a8e2839c58fa689cdb5a080caa3dbff1fa42545c74a7761f1742e0dcadb2d7d4ad", null ],
      [ "TAG", "itemquerybuilder__p_8h.html#a8e2839c58fa689cdb5a080caa3dbff1fa8a2a922ddff7976bcc4632c247600257", null ],
      [ "TAGNAME", "itemquerybuilder__p_8h.html#a8e2839c58fa689cdb5a080caa3dbff1fa89f536205ed3784202ee777624a5651f", null ],
      [ "IMAGENAME", "itemquerybuilder__p_8h.html#a8e2839c58fa689cdb5a080caa3dbff1fad2532b05e773c9170b482ebe3ec01f3f", null ],
      [ "IMAGECAPTION", "itemquerybuilder__p_8h.html#a8e2839c58fa689cdb5a080caa3dbff1faa148d1948e89620bc85447ba3bec78bf", null ],
      [ "IMAGEDATE", "itemquerybuilder__p_8h.html#a8e2839c58fa689cdb5a080caa3dbff1fa6894232e93672caa54f49e6e88613019", null ],
      [ "KEYWORD", "itemquerybuilder__p_8h.html#a8e2839c58fa689cdb5a080caa3dbff1faeb5f8f2125a827c2b15b3eb83cd75e6f", null ],
      [ "RATING", "itemquerybuilder__p_8h.html#a8e2839c58fa689cdb5a080caa3dbff1fa771d0e96b5210382fbc5de4bf2c5394c", null ]
    ] ],
    [ "SOperator", "itemquerybuilder__p_8h.html#ad433e183fd169903b66270e16b6ada18", [
      [ "EQ", "itemquerybuilder__p_8h.html#ad433e183fd169903b66270e16b6ada18a9b55a3c5cb31c7e3860e99af11b1ac26", null ],
      [ "NE", "itemquerybuilder__p_8h.html#ad433e183fd169903b66270e16b6ada18a3a96b9b2353c9ab49ddd4664d6832637", null ],
      [ "LT", "itemquerybuilder__p_8h.html#ad433e183fd169903b66270e16b6ada18a647c67dd35b7b624a0ff476468f8e9d7", null ],
      [ "GT", "itemquerybuilder__p_8h.html#ad433e183fd169903b66270e16b6ada18a8ef75270be044ff4ed5c288bdd9eaef5", null ],
      [ "LIKE", "itemquerybuilder__p_8h.html#ad433e183fd169903b66270e16b6ada18af9cfe745acd6da49be7c8f1d4dd1eb21", null ],
      [ "NLIKE", "itemquerybuilder__p_8h.html#ad433e183fd169903b66270e16b6ada18a357ec2b3dc9bc74aedf3da5c6bbc6d00", null ],
      [ "LTE", "itemquerybuilder__p_8h.html#ad433e183fd169903b66270e16b6ada18ad4ac0286f91d8a2315d3a143fe4f8962", null ],
      [ "GTE", "itemquerybuilder__p_8h.html#ad433e183fd169903b66270e16b6ada18a2c27dfae0aadd0fcf8a14271679d2b1c", null ]
    ] ]
];