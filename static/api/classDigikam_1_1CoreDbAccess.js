var classDigikam_1_1CoreDbAccess =
[
    [ "ApplicationStatus", "classDigikam_1_1CoreDbAccess.html#a1b1b7812b0682a5897f4c5a0bbeab4bb", [
      [ "MainApplication", "classDigikam_1_1CoreDbAccess.html#a1b1b7812b0682a5897f4c5a0bbeab4bba1fd484640a285c639eb01b8e6654829c", null ],
      [ "DatabaseSlave", "classDigikam_1_1CoreDbAccess.html#a1b1b7812b0682a5897f4c5a0bbeab4bba8070e02b3e23987265eb0b022ed97cd5", null ]
    ] ],
    [ "CoreDbAccess", "classDigikam_1_1CoreDbAccess.html#af389983493e5c5edcfb540452ba6bdd5", null ],
    [ "~CoreDbAccess", "classDigikam_1_1CoreDbAccess.html#a5a3494f4a0a2747774dbbf9f21dd1fa3", null ],
    [ "backend", "classDigikam_1_1CoreDbAccess.html#ac060a7015991128a26a7b8e11e727c1a", null ],
    [ "db", "classDigikam_1_1CoreDbAccess.html#a6d6c1be3a9f7d843d67e887ad9cf99f9", null ],
    [ "lastError", "classDigikam_1_1CoreDbAccess.html#a02bf29593e278d8a91fc13a545969b1e", null ],
    [ "setLastError", "classDigikam_1_1CoreDbAccess.html#a58109070379ce61ed222e1a03f29b55f", null ],
    [ "CoreDbAccessUnlock", "classDigikam_1_1CoreDbAccess.html#a84df13c98868c2753029f29f2997406e", null ]
];