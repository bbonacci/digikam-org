var classDigikam_1_1FaceScanSettings =
[
    [ "AlreadyScannedHandling", "classDigikam_1_1FaceScanSettings.html#ad2af389744f96ed8abfddd5898f244b9", [
      [ "Skip", "classDigikam_1_1FaceScanSettings.html#ad2af389744f96ed8abfddd5898f244b9a8204890149400cfe6a593ef07545a74b", null ],
      [ "Merge", "classDigikam_1_1FaceScanSettings.html#ad2af389744f96ed8abfddd5898f244b9ad6997cf7808074dd4dc882cb215d35d7", null ],
      [ "Rescan", "classDigikam_1_1FaceScanSettings.html#ad2af389744f96ed8abfddd5898f244b9a57bcc7cb35c5fcfadc990b29e615405d", null ]
    ] ],
    [ "ScanTask", "classDigikam_1_1FaceScanSettings.html#ab8c428c4e39a23d952b4edd6060e9c02", [
      [ "Detect", "classDigikam_1_1FaceScanSettings.html#ab8c428c4e39a23d952b4edd6060e9c02a56ef1e2a063753c69441c0d0fa2e8d26", null ],
      [ "DetectAndRecognize", "classDigikam_1_1FaceScanSettings.html#ab8c428c4e39a23d952b4edd6060e9c02a36f18d9e284dc2f7745438abc7e4cafe", null ],
      [ "RecognizeMarkedFaces", "classDigikam_1_1FaceScanSettings.html#ab8c428c4e39a23d952b4edd6060e9c02aa03d22cbb8b7078e1a25dd3e413c5339", null ],
      [ "RetrainAll", "classDigikam_1_1FaceScanSettings.html#ab8c428c4e39a23d952b4edd6060e9c02abfa09cc0407fe58609736c6919ee66ec", null ],
      [ "BenchmarkDetection", "classDigikam_1_1FaceScanSettings.html#ab8c428c4e39a23d952b4edd6060e9c02aaf31fb4277c9421846ab458b7110cb60", null ],
      [ "BenchmarkRecognition", "classDigikam_1_1FaceScanSettings.html#ab8c428c4e39a23d952b4edd6060e9c02a155e8293afc5226df68fcfafb9fb47af", null ]
    ] ],
    [ "FaceScanSettings", "classDigikam_1_1FaceScanSettings.html#af5e330603853f50ffd6500eb268a0822", null ],
    [ "~FaceScanSettings", "classDigikam_1_1FaceScanSettings.html#a973a1b4bf021a577f279503701d500f0", null ],
    [ "accuracy", "classDigikam_1_1FaceScanSettings.html#a22437037d621e2365090f071328569be", null ],
    [ "albums", "classDigikam_1_1FaceScanSettings.html#a9fb1a7f3177c31b5806e1f053540b51f", null ],
    [ "alreadyScannedHandling", "classDigikam_1_1FaceScanSettings.html#ae5ed59c77435a6222129f408b75b7173", null ],
    [ "infos", "classDigikam_1_1FaceScanSettings.html#adfb2324e76cfde8927b9489b259c3c98", null ],
    [ "task", "classDigikam_1_1FaceScanSettings.html#a55ef40563e33e933ac0ebb24567b3085", null ],
    [ "useFullCpu", "classDigikam_1_1FaceScanSettings.html#ab81e2bf157400b5d663edb50a0c69310", null ],
    [ "useYoloV3", "classDigikam_1_1FaceScanSettings.html#ad871ae931605270365c6f9713a0bfc7d", null ],
    [ "wholeAlbums", "classDigikam_1_1FaceScanSettings.html#a50b475a41087723558f74edf8d0c17f4", null ]
];