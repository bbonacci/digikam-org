var classDigikam_1_1SearchTextFilterSettings =
[
    [ "TextFilterFields", "classDigikam_1_1SearchTextFilterSettings.html#a542d92311aa6cc70a997c8b314c95bf8", [
      [ "None", "classDigikam_1_1SearchTextFilterSettings.html#a542d92311aa6cc70a997c8b314c95bf8a7de50b3aca6ba7283484d786864d2896", null ],
      [ "ImageName", "classDigikam_1_1SearchTextFilterSettings.html#a542d92311aa6cc70a997c8b314c95bf8a9eed6e661f7e8594685ffb644a45492f", null ],
      [ "ImageTitle", "classDigikam_1_1SearchTextFilterSettings.html#a542d92311aa6cc70a997c8b314c95bf8adc0aab57c558c77e913fa94e6900e12d", null ],
      [ "ImageComment", "classDigikam_1_1SearchTextFilterSettings.html#a542d92311aa6cc70a997c8b314c95bf8a95f4af9010b6610ca1bba64faddee2e5", null ],
      [ "TagName", "classDigikam_1_1SearchTextFilterSettings.html#a542d92311aa6cc70a997c8b314c95bf8a8fcd2be03980bdac83f4b91d0b4a826f", null ],
      [ "AlbumName", "classDigikam_1_1SearchTextFilterSettings.html#a542d92311aa6cc70a997c8b314c95bf8a7a4918d3836aea3f159739aa89c0805c", null ],
      [ "ImageAspectRatio", "classDigikam_1_1SearchTextFilterSettings.html#a542d92311aa6cc70a997c8b314c95bf8a6ca88f10053ab859062ea16fe61a60af", null ],
      [ "ImagePixelSize", "classDigikam_1_1SearchTextFilterSettings.html#a542d92311aa6cc70a997c8b314c95bf8aee2066624ae3824ce8ba7922f14cb673", null ],
      [ "All", "classDigikam_1_1SearchTextFilterSettings.html#a542d92311aa6cc70a997c8b314c95bf8a8636e78d58f66d772e558d5332e720c4", null ]
    ] ],
    [ "SearchTextFilterSettings", "classDigikam_1_1SearchTextFilterSettings.html#adebb92f15dd02bec3f2d955778633313", null ],
    [ "SearchTextFilterSettings", "classDigikam_1_1SearchTextFilterSettings.html#a47a5a4dcc824fdac965f9f053ec1b21c", null ],
    [ "caseSensitive", "classDigikam_1_1SearchTextFilterSettings.html#a87ea1c61d20d11126dd914a711679595", null ],
    [ "text", "classDigikam_1_1SearchTextFilterSettings.html#ab54fd9a53f688f65ec271128da2998ac", null ],
    [ "textFields", "classDigikam_1_1SearchTextFilterSettings.html#a65baf7c8edfa5e8456a9fc88ac59d3dc", null ]
];