var classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCaptionInfo =
[
    [ "AdvPrintCaptionInfo", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCaptionInfo.html#a6a98e4be06194881c601ffcd25c39241", null ],
    [ "AdvPrintCaptionInfo", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCaptionInfo.html#a687ff82b8ecedc030b621add35e800da", null ],
    [ "~AdvPrintCaptionInfo", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCaptionInfo.html#a8525499551776217c36ddbe8984cb001", null ],
    [ "m_captionColor", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCaptionInfo.html#a2402c3fa91232f02c2e731f0f0ef5dfa", null ],
    [ "m_captionFont", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCaptionInfo.html#a1550ca6abc12fdaeb6da9820d97e21c9", null ],
    [ "m_captionSize", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCaptionInfo.html#add6b728deb05f77f4e03c39021d0f07f", null ],
    [ "m_captionText", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCaptionInfo.html#a257606c600a1fecea687a85936b2a70b", null ],
    [ "m_captionType", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCaptionInfo.html#afc38929185166757310f02b96ccf29a1", null ]
];