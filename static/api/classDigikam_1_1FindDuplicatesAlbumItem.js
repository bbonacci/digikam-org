var classDigikam_1_1FindDuplicatesAlbumItem =
[
    [ "Column", "classDigikam_1_1FindDuplicatesAlbumItem.html#acf157923fed79bc9aeac4571d6a97117", [
      [ "REFERENCE_IMAGE", "classDigikam_1_1FindDuplicatesAlbumItem.html#acf157923fed79bc9aeac4571d6a97117a313ab369b99aa3712ddff7181d1a57e2", null ],
      [ "REFERENCE_DATE", "classDigikam_1_1FindDuplicatesAlbumItem.html#acf157923fed79bc9aeac4571d6a97117a6cc85b09e91f534683080bf843c46e7f", null ],
      [ "REFERENCE_ALBUM", "classDigikam_1_1FindDuplicatesAlbumItem.html#acf157923fed79bc9aeac4571d6a97117a17313fb16239f6fd21f8e0fc0a80cdef", null ],
      [ "RESULT_COUNT", "classDigikam_1_1FindDuplicatesAlbumItem.html#acf157923fed79bc9aeac4571d6a97117af45f582dda3cb427292bc88e1e7eaa6f", null ],
      [ "AVG_SIMILARITY", "classDigikam_1_1FindDuplicatesAlbumItem.html#acf157923fed79bc9aeac4571d6a97117ac9af84c62fd054f92779481837771857", null ]
    ] ],
    [ "FindDuplicatesAlbumItem", "classDigikam_1_1FindDuplicatesAlbumItem.html#aa229cec9923716f7ffba2ec6e296b698", null ],
    [ "~FindDuplicatesAlbumItem", "classDigikam_1_1FindDuplicatesAlbumItem.html#a2bef8e8087913512c80629b5e5466dec", null ],
    [ "album", "classDigikam_1_1FindDuplicatesAlbumItem.html#ad55a9e887a8d7a98232ec06a669efe1b", null ],
    [ "calculateInfos", "classDigikam_1_1FindDuplicatesAlbumItem.html#a63b6a6bafe35f4c973a3cc3808a150b0", null ],
    [ "duplicatedItems", "classDigikam_1_1FindDuplicatesAlbumItem.html#a320d45285c82dff8fad69a3c7188aff5", null ],
    [ "hasValidThumbnail", "classDigikam_1_1FindDuplicatesAlbumItem.html#aaa66673d0b9f994bd74bd25c8998ec2b", null ],
    [ "itemCount", "classDigikam_1_1FindDuplicatesAlbumItem.html#ae2ed8760bb067cea0866cda974dcb7ba", null ],
    [ "operator<", "classDigikam_1_1FindDuplicatesAlbumItem.html#a549f03c337df3494b75110fb8c0e9b2d", null ],
    [ "refUrl", "classDigikam_1_1FindDuplicatesAlbumItem.html#a7d187df8f09781e688b859b56f47e32c", null ],
    [ "setThumb", "classDigikam_1_1FindDuplicatesAlbumItem.html#a117f86c1dd9ad87968deb565b70ee872", null ]
];