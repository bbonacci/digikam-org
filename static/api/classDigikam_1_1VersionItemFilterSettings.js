var classDigikam_1_1VersionItemFilterSettings =
[
    [ "VersionItemFilterSettings", "classDigikam_1_1VersionItemFilterSettings.html#a021f5556867456c10dc30117d99727f5", null ],
    [ "VersionItemFilterSettings", "classDigikam_1_1VersionItemFilterSettings.html#a01a7c97f1c720d6028860e6ac793d039", null ],
    [ "isExemptedBySettings", "classDigikam_1_1VersionItemFilterSettings.html#a84c8c638676c8e2b975f6f5bb537fcdd", null ],
    [ "isFiltering", "classDigikam_1_1VersionItemFilterSettings.html#addf2a58c61c517c4829048292f998134", null ],
    [ "isFilteringByTags", "classDigikam_1_1VersionItemFilterSettings.html#ae525f9adaa4658a21a03582991999ecc", null ],
    [ "isHiddenBySettings", "classDigikam_1_1VersionItemFilterSettings.html#a2718cc353ff4a2d303e5d0b5c5f862e7", null ],
    [ "matches", "classDigikam_1_1VersionItemFilterSettings.html#a1cd5ad34b3ed380c80c2973b9564c4ad", null ],
    [ "operator==", "classDigikam_1_1VersionItemFilterSettings.html#abec0495f2a6bc41b2dd0f50ef779d0b2", null ],
    [ "setExceptionList", "classDigikam_1_1VersionItemFilterSettings.html#aca906d1ebf0ccd7bdeb036ff0e989481", null ],
    [ "setVersionManagerSettings", "classDigikam_1_1VersionItemFilterSettings.html#ad9fad0f61ac139e4690ca63d1206c67a", null ],
    [ "m_exceptionLists", "classDigikam_1_1VersionItemFilterSettings.html#ac5adc5df9d378dbbfcf162a1a8a15ce0", null ],
    [ "m_exceptionTagFilter", "classDigikam_1_1VersionItemFilterSettings.html#af3e2139edb583cf9787bf2620833c0ae", null ],
    [ "m_excludeTagFilter", "classDigikam_1_1VersionItemFilterSettings.html#ab71214b6dc3b2e10aa6e359c69718d31", null ],
    [ "m_includeTagFilter", "classDigikam_1_1VersionItemFilterSettings.html#a88fefebb51ab63578ccb6f62da5142b6", null ]
];