var dxmlguiwindow_8h =
[
    [ "DXmlGuiWindow", "classDigikam_1_1DXmlGuiWindow.html", "classDigikam_1_1DXmlGuiWindow" ],
    [ "FullScreenOptions", "dxmlguiwindow_8h.html#a61a23b7f63caada6231f77a07f7207e0", [
      [ "FS_TOOLBARS", "dxmlguiwindow_8h.html#a61a23b7f63caada6231f77a07f7207e0adbde199087fdf2b640db6b94b14fe9cf", null ],
      [ "FS_THUMBBAR", "dxmlguiwindow_8h.html#a61a23b7f63caada6231f77a07f7207e0a872ee30aa0f68a07e3f94011991c9e87", null ],
      [ "FS_SIDEBARS", "dxmlguiwindow_8h.html#a61a23b7f63caada6231f77a07f7207e0a8959718fc0c342226a0ed2b7e9c32156", null ],
      [ "FS_STATUSBAR", "dxmlguiwindow_8h.html#a61a23b7f63caada6231f77a07f7207e0ad7407d84e65c5c43b64ef3485ca74714", null ],
      [ "FS_NONE", "dxmlguiwindow_8h.html#a61a23b7f63caada6231f77a07f7207e0a224c0adb9382498bde1b45697cbd91b9", null ],
      [ "FS_ALBUMGUI", "dxmlguiwindow_8h.html#a61a23b7f63caada6231f77a07f7207e0ac726505514ac1f10531e30be90b21d16", null ],
      [ "FS_EDITOR", "dxmlguiwindow_8h.html#a61a23b7f63caada6231f77a07f7207e0a5990501321f39c2c99c67e844dac273b", null ],
      [ "FS_LIGHTTABLE", "dxmlguiwindow_8h.html#a61a23b7f63caada6231f77a07f7207e0ae489002882cc105ce528d4db10312a77", null ],
      [ "FS_IMPORTUI", "dxmlguiwindow_8h.html#a61a23b7f63caada6231f77a07f7207e0a8e2b1c9154eaef4b8eec9a9294c578b0", null ]
    ] ],
    [ "StdActionType", "dxmlguiwindow_8h.html#ac639ce7a657ee827d8afa5aa6d6b057c", [
      [ "StdCopyAction", "dxmlguiwindow_8h.html#ac639ce7a657ee827d8afa5aa6d6b057ca4461c64750c9cfdc4f73751f08d87532", null ],
      [ "StdPasteAction", "dxmlguiwindow_8h.html#ac639ce7a657ee827d8afa5aa6d6b057caf0a3da16dec0566d62d7e192f1b14374", null ],
      [ "StdCutAction", "dxmlguiwindow_8h.html#ac639ce7a657ee827d8afa5aa6d6b057ca69f5975fd09863aa8b13e455cf0ae4fb", null ],
      [ "StdQuitAction", "dxmlguiwindow_8h.html#ac639ce7a657ee827d8afa5aa6d6b057ca2c48fa867deae77e7d1d091aecc3a6b9", null ],
      [ "StdCloseAction", "dxmlguiwindow_8h.html#ac639ce7a657ee827d8afa5aa6d6b057ca1da3c11b5d4796cd02f145b7a36b8f9a", null ],
      [ "StdZoomInAction", "dxmlguiwindow_8h.html#ac639ce7a657ee827d8afa5aa6d6b057ca4d4d92f4bef82e3af7c5b46d21a46256", null ],
      [ "StdZoomOutAction", "dxmlguiwindow_8h.html#ac639ce7a657ee827d8afa5aa6d6b057cae573fc999a488cd0c884f244f0a01155", null ],
      [ "StdOpenAction", "dxmlguiwindow_8h.html#ac639ce7a657ee827d8afa5aa6d6b057ca9de2c8adc82132b265a65799d37bd4f4", null ],
      [ "StdSaveAction", "dxmlguiwindow_8h.html#ac639ce7a657ee827d8afa5aa6d6b057ca98366bed81978927cc16a066d85a48c0", null ],
      [ "StdSaveAsAction", "dxmlguiwindow_8h.html#ac639ce7a657ee827d8afa5aa6d6b057ca9eae28129f3cbcd449f675b70377dcf4", null ],
      [ "StdRevertAction", "dxmlguiwindow_8h.html#ac639ce7a657ee827d8afa5aa6d6b057ca5f8a329b4ceaa69b56d7d3cec078b4e5", null ],
      [ "StdBackAction", "dxmlguiwindow_8h.html#ac639ce7a657ee827d8afa5aa6d6b057cad78ce6378c8bb7b4c729bd3d018f1fdd", null ],
      [ "StdForwardAction", "dxmlguiwindow_8h.html#ac639ce7a657ee827d8afa5aa6d6b057ca6c30119cae52bec22f15df480309c2a5", null ]
    ] ]
];