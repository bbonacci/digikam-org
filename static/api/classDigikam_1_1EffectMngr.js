var classDigikam_1_1EffectMngr =
[
    [ "Private", "classDigikam_1_1EffectMngr_1_1Private.html", "classDigikam_1_1EffectMngr_1_1Private" ],
    [ "EffectType", "classDigikam_1_1EffectMngr.html#a97320985e4a5a7d570c8608115c83290", [
      [ "None", "classDigikam_1_1EffectMngr.html#a97320985e4a5a7d570c8608115c83290a76a6886737b72a45e3ea2b8dc8984cda", null ],
      [ "KenBurnsZoomIn", "classDigikam_1_1EffectMngr.html#a97320985e4a5a7d570c8608115c83290ad7caf293a0aef5fe5ff0b5e7a8ccd28b", null ],
      [ "KenBurnsZoomOut", "classDigikam_1_1EffectMngr.html#a97320985e4a5a7d570c8608115c83290a70176dbd85144f42440e093409e69610", null ],
      [ "KenBurnsPanLR", "classDigikam_1_1EffectMngr.html#a97320985e4a5a7d570c8608115c83290a4e6c9f2e20eb0707dccc55b7d45ecdc1", null ],
      [ "KenBurnsPanRL", "classDigikam_1_1EffectMngr.html#a97320985e4a5a7d570c8608115c83290a34479714bb19c20f67bf024e1fe9b8c0", null ],
      [ "KenBurnsPanTB", "classDigikam_1_1EffectMngr.html#a97320985e4a5a7d570c8608115c83290ac7a3bb929c0bc8349e713929fa86d9a7", null ],
      [ "KenBurnsPanBT", "classDigikam_1_1EffectMngr.html#a97320985e4a5a7d570c8608115c83290ab891a7a5146744e81f17f30873e50bd1", null ],
      [ "Random", "classDigikam_1_1EffectMngr.html#a97320985e4a5a7d570c8608115c83290a4eeb9a944d3ed96fd1d0b82670b25c05", null ]
    ] ],
    [ "EffectMngr", "classDigikam_1_1EffectMngr.html#ac565cc0840a0cae837fd778aa872afb7", null ],
    [ "~EffectMngr", "classDigikam_1_1EffectMngr.html#a21ec4ec1cd4a793f680b86249cd1fee4", null ],
    [ "currentFrame", "classDigikam_1_1EffectMngr.html#a0456d10c6561f20f37bc80dddc56d341", null ],
    [ "setEffect", "classDigikam_1_1EffectMngr.html#a5510ded06747b70b70ba6963b1462c9f", null ],
    [ "setFrames", "classDigikam_1_1EffectMngr.html#ad20626c38dfe1d7f36153ac66ac3cf24", null ],
    [ "setImage", "classDigikam_1_1EffectMngr.html#ac3d83134c19090947d894778cb3a0395", null ],
    [ "setOutputSize", "classDigikam_1_1EffectMngr.html#a7775b00a3387d4d62318a2c5a43b0e56", null ]
];