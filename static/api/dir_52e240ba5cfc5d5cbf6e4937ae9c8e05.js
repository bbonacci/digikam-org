var dir_52e240ba5cfc5d5cbf6e4937ae9c8e05 =
[
    [ "adjustcurves", "dir_db16cc7d4d47f7e303734f37d3613644.html", "dir_db16cc7d4d47f7e303734f37d3613644" ],
    [ "adjustlevels", "dir_ad6405859835177d390aa0b678b82b4a.html", "dir_ad6405859835177d390aa0b678b82b4a" ],
    [ "autocorrection", "dir_61274a38a9d1a99393dfc743779514d9.html", "dir_61274a38a9d1a99393dfc743779514d9" ],
    [ "bcg", "dir_3b652bf86078c8e0f8ccdcab886705b7.html", "dir_3b652bf86078c8e0f8ccdcab886705b7" ],
    [ "bwsepia", "dir_a77b38d75ce149b7899b8c8835e5ecf4.html", "dir_a77b38d75ce149b7899b8c8835e5ecf4" ],
    [ "channelmixer", "dir_b9320a86f2d7664f58ef9cc93af27719.html", "dir_b9320a86f2d7664f58ef9cc93af27719" ],
    [ "colorbalance", "dir_331c6712572b345390b06f7c947b209c.html", "dir_331c6712572b345390b06f7c947b209c" ],
    [ "convert16to8", "dir_1a1270d19d67550380184dc07b37b063.html", "dir_1a1270d19d67550380184dc07b37b063" ],
    [ "convert8to16", "dir_5c5c669f94b00f0efa1e0d5b0939a2f3.html", "dir_5c5c669f94b00f0efa1e0d5b0939a2f3" ],
    [ "film", "dir_a022f409b0c90e681050b43c8436dce2.html", "dir_a022f409b0c90e681050b43c8436dce2" ],
    [ "hsl", "dir_b6050b76c1cbbf2d0a4cd0186cd196e5.html", "dir_b6050b76c1cbbf2d0a4cd0186cd196e5" ],
    [ "invert", "dir_a56ae9a84f2b2f2cd7a4453b9f977b3b.html", "dir_a56ae9a84f2b2f2cd7a4453b9f977b3b" ],
    [ "profileconversion", "dir_d9560fc147a29e36e30ca0902f2a23f0.html", "dir_d9560fc147a29e36e30ca0902f2a23f0" ],
    [ "whitebalance", "dir_34553a2bf52ffbbceca5f5da6e4073ae.html", "dir_34553a2bf52ffbbceca5f5da6e4073ae" ]
];