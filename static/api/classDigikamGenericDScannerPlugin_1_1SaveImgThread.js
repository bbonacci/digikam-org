var classDigikamGenericDScannerPlugin_1_1SaveImgThread =
[
    [ "SaveImgThread", "classDigikamGenericDScannerPlugin_1_1SaveImgThread.html#a79a74157d278518a5f29bb2597c06980", null ],
    [ "~SaveImgThread", "classDigikamGenericDScannerPlugin_1_1SaveImgThread.html#af074b5dc66588003f18d4dca7a131c98", null ],
    [ "setImageData", "classDigikamGenericDScannerPlugin_1_1SaveImgThread.html#a7ec5eb4ccd7bc104fd0c879421f2dcb8", null ],
    [ "setScannerModel", "classDigikamGenericDScannerPlugin_1_1SaveImgThread.html#a6c1552935d30404d5086c819474d0139", null ],
    [ "setTargetFile", "classDigikamGenericDScannerPlugin_1_1SaveImgThread.html#a2295028789b55fe581c68d10459f0b02", null ],
    [ "signalComplete", "classDigikamGenericDScannerPlugin_1_1SaveImgThread.html#aa6f824f00c0a24a5d32072759ab48cdf", null ],
    [ "signalProgress", "classDigikamGenericDScannerPlugin_1_1SaveImgThread.html#ac8298dbf88b4afd46afb03db25f95631", null ]
];