var dir_0a8a8478fd7824016d75ca53e712b6f9 =
[
    [ "lookupaltitude.cpp", "lookupaltitude_8cpp.html", null ],
    [ "lookupaltitude.h", "lookupaltitude_8h.html", [
      [ "LookupAltitude", "classDigikam_1_1LookupAltitude.html", "classDigikam_1_1LookupAltitude" ],
      [ "Request", "classDigikam_1_1LookupAltitude_1_1Request.html", "classDigikam_1_1LookupAltitude_1_1Request" ]
    ] ],
    [ "lookupaltitudegeonames.cpp", "lookupaltitudegeonames_8cpp.html", null ],
    [ "lookupaltitudegeonames.h", "lookupaltitudegeonames_8h.html", [
      [ "LookupAltitudeGeonames", "classDigikam_1_1LookupAltitudeGeonames.html", "classDigikam_1_1LookupAltitudeGeonames" ]
    ] ],
    [ "lookupfactory.cpp", "lookupfactory_8cpp.html", null ],
    [ "lookupfactory.h", "lookupfactory_8h.html", [
      [ "LookupFactory", "classDigikam_1_1LookupFactory.html", null ]
    ] ]
];