var classDigikam_1_1TrackCorrelator =
[
    [ "Correlation", "classDigikam_1_1TrackCorrelator_1_1Correlation.html", "classDigikam_1_1TrackCorrelator_1_1Correlation" ],
    [ "CorrelationOptions", "classDigikam_1_1TrackCorrelator_1_1CorrelationOptions.html", "classDigikam_1_1TrackCorrelator_1_1CorrelationOptions" ],
    [ "CorrelationFlags", "classDigikam_1_1TrackCorrelator.html#a7d42f8daf06020d665a72936e27fd131", [
      [ "CorrelationFlagCoordinates", "classDigikam_1_1TrackCorrelator.html#a7d42f8daf06020d665a72936e27fd131a06332a9b14f6bd043dded16e341a17db", null ],
      [ "CorrelationFlagInterpolated", "classDigikam_1_1TrackCorrelator.html#a7d42f8daf06020d665a72936e27fd131a2ad1ebed5c62c8e6f569b94587af295c", null ],
      [ "CorrelationFlagAltitude", "classDigikam_1_1TrackCorrelator.html#a7d42f8daf06020d665a72936e27fd131abab9388ad23b51545bd477ede85d0071", null ]
    ] ],
    [ "TrackCorrelator", "classDigikam_1_1TrackCorrelator.html#afc68823ebc69de8fe00cd6a9b47ed4e7", null ],
    [ "~TrackCorrelator", "classDigikam_1_1TrackCorrelator.html#ac9dc5221ecc1a5ee3c2ff01f7205785d", null ],
    [ "cancelCorrelation", "classDigikam_1_1TrackCorrelator.html#a2377b3aac076ef15e7182337e6040604", null ],
    [ "correlate", "classDigikam_1_1TrackCorrelator.html#a8db15cd8b188137343c2c89524fa0989", null ],
    [ "signalAllItemsCorrelated", "classDigikam_1_1TrackCorrelator.html#a14c7f2f9d1c2b5ecccb811e832e20e1f", null ],
    [ "signalCorrelationCanceled", "classDigikam_1_1TrackCorrelator.html#ad957e043a53f1311a4b10a270cc4ee79", null ],
    [ "signalItemsCorrelated", "classDigikam_1_1TrackCorrelator.html#a188ddb0edcd37e34a53df894cac57c42", null ]
];