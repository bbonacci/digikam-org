var classDigikam_1_1HEIFSettings =
[
    [ "HEIFSettings", "classDigikam_1_1HEIFSettings.html#a8c847c5c6a8795539d1f88faee3c0d5b", null ],
    [ "~HEIFSettings", "classDigikam_1_1HEIFSettings.html#ac08d0be86ea1ecc30485c2d2056f42c9", null ],
    [ "getCompressionValue", "classDigikam_1_1HEIFSettings.html#a7d318b65711eccc57c717d9d0904b2dc", null ],
    [ "getLossLessCompression", "classDigikam_1_1HEIFSettings.html#a15ce621e3cc0589835b83a9071182102", null ],
    [ "setCompressionValue", "classDigikam_1_1HEIFSettings.html#a68daa26b4163c269e3765359651626b5", null ],
    [ "setLossLessCompression", "classDigikam_1_1HEIFSettings.html#aa94eaacaebf00a801e7bd44653128def", null ],
    [ "signalSettingsChanged", "classDigikam_1_1HEIFSettings.html#ae437d4448674e9d4b4a163589c103c69", null ]
];