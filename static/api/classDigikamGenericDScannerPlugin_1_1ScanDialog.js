var classDigikamGenericDScannerPlugin_1_1ScanDialog =
[
    [ "ScanDialog", "classDigikamGenericDScannerPlugin_1_1ScanDialog.html#a6a5f9c6f24337f11dad942ee85cbb5f0", null ],
    [ "~ScanDialog", "classDigikamGenericDScannerPlugin_1_1ScanDialog.html#a189869a00d7b62ecd61be672df5a115c", null ],
    [ "closeEvent", "classDigikamGenericDScannerPlugin_1_1ScanDialog.html#ac46bc95cfe2f2c32c0730fc82062d66b", null ],
    [ "restoreDialogSize", "classDigikamGenericDScannerPlugin_1_1ScanDialog.html#a5da66f5eb64468e9524230138e09c2a2", null ],
    [ "saveDialogSize", "classDigikamGenericDScannerPlugin_1_1ScanDialog.html#a5496bc5fd80e57edd35e652dbb8361ae", null ],
    [ "setPlugin", "classDigikamGenericDScannerPlugin_1_1ScanDialog.html#a8f9473a19990a8001d98cde6286d1059", null ],
    [ "setTargetDir", "classDigikamGenericDScannerPlugin_1_1ScanDialog.html#aee4a31ec0ca76e957f3524bc3162537b", null ],
    [ "signalImportedImage", "classDigikamGenericDScannerPlugin_1_1ScanDialog.html#a8f9f4bde65d2a50ea5f26fb6bf08f527", null ],
    [ "m_buttons", "classDigikamGenericDScannerPlugin_1_1ScanDialog.html#a06170b9f45856493f30cb0edd368a3c0", null ]
];