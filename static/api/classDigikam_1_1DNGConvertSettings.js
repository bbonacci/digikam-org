var classDigikam_1_1DNGConvertSettings =
[
    [ "DNGConvertSettings", "classDigikam_1_1DNGConvertSettings.html#ab5a894d7472b41880574c2e365b6fa80", null ],
    [ "~DNGConvertSettings", "classDigikam_1_1DNGConvertSettings.html#a24e38ba45f49bf3dcdf9d1ab48f223c2", null ],
    [ "readSettings", "classDigikam_1_1DNGConvertSettings.html#adb4f5543f3252c3bbf6960a29b0c7a05", null ],
    [ "saveSettings", "classDigikam_1_1DNGConvertSettings.html#a13c3a4a5f32600fea2d5e3dc9f37bd26", null ],
    [ "settings", "classDigikam_1_1DNGConvertSettings.html#adc81e98f1f3b893c8c9f749af8ae3de3", null ],
    [ "signalDownloadNameChanged", "classDigikam_1_1DNGConvertSettings.html#a7e58dd55ef8b07751a8174d9c0427314", null ]
];