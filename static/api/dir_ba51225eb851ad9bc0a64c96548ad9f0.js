var dir_ba51225eb851ad9bc0a64c96548ad9f0 =
[
    [ "gpscorrelatorwidget.cpp", "gpscorrelatorwidget_8cpp.html", null ],
    [ "gpscorrelatorwidget.h", "gpscorrelatorwidget_8h.html", [
      [ "GPSCorrelatorWidget", "classDigikam_1_1GPSCorrelatorWidget.html", "classDigikam_1_1GPSCorrelatorWidget" ]
    ] ],
    [ "gpsdatacontainer.h", "gpsdatacontainer_8h.html", [
      [ "GPSDataContainer", "classDigikam_1_1GPSDataContainer.html", "classDigikam_1_1GPSDataContainer" ]
    ] ],
    [ "track_correlator.cpp", "track__correlator_8cpp.html", null ],
    [ "track_correlator.h", "track__correlator_8h.html", [
      [ "TrackCorrelator", "classDigikam_1_1TrackCorrelator.html", "classDigikam_1_1TrackCorrelator" ],
      [ "Correlation", "classDigikam_1_1TrackCorrelator_1_1Correlation.html", "classDigikam_1_1TrackCorrelator_1_1Correlation" ],
      [ "CorrelationOptions", "classDigikam_1_1TrackCorrelator_1_1CorrelationOptions.html", "classDigikam_1_1TrackCorrelator_1_1CorrelationOptions" ]
    ] ],
    [ "track_correlator_thread.cpp", "track__correlator__thread_8cpp.html", "track__correlator__thread_8cpp" ],
    [ "track_correlator_thread.h", "track__correlator__thread_8h.html", [
      [ "TrackCorrelatorThread", "classDigikam_1_1TrackCorrelatorThread.html", "classDigikam_1_1TrackCorrelatorThread" ]
    ] ],
    [ "track_listmodel.cpp", "track__listmodel_8cpp.html", "track__listmodel_8cpp" ],
    [ "track_listmodel.h", "track__listmodel_8h.html", [
      [ "TrackListModel", "classDigikam_1_1TrackListModel.html", "classDigikam_1_1TrackListModel" ]
    ] ]
];