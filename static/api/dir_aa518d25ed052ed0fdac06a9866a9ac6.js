var dir_aa518d25ed052ed0fdac06a9866a9ac6 =
[
    [ "redeyecorrectioncontainer.cpp", "redeyecorrectioncontainer_8cpp.html", null ],
    [ "redeyecorrectioncontainer.h", "redeyecorrectioncontainer_8h.html", [
      [ "RedEyeCorrectionContainer", "classDigikam_1_1RedEyeCorrectionContainer.html", "classDigikam_1_1RedEyeCorrectionContainer" ]
    ] ],
    [ "redeyecorrectionfilter.cpp", "redeyecorrectionfilter_8cpp.html", null ],
    [ "redeyecorrectionfilter.h", "redeyecorrectionfilter_8h.html", [
      [ "RedEyeCorrectionFilter", "classDigikam_1_1RedEyeCorrectionFilter.html", "classDigikam_1_1RedEyeCorrectionFilter" ]
    ] ],
    [ "redeyecorrectionsettings.cpp", "redeyecorrectionsettings_8cpp.html", null ],
    [ "redeyecorrectionsettings.h", "redeyecorrectionsettings_8h.html", [
      [ "RedEyeCorrectionSettings", "classDigikam_1_1RedEyeCorrectionSettings.html", "classDigikam_1_1RedEyeCorrectionSettings" ]
    ] ]
];