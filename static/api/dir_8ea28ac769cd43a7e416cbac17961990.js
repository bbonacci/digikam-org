var dir_8ea28ac769cd43a7e416cbac17961990 =
[
    [ "iptccategories.cpp", "iptccategories_8cpp.html", null ],
    [ "iptccategories.h", "iptccategories_8h.html", [
      [ "IPTCCategories", "classDigikamGenericMetadataEditPlugin_1_1IPTCCategories.html", "classDigikamGenericMetadataEditPlugin_1_1IPTCCategories" ]
    ] ],
    [ "iptccontent.cpp", "iptccontent_8cpp.html", null ],
    [ "iptccontent.h", "iptccontent_8h.html", [
      [ "IPTCContent", "classDigikamGenericMetadataEditPlugin_1_1IPTCContent.html", "classDigikamGenericMetadataEditPlugin_1_1IPTCContent" ]
    ] ],
    [ "iptccredits.cpp", "iptccredits_8cpp.html", null ],
    [ "iptccredits.h", "iptccredits_8h.html", [
      [ "IPTCCredits", "classDigikamGenericMetadataEditPlugin_1_1IPTCCredits.html", "classDigikamGenericMetadataEditPlugin_1_1IPTCCredits" ]
    ] ],
    [ "iptceditwidget.cpp", "iptceditwidget_8cpp.html", null ],
    [ "iptceditwidget.h", "iptceditwidget_8h.html", [
      [ "IPTCEditWidget", "classDigikamGenericMetadataEditPlugin_1_1IPTCEditWidget.html", "classDigikamGenericMetadataEditPlugin_1_1IPTCEditWidget" ]
    ] ],
    [ "iptcenvelope.cpp", "iptcenvelope_8cpp.html", null ],
    [ "iptcenvelope.h", "iptcenvelope_8h.html", [
      [ "IPTCEnvelope", "classDigikamGenericMetadataEditPlugin_1_1IPTCEnvelope.html", "classDigikamGenericMetadataEditPlugin_1_1IPTCEnvelope" ]
    ] ],
    [ "iptckeywords.cpp", "iptckeywords_8cpp.html", null ],
    [ "iptckeywords.h", "iptckeywords_8h.html", [
      [ "IPTCKeywords", "classDigikamGenericMetadataEditPlugin_1_1IPTCKeywords.html", "classDigikamGenericMetadataEditPlugin_1_1IPTCKeywords" ]
    ] ],
    [ "iptcorigin.cpp", "iptcorigin_8cpp.html", null ],
    [ "iptcorigin.h", "iptcorigin_8h.html", [
      [ "IPTCOrigin", "classDigikamGenericMetadataEditPlugin_1_1IPTCOrigin.html", "classDigikamGenericMetadataEditPlugin_1_1IPTCOrigin" ]
    ] ],
    [ "iptcproperties.cpp", "iptcproperties_8cpp.html", null ],
    [ "iptcproperties.h", "iptcproperties_8h.html", [
      [ "IPTCProperties", "classDigikamGenericMetadataEditPlugin_1_1IPTCProperties.html", "classDigikamGenericMetadataEditPlugin_1_1IPTCProperties" ]
    ] ],
    [ "iptcstatus.cpp", "iptcstatus_8cpp.html", null ],
    [ "iptcstatus.h", "iptcstatus_8h.html", [
      [ "IPTCStatus", "classDigikamGenericMetadataEditPlugin_1_1IPTCStatus.html", "classDigikamGenericMetadataEditPlugin_1_1IPTCStatus" ]
    ] ],
    [ "iptcsubjects.cpp", "iptcsubjects_8cpp.html", null ],
    [ "iptcsubjects.h", "iptcsubjects_8h.html", [
      [ "IPTCSubjects", "classDigikamGenericMetadataEditPlugin_1_1IPTCSubjects.html", "classDigikamGenericMetadataEditPlugin_1_1IPTCSubjects" ]
    ] ]
];