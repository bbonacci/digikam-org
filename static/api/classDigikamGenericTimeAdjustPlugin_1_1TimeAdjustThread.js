var classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread =
[
    [ "TimeAdjustThread", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html#a2b5cfbf20002877fab390e95580a9ef2", null ],
    [ "~TimeAdjustThread", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html#a54d2894556eb882a1a8774aa32734ce3", null ],
    [ "appendJobs", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html#a354a7c86ac83b4aa46e723149032a471", null ],
    [ "cancel", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html#a577d6798624415894fc6c22e85fcd3d3", null ],
    [ "indexForUrl", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html#a97d01b618154f09dd33ff6bbd985afad", null ],
    [ "isEmpty", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html#a61f8a4a39061aed1ec1c105a0eddad67", null ],
    [ "maximumNumberOfThreads", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html#ae1dc672ceb1c9f127db1d53fe6d40700", null ],
    [ "pendingCount", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html#abf47dc040dce8ea4779ecb8c1fb375d0", null ],
    [ "readTimestamp", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html#a5309a782f7f59949cf63805f34151614", null ],
    [ "run", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html#a2373a97a31a8897956bf86b4abebff09", null ],
    [ "setDefaultMaximumNumberOfThreads", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html#a99e7e530af4825351ba34362385e08fa", null ],
    [ "setMaximumNumberOfThreads", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html#a3c5aecc58f8d60ea5d2f8d35ef5649c2", null ],
    [ "setPreviewDates", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html#ab405b76f76a5b4fc67b6c69c747fe937", null ],
    [ "setSettings", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html#a11c1bac41271641ab0335245b3417b53", null ],
    [ "setUpdatedDates", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html#a8bb85d0e6ededd21642002c64820ca3e", null ],
    [ "signalDateTimeForUrl", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html#a66b0565153fa2cc2b6b7305f8ee243b8", null ],
    [ "signalPreviewReady", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html#a8aa4f1786dd98361c15268ae06afc168", null ],
    [ "signalProcessEnded", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html#ad411d7c2ccddb4c466c3583dafc56e64", null ],
    [ "signalProcessStarted", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html#acd91c523e254eecfeba54fb9dc7857d9", null ],
    [ "slotJobFinished", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html#a90f7300fc37ec60e08a3101b68da6409", null ]
];