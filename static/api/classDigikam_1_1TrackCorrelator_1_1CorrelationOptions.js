var classDigikam_1_1TrackCorrelator_1_1CorrelationOptions =
[
    [ "CorrelationOptions", "classDigikam_1_1TrackCorrelator_1_1CorrelationOptions.html#a57b5b1bae38962a35c438b2640fbbfcd", null ],
    [ "interpolate", "classDigikam_1_1TrackCorrelator_1_1CorrelationOptions.html#aa4a8e3e56ed8e31c3293cb0253d37e70", null ],
    [ "interpolationDstTime", "classDigikam_1_1TrackCorrelator_1_1CorrelationOptions.html#ac1fdf445884f95882a30ab85be2f9fe4", null ],
    [ "maxGapTime", "classDigikam_1_1TrackCorrelator_1_1CorrelationOptions.html#ae6f0c963637da8a9c5babb47f95e4923", null ],
    [ "secondsOffset", "classDigikam_1_1TrackCorrelator_1_1CorrelationOptions.html#a2c3bf813b96116fff1903b597eae19ad", null ],
    [ "timeZoneOffset", "classDigikam_1_1TrackCorrelator_1_1CorrelationOptions.html#a8dacc806e7bdf4135f9637ab73bbb33b", null ]
];