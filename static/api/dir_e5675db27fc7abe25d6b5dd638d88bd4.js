var dir_e5675db27fc7abe25d6b5dd638d88bd4 =
[
    [ "digikam-lcms.cpp", "digikam-lcms_8cpp.html", "digikam-lcms_8cpp" ],
    [ "digikam-lcms.h", "digikam-lcms_8h.html", "digikam-lcms_8h" ],
    [ "iccmanager.cpp", "iccmanager_8cpp.html", null ],
    [ "iccmanager.h", "iccmanager_8h.html", [
      [ "IccManager", "classDigikam_1_1IccManager.html", "classDigikam_1_1IccManager" ]
    ] ],
    [ "iccprofile.cpp", "iccprofile_8cpp.html", null ],
    [ "iccprofile.h", "iccprofile_8h.html", [
      [ "IccProfile", "classDigikam_1_1IccProfile.html", "classDigikam_1_1IccProfile" ],
      [ "LcmsLock", "classDigikam_1_1LcmsLock.html", "classDigikam_1_1LcmsLock" ]
    ] ],
    [ "iccprofilesettings.cpp", "iccprofilesettings_8cpp.html", null ],
    [ "iccprofilesettings.h", "iccprofilesettings_8h.html", [
      [ "IccProfilesSettings", "classDigikam_1_1IccProfilesSettings.html", "classDigikam_1_1IccProfilesSettings" ]
    ] ],
    [ "iccsettings.cpp", "iccsettings_8cpp.html", null ],
    [ "iccsettings.h", "iccsettings_8h.html", [
      [ "IccSettings", "classDigikam_1_1IccSettings.html", "classDigikam_1_1IccSettings" ]
    ] ],
    [ "iccsettings_p.cpp", "iccsettings__p_8cpp.html", null ],
    [ "iccsettings_p.h", "iccsettings__p_8h.html", [
      [ "Private", "classDigikam_1_1IccSettings_1_1Private.html", "classDigikam_1_1IccSettings_1_1Private" ]
    ] ],
    [ "iccsettings_p_qt5.cpp", "iccsettings__p__qt5_8cpp.html", null ],
    [ "iccsettings_p_qt6.cpp", "iccsettings__p__qt6_8cpp.html", null ],
    [ "iccsettingscontainer.cpp", "iccsettingscontainer_8cpp.html", null ],
    [ "iccsettingscontainer.h", "iccsettingscontainer_8h.html", [
      [ "ICCSettingsContainer", "classDigikam_1_1ICCSettingsContainer.html", "classDigikam_1_1ICCSettingsContainer" ]
    ] ],
    [ "icctransform.cpp", "icctransform_8cpp.html", null ],
    [ "icctransform.h", "icctransform_8h.html", [
      [ "IccTransform", "classDigikam_1_1IccTransform.html", "classDigikam_1_1IccTransform" ]
    ] ],
    [ "icctransformfilter.cpp", "icctransformfilter_8cpp.html", null ],
    [ "icctransformfilter.h", "icctransformfilter_8h.html", [
      [ "IccTransformFilter", "classDigikam_1_1IccTransformFilter.html", "classDigikam_1_1IccTransformFilter" ]
    ] ]
];