var classDigikamGenericPresentationPlugin_1_1PresentationMainPage =
[
    [ "PresentationMainPage", "classDigikamGenericPresentationPlugin_1_1PresentationMainPage.html#a133e006dd4fe82baee4f73a80df2e0d5", null ],
    [ "~PresentationMainPage", "classDigikamGenericPresentationPlugin_1_1PresentationMainPage.html#a5caec433c7a66261de0da47d0dfcfaa0", null ],
    [ "readSettings", "classDigikamGenericPresentationPlugin_1_1PresentationMainPage.html#ad32f04f8108a3d21c612ed9aac1c99da", null ],
    [ "removeImageFromList", "classDigikamGenericPresentationPlugin_1_1PresentationMainPage.html#a0ae505cef29532e585bb7fdc9fe68f30", null ],
    [ "saveSettings", "classDigikamGenericPresentationPlugin_1_1PresentationMainPage.html#a3c99ab7bc967deec8a8b077a843b29b5", null ],
    [ "signalTotalTimeChanged", "classDigikamGenericPresentationPlugin_1_1PresentationMainPage.html#a6017d140a47235b9b8297bcd0b991a7d", null ],
    [ "updateUrlList", "classDigikamGenericPresentationPlugin_1_1PresentationMainPage.html#af81967c95eee465ebf768e5a5a495a62", null ]
];