var classDigikam_1_1EditorCore =
[
    [ "Private", "classDigikam_1_1EditorCore_1_1Private.html", "classDigikam_1_1EditorCore_1_1Private" ],
    [ "EditorCore", "classDigikam_1_1EditorCore.html#a23470ad14bcf57cd6a1c441e32c67b70", null ],
    [ "~EditorCore", "classDigikam_1_1EditorCore.html#a427fe3846e3d05516cb0844af59fb674", null ],
    [ "abortSaving", "classDigikam_1_1EditorCore.html#a23c525ee1838e425d5d587c1ebc8ef78", null ],
    [ "applyTransform", "classDigikam_1_1EditorCore.html#ad72d4d519038b437768bf82533fd6cf5", null ],
    [ "availableRedoSteps", "classDigikam_1_1EditorCore.html#a29ad1fb4479579a511fb80e80ed75946", null ],
    [ "availableUndoSteps", "classDigikam_1_1EditorCore.html#a42001ce1a302edea40be702c22f46a03", null ],
    [ "bytesDepth", "classDigikam_1_1EditorCore.html#a7d5a75ba7ab040ea6f6763dad0e5a415", null ],
    [ "clearUndoManager", "classDigikam_1_1EditorCore.html#a4038f034b06bac6b958652a7013be157", null ],
    [ "convertDepth", "classDigikam_1_1EditorCore.html#af1e296ab14593140dc2d1825f00a6c64", null ],
    [ "convertToPixmap", "classDigikam_1_1EditorCore.html#a3991f0245e6c051b3efe77acc1227b19", null ],
    [ "crop", "classDigikam_1_1EditorCore.html#af71c9cc8c5bd7b51cbefc0ee812165b3", null ],
    [ "ensureHasCurrentUuid", "classDigikam_1_1EditorCore.html#a7028872460501dfb84d3f2c0c11a2899", null ],
    [ "exifRotated", "classDigikam_1_1EditorCore.html#a9a1c96618150b0e3c5a7506ca4d39586", null ],
    [ "flipHoriz", "classDigikam_1_1EditorCore.html#a60645426eddc7ce89834e58e6ef13272", null ],
    [ "flipVert", "classDigikam_1_1EditorCore.html#a99e42b00f25c418d03016ebf29eaa838", null ],
    [ "getEmbeddedICC", "classDigikam_1_1EditorCore.html#ad73009ac9fcba88b74ba79315e117a8f", null ],
    [ "getExposureSettings", "classDigikam_1_1EditorCore.html#a07be1e6c2ec911ca654330bf5a469eab", null ],
    [ "getICCSettings", "classDigikam_1_1EditorCore.html#a3aa2fc7a413f2ca8984267752b5aa5a4", null ],
    [ "getImageFileName", "classDigikam_1_1EditorCore.html#a3effe54b4781b757d48875ba3a2bdb99", null ],
    [ "getImageFilePath", "classDigikam_1_1EditorCore.html#adf530d888017f819ddced468ac5819b5", null ],
    [ "getImageFormat", "classDigikam_1_1EditorCore.html#a95518c797a87866a77c0914a529db386", null ],
    [ "getImageHistoryOfFullRedo", "classDigikam_1_1EditorCore.html#ae8fb57502cb1dbdd938e22a7abd0056f", null ],
    [ "getImg", "classDigikam_1_1EditorCore.html#ac2caeb8df5a3b576c36374190a72acc0", null ],
    [ "getImgSelection", "classDigikam_1_1EditorCore.html#a15bd8e3a005a9aab4022922664bbee4f", null ],
    [ "getInitialImageHistory", "classDigikam_1_1EditorCore.html#a5700b93590881503151ed2b005c45d84", null ],
    [ "getItemHistory", "classDigikam_1_1EditorCore.html#afa7ce199069878798f406dc43f4ae240", null ],
    [ "getMetadata", "classDigikam_1_1EditorCore.html#a3d786091891a06281f72920288c37d8f", null ],
    [ "getRedoHistory", "classDigikam_1_1EditorCore.html#a0c096da4defbbd9a34e452cab6e731b1", null ],
    [ "getResolvedInitialHistory", "classDigikam_1_1EditorCore.html#a895e24c47c5f125a7540386003bb9879", null ],
    [ "getSelectedArea", "classDigikam_1_1EditorCore.html#aea5602d0844e860ad77834ebc2cee28d", null ],
    [ "getUndoHistory", "classDigikam_1_1EditorCore.html#a3655962a2c5c5ebf636068764d2de4d3", null ],
    [ "hasAlpha", "classDigikam_1_1EditorCore.html#a89a5ce82eb665944e9933767cff934e8", null ],
    [ "height", "classDigikam_1_1EditorCore.html#a491e9dd7f81797492014fcb6abe4d3aa", null ],
    [ "imageUndoChanged", "classDigikam_1_1EditorCore.html#aa9e39ed85f21eb07c641c361f2df86d2", null ],
    [ "isReadOnly", "classDigikam_1_1EditorCore.html#a2314a1f8a561cf0f3211cd45b1a7dd95", null ],
    [ "isValid", "classDigikam_1_1EditorCore.html#a86ab02c6d5ceb8396a413bfed0a10ac6", null ],
    [ "load", "classDigikam_1_1EditorCore.html#a1e68e815757c0d46a6a4899d5be5f1c6", null ],
    [ "origHeight", "classDigikam_1_1EditorCore.html#ab515d21aeaf76d7a64eb820d700a7a3f", null ],
    [ "origWidth", "classDigikam_1_1EditorCore.html#a2ac732f60e8a340fbce56fd6f627771d", null ],
    [ "provideCurrentUuid", "classDigikam_1_1EditorCore.html#adc39b12f33aedd2f8fc05e2a69a7b97b", null ],
    [ "putIccProfile", "classDigikam_1_1EditorCore.html#a38db65e478765424dc9e2570be2cdd55", null ],
    [ "putImg", "classDigikam_1_1EditorCore.html#a01a8708fdbe261d2294d6f80cfb56705", null ],
    [ "putImgSelection", "classDigikam_1_1EditorCore.html#a9d7830289b3bca92f1f540ac049c9d4a", null ],
    [ "readMetadataFromFile", "classDigikam_1_1EditorCore.html#a1338adbf0b065ab735fe5e8b20194beb", null ],
    [ "redo", "classDigikam_1_1EditorCore.html#acae611638bb9c028bffcede5daffefbf", null ],
    [ "resetImage", "classDigikam_1_1EditorCore.html#ac76e9849f08437684c5a2edeb6fd109e", null ],
    [ "restore", "classDigikam_1_1EditorCore.html#ae3a0a5c9036fdda05634df37b46f6335", null ],
    [ "rollbackToOrigin", "classDigikam_1_1EditorCore.html#ad21a6015bd14a2451ff089f395f411aa", null ],
    [ "rotate180", "classDigikam_1_1EditorCore.html#ae2474f785a5bb81d66b9e91510274bb3", null ],
    [ "rotate270", "classDigikam_1_1EditorCore.html#a5af550d4eaa744867549e4ebb24d7879", null ],
    [ "rotate90", "classDigikam_1_1EditorCore.html#aec56afdbca2710d476da05eac2a5b5e5", null ],
    [ "saveAs", "classDigikam_1_1EditorCore.html#a0d9baa862de521785de0f92f81b50f8d", null ],
    [ "saveAs", "classDigikam_1_1EditorCore.html#a3b81b622535aa17267c24c176ef35dc4", null ],
    [ "setDisplayingWidget", "classDigikam_1_1EditorCore.html#ab0175b21428f5275e9ac577338625a13", null ],
    [ "setExifOrient", "classDigikam_1_1EditorCore.html#aaafa5c1568d721eec8bed3816357f014", null ],
    [ "setExposureSettings", "classDigikam_1_1EditorCore.html#a976040a776c35de3bd27a723e073b471", null ],
    [ "setFileOriginData", "classDigikam_1_1EditorCore.html#a690bac6ac84367cb1b12e9e13882938e", null ],
    [ "setHistoryIsBranch", "classDigikam_1_1EditorCore.html#afb37c514ca7b8c189b30049e18a9eb92", null ],
    [ "setICCSettings", "classDigikam_1_1EditorCore.html#a182ef2b5892bd4ab804b425654c442ca", null ],
    [ "setLastSaved", "classDigikam_1_1EditorCore.html#afdffd869c65f58b24b8c1672e84c67d2", null ],
    [ "setModified", "classDigikam_1_1EditorCore.html#a339b02818d195609c4c7a7a176160e64", null ],
    [ "setResolvedInitialHistory", "classDigikam_1_1EditorCore.html#a9814329ab9449ba5eac3f6b770d85c3a", null ],
    [ "setSelectedArea", "classDigikam_1_1EditorCore.html#a250d8e8fb61865b6d4fb07be6a701bad", null ],
    [ "setSoftProofingEnabled", "classDigikam_1_1EditorCore.html#ae6d9ad063c8e896b5555ce77f8801789", null ],
    [ "setUndoImg", "classDigikam_1_1EditorCore.html#a4806c23caca5b8f3a2c4a4588209fc18", null ],
    [ "setUndoManagerOrigin", "classDigikam_1_1EditorCore.html#aef3ae0f0ea02e2dbd1016722705c771c", null ],
    [ "signalFileOriginChanged", "classDigikam_1_1EditorCore.html#a84e0cca42145f2c31d3319e5a7fab1cc", null ],
    [ "signalImageLoaded", "classDigikam_1_1EditorCore.html#a4f281be9ff973d08a197f589703148e9", null ],
    [ "signalImageSaved", "classDigikam_1_1EditorCore.html#a485b13eb96fdc4c90eefc313a54aa206", null ],
    [ "signalLoadingProgress", "classDigikam_1_1EditorCore.html#ab4a5c807567f0f95d0bb0886de420418", null ],
    [ "signalLoadingStarted", "classDigikam_1_1EditorCore.html#ae5bb504bfb182b56599509bc09d86ce8", null ],
    [ "signalModified", "classDigikam_1_1EditorCore.html#a1523972842ce17c8657ddc49b8775a8e", null ],
    [ "signalSavingProgress", "classDigikam_1_1EditorCore.html#ac1329289267cddc2e08c1137606850d6", null ],
    [ "signalSavingStarted", "classDigikam_1_1EditorCore.html#a76a9d58158354a28105ed773828af332", null ],
    [ "signalUndoStateChanged", "classDigikam_1_1EditorCore.html#aa10a97be3cfcd1312b4919eac1de87d8", null ],
    [ "sixteenBit", "classDigikam_1_1EditorCore.html#a8f322f95533ee3779197a8ccfe015504", null ],
    [ "slotImageLoaded", "classDigikam_1_1EditorCore.html#a7531696cf0c8a296c9c729d4b06d4118", null ],
    [ "slotImageSaved", "classDigikam_1_1EditorCore.html#adcb2d0f7b46771fc61eb47ceec365343", null ],
    [ "slotLoadingProgress", "classDigikam_1_1EditorCore.html#a1f1ea7fb6aaa4710214f83d1619389d8", null ],
    [ "slotSavingProgress", "classDigikam_1_1EditorCore.html#a3232e2ad0bfd6a07d566b56ef259fc09", null ],
    [ "softProofingEnabled", "classDigikam_1_1EditorCore.html#a37d494a6455442e348e65e58042c1f56", null ],
    [ "switchToLastSaved", "classDigikam_1_1EditorCore.html#aeb36ce9896c7cb73e4b56979d40860a2", null ],
    [ "undo", "classDigikam_1_1EditorCore.html#aff363305b2f3dda1df9c6d0a2e93ff57", null ],
    [ "undoState", "classDigikam_1_1EditorCore.html#aadf48b202470cf0ee372ee9f3af49fd7", null ],
    [ "width", "classDigikam_1_1EditorCore.html#af7a7a46e1367d6052e223f199261c868", null ],
    [ "zoom", "classDigikam_1_1EditorCore.html#aba90bc70175aca79e0ec566224c3518b", null ]
];