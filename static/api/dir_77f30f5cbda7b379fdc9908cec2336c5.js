var dir_77f30f5cbda7b379fdc9908cec2336c5 =
[
    [ "dplugin.cpp", "dplugin_8cpp.html", null ],
    [ "dplugin.h", "dplugin_8h.html", "dplugin_8h" ],
    [ "dpluginaction.cpp", "dpluginaction_8cpp.html", null ],
    [ "dpluginaction.h", "dpluginaction_8h.html", [
      [ "DPluginAction", "classDigikam_1_1DPluginAction.html", "classDigikam_1_1DPluginAction" ]
    ] ],
    [ "dpluginauthor.cpp", "dpluginauthor_8cpp.html", null ],
    [ "dpluginauthor.h", "dpluginauthor_8h.html", [
      [ "DPluginAuthor", "classDigikam_1_1DPluginAuthor.html", "classDigikam_1_1DPluginAuthor" ]
    ] ],
    [ "dplugindimg.cpp", "dplugindimg_8cpp.html", null ],
    [ "dplugindimg.h", "dplugindimg_8h.html", [
      [ "DPluginDImg", "classDigikam_1_1DPluginDImg.html", "classDigikam_1_1DPluginDImg" ]
    ] ],
    [ "dplugineditor.cpp", "dplugineditor_8cpp.html", null ],
    [ "dplugineditor.h", "dplugineditor_8h.html", [
      [ "DPluginEditor", "classDigikam_1_1DPluginEditor.html", "classDigikam_1_1DPluginEditor" ]
    ] ],
    [ "dplugingeneric.cpp", "dplugingeneric_8cpp.html", null ],
    [ "dplugingeneric.h", "dplugingeneric_8h.html", [
      [ "DPluginGeneric", "classDigikam_1_1DPluginGeneric.html", "classDigikam_1_1DPluginGeneric" ]
    ] ],
    [ "dpluginrawimport.cpp", "dpluginrawimport_8cpp.html", null ],
    [ "dpluginrawimport.h", "dpluginrawimport_8h.html", [
      [ "DPluginRawImport", "classDigikam_1_1DPluginRawImport.html", "classDigikam_1_1DPluginRawImport" ]
    ] ]
];