var panoactions_8h =
[
    [ "PanoActionData", "structDigikamGenericPanoramaPlugin_1_1PanoActionData.html", "structDigikamGenericPanoramaPlugin_1_1PanoActionData" ],
    [ "PanoramaPreprocessedUrls", "classDigikamGenericPanoramaPlugin_1_1PanoramaPreprocessedUrls.html", "classDigikamGenericPanoramaPlugin_1_1PanoramaPreprocessedUrls" ],
    [ "PanoramaItemUrlsMap", "panoactions_8h.html#a3cd88edfce8112c283866f63b4f1cf63", null ],
    [ "PanoAction", "panoactions_8h.html#a16637a5b27722b25bdf213184ea12722", [
      [ "PANO_NONE", "panoactions_8h.html#a16637a5b27722b25bdf213184ea12722a43664bba5332e596bc9b73f9df43242a", null ],
      [ "PANO_PREPROCESS_INPUT", "panoactions_8h.html#a16637a5b27722b25bdf213184ea12722a4a2814633ccc7b79037f63d9d88562e4", null ],
      [ "PANO_CREATEPTO", "panoactions_8h.html#a16637a5b27722b25bdf213184ea12722ae04913639b8620672760751ca553cb75", null ],
      [ "PANO_CPFIND", "panoactions_8h.html#a16637a5b27722b25bdf213184ea12722af253807e24dfec621b39c7a3a818fc48", null ],
      [ "PANO_CPCLEAN", "panoactions_8h.html#a16637a5b27722b25bdf213184ea12722a1914b433ed46dafedd2a68e1a86263aa", null ],
      [ "PANO_OPTIMIZE", "panoactions_8h.html#a16637a5b27722b25bdf213184ea12722a5de39d213f18e985d37d1ed238c2c56e", null ],
      [ "PANO_AUTOCROP", "panoactions_8h.html#a16637a5b27722b25bdf213184ea12722a0969ec228f1229e6bcc79ce855ac6aed", null ],
      [ "PANO_CREATEPREVIEWPTO", "panoactions_8h.html#a16637a5b27722b25bdf213184ea12722aae14876bfd76423cfb31282f5a6a2633", null ],
      [ "PANO_CREATEMK", "panoactions_8h.html#a16637a5b27722b25bdf213184ea12722aa8e5b06c0a7eb6140cd65f6b5324ef6f", null ],
      [ "PANO_CREATEMKPREVIEW", "panoactions_8h.html#a16637a5b27722b25bdf213184ea12722a031b8663650bac994cfe2f5591d67c83", null ],
      [ "PANO_CREATEFINALPTO", "panoactions_8h.html#a16637a5b27722b25bdf213184ea12722ab6cb9d0fe042b32930c3ba584d85aa5b", null ],
      [ "PANO_NONAFILE", "panoactions_8h.html#a16637a5b27722b25bdf213184ea12722a8aaf72b9fdf052d657623d18a05c56ba", null ],
      [ "PANO_NONAFILEPREVIEW", "panoactions_8h.html#a16637a5b27722b25bdf213184ea12722a78db7f68c629a05a039e939ea9b07b89", null ],
      [ "PANO_STITCH", "panoactions_8h.html#a16637a5b27722b25bdf213184ea12722ac41b9cb406a2f3b8053413e0015597e4", null ],
      [ "PANO_STITCHPREVIEW", "panoactions_8h.html#a16637a5b27722b25bdf213184ea12722a4126810bfec617b4e880a55f3aebeca2", null ],
      [ "PANO_HUGINEXECUTOR", "panoactions_8h.html#a16637a5b27722b25bdf213184ea12722a6d577ff1466df384cac14a0a9078803b", null ],
      [ "PANO_HUGINEXECUTORPREVIEW", "panoactions_8h.html#a16637a5b27722b25bdf213184ea12722a7efa4fb2b2a8de1edc8fa59da56a6475", null ],
      [ "PANO_COPY", "panoactions_8h.html#a16637a5b27722b25bdf213184ea12722a47d5a415b6aec69adb818a34412a667a", null ]
    ] ],
    [ "PanoramaFileType", "panoactions_8h.html#a11206854557bf007056b9e4159a98a11", [
      [ "JPEG", "panoactions_8h.html#a11206854557bf007056b9e4159a98a11acc602c3e08b6e938c4fe2b07d2b6d2eb", null ],
      [ "TIFF", "panoactions_8h.html#a11206854557bf007056b9e4159a98a11adb025fc2d1ec594177b8ebf365cdeaec", null ],
      [ "HDR", "panoactions_8h.html#a11206854557bf007056b9e4159a98a11aa8d6a54ea3b30483893a85170b37fcba", null ]
    ] ]
];