var classDigikam_1_1AdvancedMetadataTab =
[
    [ "AdvancedMetadataTab", "classDigikam_1_1AdvancedMetadataTab.html#a1bb96e61064121e4cdb1e81b92fac810", null ],
    [ "~AdvancedMetadataTab", "classDigikam_1_1AdvancedMetadataTab.html#a1d22a6859a906c5a4ef60aaf46cd9b7f", null ],
    [ "applySettings", "classDigikam_1_1AdvancedMetadataTab.html#a8828c05aa3e75a2eb1d1ae7a5529dbd5", null ],
    [ "slotAddNewNamespace", "classDigikam_1_1AdvancedMetadataTab.html#a270d63b87a5e382e3452e74d6044d4e4", null ],
    [ "slotEditNamespace", "classDigikam_1_1AdvancedMetadataTab.html#aa00fc7e7c3034726fa94e2cc85ab4db8", null ],
    [ "slotResetToDefault", "classDigikam_1_1AdvancedMetadataTab.html#ac78cc44db739c4639cc7beb5d66f6ca7", null ],
    [ "slotRevertChanges", "classDigikam_1_1AdvancedMetadataTab.html#ae1632aa8e2ea32286662ac4b2bfb2831", null ]
];