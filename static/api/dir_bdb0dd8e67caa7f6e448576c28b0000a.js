var dir_bdb0dd8e67caa7f6e448576c28b0000a =
[
    [ "metaengine.cpp", "metaengine_8cpp.html", null ],
    [ "metaengine.h", "metaengine_8h.html", [
      [ "MetaEngine", "classDigikam_1_1MetaEngine.html", "classDigikam_1_1MetaEngine" ]
    ] ],
    [ "metaengine_comments.cpp", "metaengine__comments_8cpp.html", null ],
    [ "metaengine_data.cpp", "metaengine__data_8cpp.html", null ],
    [ "metaengine_data.h", "metaengine__data_8h.html", [
      [ "MetaEngineData", "classDigikam_1_1MetaEngineData.html", "classDigikam_1_1MetaEngineData" ]
    ] ],
    [ "metaengine_data_p.cpp", "metaengine__data__p_8cpp.html", null ],
    [ "metaengine_data_p.h", "metaengine__data__p_8h.html", [
      [ "Private", "classDigikam_1_1MetaEngineData_1_1Private.html", "classDigikam_1_1MetaEngineData_1_1Private" ]
    ] ],
    [ "metaengine_exif.cpp", "metaengine__exif_8cpp.html", null ],
    [ "metaengine_fileio.cpp", "metaengine__fileio_8cpp.html", null ],
    [ "metaengine_gps.cpp", "metaengine__gps_8cpp.html", null ],
    [ "metaengine_iptc.cpp", "metaengine__iptc_8cpp.html", null ],
    [ "metaengine_item.cpp", "metaengine__item_8cpp.html", null ],
    [ "metaengine_mergehelper.h", "metaengine__mergehelper_8h.html", [
      [ "ExifMetaEngineMergeHelper", "classDigikam_1_1ExifMetaEngineMergeHelper.html", "classDigikam_1_1ExifMetaEngineMergeHelper" ],
      [ "IptcMetaEngineMergeHelper", "classDigikam_1_1IptcMetaEngineMergeHelper.html", "classDigikam_1_1IptcMetaEngineMergeHelper" ],
      [ "MetaEngineMergeHelper", "classDigikam_1_1MetaEngineMergeHelper.html", "classDigikam_1_1MetaEngineMergeHelper" ]
    ] ],
    [ "metaengine_p.cpp", "metaengine__p_8cpp.html", "metaengine__p_8cpp" ],
    [ "metaengine_p.h", "metaengine__p_8h.html", "metaengine__p_8h" ],
    [ "metaengine_previews.cpp", "metaengine__previews_8cpp.html", null ],
    [ "metaengine_previews.h", "metaengine__previews_8h.html", [
      [ "MetaEnginePreviews", "classDigikam_1_1MetaEnginePreviews.html", "classDigikam_1_1MetaEnginePreviews" ]
    ] ],
    [ "metaengine_rotation.cpp", "metaengine__rotation_8cpp.html", "metaengine__rotation_8cpp" ],
    [ "metaengine_rotation.h", "metaengine__rotation_8h.html", [
      [ "MetaEngineRotation", "classDigikam_1_1MetaEngineRotation.html", "classDigikam_1_1MetaEngineRotation" ]
    ] ],
    [ "metaengine_xmp.cpp", "metaengine__xmp_8cpp.html", null ],
    [ "metaenginesettings.cpp", "metaenginesettings_8cpp.html", null ],
    [ "metaenginesettings.h", "metaenginesettings_8h.html", [
      [ "MetaEngineSettings", "classDigikam_1_1MetaEngineSettings.html", "classDigikam_1_1MetaEngineSettings" ]
    ] ],
    [ "metaenginesettingscontainer.cpp", "metaenginesettingscontainer_8cpp.html", "metaenginesettingscontainer_8cpp" ],
    [ "metaenginesettingscontainer.h", "metaenginesettingscontainer_8h.html", "metaenginesettingscontainer_8h" ]
];