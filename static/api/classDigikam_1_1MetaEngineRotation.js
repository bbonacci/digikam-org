var classDigikam_1_1MetaEngineRotation =
[
    [ "TransformationAction", "classDigikam_1_1MetaEngineRotation.html#a5c89cad88fd85f894b9bfc9f2cd7b77f", [
      [ "NoTransformation", "classDigikam_1_1MetaEngineRotation.html#a5c89cad88fd85f894b9bfc9f2cd7b77fa078bb0df1ae55678928475cf4ca65223", null ],
      [ "FlipHorizontal", "classDigikam_1_1MetaEngineRotation.html#a5c89cad88fd85f894b9bfc9f2cd7b77fa98e897a54592ed1ee66d5deedc9f6bf1", null ],
      [ "FlipVertical", "classDigikam_1_1MetaEngineRotation.html#a5c89cad88fd85f894b9bfc9f2cd7b77fa0d86880403e7c6c7112343eddc2fbade", null ],
      [ "Rotate90", "classDigikam_1_1MetaEngineRotation.html#a5c89cad88fd85f894b9bfc9f2cd7b77fae7588dbc237342eb78f9226d2eb45be6", null ],
      [ "Rotate180", "classDigikam_1_1MetaEngineRotation.html#a5c89cad88fd85f894b9bfc9f2cd7b77fa683e3b15b0d2ef283a49288ae5f715aa", null ],
      [ "Rotate270", "classDigikam_1_1MetaEngineRotation.html#a5c89cad88fd85f894b9bfc9f2cd7b77fa8e10522310fcb5a68fae9d49cf12ddd4", null ]
    ] ],
    [ "MetaEngineRotation", "classDigikam_1_1MetaEngineRotation.html#af2091dd328de0da211063f6566a2aad5", null ],
    [ "MetaEngineRotation", "classDigikam_1_1MetaEngineRotation.html#a5015e58459252108346d75e60de9b57a", null ],
    [ "MetaEngineRotation", "classDigikam_1_1MetaEngineRotation.html#a58ea30a7a3a7485a80fa5f4b4872c1c6", null ],
    [ "MetaEngineRotation", "classDigikam_1_1MetaEngineRotation.html#aa77578307ea07bcd6f570110cb255ffd", null ],
    [ "exifOrientation", "classDigikam_1_1MetaEngineRotation.html#a1a7a1f9895faf6f41790813209d9edad", null ],
    [ "isNoTransform", "classDigikam_1_1MetaEngineRotation.html#a9fcd4c1065926210b6e6dadf24a4ea57", null ],
    [ "operator!=", "classDigikam_1_1MetaEngineRotation.html#aa7c343840f336495dca1bc544d013f8a", null ],
    [ "operator*=", "classDigikam_1_1MetaEngineRotation.html#a2db6d03cf26e093f2189f2f5d44a30ef", null ],
    [ "operator*=", "classDigikam_1_1MetaEngineRotation.html#a66633e5776bf368671e17093d2b14d11", null ],
    [ "operator*=", "classDigikam_1_1MetaEngineRotation.html#af035849ba18787ac7f6c0d545680eec2", null ],
    [ "operator*=", "classDigikam_1_1MetaEngineRotation.html#abcc379f1c78c2ac8170e088813b06558", null ],
    [ "operator==", "classDigikam_1_1MetaEngineRotation.html#ae9d7bd4ac5690a3fc995d442118cb32f", null ],
    [ "set", "classDigikam_1_1MetaEngineRotation.html#a2bc62ce83636bca6c86a8c920ef27e89", null ],
    [ "toTransform", "classDigikam_1_1MetaEngineRotation.html#a2cf2867e21f1193328f881c0c0aa64cb", null ],
    [ "transformations", "classDigikam_1_1MetaEngineRotation.html#ada0c53f65fff4ab6ce9d98845dbc8a3a", null ],
    [ "m", "classDigikam_1_1MetaEngineRotation.html#a8184e182fec234fe40cdaef5588b5de2", null ]
];