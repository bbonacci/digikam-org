var classDigikam_1_1SharpContainer =
[
    [ "SharpingMethods", "classDigikam_1_1SharpContainer.html#a03d65d274537927e957708cbf07c5eaf", [
      [ "SimpleSharp", "classDigikam_1_1SharpContainer.html#a03d65d274537927e957708cbf07c5eafa2c31eb06f09031f0acc4433a45d8d3b2", null ],
      [ "UnsharpMask", "classDigikam_1_1SharpContainer.html#a03d65d274537927e957708cbf07c5eafaa5682f2968684b1aee77633da60feffe", null ],
      [ "Refocus", "classDigikam_1_1SharpContainer.html#a03d65d274537927e957708cbf07c5eafac23c6c66b9094ae0189ac725920a0264", null ]
    ] ],
    [ "SharpContainer", "classDigikam_1_1SharpContainer.html#ade663673c157fd6db7efdbfcc1eb6d77", null ],
    [ "~SharpContainer", "classDigikam_1_1SharpContainer.html#abc7a71fd8e9b50fe03f62b57b4510cf6", null ],
    [ "method", "classDigikam_1_1SharpContainer.html#aefaaf7859c6daac880c95b227e3ae085", null ],
    [ "rfCorrelation", "classDigikam_1_1SharpContainer.html#a6344a21c799fcddec7a28e88d1c24f35", null ],
    [ "rfGauss", "classDigikam_1_1SharpContainer.html#af1168d53243561a7790fec46ace4ace9", null ],
    [ "rfMatrix", "classDigikam_1_1SharpContainer.html#a924e4e86b8f4087b3dfa9695fec1688f", null ],
    [ "rfNoise", "classDigikam_1_1SharpContainer.html#a001140d60c2f7f03fee15c24d4e22e9f", null ],
    [ "rfRadius", "classDigikam_1_1SharpContainer.html#a496f95a2a870d48a4ad8db296f7eeec4", null ],
    [ "ssRadius", "classDigikam_1_1SharpContainer.html#a5169ca079f0f673b3833ecc928dd60ef", null ],
    [ "umAmount", "classDigikam_1_1SharpContainer.html#afb609ee91fc6932e1377ea53b952d4f0", null ],
    [ "umLumaOnly", "classDigikam_1_1SharpContainer.html#adf1ff9e04691b8495208e9dfbf261c73", null ],
    [ "umRadius", "classDigikam_1_1SharpContainer.html#acae9d685534b4bc12b21b2ae23cfa326", null ],
    [ "umThreshold", "classDigikam_1_1SharpContainer.html#a033a9600245f718045fc21b99e28cf72", null ]
];