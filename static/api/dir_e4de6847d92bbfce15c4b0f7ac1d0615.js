var dir_e4de6847d92bbfce15c4b0f7ac1d0615 =
[
    [ "dbusydlg.cpp", "dbusydlg_8cpp.html", null ],
    [ "dbusydlg.h", "dbusydlg_8h.html", [
      [ "DBusyDlg", "classDigikam_1_1DBusyDlg.html", "classDigikam_1_1DBusyDlg" ],
      [ "DBusyThread", "classDigikam_1_1DBusyThread.html", "classDigikam_1_1DBusyThread" ]
    ] ],
    [ "dconfigdlg.cpp", "dconfigdlg_8cpp.html", null ],
    [ "dconfigdlg.h", "dconfigdlg_8h.html", [
      [ "DConfigDlg", "classDigikam_1_1DConfigDlg.html", "classDigikam_1_1DConfigDlg" ]
    ] ],
    [ "dconfigdlgmngr.cpp", "dconfigdlgmngr_8cpp.html", "dconfigdlgmngr_8cpp" ],
    [ "dconfigdlgmngr.h", "dconfigdlgmngr_8h.html", [
      [ "DConfigDlgMngr", "classDigikam_1_1DConfigDlgMngr.html", "classDigikam_1_1DConfigDlgMngr" ]
    ] ],
    [ "dconfigdlgmodels.cpp", "dconfigdlgmodels_8cpp.html", null ],
    [ "dconfigdlgmodels.h", "dconfigdlgmodels_8h.html", [
      [ "DConfigDlgModel", "classDigikam_1_1DConfigDlgModel.html", "classDigikam_1_1DConfigDlgModel" ],
      [ "DConfigDlgWdgItem", "classDigikam_1_1DConfigDlgWdgItem.html", "classDigikam_1_1DConfigDlgWdgItem" ],
      [ "DConfigDlgWdgModel", "classDigikam_1_1DConfigDlgWdgModel.html", "classDigikam_1_1DConfigDlgWdgModel" ]
    ] ],
    [ "dconfigdlgmodels_p.h", "dconfigdlgmodels__p_8h.html", [
      [ "DConfigDlgModelPrivate", "classDigikam_1_1DConfigDlgModelPrivate.html", "classDigikam_1_1DConfigDlgModelPrivate" ],
      [ "DConfigDlgWdgModelPrivate", "classDigikam_1_1DConfigDlgWdgModelPrivate.html", "classDigikam_1_1DConfigDlgWdgModelPrivate" ],
      [ "PageItem", "classDigikam_1_1PageItem.html", "classDigikam_1_1PageItem" ]
    ] ],
    [ "dconfigdlgview.cpp", "dconfigdlgview_8cpp.html", null ],
    [ "dconfigdlgview.h", "dconfigdlgview_8h.html", [
      [ "DConfigDlgView", "classDigikam_1_1DConfigDlgView.html", "classDigikam_1_1DConfigDlgView" ]
    ] ],
    [ "dconfigdlgview_p.cpp", "dconfigdlgview__p_8cpp.html", null ],
    [ "dconfigdlgview_p.h", "dconfigdlgview__p_8h.html", [
      [ "DConfigDlgListView", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgListView.html", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgListView" ],
      [ "DConfigDlgListViewDelegate", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgListViewDelegate.html", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgListViewDelegate" ],
      [ "DConfigDlgListViewProxy", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgListViewProxy.html", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgListViewProxy" ],
      [ "DConfigDlgPlainView", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgPlainView.html", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgPlainView" ],
      [ "DConfigDlgTabbedView", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgTabbedView.html", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgTabbedView" ],
      [ "DConfigDlgTreeView", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgTreeView.html", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgTreeView" ],
      [ "SelectionModel", "classDigikam_1_1DConfigDlgInternal_1_1SelectionModel.html", "classDigikam_1_1DConfigDlgInternal_1_1SelectionModel" ],
      [ "DConfigDlgStackedWidget", "classDigikam_1_1DConfigDlgStackedWidget.html", "classDigikam_1_1DConfigDlgStackedWidget" ],
      [ "DConfigDlgViewPrivate", "classDigikam_1_1DConfigDlgViewPrivate.html", "classDigikam_1_1DConfigDlgViewPrivate" ]
    ] ],
    [ "dconfigdlgwidgets.cpp", "dconfigdlgwidgets_8cpp.html", null ],
    [ "dconfigdlgwidgets.h", "dconfigdlgwidgets_8h.html", [
      [ "DConfigDlgTitle", "classDigikam_1_1DConfigDlgTitle.html", "classDigikam_1_1DConfigDlgTitle" ],
      [ "DConfigDlgWdg", "classDigikam_1_1DConfigDlgWdg.html", "classDigikam_1_1DConfigDlgWdg" ]
    ] ],
    [ "dconfigdlgwidgets_p.h", "dconfigdlgwidgets__p_8h.html", [
      [ "Private", "classDigikam_1_1DConfigDlgTitle_1_1Private.html", "classDigikam_1_1DConfigDlgTitle_1_1Private" ],
      [ "DConfigDlgWdgPrivate", "classDigikam_1_1DConfigDlgWdgPrivate.html", "classDigikam_1_1DConfigDlgWdgPrivate" ]
    ] ],
    [ "deletedialog.cpp", "deletedialog_8cpp.html", null ],
    [ "deletedialog.h", "deletedialog_8h.html", "deletedialog_8h" ],
    [ "dmessagebox.cpp", "dmessagebox_8cpp.html", null ],
    [ "dmessagebox.h", "dmessagebox_8h.html", [
      [ "DMessageBox", "classDigikam_1_1DMessageBox.html", null ]
    ] ],
    [ "dprogressdlg.cpp", "dprogressdlg_8cpp.html", null ],
    [ "dprogressdlg.h", "dprogressdlg_8h.html", [
      [ "DProgressDlg", "classDigikam_1_1DProgressDlg.html", "classDigikam_1_1DProgressDlg" ]
    ] ],
    [ "dsplashscreen.cpp", "dsplashscreen_8cpp.html", null ],
    [ "dsplashscreen.h", "dsplashscreen_8h.html", [
      [ "DSplashScreen", "classDigikam_1_1DSplashScreen.html", "classDigikam_1_1DSplashScreen" ]
    ] ],
    [ "filesaveoptionsdlg.cpp", "filesaveoptionsdlg_8cpp.html", null ],
    [ "filesaveoptionsdlg.h", "filesaveoptionsdlg_8h.html", [
      [ "FileSaveOptionsDlg", "classDigikam_1_1FileSaveOptionsDlg.html", "classDigikam_1_1FileSaveOptionsDlg" ]
    ] ],
    [ "iccprofileinfodlg.cpp", "iccprofileinfodlg_8cpp.html", null ],
    [ "iccprofileinfodlg.h", "iccprofileinfodlg_8h.html", [
      [ "ICCProfileInfoDlg", "classDigikam_1_1ICCProfileInfoDlg.html", "classDigikam_1_1ICCProfileInfoDlg" ]
    ] ],
    [ "imagedialog.cpp", "imagedialog_8cpp.html", null ],
    [ "imagedialog.h", "imagedialog_8h.html", [
      [ "ImageDialog", "classDigikam_1_1ImageDialog.html", "classDigikam_1_1ImageDialog" ],
      [ "ImageDialogIconProvider", "classDigikam_1_1ImageDialogIconProvider.html", "classDigikam_1_1ImageDialogIconProvider" ],
      [ "ImageDialogPreview", "classDigikam_1_1ImageDialogPreview.html", "classDigikam_1_1ImageDialogPreview" ],
      [ "ImageDialogToolTip", "classDigikam_1_1ImageDialogToolTip.html", "classDigikam_1_1ImageDialogToolTip" ]
    ] ],
    [ "imagedialog_iconprovider.cpp", "imagedialog__iconprovider_8cpp.html", null ],
    [ "imagedialog_p.h", "imagedialog__p_8h.html", null ],
    [ "imagedialog_preview.cpp", "imagedialog__preview_8cpp.html", null ],
    [ "imagedialog_tooltip.cpp", "imagedialog__tooltip_8cpp.html", null ],
    [ "infodlg.cpp", "infodlg_8cpp.html", null ],
    [ "infodlg.h", "infodlg_8h.html", [
      [ "InfoDlg", "classDigikam_1_1InfoDlg.html", "classDigikam_1_1InfoDlg" ]
    ] ],
    [ "libsinfodlg.cpp", "libsinfodlg_8cpp.html", null ],
    [ "libsinfodlg.h", "libsinfodlg_8h.html", [
      [ "LibsInfoDlg", "classDigikam_1_1LibsInfoDlg.html", "classDigikam_1_1LibsInfoDlg" ]
    ] ],
    [ "rawcameradlg.cpp", "rawcameradlg_8cpp.html", null ],
    [ "rawcameradlg.h", "rawcameradlg_8h.html", [
      [ "RawCameraDlg", "classDigikam_1_1RawCameraDlg.html", "classDigikam_1_1RawCameraDlg" ]
    ] ],
    [ "solidhardwaredlg.cpp", "solidhardwaredlg_8cpp.html", null ],
    [ "solidhardwaredlg.h", "solidhardwaredlg_8h.html", [
      [ "SolidHardwareDlg", "classDigikam_1_1SolidHardwareDlg.html", "classDigikam_1_1SolidHardwareDlg" ]
    ] ],
    [ "webbrowserdlg.cpp", "webbrowserdlg_8cpp.html", null ],
    [ "webbrowserdlg.h", "webbrowserdlg_8h.html", [
      [ "WebBrowserDlg", "classDigikam_1_1WebBrowserDlg.html", "classDigikam_1_1WebBrowserDlg" ]
    ] ]
];