var classDigikam_1_1EffectMngr_1_1Private =
[
    [ "EffectMethod", "classDigikam_1_1EffectMngr_1_1Private.html#afa867d1bb95481200497bdf1d140c447", null ],
    [ "Private", "classDigikam_1_1EffectMngr_1_1Private.html#a171cfa53ee66a26f1170fd57a6d17750", null ],
    [ "~Private", "classDigikam_1_1EffectMngr_1_1Private.html#ab58479d146857a11d2020ebe7373ba95", null ],
    [ "getRandomEffect", "classDigikam_1_1EffectMngr_1_1Private.html#ad7f6276e1046b165f045ed913dd8d926", null ],
    [ "registerEffects", "classDigikam_1_1EffectMngr_1_1Private.html#aa2a519eccf2dc8262531130131809294", null ],
    [ "eff_curEffect", "classDigikam_1_1EffectMngr_1_1Private.html#a773c9a3a79a6c28c6c12c0fa76e48bc6", null ],
    [ "eff_curFrame", "classDigikam_1_1EffectMngr_1_1Private.html#ac3a364b6d9e2cf8a847d1865e871f12f", null ],
    [ "eff_effectList", "classDigikam_1_1EffectMngr_1_1Private.html#a5905973bfeb613547bcf53222a22a76c", null ],
    [ "eff_image", "classDigikam_1_1EffectMngr_1_1Private.html#a6d96029877100d526e84d872f1609fe3", null ],
    [ "eff_imgFrames", "classDigikam_1_1EffectMngr_1_1Private.html#a24a0ad85e3285ef4b7231c560c4d4b06", null ],
    [ "eff_isRunning", "classDigikam_1_1EffectMngr_1_1Private.html#aff19ee6a26154417f493eac8e9bfd8be", null ],
    [ "eff_outSize", "classDigikam_1_1EffectMngr_1_1Private.html#a488b851eb8cefa38e47217fa77b1ba90", null ],
    [ "eff_step", "classDigikam_1_1EffectMngr_1_1Private.html#a41fda6127b375cd6ac2fe84f19c8aa09", null ]
];