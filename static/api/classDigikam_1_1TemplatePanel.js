var classDigikam_1_1TemplatePanel =
[
    [ "TemplateTab", "classDigikam_1_1TemplatePanel.html#a89be183792123475c326fa7bd190c8b5", [
      [ "RIGHTS", "classDigikam_1_1TemplatePanel.html#a89be183792123475c326fa7bd190c8b5a58824f85578b199cc2e6153a4595ba11", null ],
      [ "LOCATION", "classDigikam_1_1TemplatePanel.html#a89be183792123475c326fa7bd190c8b5ac6b217e487b2e8f62b2f8faeae5e14e8", null ],
      [ "CONTACT", "classDigikam_1_1TemplatePanel.html#a89be183792123475c326fa7bd190c8b5a60835858452631a5365e8725fff920cb", null ],
      [ "SUBJECTS", "classDigikam_1_1TemplatePanel.html#a89be183792123475c326fa7bd190c8b5a7c8a416af29d21f8ccbd53ba1653108d", null ]
    ] ],
    [ "TemplatePanel", "classDigikam_1_1TemplatePanel.html#a12536eeea349a0da39702b63eac87123", null ],
    [ "~TemplatePanel", "classDigikam_1_1TemplatePanel.html#aa9092fe238c3537c3e119c94c953e7cb", null ],
    [ "apply", "classDigikam_1_1TemplatePanel.html#ada664d2353d5051da59314499034eb2e", null ],
    [ "getTemplate", "classDigikam_1_1TemplatePanel.html#ac73d340271feae51a73ae28f09eb40e6", null ],
    [ "setTemplate", "classDigikam_1_1TemplatePanel.html#a9b47192f3f7ffb0d5ba57ae60bed8c7e", null ]
];