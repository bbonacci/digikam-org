var dir_9188495e614aab9437d55f6d8601f54d =
[
    [ "captionedit.cpp", "captionedit_8cpp.html", null ],
    [ "captionedit.h", "captionedit_8h.html", [
      [ "CaptionEdit", "classDigikam_1_1CaptionEdit.html", "classDigikam_1_1CaptionEdit" ]
    ] ],
    [ "disjointmetadata.cpp", "disjointmetadata_8cpp.html", null ],
    [ "disjointmetadata.h", "disjointmetadata_8h.html", [
      [ "DisjointMetadata", "classDigikam_1_1DisjointMetadata.html", "classDigikam_1_1DisjointMetadata" ]
    ] ],
    [ "disjointmetadata_p.cpp", "disjointmetadata__p_8cpp.html", null ],
    [ "disjointmetadata_p.h", "disjointmetadata__p_8h.html", [
      [ "Private", "classDigikam_1_1DisjointMetadata_1_1Private.html", "classDigikam_1_1DisjointMetadata_1_1Private" ]
    ] ],
    [ "disjointmetadatadatafields.h", "disjointmetadatadatafields_8h.html", [
      [ "DisjointMetadataDataFields", "classDigikam_1_1DisjointMetadataDataFields.html", "classDigikam_1_1DisjointMetadataDataFields" ]
    ] ],
    [ "itemdescedittab.cpp", "itemdescedittab_8cpp.html", null ],
    [ "itemdescedittab.h", "itemdescedittab_8h.html", [
      [ "ItemDescEditTab", "classDigikam_1_1ItemDescEditTab.html", "classDigikam_1_1ItemDescEditTab" ]
    ] ]
];