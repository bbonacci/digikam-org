var classDigikam_1_1WBSettings =
[
    [ "WBSettings", "classDigikam_1_1WBSettings.html#a20d1c2d7857d91aef34328758bad3851", null ],
    [ "~WBSettings", "classDigikam_1_1WBSettings.html#a9c7b24e792f59633a3552fbf772636a2", null ],
    [ "defaultSettings", "classDigikam_1_1WBSettings.html#a26249657439952e4c04848ee65f635a6", null ],
    [ "loadSettings", "classDigikam_1_1WBSettings.html#a8e46961508bf8b05749dbeed779a376a", null ],
    [ "pickTemperatureIsChecked", "classDigikam_1_1WBSettings.html#a2dc584c4cbdd20695ac16068d8ddca15", null ],
    [ "readSettings", "classDigikam_1_1WBSettings.html#a395b45dfc1e03947df54d13478f30304", null ],
    [ "resetToDefault", "classDigikam_1_1WBSettings.html#a1a34502562e3f9e67006da6e240bcb32", null ],
    [ "saveAsSettings", "classDigikam_1_1WBSettings.html#a2a00634df5f2e500422ab79a9226ce0b", null ],
    [ "setCheckedPickTemperature", "classDigikam_1_1WBSettings.html#a4f50c6469cac00afbcd75b8875039438", null ],
    [ "setSettings", "classDigikam_1_1WBSettings.html#ae010553e2fae210d230159fc78b512cc", null ],
    [ "settings", "classDigikam_1_1WBSettings.html#ade7406f5d75da17b0935ca94740b0157", null ],
    [ "showAdvancedButtons", "classDigikam_1_1WBSettings.html#ac67402b12cc5c764d451e2fd68727a8d", null ],
    [ "signalAutoAdjustExposure", "classDigikam_1_1WBSettings.html#acfedf8df45200e09fe14a58469cfdb68", null ],
    [ "signalPickerColorButtonActived", "classDigikam_1_1WBSettings.html#a4c4f0f65b3e02172a18ab68f2e58847d", null ],
    [ "signalSettingsChanged", "classDigikam_1_1WBSettings.html#a8f8cf973dca876f6712e329c0217e3bc", null ],
    [ "writeSettings", "classDigikam_1_1WBSettings.html#aab6107baa3f7925293644c4b92cf3609", null ]
];