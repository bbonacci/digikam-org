var classsop__creator__trivial__low__delay =
[
    [ "params", "structsop__creator__trivial__low__delay_1_1params.html", "structsop__creator__trivial__low__delay_1_1params" ],
    [ "sop_creator_trivial_low_delay", "classsop__creator__trivial__low__delay.html#ae2309b21ffa0a108c49dd50d3156d21f", null ],
    [ "advance_frame", "classsop__creator__trivial__low__delay.html#a60edacb61171aad35a831cee1483b442", null ],
    [ "get_frame_number", "classsop__creator__trivial__low__delay.html#ad39b89b444b7b9ccacb704414ffac0b6", null ],
    [ "get_num_poc_lsb_bits", "classsop__creator__trivial__low__delay.html#afad19a3681533a7dc33fdb26c3c005e2", null ],
    [ "get_number_of_temporal_layers", "classsop__creator__trivial__low__delay.html#a1284c3b1405c1bd458d195bd28300c7d", null ],
    [ "get_pic_order_count", "classsop__creator__trivial__low__delay.html#a766a4888e84fcc96cde5bcd2f60c0e5a", null ],
    [ "get_pic_order_count_lsb", "classsop__creator__trivial__low__delay.html#acac9f24f5352612daedf17abefa9ce1b", null ],
    [ "insert_end_of_stream", "classsop__creator__trivial__low__delay.html#a522e094509edd7bd0ca9067da632f5ec", null ],
    [ "insert_new_input_image", "classsop__creator__trivial__low__delay.html#adc192455ccd878692b91ae7047624778", null ],
    [ "reset_poc", "classsop__creator__trivial__low__delay.html#a044e001da611a750d5407f2eacabd13f", null ],
    [ "set_encoder_context", "classsop__creator__trivial__low__delay.html#ace1d24abf11402cc377659d0b7eb27b3", null ],
    [ "set_encoder_picture_buffer", "classsop__creator__trivial__low__delay.html#a361fa176bf1f316e3497ce69833bd158", null ],
    [ "set_num_poc_lsb_bits", "classsop__creator__trivial__low__delay.html#ac17255e52c08c846c6b45880a5b97646", null ],
    [ "set_SPS_header_values", "classsop__creator__trivial__low__delay.html#af8b39c7736bd6522f57d817d65d8e48e", null ],
    [ "setParams", "classsop__creator__trivial__low__delay.html#a8f1c9fb57c78bdb434d29e3eff1f4c7f", null ],
    [ "mEncCtx", "classsop__creator__trivial__low__delay.html#aff8c3a1ea2092592940aa8c9f8451f38", null ],
    [ "mEncPicBuf", "classsop__creator__trivial__low__delay.html#a7744c96be08b27a553282ad9d9b23a25", null ]
];