var classDigikam_1_1ItemPropertiesColorsTab =
[
    [ "ItemPropertiesColorsTab", "classDigikam_1_1ItemPropertiesColorsTab.html#a1affb585623e632f5d70b50cfd287c17", null ],
    [ "~ItemPropertiesColorsTab", "classDigikam_1_1ItemPropertiesColorsTab.html#a989f8b43b5010d919debd908a4f9a7e0", null ],
    [ "readSettings", "classDigikam_1_1ItemPropertiesColorsTab.html#abf155a9493cffb5bb55d79ca932d0a99", null ],
    [ "setData", "classDigikam_1_1ItemPropertiesColorsTab.html#a4ae45111119d3cab2e4c03dc21cc90aa", null ],
    [ "setSelection", "classDigikam_1_1ItemPropertiesColorsTab.html#a1e3560117b5cdc57317cb2936a70582d", null ],
    [ "writeSettings", "classDigikam_1_1ItemPropertiesColorsTab.html#a3eef7c305635e6e90b6433831cefed61", null ]
];