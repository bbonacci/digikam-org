var fileactionmngr__p_8h =
[
    [ "Private", "classDigikam_1_1FileActionMngr_1_1Private.html", "classDigikam_1_1FileActionMngr_1_1Private" ],
    [ "PrivateProgressItemCreator", "classDigikam_1_1PrivateProgressItemCreator.html", "classDigikam_1_1PrivateProgressItemCreator" ],
    [ "GroupAction", "fileactionmngr__p_8h.html#adc3d6b77f77c5aba4a63d90470218a51", [
      [ "AddToGroup", "fileactionmngr__p_8h.html#adc3d6b77f77c5aba4a63d90470218a51a539dc0ba2a1d7911c1cececf390f5ce6", null ],
      [ "RemoveFromGroup", "fileactionmngr__p_8h.html#adc3d6b77f77c5aba4a63d90470218a51af63d279fc97c889603aebfe0bff98b5f", null ],
      [ "Ungroup", "fileactionmngr__p_8h.html#adc3d6b77f77c5aba4a63d90470218a51aa5e37421142443b417ec4c192fed359e", null ]
    ] ]
];