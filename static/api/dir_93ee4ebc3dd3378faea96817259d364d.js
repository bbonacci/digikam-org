var dir_93ee4ebc3dd3378faea96817259d364d =
[
    [ "hslfilter.cpp", "hslfilter_8cpp.html", null ],
    [ "hslfilter.h", "hslfilter_8h.html", [
      [ "HSLContainer", "classDigikam_1_1HSLContainer.html", "classDigikam_1_1HSLContainer" ],
      [ "HSLFilter", "classDigikam_1_1HSLFilter.html", "classDigikam_1_1HSLFilter" ]
    ] ],
    [ "hslsettings.cpp", "hslsettings_8cpp.html", null ],
    [ "hslsettings.h", "hslsettings_8h.html", [
      [ "HSLSettings", "classDigikam_1_1HSLSettings.html", "classDigikam_1_1HSLSettings" ]
    ] ],
    [ "hspreviewwidget.cpp", "hspreviewwidget_8cpp.html", null ],
    [ "hspreviewwidget.h", "hspreviewwidget_8h.html", [
      [ "HSPreviewWidget", "classDigikam_1_1HSPreviewWidget.html", "classDigikam_1_1HSPreviewWidget" ]
    ] ]
];