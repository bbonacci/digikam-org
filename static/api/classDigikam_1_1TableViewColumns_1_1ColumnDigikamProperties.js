var classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties =
[
    [ "ColumnCompareResult", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#a0714c73efa682e4bcc6dac00989cabd7", [
      [ "CmpEqual", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#a0714c73efa682e4bcc6dac00989cabd7ab1fd7950c9141af71b6d915d7619da20", null ],
      [ "CmpABiggerB", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#a0714c73efa682e4bcc6dac00989cabd7a3b46fa13fd837bb5e8303e15579e9c0a", null ],
      [ "CmpALessB", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#a0714c73efa682e4bcc6dac00989cabd7af64727fba2a786f83c4032b9ac4e2ac7", null ]
    ] ],
    [ "ColumnFlag", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4", [
      [ "ColumnNoFlags", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4a3a1867e93424ceda7439df444b42b7a8", null ],
      [ "ColumnCustomPainting", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4a2496490a69825be1607d673758561fea", null ],
      [ "ColumnCustomSorting", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4ab775074b18540b93dbbe923cc7977b0c", null ],
      [ "ColumnHasConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4a22debcee2a26f5a6a7fda09fdd1e3c0c", null ]
    ] ],
    [ "SubColumn", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#afe734bf33985b5efc493f0c804a98041", [
      [ "SubColumnRating", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#afe734bf33985b5efc493f0c804a98041a39bd2507a72792fc18980ed408ff7aa3", null ],
      [ "SubColumnPickLabel", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#afe734bf33985b5efc493f0c804a98041af20a5b7163f3f0b624ba6a04cf97962a", null ],
      [ "SubColumnColorLabel", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#afe734bf33985b5efc493f0c804a98041a50b884b51e3035be216063cd50a57d08", null ],
      [ "SubColumnTitle", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#afe734bf33985b5efc493f0c804a98041aa9670d0adcff2855b815c0998e7bf1a5", null ],
      [ "SubColumnCaption", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#afe734bf33985b5efc493f0c804a98041a8ae7d7e40f577216f7ed9bec7c07aaa2", null ],
      [ "SubColumnTags", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#afe734bf33985b5efc493f0c804a98041aff51b6b88a5def77fbfc16c8adbab4df", null ]
    ] ],
    [ "ColumnDigikamProperties", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#a9aa93d70748fffdf63acf94264da0098", null ],
    [ "~ColumnDigikamProperties", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#a042e5847e6d02acc026d84e7eb8683e0", null ],
    [ "columnAffectedByChangeset", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#a9e95fc44da484fe12d1174543c8f96f2", null ],
    [ "compare", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#af2118f1298af1b33df6b19b982754a9e", null ],
    [ "data", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#a2c4b066e56d4e02bc3fb9470b8e16661", null ],
    [ "getColumnFlags", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#a27c665beb41b1487704557a656232dff", null ],
    [ "getConfiguration", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#ace668cd97cd6d0b584332b803a0ec665", null ],
    [ "getConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#a70ac95911d3bf9ad063cdaed7203a7cd", null ],
    [ "getTitle", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#a0edf711f158a682088756ef858d2b92c", null ],
    [ "paint", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#ae53896a52f61680c4263e571e19eec7f", null ],
    [ "setConfiguration", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#aaae5e73c6b1b9c0a9b79e19c5bfbbad1", null ],
    [ "signalAllDataChanged", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#ab288b91b167fc9f40490e3d41dee38ee", null ],
    [ "signalDataChanged", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#ac52d1ec1e5839d96de9e1b365582fdfc", null ],
    [ "sizeHint", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#a3a5f8ab59b570eb69d51b0d01c452190", null ],
    [ "updateThumbnailSize", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#ac4696688718ef915e4fb096ed8a2efe3", null ],
    [ "configuration", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#a1e0c6be1da4fa29ddecaf0bc07a87a37", null ],
    [ "s", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html#a90a53ac037c5230322f608a687680efa", null ]
];