var classDigikam_1_1DMetadataSettings =
[
    [ "setSettings", "classDigikam_1_1DMetadataSettings.html#a210696bc670779e267f40e06456799d5", null ],
    [ "settings", "classDigikam_1_1DMetadataSettings.html#ac48baa2f55b002cd92b6118605b37f10", null ],
    [ "signalDMetadataSettingsChanged", "classDigikam_1_1DMetadataSettings.html#ae6a51f7ba66bfdb1f87f2ab938dd110e", null ],
    [ "signalSettingsChanged", "classDigikam_1_1DMetadataSettings.html#a84eed5a2b98bae7325a7dd78ffdfd437", null ],
    [ "DMetadataSettingsCreator", "classDigikam_1_1DMetadataSettings.html#a2debe9defc5f5df5cda1968bef39eb14", null ]
];