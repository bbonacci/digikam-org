var dir_29aac68c0952930b806ccfed070edc8b =
[
    [ "dmetadata.cpp", "dmetadata_8cpp.html", null ],
    [ "dmetadata.h", "dmetadata_8h.html", [
      [ "DMetadata", "classDigikam_1_1DMetadata.html", "classDigikam_1_1DMetadata" ]
    ] ],
    [ "dmetadata_comments.cpp", "dmetadata__comments_8cpp.html", null ],
    [ "dmetadata_exif.cpp", "dmetadata__exif_8cpp.html", null ],
    [ "dmetadata_faces.cpp", "dmetadata__faces_8cpp.html", null ],
    [ "dmetadata_fileio.cpp", "dmetadata__fileio_8cpp.html", null ],
    [ "dmetadata_generic.cpp", "dmetadata__generic_8cpp.html", null ],
    [ "dmetadata_history.cpp", "dmetadata__history_8cpp.html", null ],
    [ "dmetadata_imagemagick.cpp", "dmetadata__imagemagick_8cpp.html", null ],
    [ "dmetadata_iptc.cpp", "dmetadata__iptc_8cpp.html", null ],
    [ "dmetadata_labels.cpp", "dmetadata__labels_8cpp.html", null ],
    [ "dmetadata_libheif.cpp", "dmetadata__libheif_8cpp.html", "dmetadata__libheif_8cpp" ],
    [ "dmetadata_libraw.cpp", "dmetadata__libraw_8cpp.html", null ],
    [ "dmetadata_photo.cpp", "dmetadata__photo_8cpp.html", null ],
    [ "dmetadata_tags.cpp", "dmetadata__tags_8cpp.html", null ],
    [ "dmetadata_template.cpp", "dmetadata__template_8cpp.html", null ],
    [ "dmetadata_video.cpp", "dmetadata__video_8cpp.html", "dmetadata__video_8cpp" ],
    [ "dmetadata_xmp.cpp", "dmetadata__xmp_8cpp.html", null ],
    [ "dmetadatasettings.cpp", "dmetadatasettings_8cpp.html", null ],
    [ "dmetadatasettings.h", "dmetadatasettings_8h.html", [
      [ "DMetadataSettings", "classDigikam_1_1DMetadataSettings.html", "classDigikam_1_1DMetadataSettings" ]
    ] ],
    [ "dmetadatasettingscontainer.cpp", "dmetadatasettingscontainer_8cpp.html", "dmetadatasettingscontainer_8cpp" ],
    [ "dmetadatasettingscontainer.h", "dmetadatasettingscontainer_8h.html", "dmetadatasettingscontainer_8h" ],
    [ "geodetictools.cpp", "geodetictools_8cpp.html", null ],
    [ "geodetictools.h", "geodetictools_8h.html", "geodetictools_8h" ]
];