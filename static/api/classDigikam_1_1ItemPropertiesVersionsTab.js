var classDigikam_1_1ItemPropertiesVersionsTab =
[
    [ "ItemPropertiesVersionsTab", "classDigikam_1_1ItemPropertiesVersionsTab.html#a5c60aff4374362bed82f4398d344048d", null ],
    [ "~ItemPropertiesVersionsTab", "classDigikam_1_1ItemPropertiesVersionsTab.html#a71f4d1b8dea49de87588fd792a873f0b", null ],
    [ "actionTriggered", "classDigikam_1_1ItemPropertiesVersionsTab.html#a0eccca07ccc5c58d0e8835d396818592", null ],
    [ "addOpenAlbumAction", "classDigikam_1_1ItemPropertiesVersionsTab.html#a5d1712d836ba2bab7ed80a1f4e6acb46", null ],
    [ "addOpenImageAction", "classDigikam_1_1ItemPropertiesVersionsTab.html#a65ce042079a272fd3ba78e5056d581c6", null ],
    [ "addShowHideOverlay", "classDigikam_1_1ItemPropertiesVersionsTab.html#afe43a661a0a29d50bc7dcbf534e52b64", null ],
    [ "clear", "classDigikam_1_1ItemPropertiesVersionsTab.html#aa44eb8efd5fa269b9e1098f39e4c856a", null ],
    [ "filtersHistoryWidget", "classDigikam_1_1ItemPropertiesVersionsTab.html#ab386dc13bc9f2e85037cbc6161866dd3", null ],
    [ "imageSelected", "classDigikam_1_1ItemPropertiesVersionsTab.html#a702c6a028408f009def1eab803a8f29c", null ],
    [ "readSettings", "classDigikam_1_1ItemPropertiesVersionsTab.html#a3e9df1135f4641d61f644207b773e03d", null ],
    [ "setEnabledHistorySteps", "classDigikam_1_1ItemPropertiesVersionsTab.html#a289e9eb75d7d3c8509b9270a663c334d", null ],
    [ "setItem", "classDigikam_1_1ItemPropertiesVersionsTab.html#a03c8fb7b4e74942d32b3fc930acf3480", null ],
    [ "versionsWidget", "classDigikam_1_1ItemPropertiesVersionsTab.html#af0234820b1f82657581942a69a00ed17", null ],
    [ "writeSettings", "classDigikam_1_1ItemPropertiesVersionsTab.html#af55385ab411b1d901dc13c2ea22ce01d", null ]
];