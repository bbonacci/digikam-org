var dir_16cf9e2a59f90956b15c67cec0e63655 =
[
    [ "blur_detector.cpp", "blur__detector_8cpp.html", null ],
    [ "blur_detector.h", "blur__detector_8h.html", [
      [ "BlurDetector", "classDigikam_1_1BlurDetector.html", "classDigikam_1_1BlurDetector" ]
    ] ],
    [ "compression_detector.cpp", "compression__detector_8cpp.html", "compression__detector_8cpp" ],
    [ "compression_detector.h", "compression__detector_8h.html", [
      [ "CompressionDetector", "classDigikam_1_1CompressionDetector.html", "classDigikam_1_1CompressionDetector" ]
    ] ],
    [ "detector.cpp", "detector_8cpp.html", null ],
    [ "detector.h", "detector_8h.html", [
      [ "DetectorDistortion", "classDigikam_1_1DetectorDistortion.html", "classDigikam_1_1DetectorDistortion" ]
    ] ],
    [ "exposure_detector.cpp", "exposure__detector_8cpp.html", null ],
    [ "exposure_detector.h", "exposure__detector_8h.html", [
      [ "ExposureDetector", "classDigikam_1_1ExposureDetector.html", "classDigikam_1_1ExposureDetector" ]
    ] ],
    [ "noise_detector.cpp", "noise__detector_8cpp.html", "noise__detector_8cpp" ],
    [ "noise_detector.h", "noise__detector_8h.html", [
      [ "NoiseDetector", "classDigikam_1_1NoiseDetector.html", "classDigikam_1_1NoiseDetector" ]
    ] ]
];