var dir_c3f1bc18a4bb8553d3736cafecc0c396 =
[
    [ "albumdragdrop.cpp", "albumdragdrop_8cpp.html", null ],
    [ "albumdragdrop.h", "albumdragdrop_8h.html", [
      [ "AlbumDragDropHandler", "classDigikam_1_1AlbumDragDropHandler.html", "classDigikam_1_1AlbumDragDropHandler" ]
    ] ],
    [ "ddragobjects.cpp", "ddragobjects_8cpp.html", null ],
    [ "ddragobjects.h", "ddragobjects_8h.html", [
      [ "DAlbumDrag", "classDigikam_1_1DAlbumDrag.html", "classDigikam_1_1DAlbumDrag" ],
      [ "DCameraDragObject", "classDigikam_1_1DCameraDragObject.html", "classDigikam_1_1DCameraDragObject" ],
      [ "DCameraItemListDrag", "classDigikam_1_1DCameraItemListDrag.html", "classDigikam_1_1DCameraItemListDrag" ],
      [ "DItemDrag", "classDigikam_1_1DItemDrag.html", "classDigikam_1_1DItemDrag" ],
      [ "DTagListDrag", "classDigikam_1_1DTagListDrag.html", "classDigikam_1_1DTagListDrag" ]
    ] ],
    [ "importdragdrop.cpp", "importdragdrop_8cpp.html", null ],
    [ "importdragdrop.h", "importdragdrop_8h.html", [
      [ "ImportDragDropHandler", "classDigikam_1_1ImportDragDropHandler.html", "classDigikam_1_1ImportDragDropHandler" ]
    ] ],
    [ "itemdragdrop.cpp", "itemdragdrop_8cpp.html", "itemdragdrop_8cpp" ],
    [ "itemdragdrop.h", "itemdragdrop_8h.html", [
      [ "ItemDragDropHandler", "classDigikam_1_1ItemDragDropHandler.html", "classDigikam_1_1ItemDragDropHandler" ]
    ] ],
    [ "tagdragdrop.cpp", "tagdragdrop_8cpp.html", null ],
    [ "tagdragdrop.h", "tagdragdrop_8h.html", [
      [ "TagDragDropHandler", "classDigikam_1_1TagDragDropHandler.html", "classDigikam_1_1TagDragDropHandler" ]
    ] ]
];