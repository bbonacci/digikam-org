var classShowFoto_1_1ShowfotoItemSortSettings =
[
    [ "CategorizationMode", "classShowFoto_1_1ShowfotoItemSortSettings.html#abe1ac271ea9b2a47eab81e04ec16ad8d", [
      [ "NoCategories", "classShowFoto_1_1ShowfotoItemSortSettings.html#abe1ac271ea9b2a47eab81e04ec16ad8dab94244d143b943a1a630f834dc512483", null ],
      [ "CategoryByFolder", "classShowFoto_1_1ShowfotoItemSortSettings.html#abe1ac271ea9b2a47eab81e04ec16ad8da9c613ca8189db5c8ba465da3d96770da", null ],
      [ "CategoryByFormat", "classShowFoto_1_1ShowfotoItemSortSettings.html#abe1ac271ea9b2a47eab81e04ec16ad8dab0538c131f5b1de65e9b7c8862370129", null ]
    ] ],
    [ "SortOrder", "classShowFoto_1_1ShowfotoItemSortSettings.html#a7e81ab984e72507ac386b487b6babe36", [
      [ "AscendingOrder", "classShowFoto_1_1ShowfotoItemSortSettings.html#a7e81ab984e72507ac386b487b6babe36a91021feb54f8c316cef0b3ad77f13786", null ],
      [ "DescendingOrder", "classShowFoto_1_1ShowfotoItemSortSettings.html#a7e81ab984e72507ac386b487b6babe36acf2e9b10c18b4a280d0cac5e123c6f49", null ],
      [ "DefaultOrder", "classShowFoto_1_1ShowfotoItemSortSettings.html#a7e81ab984e72507ac386b487b6babe36a03e731317c8fc2003a91a5338f8a6334", null ]
    ] ],
    [ "SortRole", "classShowFoto_1_1ShowfotoItemSortSettings.html#a8dae638a70d611c2cc015ff94f9a8b80", [
      [ "SortByCreationDate", "classShowFoto_1_1ShowfotoItemSortSettings.html#a8dae638a70d611c2cc015ff94f9a8b80a892f02e8075fd6395f0fb95b9457a5b6", null ],
      [ "SortByFileName", "classShowFoto_1_1ShowfotoItemSortSettings.html#a8dae638a70d611c2cc015ff94f9a8b80a31f61cee9bf1d44ee0c27e9b2afbaa6b", null ],
      [ "SortByFileSize", "classShowFoto_1_1ShowfotoItemSortSettings.html#a8dae638a70d611c2cc015ff94f9a8b80a877a97e6035a45ec4b85249a70189f93", null ]
    ] ],
    [ "ShowfotoItemSortSettings", "classShowFoto_1_1ShowfotoItemSortSettings.html#aad8f53b5e052e5180b797df691c9fc97", null ],
    [ "~ShowfotoItemSortSettings", "classShowFoto_1_1ShowfotoItemSortSettings.html#aeab6e172cceb8af620a698fe89926dd9", null ],
    [ "compare", "classShowFoto_1_1ShowfotoItemSortSettings.html#ace44541a5ea47f9984ebd2b19f0f7f86", null ],
    [ "compare", "classShowFoto_1_1ShowfotoItemSortSettings.html#aa19569d60763f5923b411b3277de9ef6", null ],
    [ "compareCategories", "classShowFoto_1_1ShowfotoItemSortSettings.html#a793a52abd2fb1aaafbf7b595bb59f79b", null ],
    [ "isCategorized", "classShowFoto_1_1ShowfotoItemSortSettings.html#a6753d8ad4e100b513463b6fc71199115", null ],
    [ "lessThan", "classShowFoto_1_1ShowfotoItemSortSettings.html#a65cd048e033c4d3179d679385c43b53a", null ],
    [ "lessThan", "classShowFoto_1_1ShowfotoItemSortSettings.html#aac87cc5d9bd4830f614ecaf608e11ead", null ],
    [ "operator==", "classShowFoto_1_1ShowfotoItemSortSettings.html#acd63149d17ec4961307fd7acd00002b3", null ],
    [ "setCategorizationMode", "classShowFoto_1_1ShowfotoItemSortSettings.html#aebfd859cc7f5d8f2bdcd7549171ee744", null ],
    [ "setCategorizationSortOrder", "classShowFoto_1_1ShowfotoItemSortSettings.html#a061bf829dfa90efea1a33a7b184b8fb2", null ],
    [ "setSortOrder", "classShowFoto_1_1ShowfotoItemSortSettings.html#a4721cc98832d24573965debf4149e993", null ],
    [ "setSortRole", "classShowFoto_1_1ShowfotoItemSortSettings.html#a899763a37dc526b80132c2aced964761", null ],
    [ "categorizationCaseSensitivity", "classShowFoto_1_1ShowfotoItemSortSettings.html#ab7d56f3caddd29e224dbd240760ad93d", null ],
    [ "categorizationMode", "classShowFoto_1_1ShowfotoItemSortSettings.html#a134e213a86e4a8de5f964414c5819f12", null ],
    [ "categorizationSortOrder", "classShowFoto_1_1ShowfotoItemSortSettings.html#a24817eb477f1cf031a92cf55339dd1c6", null ],
    [ "currentCategorizationSortOrder", "classShowFoto_1_1ShowfotoItemSortSettings.html#a546a463beadf4e2eba11a9599014aa80", null ],
    [ "currentSortOrder", "classShowFoto_1_1ShowfotoItemSortSettings.html#aac16fb2f5e0d18bb4d75465060703ced", null ],
    [ "sortCaseSensitivity", "classShowFoto_1_1ShowfotoItemSortSettings.html#a0268ae2857609ea88483f126a7440a6c", null ],
    [ "sortOrder", "classShowFoto_1_1ShowfotoItemSortSettings.html#aca35d2b0145a7b3e7bf00ec3243ebce2", null ],
    [ "sortRole", "classShowFoto_1_1ShowfotoItemSortSettings.html#a2e26d396080d29c4a0aa4ef1b051f422", null ]
];