var structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult =
[
    [ "ImgurAccount", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurAccount.html", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurAccount" ],
    [ "ImgurImage", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurImage.html", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurImage" ],
    [ "account", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult.html#af18db81fd836b0e85297f296f3a87593", null ],
    [ "action", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult.html#a51c578ec12db01c44acbbad27b80cfd6", null ],
    [ "image", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult.html#a1490856c5f05982926c53a19098c9efd", null ]
];