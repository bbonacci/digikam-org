var structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadRequest =
[
    [ "PhotoUploadRequest", "structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadRequest.html#af90b63fbb0bd16622e0a329594ec7a26", null ],
    [ "PhotoUploadRequest", "structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadRequest.html#aec7c8e88ba416a2586cd6ac505533635", null ],
    [ "m_apiKey", "structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadRequest.html#af5467ee535e235fba02ba12b4e8615db", null ],
    [ "m_images", "structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadRequest.html#a2f46fb1a8456d4e00f9fbd9e8607abdd", null ],
    [ "m_maxDim", "structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadRequest.html#a9effb95e268da75f193e4b5abeac64f1", null ],
    [ "m_observationId", "structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadRequest.html#a9e70a64d0585ebc92f0217c27581427a", null ],
    [ "m_quality", "structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadRequest.html#a370973dfcc32fd01cc2a0f8f7da03c4d", null ],
    [ "m_rescale", "structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadRequest.html#aaef3b4784f29da33fd49a00ab3c2f6fd", null ],
    [ "m_totalImages", "structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadRequest.html#a349adc099211fdfdaede20eaef022878", null ],
    [ "m_updateIds", "structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadRequest.html#ac4646199c9c0f44579c7fd2e89ed2e5d", null ],
    [ "m_user", "structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadRequest.html#af446af34ca8cd0237636e516dfaa166d", null ]
];