var jpegutils_8h =
[
    [ "JpegRotator", "classDigikam_1_1JPEGUtils_1_1JpegRotator.html", "classDigikam_1_1JPEGUtils_1_1JpegRotator" ],
    [ "TransformAction", "jpegutils_8h.html#a8d0b58b9278013f0286752da00c84b48", null ],
    [ "getJpegQuality", "jpegutils_8h.html#a21e3fdcb74544837c243205ea939594e", null ],
    [ "isJpegImage", "jpegutils_8h.html#a0ffb1734e8e905b500c86b5709c8f341", null ],
    [ "jpegConvert", "jpegutils_8h.html#a30d5b3b05392d002a35b599f5ad13fd4", null ],
    [ "loadJPEGScaled", "jpegutils_8h.html#aa441a3222bb8a0fb8eb2be8edd40c4cf", null ]
];