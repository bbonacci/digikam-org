var classDigikam_1_1BCGContainer =
[
    [ "BCGContainer", "classDigikam_1_1BCGContainer.html#ae90a062bd68873b12d1bfbb792c95dde", null ],
    [ "isDefault", "classDigikam_1_1BCGContainer.html#a1b4ec9bb4dcc6f55a0458c0da17678be", null ],
    [ "operator==", "classDigikam_1_1BCGContainer.html#a50c8600acd3541b2108e002f42a11234", null ],
    [ "writeToFilterAction", "classDigikam_1_1BCGContainer.html#a8e894c5c8f05d82adca5636a3f4498d1", null ],
    [ "brightness", "classDigikam_1_1BCGContainer.html#a6b31f9da6299d658aa7f6bc80dc9168f", null ],
    [ "channel", "classDigikam_1_1BCGContainer.html#a9b84528ce594b4ba127dd92d7fc641c0", null ],
    [ "contrast", "classDigikam_1_1BCGContainer.html#a1689c09914288d54aa6ac71d661c8767", null ],
    [ "gamma", "classDigikam_1_1BCGContainer.html#a6260e8784b52203732356bde585e4674", null ]
];