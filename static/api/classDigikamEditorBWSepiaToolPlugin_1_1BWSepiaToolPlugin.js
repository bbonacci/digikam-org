var classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin =
[
    [ "BWSepiaToolPlugin", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a87cb323d3cf4aa458a9c3ab7826d3779", null ],
    [ "~BWSepiaToolPlugin", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a0d9444a2d7cae781aa601ab0e9a34859", null ],
    [ "actions", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#af244555adac28e4baec9e5938182f914", null ],
    [ "addAction", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a07e25f4a176a823feb70e6dd80e6b3f7", null ],
    [ "authors", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a7fe5eecb05dc49702409418115c55bab", null ],
    [ "categories", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a35d45cfa520c713de9348130b0a4d26c", null ],
    [ "cleanUp", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a586144ad9625ffa1f503ad74e341c639", null ],
    [ "count", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a8199ffdd14c09124360cb4b2ba08e1f9", null ],
    [ "description", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a16e1df3e2e2f7afb96b527f5e8bc2feb", null ],
    [ "details", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#ac5073d9614f323bcfc320b3215c44017", null ],
    [ "extraAboutData", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a3895819eddf64a8800a5b0f155f114b1", null ],
    [ "extraAboutDataTitle", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a5ab9e1bce54d762f0e65acc11069896b", null ],
    [ "findActionByName", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a7c27ed94fba2b6e4e1eeb63aca55096a", null ],
    [ "hasVisibilityProperty", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a996af3ec25ecb4c44788435b8ca5d079", null ],
    [ "icon", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a93b6ed78e5cba3ce99607c34a710a96d", null ],
    [ "ifaceIid", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a2a64b992e50d15b4c67f6112b6bcaef8", null ],
    [ "iid", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a43a559bdc445a7032e784bcd570330e4", null ],
    [ "infoIface", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a7aa1f0e7d5365c77f9dd44b7b0dd42da", null ],
    [ "libraryFileName", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a149a7b768dc09157dc9cb2dc118d2285", null ],
    [ "name", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#aa1ca2f651a7647bee09d2a50d892de25", null ],
    [ "pluginAuthors", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a98546b3aa26a43b6695a25643b73725e", null ],
    [ "setLibraryFileName", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#afde82d7d92a1d75dbae094a66c669057", null ],
    [ "setShouldLoaded", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a27e50fb1f2756122f15cc289f7e02c7b", null ],
    [ "setup", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a72294714239590eed94077c18093a5f3", null ],
    [ "setVisible", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a8140de1f7aa50c20fd137921f0ad63a8", null ],
    [ "shouldLoaded", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a0806aedffe128e70253d910c1dbf5690", null ],
    [ "version", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html#a71a6f035204fb005960edf5d285884a9", null ]
];