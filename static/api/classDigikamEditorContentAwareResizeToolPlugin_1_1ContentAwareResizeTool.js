var classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool =
[
    [ "RenderingMode", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#ac3d6fd9ffa7c089539b893a7a58be6b2", [
      [ "NoneRendering", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#ac3d6fd9ffa7c089539b893a7a58be6b2adbde621cb0749a02dd6a0a295fabbd44", null ],
      [ "PreviewRendering", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#ac3d6fd9ffa7c089539b893a7a58be6b2ab66447d37e1ba796bfad3849f5fbf725", null ],
      [ "FinalRendering", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#ac3d6fd9ffa7c089539b893a7a58be6b2ac4f7158b77330e3c765a11ced16f031c", null ]
    ] ],
    [ "ContentAwareResizeTool", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a7318beb01c3ab31035d80a014d21b77d", null ],
    [ "~ContentAwareResizeTool", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a901430fe4c1abd4ae0c2c1f67d62ddbd", null ],
    [ "analyser", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#ac5f059e8e6f16354639aaff6511d9c59", null ],
    [ "analyserCompleted", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a2f780143587b25066940ceae39b5ed58", null ],
    [ "cancelClicked", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a079c4e8e0189f2b08ac97c57d39d4e1d", null ],
    [ "deleteFilterInstance", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a76cad493ae538f89dd9df7f42f1cb3fb", null ],
    [ "exposureSettingsChanged", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a77604957de501c1286831c44c32691f3", null ],
    [ "filter", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a483e012282390f2aae585612cc233adf", null ],
    [ "finalRendering", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a5bd206334d2f6e89e1b7900c944d7c03", null ],
    [ "ICCSettingsChanged", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a9220394e71620f7262b306d2ceda49fd", null ],
    [ "init", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#ade313efb59416fe168e9d494a8261714", null ],
    [ "okClicked", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#aaf16461888d12351963e1892317b8d02", null ],
    [ "plugin", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#ac9f258dfdcc8e161b2e3e04945640053", null ],
    [ "renderingMode", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#adb3cf4a66b58fe11279cc443302a9661", null ],
    [ "setAnalyser", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#aa085c2dc38420de813a896f74173b6d1", null ],
    [ "setBackgroundColor", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a07c8dabdc2fa11e3fbf45a5de6e3084a", null ],
    [ "setBusy", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#adc9455ddc0cc3e74ffd177fa85a89c2c", null ],
    [ "setFilter", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a2e62e2ce1707123ebc1f6b790080e378", null ],
    [ "setInitPreview", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#ac253001bb85c9329a687fc21298e680d", null ],
    [ "setPlugin", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#af1c52946b840a515d1e15c05a077e0e6", null ],
    [ "setPreviewModeMask", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#aaf540040a7c43ac693eed06537d8c55f", null ],
    [ "setProgressMessage", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a9350e3f48796b382e658d256fd5fa666", null ],
    [ "setToolCategory", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a9a2b760ae7d24d126b5e8b8295a804bf", null ],
    [ "setToolHelp", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a5a58fa2bcc13672c3fa3f372e006a416", null ],
    [ "setToolIcon", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#aab748c0d89f3791f1096867dcc03a0ca", null ],
    [ "setToolInfoMessage", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#aafb8d4048e28c142cdec61af4e9f791f", null ],
    [ "setToolName", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#ab4f48edf807c06660bba77d411672336", null ],
    [ "setToolSettings", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a81a16e17809e0c1517c09cff1134041e", null ],
    [ "setToolVersion", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#abd5232a8268bb211859bba4acd386277", null ],
    [ "setToolView", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a22889920743aafd0773c877a25f69f2e", null ],
    [ "slotAbort", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a41de685d774c6a146ad8078933b27adf", null ],
    [ "slotAnalyserFinished", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#af856e4ce609759e7c36dcc815a93d196", null ],
    [ "slotAnalyserStarted", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a117e0b69a814c8f79710b16497c0c0dc", null ],
    [ "slotApplyTool", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#acb6a259870c46dcaf95ff3ea0d583e83", null ],
    [ "slotCancel", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a0c449a1ea7e9fdc01e4383117b8c6463", null ],
    [ "slotChannelChanged", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a475dbb46a84bc63ebf9cb6eeb9863b2a", null ],
    [ "slotCloseTool", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a2baf799e8d8ae25fd947696fe99263bb", null ],
    [ "slotFilterFinished", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#ab2be25a913731793eb9209422a7f6508", null ],
    [ "slotFilterStarted", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a9ce953ad6bc30cf8e86729abe65c08f1", null ],
    [ "slotInit", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a0cecdbcbea20574ce8ccd6fcc6127a8d", null ],
    [ "slotLoadSettings", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#af4ebc905a87855fcd79705bdfe835afa", null ],
    [ "slotOk", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#ace3e7bea6fd42a19edf0e08c0692ed3a", null ],
    [ "slotPreview", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#aab459857669608b0544bf312b561f6bc", null ],
    [ "slotPreviewModeChanged", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a5d112b1773806c67813deb6ef764787e", null ],
    [ "slotProgress", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a2d12fadb5f6fc61d799bfe049bb22558", null ],
    [ "slotSaveAsSettings", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#afc11c0ed3ea697f00b4fc166fb4ea8ce", null ],
    [ "slotScaleChanged", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#ac2385c79f4cf636bc1c615dc23ec657a", null ],
    [ "slotTimer", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a5cef4546adc836a3fd9bfea6aad8e1d2", null ],
    [ "slotUpdateSpotInfo", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#abf7a46f52815a5ead35007caa77a3ff7", null ],
    [ "toolCategory", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a96deea8a2ee1155e014bfef257b84d3f", null ],
    [ "toolHelp", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#aa3a4ca4085bc733beb0a577929a535c2", null ],
    [ "toolIcon", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#ac8e1df7974d5dbfd448a1d4d87b4273f", null ],
    [ "toolName", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a18c70c25e0103b592fd74c5dcdf30a21", null ],
    [ "toolSettings", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a20fa60db7f77d9955314bf52900b42a2", null ],
    [ "toolVersion", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#a3deae60f1ffeea29811b8fe75783f0a4", null ],
    [ "toolView", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html#aeb9bb1a5bcab1ee44299230a1c935a58", null ]
];