var namespaceDigikamGenericSendByMailPlugin =
[
    [ "BalsaBinary", "classDigikamGenericSendByMailPlugin_1_1BalsaBinary.html", "classDigikamGenericSendByMailPlugin_1_1BalsaBinary" ],
    [ "ClawsMailBinary", "classDigikamGenericSendByMailPlugin_1_1ClawsMailBinary.html", "classDigikamGenericSendByMailPlugin_1_1ClawsMailBinary" ],
    [ "EvolutionBinary", "classDigikamGenericSendByMailPlugin_1_1EvolutionBinary.html", "classDigikamGenericSendByMailPlugin_1_1EvolutionBinary" ],
    [ "ImageResizeJob", "classDigikamGenericSendByMailPlugin_1_1ImageResizeJob.html", "classDigikamGenericSendByMailPlugin_1_1ImageResizeJob" ],
    [ "ImageResizeThread", "classDigikamGenericSendByMailPlugin_1_1ImageResizeThread.html", "classDigikamGenericSendByMailPlugin_1_1ImageResizeThread" ],
    [ "KmailBinary", "classDigikamGenericSendByMailPlugin_1_1KmailBinary.html", "classDigikamGenericSendByMailPlugin_1_1KmailBinary" ],
    [ "MailAlbumsPage", "classDigikamGenericSendByMailPlugin_1_1MailAlbumsPage.html", "classDigikamGenericSendByMailPlugin_1_1MailAlbumsPage" ],
    [ "MailFinalPage", "classDigikamGenericSendByMailPlugin_1_1MailFinalPage.html", "classDigikamGenericSendByMailPlugin_1_1MailFinalPage" ],
    [ "MailImagesPage", "classDigikamGenericSendByMailPlugin_1_1MailImagesPage.html", "classDigikamGenericSendByMailPlugin_1_1MailImagesPage" ],
    [ "MailIntroPage", "classDigikamGenericSendByMailPlugin_1_1MailIntroPage.html", "classDigikamGenericSendByMailPlugin_1_1MailIntroPage" ],
    [ "MailProcess", "classDigikamGenericSendByMailPlugin_1_1MailProcess.html", "classDigikamGenericSendByMailPlugin_1_1MailProcess" ],
    [ "MailSettings", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html", "classDigikamGenericSendByMailPlugin_1_1MailSettings" ],
    [ "MailSettingsPage", "classDigikamGenericSendByMailPlugin_1_1MailSettingsPage.html", "classDigikamGenericSendByMailPlugin_1_1MailSettingsPage" ],
    [ "MailWizard", "classDigikamGenericSendByMailPlugin_1_1MailWizard.html", "classDigikamGenericSendByMailPlugin_1_1MailWizard" ],
    [ "NetscapeBinary", "classDigikamGenericSendByMailPlugin_1_1NetscapeBinary.html", "classDigikamGenericSendByMailPlugin_1_1NetscapeBinary" ],
    [ "SendByMailPlugin", "classDigikamGenericSendByMailPlugin_1_1SendByMailPlugin.html", "classDigikamGenericSendByMailPlugin_1_1SendByMailPlugin" ],
    [ "SylpheedBinary", "classDigikamGenericSendByMailPlugin_1_1SylpheedBinary.html", "classDigikamGenericSendByMailPlugin_1_1SylpheedBinary" ],
    [ "ThunderbirdBinary", "classDigikamGenericSendByMailPlugin_1_1ThunderbirdBinary.html", "classDigikamGenericSendByMailPlugin_1_1ThunderbirdBinary" ]
];