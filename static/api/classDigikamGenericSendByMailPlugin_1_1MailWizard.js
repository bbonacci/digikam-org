var classDigikamGenericSendByMailPlugin_1_1MailWizard =
[
    [ "MailWizard", "classDigikamGenericSendByMailPlugin_1_1MailWizard.html#a9a4cca30cb9344098e65b4c9ddc72f7b", null ],
    [ "~MailWizard", "classDigikamGenericSendByMailPlugin_1_1MailWizard.html#a5b31445d1e4b06ebdfa462038e0c4d72", null ],
    [ "iface", "classDigikamGenericSendByMailPlugin_1_1MailWizard.html#a77d0a3a57cf7f8375b3fc112b5f93f65", null ],
    [ "nextId", "classDigikamGenericSendByMailPlugin_1_1MailWizard.html#a2ed8a38a97f144f8630ff25bce897706", null ],
    [ "restoreDialogSize", "classDigikamGenericSendByMailPlugin_1_1MailWizard.html#a99a0591ddddda89952710d912c0a8e68", null ],
    [ "saveDialogSize", "classDigikamGenericSendByMailPlugin_1_1MailWizard.html#a37a3b930d03a859f95dfef14004c3448", null ],
    [ "setItemsList", "classDigikamGenericSendByMailPlugin_1_1MailWizard.html#aedaaf1decfd2ff1116390c3915737817", null ],
    [ "setPlugin", "classDigikamGenericSendByMailPlugin_1_1MailWizard.html#a1aa458a15664294147f3b6cb10c9323f", null ],
    [ "settings", "classDigikamGenericSendByMailPlugin_1_1MailWizard.html#ab5ea5cd08d0f195cd5abffc02fbaa2b5", null ],
    [ "validateCurrentPage", "classDigikamGenericSendByMailPlugin_1_1MailWizard.html#a69cd64cd62436eea0e5506b2348e79fb", null ]
];