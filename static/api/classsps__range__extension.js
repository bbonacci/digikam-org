var classsps__range__extension =
[
    [ "sps_range_extension", "classsps__range__extension.html#a8ef407961e2e207174252312f2d356e0", null ],
    [ "dump", "classsps__range__extension.html#a53740ab2068cbbc35dbe10964f4e37fa", null ],
    [ "read", "classsps__range__extension.html#aae0050596e4c9cf5650d99704e4f6c71", null ],
    [ "cabac_bypass_alignment_enabled_flag", "classsps__range__extension.html#a2cb841cac5c6cc96d7da79f053d18343", null ],
    [ "explicit_rdpcm_enabled_flag", "classsps__range__extension.html#a06dce5a197c160640243fd11a556251c", null ],
    [ "extended_precision_processing_flag", "classsps__range__extension.html#afb22906479bdb80bb75e86e195a3869d", null ],
    [ "high_precision_offsets_enabled_flag", "classsps__range__extension.html#a11b2f0d880ae0202ffb7da53745850ae", null ],
    [ "implicit_rdpcm_enabled_flag", "classsps__range__extension.html#a2eb73b25372df60c0feae389a2aae4df", null ],
    [ "intra_smoothing_disabled_flag", "classsps__range__extension.html#a2582ce1c883c1023e444fa40a0bbfa38", null ],
    [ "persistent_rice_adaptation_enabled_flag", "classsps__range__extension.html#a8e5a2e5e2b325d83147f7d5d27091cbc", null ],
    [ "transform_skip_context_enabled_flag", "classsps__range__extension.html#a5f4dc5ac85bbb906c6db5b36b188010c", null ],
    [ "transform_skip_rotation_enabled_flag", "classsps__range__extension.html#a5dc5be61bb81556f82adb00966629aad", null ]
];