var iccjpeg_8c =
[
    [ "ICC_MARKER", "iccjpeg_8c.html#a2af4a4a687d04eb425e7558baa0afaac", null ],
    [ "ICC_OVERHEAD_LEN", "iccjpeg_8c.html#afaa2f0efa2c6881450456a6f3f2902f6", null ],
    [ "MAX_BYTES_IN_MARKER", "iccjpeg_8c.html#ae6815b54203c94d91515db5b3a6486f9", null ],
    [ "MAX_DATA_BYTES_IN_MARKER", "iccjpeg_8c.html#aea5d196235eafea80dee110c23135164", null ],
    [ "MAX_SEQ_NO", "iccjpeg_8c.html#a644abd334cf3b5ad6f0f5adde4924eb4", null ],
    [ "read_icc_profile", "iccjpeg_8c.html#acfda1ba64bd6d54c850e542d99c3bad6", null ],
    [ "setup_read_icc_profile", "iccjpeg_8c.html#ac6515cfd300896100d3c9404a23121ff", null ],
    [ "write_icc_profile", "iccjpeg_8c.html#ae24d4513b6283fe2da1c429d5affcda0", null ]
];