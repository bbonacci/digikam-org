var dir_e38047d7c7da44dd3ff7414967cf6fe0 =
[
    [ "presentation_audiopage.cpp", "presentation__audiopage_8cpp.html", null ],
    [ "presentation_audiopage.h", "presentation__audiopage_8h.html", [
      [ "PresentationAudioPage", "classDigikamGenericPresentationPlugin_1_1PresentationAudioPage.html", "classDigikamGenericPresentationPlugin_1_1PresentationAudioPage" ],
      [ "SoundtrackPreview", "classDigikamGenericPresentationPlugin_1_1SoundtrackPreview.html", "classDigikamGenericPresentationPlugin_1_1SoundtrackPreview" ]
    ] ],
    [ "presentationaudiolist.cpp", "presentationaudiolist_8cpp.html", null ],
    [ "presentationaudiolist.h", "presentationaudiolist_8h.html", [
      [ "PresentationAudioList", "classDigikamGenericPresentationPlugin_1_1PresentationAudioList.html", "classDigikamGenericPresentationPlugin_1_1PresentationAudioList" ],
      [ "PresentationAudioListItem", "classDigikamGenericPresentationPlugin_1_1PresentationAudioListItem.html", "classDigikamGenericPresentationPlugin_1_1PresentationAudioListItem" ]
    ] ],
    [ "presentationaudiowidget.cpp", "presentationaudiowidget_8cpp.html", null ],
    [ "presentationaudiowidget.h", "presentationaudiowidget_8h.html", [
      [ "PresentationAudioWidget", "classDigikamGenericPresentationPlugin_1_1PresentationAudioWidget.html", "classDigikamGenericPresentationPlugin_1_1PresentationAudioWidget" ]
    ] ]
];