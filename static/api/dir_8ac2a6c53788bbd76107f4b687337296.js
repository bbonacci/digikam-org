var dir_8ac2a6c53788bbd76107f4b687337296 =
[
    [ "oditem.h", "oditem_8h.html", [
      [ "ODFolder", "classDigikamGenericOneDrivePlugin_1_1ODFolder.html", "classDigikamGenericOneDrivePlugin_1_1ODFolder" ],
      [ "ODPhoto", "classDigikamGenericOneDrivePlugin_1_1ODPhoto.html", "classDigikamGenericOneDrivePlugin_1_1ODPhoto" ]
    ] ],
    [ "odmpform.cpp", "odmpform_8cpp.html", null ],
    [ "odmpform.h", "odmpform_8h.html", [
      [ "ODMPForm", "classDigikamGenericOneDrivePlugin_1_1ODMPForm.html", "classDigikamGenericOneDrivePlugin_1_1ODMPForm" ]
    ] ],
    [ "odnewalbumdlg.cpp", "odnewalbumdlg_8cpp.html", null ],
    [ "odnewalbumdlg.h", "odnewalbumdlg_8h.html", [
      [ "ODNewAlbumDlg", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg.html", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg" ]
    ] ],
    [ "odplugin.cpp", "odplugin_8cpp.html", null ],
    [ "odplugin.h", "odplugin_8h.html", "odplugin_8h" ],
    [ "odtalker.cpp", "odtalker_8cpp.html", null ],
    [ "odtalker.h", "odtalker_8h.html", [
      [ "ODTalker", "classDigikamGenericOneDrivePlugin_1_1ODTalker.html", "classDigikamGenericOneDrivePlugin_1_1ODTalker" ]
    ] ],
    [ "odwidget.cpp", "odwidget_8cpp.html", null ],
    [ "odwidget.h", "odwidget_8h.html", [
      [ "ODWidget", "classDigikamGenericOneDrivePlugin_1_1ODWidget.html", "classDigikamGenericOneDrivePlugin_1_1ODWidget" ]
    ] ],
    [ "odwindow.cpp", "odwindow_8cpp.html", null ],
    [ "odwindow.h", "odwindow_8h.html", [
      [ "ODWindow", "classDigikamGenericOneDrivePlugin_1_1ODWindow.html", "classDigikamGenericOneDrivePlugin_1_1ODWindow" ]
    ] ]
];