var classDigikamGenericImageShackPlugin_1_1ImageShackNewAlbumDlg =
[
    [ "ImageShackNewAlbumDlg", "classDigikamGenericImageShackPlugin_1_1ImageShackNewAlbumDlg.html#a042da0bb3d3ae0ec7eefcfeea0eed211", null ],
    [ "~ImageShackNewAlbumDlg", "classDigikamGenericImageShackPlugin_1_1ImageShackNewAlbumDlg.html#a23cd1601d789019bd9be22359955538b", null ],
    [ "addToMainLayout", "classDigikamGenericImageShackPlugin_1_1ImageShackNewAlbumDlg.html#add6de4ea3bfd2840ec32de4b3ec3e62d", null ],
    [ "getAlbumBox", "classDigikamGenericImageShackPlugin_1_1ImageShackNewAlbumDlg.html#a2a796c4f2807c1e8ed6cf40affa5afa0", null ],
    [ "getAlbumTitle", "classDigikamGenericImageShackPlugin_1_1ImageShackNewAlbumDlg.html#a9a12db59181a9f0e384ff6161d91fc4b", null ],
    [ "getBaseAlbumProperties", "classDigikamGenericImageShackPlugin_1_1ImageShackNewAlbumDlg.html#ab06d1922af37d55a29df704167231037", null ],
    [ "getButtonBox", "classDigikamGenericImageShackPlugin_1_1ImageShackNewAlbumDlg.html#a17db77c1fc6054529b012766af159b55", null ],
    [ "getDateTimeEdit", "classDigikamGenericImageShackPlugin_1_1ImageShackNewAlbumDlg.html#af7f30d95ea761e4c49e7970894690591", null ],
    [ "getDescEdit", "classDigikamGenericImageShackPlugin_1_1ImageShackNewAlbumDlg.html#a48787b212858a75b47331abf230f4412", null ],
    [ "getLocEdit", "classDigikamGenericImageShackPlugin_1_1ImageShackNewAlbumDlg.html#a3578365a1d3f6662decfb76b077611d2", null ],
    [ "getMainWidget", "classDigikamGenericImageShackPlugin_1_1ImageShackNewAlbumDlg.html#a12076a20f061f220d81a951734d29a67", null ],
    [ "getTitleEdit", "classDigikamGenericImageShackPlugin_1_1ImageShackNewAlbumDlg.html#a356426352dc81b5419852be6b742397b", null ],
    [ "hideDateTime", "classDigikamGenericImageShackPlugin_1_1ImageShackNewAlbumDlg.html#a7d7d1c674a00240ed5628a621dd1471d", null ],
    [ "hideDesc", "classDigikamGenericImageShackPlugin_1_1ImageShackNewAlbumDlg.html#a90e69c3d0fbae903d3e3db5268aa8df1", null ],
    [ "hideLocation", "classDigikamGenericImageShackPlugin_1_1ImageShackNewAlbumDlg.html#a335987a85da0b9b0bd083f3b85250370", null ]
];