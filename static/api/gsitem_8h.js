var gsitem_8h =
[
    [ "GSFolder", "classDigikamGenericGoogleServicesPlugin_1_1GSFolder.html", "classDigikamGenericGoogleServicesPlugin_1_1GSFolder" ],
    [ "GSPhoto", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto" ],
    [ "GoogleService", "gsitem_8h.html#a0771e4280da801e307777a48826af296", [
      [ "GDrive", "gsitem_8h.html#a0771e4280da801e307777a48826af296ad431a13f1bdc0ce053d6bb0f1d56ad39", null ],
      [ "GPhotoExport", "gsitem_8h.html#a0771e4280da801e307777a48826af296a377b5da2eb07d57046c08bd143f97ff9", null ],
      [ "GPhotoImport", "gsitem_8h.html#a0771e4280da801e307777a48826af296aae0f1cc2b990bb7a68dc5737ce0409fa", null ]
    ] ]
];