var dir_b888119061e974e1b462d4737ba9c3a6 =
[
    [ "facepipeline.cpp", "facepipeline_8cpp.html", null ],
    [ "facepipeline.h", "facepipeline_8h.html", [
      [ "FacePipeline", "classDigikam_1_1FacePipeline.html", "classDigikam_1_1FacePipeline" ]
    ] ],
    [ "facepipeline_p.cpp", "facepipeline__p_8cpp.html", null ],
    [ "facepipeline_p.h", "facepipeline__p_8h.html", [
      [ "Private", "classDigikam_1_1FacePipeline_1_1Private.html", "classDigikam_1_1FacePipeline_1_1Private" ]
    ] ],
    [ "facepipelinepackage.cpp", "facepipelinepackage_8cpp.html", null ],
    [ "facepipelinepackage.h", "facepipelinepackage_8h.html", [
      [ "FacePipelineExtendedPackage", "classDigikam_1_1FacePipelineExtendedPackage.html", "classDigikam_1_1FacePipelineExtendedPackage" ],
      [ "FacePipelineFaceTagsIface", "classDigikam_1_1FacePipelineFaceTagsIface.html", "classDigikam_1_1FacePipelineFaceTagsIface" ],
      [ "FacePipelineFaceTagsIfaceList", "classDigikam_1_1FacePipelineFaceTagsIfaceList.html", "classDigikam_1_1FacePipelineFaceTagsIfaceList" ],
      [ "FacePipelinePackage", "classDigikam_1_1FacePipelinePackage.html", "classDigikam_1_1FacePipelinePackage" ],
      [ "PackageLoadingDescriptionList", "classDigikam_1_1PackageLoadingDescriptionList.html", "classDigikam_1_1PackageLoadingDescriptionList" ]
    ] ],
    [ "facepreviewloader.cpp", "facepreviewloader_8cpp.html", null ],
    [ "facepreviewloader.h", "facepreviewloader_8h.html", [
      [ "FacePreviewLoader", "classDigikam_1_1FacePreviewLoader.html", "classDigikam_1_1FacePreviewLoader" ]
    ] ],
    [ "parallelpipes.cpp", "parallelpipes_8cpp.html", null ],
    [ "parallelpipes.h", "parallelpipes_8h.html", [
      [ "ParallelPipes", "classDigikam_1_1ParallelPipes.html", "classDigikam_1_1ParallelPipes" ]
    ] ],
    [ "scanstatefilter.cpp", "scanstatefilter_8cpp.html", null ],
    [ "scanstatefilter.h", "scanstatefilter_8h.html", [
      [ "ScanStateFilter", "classDigikam_1_1ScanStateFilter.html", "classDigikam_1_1ScanStateFilter" ]
    ] ]
];