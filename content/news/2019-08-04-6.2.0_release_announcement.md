---
date: "2019-08-04T00:00:00"
title: "digiKam 6.2.0 is released"
author: "digiKam Team"
description: "Dear digiKam fans and users, after 4 months of development, we are proud to announce the new digiKam 6.2.0."
category: "news"
---

[![](https://i.imgur.com/yXIstf1.jpg "digiKam 6.2.0-01)](https://imgur.com/yXIstf1)

Dear digiKam fans and users, 

We received a lot of excellent user feedback after publishing the second
digiKam 6 release in April 2019. We are now proud to briefly announce the
new digiKam 6.2.0, a maintenance version which consolidates this feedback
and acts as an important phase of this 2-year-old project.

#### New Cameras Supported by Raw Files Engine

[![](https://i.imgur.com/olYs6bl.jpg "digiKam 6.2.0-02")](https://imgur.com/olYs6bl)

digiKam tries to fully support all digital camera formats. However, Raw file
support is a big challenge. This complexity, and the associated difficulty
and commitment required to continue full support over time, has led to the
development of applications specifically created only to support Raw files.

Raw files are not like JPEG. Nothing is standardized, and camera makers are
free to change everything inside these digital containers without
documentation. Raw files permit manufacturers to reinvent existing methods,
implement hidden features, cache metadata, and require a powerful computer
to process data. When you buy an expensive camera, you might expect that the
images provided are seriously pre-processed by the camera firmware and ready
to use immediately.

This is true for JPEG, not Raw files. Even if JPEG is not perfect, it's well
standardized and well documented. For Raw, the formats can change with each
new camera release as it provides raw, in-depth on-camera sensor data not
processed by camera firmware. Supporting these changes requires intensive
reverse-engineering that the digiKam team cannot directly support.
Consequently, we use the powerful LibRaw library to post-process the Raw
files on the computer. This library includes complex algorithms to support
all different Raw file formats.

In digiKam 6.2.0, we use the LibRaw 0.19.3 maintenance version which
introduces a few new Raw formats to support the most recent camera models
available on the photo market. See the list below for details:

 * Canon Powershot A560
 * FujiFilm X-T30
 * Nikon Coolpix A1000, Z6, Z7
 * Olympus E-M1X
 * Sony ILCE-6400
 * Several dng files from phones and drones.

This LibRaw version is able to process in totality more than 1000 Raw
formats. You can find the complete list in digiKam and Showfoto through the
Help/Supported Raw Camera dialog or [at this
url](https://www.libraw.org/supported-cameras-snapshot-201903).

Thanks to the LibRaw team for sharing and maintaining this experience.

### Low Level File Metadata Library Exiv2 0.27.2 Support

With this new release we are following the Exiv2 project which has released
their new stable version 0.27.2. This library is a main component of digiKam
and
used for file metadata interactions like populating database contents,
updating 
item textual information, or handling XMP side-cars for read only files.

digiKam 6.2.0 now supports the new API of this library. All 6.2.0 binary
bundles
that we package for Windows, MacOS and Linux have been compiled with this
Exiv2 version.

[![](https://i.imgur.com/6IgdT0a.png "digiKam 6.2.0-03")](https://imgur.com/6IgdT0a)

#### Embedded Video Player QtAV 1.13.0 Support

In release 5.5.0 we began using the QtAV framework to play video media in
digiKam. We chose QtAV because this framework directly uses ffmpeg codecs
which de facto supports all formats very well. In contrast, the QtMultimedia
framework requires extra platform codecs that end users need to explicitly
install to obtain functional video support. digiKam users should not need to
install extra programs for video; it should work automatically upon
installation. digiKam must be as simple to deal with video as it is for
photo.

digiKam has the capability to play video files directly, without requiring
an extra video player and dedicated codecs (thanks to
[FFmpeg](https://www.ffmpeg.org/) and [QtAV](http://www.qtav.org/)
frameworks).

2 years ago, QtAV 1.12.0 was released with CMake support, but no updates
were published since. We were very happy to see a new release available just
in time to follow the digiKam 6.2.0 release plan, and so digiKam has been
updated to support the new API from the QtAV 1.13.0 library. All 6.2.0
binary bundles that we package for Windows, MacOS and Linux have been
compiled with this QtAV version.

[![](https://i.imgur.com/ohRA5hG.png "digiKam 6.2.0-04")](https://imgur.com/ohRA5hG)

### Iconview : HiDPI Support for 4K Screens

With this release, Album management gets a new feature: support for
icon-view items rendering on HiDPI 4K screens. Before this digiKam release,
if you have a 27 inch 4K display (typically with a resolution of 3840x2160
pixels), the icon view contents became small and pixelated. Now, digiKam
scales up images to display them correctly on HiDPI-enabled screens. To use
this feature, go to the Setup/View/Icons configuration dialog page.

[![](https://i.imgur.com/AGQszU1.png "digiKam 6.2.0-05")](https://imgur.com/AGQszU1)

### Portable Archive for Windows

With this release, we start to support the [Portable project](https://portableapps.com/) to publish
a 32 and 64 bits archive of the official Windows installers internal contents. This permit to end-users
to just uncompress the binary tarball in home directory, and run digiKam or Showfoto as well without
to use the standard Windows installer, as this one can require administrator rights. Of course, this method do
not register the application links to Start menu and is only available to just one end user.

[![](https://i.imgur.com/tyDMvBy.png "digiKam 6.2.0-06")](https://imgur.com/tyDMvBy)

### The Final Words

Thanks to all users for your support, and to all contributors, students, and
testers who allowed us to improve this release.

digiKam 6.2.0 source code tarball, Linux 32/64 bits AppImage bundles, MacOS
package and Windows 32/64 bits installers can be downloaded from [this
repository](https://download.kde.org/stable/digikam/6.2.0/).

Rendez-vous in few months for the next 6.3.0 release and a first report
about students who completed their summer jobs for the [GSoC 2019
event](https://community.kde.org/GSoC/2019/Ideas#digiKam).

Happy digiKaming...
