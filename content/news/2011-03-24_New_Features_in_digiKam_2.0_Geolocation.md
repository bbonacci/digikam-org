---
date: "2011-03-24T10:36:00Z"
title: "New Features in digiKam 2.0: Geolocation"
author: "Dmitri Popov"
description: "Geolocation is not a new feature, but in digiKam 2.0 it has been thoroughly reworked to streamline the process of geotagging photos. The new Geolocation"
category: "news"
aliases: "/node/588"

---

<p>Geolocation is not a new feature, but in digiKam 2.0 it has been thoroughly reworked to streamline the process of geotagging photos. The new Geolocation interface (Image » Geo-location) aggregates all geotagging tools in one place. <a href="http://scribblesandsnaps.wordpress.com/2011/03/24/new-features-in-digikam-2-0-geolocation/">Continue to read</a></p>

<div class="legacy-comments">

</div>