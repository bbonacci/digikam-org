---
date: "2014-10-09T12:48:00Z"
title: "digiKam Software Collection 4.4.0 released..."
author: "digiKam"
description: "Dear digiKam fans and users, The digiKam Team is proud to announce the release of digiKam Software Collection 4.4.0. This release includes some new features:"
category: "news"
aliases: "/node/719"

---

<a href="https://www.flickr.com/photos/digikam/15428982716"><img src="https://farm4.staticflickr.com/3929/15428982716_f238a6b06f_c.jpg" width="800" height="225"></a>

<p>Dear digiKam fans and users,</p>

<p>
The digiKam Team is proud to announce the release of digiKam Software Collection 4.4.0. This release includes some new features:
</p>

<ul>

<li>
Slideshow support multi-monitors computer and use a new OSD widget. Users can also change Tags and Labels through keyboard shortcuts.
</li>

<li>
New keyboard shortcut to switch on/off color managed view for thumbnails and preview.
</li>

<li>
New keyboard shortcuts to improve usability while photo review (as to switch focus on text edit widget, or to toggle tags view). It's also possible to navigate between pictures using PageUp/PageDown while editing properties through captions/tags sidebar.
</li>

</ul>

<p>
After a second long bugs triage since 4.3.0 release, we have worked hard to close another sets of reported issues.. 
See the new list of the <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=4.4.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">issues closed</a> in digiKam 4.4.0 available through the KDE Bugtracking System.
</p>

<p>The digiKam software collection tarball can be downloaded from the <a href="http://download.kde.org/stable/digikam/digikam-4.4.0.tar.bz2.mirrorlist">KDE repository</a>.
</p>

<p>Have fun playing with your photos using this new release,</p>

<p>digiKam Team...</p>
<div class="legacy-comments">

  <a id="comment-20877"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20877" class="active">hmm</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2014-10-09 18:28.</div>
    <div class="content">
     <p>Is this now the trend in KDE land, to call everything a Software Collection?</p>
<p>DigiKam is mature, but to call it a SC is a bit over the top</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20878"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20878" class="active">digiKam =</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2014-10-09 18:37.</div>
    <div class="content">
     <p>- core<br>
- kipi-plugins<br>
- libkexiv2<br>
- libkdcraw<br>
- libkipi<br>
- libkface<br>
- libkgeomap </p>
<p>This is a Software Collection. That all !</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20882"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20882" class="active">Thanks for this comment! I</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2014-10-10 13:07.</div>
    <div class="content">
     <p>Thanks for this comment! I was always confused as to what "software collection" meant. So, digikam 4.4 is out.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20879"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20879" class="active">Thank you</a></h3>    <div class="submitted">Submitted by <a href="http://www.researchut.com" rel="nofollow">Ritesh Raj Sarraf</a> (not verified) on Thu, 2014-10-09 19:43.</div>
    <div class="content">
     <p>Thank you for Digikam. I am almost done tagging my entire photo colleciton (3000 so far) with facial recognition. And the best part is that my data, my annotations, and my tagging. All remain mine. That is the most valuable part of free software.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20880"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20880" class="active">Install on Kubuntu 14.04</a></h3>    <div class="submitted">Submitted by Christian Graesser (not verified) on Fri, 2014-10-10 06:50.</div>
    <div class="content">
     <p>It was a nightmare with DigiKam 4.3.0 but the new one 4.4.0 works nicely - no problems so far, Videos, Images, printing, tagging, viewing. I like the integrated image editing a lot - Very useful interfaces and workflows. Thanks to digiKam team!</p>
<p>Thanks to Philip Johnsson for his backports to install it on Kubuntu: https://launchpad.net/~philip5/+archive/ubuntu/kubuntu-backports.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20884"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20884" class="active">I previously had ver. 4.0.0</a></h3>    <div class="submitted">Submitted by Charles (not verified) on Fri, 2014-10-10 20:45.</div>
    <div class="content">
     <p>I previously had ver. 4.0.0 installed from another ppa, which I removed. It worked but was unstable, crashing without warning 3-4 times a day. I installed 4.4.0 on my Kubuntu 14.04 machine (recent install) and now all I get is the splash screen. Then nothing.</p>
<p>I tried launching DigiKam from a console and this is the errors I'm getting:</p>
<p>charles@Charles-Kubuntu:~$ digikam<br>
Object::connect: No such signal org::freedesktop::UPower::DeviceAdded(QDBusObjectPath)<br>
Object::connect: No such signal org::freedesktop::UPower::DeviceRemoved(QDBusObjectPath)<br>
Bus::open: Can not get ibus-daemon's address.<br>
IBusInputContext::createInputContext: no connection to ibus-daemon<br>
digikam: symbol lookup error: digikam: undefined symbol: _ZNK11KExiv2Iface14AltLangStrEdit8textEditEv<br>
charles@Charles-Kubuntu:~$ </p>
<p>Thanks.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20885"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20885" class="active">I've installed ibus and now</a></h3>    <div class="submitted">Submitted by Charles (not verified) on Fri, 2014-10-10 20:55.</div>
    <div class="content">
     <p>I've installed ibus and now I'm not getting the error concerning it but the program still crashes:</p>
<p>charles@Charles-Kubuntu:~$ digikam<br>
Object::connect: No such signal org::freedesktop::UPower::DeviceAdded(QDBusObjectPath)<br>
Object::connect: No such signal org::freedesktop::UPower::DeviceRemoved(QDBusObjectPath)<br>
digikam: symbol lookup error: digikam: undefined symbol: _ZNK11KExiv2Iface14AltLangStrEdit8textEditEv<br>
charles@Charles-Kubuntu:~$</p>
<p>Thanks</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20887"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20887" class="active">Exactly the same isue here :(</a></h3>    <div class="submitted">Submitted by hamster (not verified) on Fri, 2014-10-10 21:19.</div>
    <div class="content">
     <p>Exactly the same isue here :(</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20888"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20888" class="active">If you are going to use my</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Fri, 2014-10-10 21:40.</div>
    <div class="content">
     <p>If you are going to use my kubuntu-backport PPA then it's also dependent on packages found in my "extra" PPA and also that you use KDE 4.14.x that the kubuntu team provides in their PPA. Just using my kubuntu-backport PPA isn't enough.</p>
<p>Hope that solves things.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20889"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20889" class="active">My Kubuntu install is fresh</a></h3>    <div class="submitted">Submitted by Charles (not verified) on Sat, 2014-10-11 02:17.</div>
    <div class="content">
     <p>My Kubuntu install is fresh from 2 days ago and KDE is 4.14.1. Check.</p>
<p>Removed Digikam 4.0.0 (which I'd returned to while I waited for a solution).</p>
<p>Added your "extra" PPA to my sources. </p>
<p>Updated everything that needed it (18 files).</p>
<p>Re-installed Digikam.</p>
<p>Started it and now it works. </p>
<p>Thanks!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20890"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20890" class="active">But... crashed again. Just</a></h3>    <div class="submitted">Submitted by Charles (not verified) on Sat, 2014-10-11 02:27.</div>
    <div class="content">
     <p>But... crashed again. Just clicking an image to display it does it.</p>
<p>I'm sending a full incident report to the developers.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20903"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20903" class="active">Still crashing 10 times a day</a></h3>    <div class="submitted">Submitted by Charles (not verified) on Thu, 2014-10-16 17:10.</div>
    <div class="content">
     <p>Still crashing 5+ times a day! I'm starting to get rather annoyed. </p>
<p>Is there anything I can do to resolve this? </p>
<p>I'm starting to think about dropping digiKam in favor of... something else. It's nice to have new features. But not at the price of stability.</p>
<p>Thanks!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20904"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20904" class="active">If you use Ubuntu then make</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Thu, 2014-10-16 19:26.</div>
    <div class="content">
     <p>If you use Ubuntu then make sure to get your libsqlite3-0 package updated and don't use the buggy 3.8.2 version that comes as default.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20909"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20909" class="active">I'm using Kubuntu 14.04 LTS.</a></h3>    <div class="submitted">Submitted by Charles (not verified) on Fri, 2014-10-17 14:30.</div>
    <div class="content">
     <p>I'm using Kubuntu 14.04 LTS. I have done all the updates. And that's where trouble seems to be coming from. Yesterday, 186 files had to be updated, requiring a reboot. I did this. And now Digikam 4.4.0 won't even start (again!).</p>
<p>Here is what happens at the console:</p>
<p>charles@Charles-Kubuntu:~$ digikam<br>
Object::connect: No such signal org::freedesktop::UPower::DeviceAdded(QDBusObjectPath)<br>
Object::connect: No such signal org::freedesktop::UPower::DeviceRemoved(QDBusObjectPath)<br>
digikam: symbol lookup error: digikam: undefined symbol: _ZNK11KExiv2Iface14AltLangStrEdit8textEditEv<br>
charles@Charles-Kubuntu:~$</p>
<p>Similar errors to what I had before I added the "extra" PPA, etc.</p>
<p>Thanks.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20913"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20913" class="active">"If you use Ubuntu then make</a></h3>    <div class="submitted">Submitted by Charles (not verified) on Fri, 2014-10-17 19:07.</div>
    <div class="content">
     <p>"If you use Ubuntu then make sure to get your libsqlite3-0 package updated and don't use the buggy 3.8.2 version that comes as default."</p>
<p>That seems to be precisely the version I've got presently. Where/how do I get a newer one?</p>
<p>Thanks.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20921"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20921" class="active">"If you use Ubuntu then make</a></h3>    <div class="submitted">Submitted by Charles (not verified) on Sat, 2014-10-18 21:45.</div>
    <div class="content">
     <p>"If you use Ubuntu then make sure to get your libsqlite3-0 package updated and don't use the buggy 3.8.2 version that comes as default."</p>
<p>I was able to download a replacement library at:</p>
<p>http://packages.ubuntu.com/utopic/libsqlite3-0</p>
<p>Then I did:</p>
<p>$ sudo dpkg -i libsqlite3-0_3.8.6-1_amd64.deb</p>
<p>and got:</p>
<p>[sudo] password for charles:<br>
(Lecture de la base de données... 170651 fichiers et répertoires déjà installés.)<br>
Préparation du décompactage de libsqlite3-0_3.8.6-1_amd64.deb ...<br>
Décompactage de libsqlite3-0:amd64 (3.8.6-1) sur (3.8.2-1ubuntu2) ...<br>
Paramétrage de libsqlite3-0:amd64 (3.8.6-1) ...<br>
Traitement déclenché pour libc-bin (2.19-0ubuntu6.3) ...</p>
<p>I then reactivated the "extra" and other PPAs and updated Digikam back to 4.4.0. in Muon package manager.</p>
<p>Now Digikam 4.4.0 started normally (which it had repeatedly refused to do) and has not crashed yet, after about 1 hour of use. So far so good then. I'll keep you posted how it's doing in a couple days.</p>
<p>Thanks.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div><a id="comment-20891"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20891" class="active">Thank you Philip! I was sure</a></h3>    <div class="submitted">Submitted by hamster (not verified) on Sat, 2014-10-11 13:20.</div>
    <div class="content">
     <p>Thank you Philip! I was sure I had extra ppa, but I must have removed it while cleaning my ppa list :( Sure tt works now. You are doing a great job, thank you very much!</p>
<p>On a side note: is it only me, right now on album view the smallest size of thumbnail is 128px. In 4.0-4.2 it was still 118, in 3.x series even less. Is there any way to change that?</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20886"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20886" class="active">update in native ubuntu 14.04</a></h3>    <div class="submitted">Submitted by GL (not verified) on Fri, 2014-10-10 21:12.</div>
    <div class="content">
     <p>Update from digikam 4.0 to 4.4 does not work for me using philipp/extra and kubuntu/backports.</p>
<p>I have a "native" Ubuntu 14.04 installed.</p>
<p>Die folgenden Pakete haben unerfüllte Abhängigkeiten:<br>
 digikam : Hängt ab von: libkgeomap1 (&gt;= 1.0~digikam4.3.0) aber 1.0~digikam4.0.0-0ubuntu1~ubuntu14.04~ppa2 soll installiert werden<br>
E: Probleme können nicht korrigiert werden, Sie haben zurückgehaltene defekte Pakete.</p>
<p>Any suggestions or workarounds,</p>
<p>Georg</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20892"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20892" class="active">*If you use (k)ubuntu 14.04</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Sat, 2014-10-11 19:17.</div>
    <div class="content">
     <p>*If you use (k)ubuntu 14.04 with standard KDE 4.13.x packages that comes with the official (k)ubuntu release then you only use my "extra" PPA and not the other.<br>
* If you use (k)ubuntu 14.04 with KDE 4.14.x that are provided with the kubuntu teams Kubuntu Backports PPA on Launchpad then you need BOTH my "extra" PPA AND my kubuntu-backports PPA.</p>
<p>My kubuntu-backports PPA only have special rebuilds of packages that need to be rebuilt aginst KDE 4.14.x to work with the newer KDE version. my "extra" PPA also have other updated packages that Digikam need with my builds.</p>
<p>If you use Linux Mint then it's up to you to find out what combination of PPAs you might need but I don't have or use Linux Mint to test my packages.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20898"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20898" class="active">Actually I use Mint KDE. And</a></h3>    <div class="submitted">Submitted by hamster (not verified) on Tue, 2014-10-14 16:13.</div>
    <div class="content">
     <p>Actually I use Mint KDE. And it works exactly the same way as for Ubuntu :)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20900"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20900" class="active">digikam keep crashing</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2014-10-15 05:19.</div>
    <div class="content">
     <p>I update to the latest through extra/ppa and now digikam is keep crashing.  I even try to rollback some digikam and exiv2 from .deb from /var/cache/apt/archive and it has the same crash.  So don't know what's going on. </p>
<p>The stack trace point to problem with RiffVideo::InfoTagsHandler()   </p>
<p>#0  0x00007ffff1008bb9 in __GI_raise (sig=sig@entry=6) at ../nptl/sysdeps/unix/sysv/linux/raise.c:56<br>
#1  0x00007ffff100bfc8 in __GI_abort () at abort.c:89<br>
#2  0x00007ffff1045e14 in __libc_message (do_abort=do_abort@entry=1,<br>
    fmt=fmt@entry=0x7ffff1154668 "*** Error in `%s': %s: 0x%s ***\n") at ../sysdeps/posix/libc_fatal.c:175<br>
#3  0x00007ffff10520ee in malloc_printerr (ptr=,<br>
    str=0x7ffff1154808 "free(): invalid next size (fast)", action=1) at malloc.c:4996<br>
#4  _int_free (av=, p=, have_lock=0) at malloc.c:3840<br>
#5  0x00007fffee2649c7 in Exiv2::RiffVideo::infoTagsHandler() () from /usr/lib/x86_64-linux-gnu/libexiv2.so.13<br>
#6  0x00007fffee269855 in Exiv2::RiffVideo::decodeBlock() () from /usr/lib/x86_64-linux-gnu/libexiv2.so.13<br>
#7  0x00007fffee2694a8 in Exiv2::RiffVideo::tagDecoder(Exiv2::DataBuf&amp;, unsigned long) ()<br>
   from /usr/lib/x86_64-linux-gnu/libexiv2.so.13<br>
#8  0x00007fffee269855 in Exiv2::RiffVideo::decodeBlock() () from /usr/lib/x86_64-linux-gnu/libexiv2.so.13<br>
#9  0x00007fffee269bc8 in Exiv2::RiffVideo::readMetadata() () from /usr/lib/x86_64-linux-gnu/libexiv2.so.13<br>
#10 0x00007ffff5c7a935 in KExiv2Iface::KExiv2::load(QString const&amp;) const () from /usr/lib/libkexiv2.so.11<br>
#11 0x00007ffff5596446 in Digikam::DMetadata::load(QString const&amp;) const ()<br>
   from /usr/lib/digikam/libdigikamcore.so.4.2.0<br>
#12 0x00007ffff4fede1f in Digikam::ImageScanner::loadFromDisk() ()<br>
   from /usr/lib/digikam/libdigikamdatabase.so.4.2.0<br>
#13 0x00007ffff4fee000 in Digikam::ImageScanner::newFile(int) ()<br>
   from /usr/lib/digikam/libdigikamdatabase.so.4.2.0<br>
#14 0x00007ffff4f861f6 in Digikam::CollectionScanner::scanNewFile(QFileInfo const&amp;, int) ()<br>
   from /usr/lib/digikam/libdigikamdatabase.so.4.2.0<br>
#15 0x00007ffff4f8936f in Digikam::CollectionScanner::scanAlbum(Digikam::CollectionLocation const&amp;, QString const&amp;) () from /usr/lib/digikam/libdigikamdatabase.so.4.2.0<br>
#16 0x00007ffff4f89227 in Digikam::CollectionScanner::scanAlbum(Digikam::CollectionLocation const&amp;, QString const&amp;) () from /usr/lib/digikam/libdigikamdatabase.so.4.2.0<br>
#17 0x00007ffff4f89c33 in Digikam::CollectionScanner::scanAlbumRoot(Digikam::CollectionLocation const&amp;) ()<br>
   from /usr/lib/digikam/libdigikamdatabase.so.4.2.0<br>
#18 0x00007ffff4f8a80d in Digikam::CollectionScanner::completeScan() ()<br>
   from /usr/lib/digikam/libdigikamdatabase.so.4.2.0</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20901"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20901" class="active">Exiv2 crash</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2014-10-15 06:23.</div>
    <div class="content">
     <p>This crash come from Exiv2 about video metadata parser. Please report this problem to Exiv2 bugzilla</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20895"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20895" class="active">digikam 4.4 on ubuntu 14.04</a></h3>    <div class="submitted">Submitted by GL (not verified) on Sun, 2014-10-12 18:00.</div>
    <div class="content">
     <p>Solved it:</p>
<p>http://ppa.launchpad.net/philip5/kubuntu-backports/ubuntu needs to be included as well</p>
<p>Thanks Philip for providing the packages.</p>
<p>Georg</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20905"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20905" class="active">After update KDE to 4:4.14.2 in my Kubuntu 14.04 digikam Problem</a></h3>    <div class="submitted">Submitted by Christian Graesser (not verified) on Fri, 2014-10-17 06:32.</div>
    <div class="content">
     <p>After update KDE to 4:4.14.2 in my Kubuntu 14.04 digikam does not start.<br>
This Error comes up:<br>
Object::connect: No such signal org::freedesktop::UPower::DeviceAdded(QDBusObjectPath)<br>
Object::connect: No such signal org::freedesktop::UPower::DeviceRemoved(QDBusObjectPath)<br>
Bus::open: Can not get ibus-daemon's address.<br>
IBusInputContext::createInputContext: no connection to ibus-daemon<br>
digikam: symbol lookup error: digikam: undefined symbol: _ZNK11KExiv2Iface14AltLangStrEdit8textEditEv</p>
<p>I had both backports enabled<br>
http://ppa.launchpad.net/philip5/extra/ubuntu<br>
http://ppa.launchpad.net/philip5/kubuntu-backports/ubuntu</p>
<p>Solution for me at the moment: uninstall DigiKam 4.4 (apt-get uninstall digikam) and untick the software source from philip5 and go back to digikam 4.0 (apt-get install digikam)</p>
<p>Thanks</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20906"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20906" class="active">libkface</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2014-10-17 06:34.</div>
    <div class="content">
     <p>A Symbol is missing in libkexiv2. update this package too...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20907"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20907" class="active">I have absolutly same</a></h3>    <div class="submitted">Submitted by Timophey (not verified) on Fri, 2014-10-17 07:28.</div>
    <div class="content">
     <p>I have absolutly same problem. How can I update libkexiv2?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20908"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20908" class="active">I have the same problem in</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2014-10-17 08:11.</div>
    <div class="content">
     <p>I have the same problem in Mint KDE.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20910"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20910" class="active">Missing Symbol _ZNK11KExiv2Iface14AltLangStrEdit8textEditEv</a></h3>    <div class="submitted">Submitted by Kai (not verified) on Fri, 2014-10-17 16:45.</div>
    <div class="content">
     <p>I think this is because the libkexiv2-11 of the 4.14.2 release of kubuntu-backports is now ahead of the 4.14.1 Version of the philip5 repository, but not compatible.</p>
<p>Downgrading libkexiv2-11 from 4.14.2-0ubuntu1~ubuntu14.04~ppa1  to<br>
4.14.1-trusty~ppa1</p>
<p>(The packages libkexiv2-11 libkexiv2-data libkexiv2-dbg libkexiv2-dev)</p>
<p>solved the problem for me for the moment</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20911"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20911" class="active">It's true that the error</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Fri, 2014-10-17 17:29.</div>
    <div class="content">
     <p>It's true that the error message comes from mismatching versions of exiv2. latest version of libkexiv2 is dependent on exiv2 0.24 or newer. </p>
<p>"kubuntu-backports" is never a stand-alone PPA of mine. It's always built against my "extra" PPA and that provides all necessary packages not found in my "kubuntu-backports" PPA. I have read in blog posts around the internet about using my PPA for digikam that you EITHER use my "kubuntu-backports" or my "extra" PPA depending on KDE version you use. That's WRONG. You always need my "extra" PPA and in some cases like if you use a newer version of KDE provided by the Kubuntu teams PPA you ALSO need my "kubuntu-backports" PPA.</p>
<p>How this transfer for Linux Mint users are up to you to find out as I use (K)Ubuntu.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20914"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20914" class="active">After update KDE to 4:4.14.2 in Kubuntu 14.04 digikam hasproblem</a></h3>    <div class="submitted">Submitted by Christian Graesser (not verified) on Fri, 2014-10-17 19:52.</div>
    <div class="content">
     <p>I have still the same problem, while *BOTH* philip5 ppa's are used:</p>
<p>http://ppa.launchpad.net/philip5/extra/ubuntu trusty main<br>
http://ppa.launchpad.net/philip5/kubuntu-backports/ubuntu  trusty main </p>
<p>fam@fam-HP-ProBook-4330s:~$ digikam<br>
Object::connect: No such signal org::freedesktop::UPower::DeviceAdded(QDBusObjectPath)<br>
Object::connect: No such signal org::freedesktop::UPower::DeviceRemoved(QDBusObjectPath)<br>
digikam: symbol lookup error: digikam: undefined symbol: _ZNK11KExiv2Iface14AltLangStrEdit8textEditEv</p>
<p>The new KDE 4:4.14.2 seem *not* to be compatible with the digikam 4:4.4.0-trusty~ppa2 from philip5</p>
<p>Next try was to remove the ppa which gave me the KDE-Update:<br>
deb http://ppa.launchpad.net/kubuntu-ppa/backports/ubuntu trusty main<br>
 and then removed digikam and all parts and reinstalled digikam from philip5 hoping to get all I need from his ppa.<br>
But that didn't work either</p>
<p>Does someone has a further idea ?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20915"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20915" class="active">Check from where and what</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Fri, 2014-10-17 21:18.</div>
    <div class="content">
     <p>Check from where and what packages of libexiv2-13 and libkexiv2-11 you have installed on your system. My packages of digikam is built against and uses libkexiv2-11 that is built against the updated version of libexiv2-13 that is 0.24 and supports more exif data etc. If this chain of packages are not installed then you get "undefined symbol" errors or segment faults.</p>
<p>To check what you use and from where you got the packages type the commands below. Somehow they are either in conflict with some other package in your system or just held back. Troubleshoot from there what you have and why you don't have the packages from my PPA.</p>
<p>apt-cache policy libexiv2-13<br>
apt-cache policy libkexiv2-11</p>
<p>Maybe this kind of support could be handled in the digikam user mailinglist instead of in this comment posts?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20917"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20917" class="active">Updated Kubuntu Backports package breaks Digikam</a></h3>    <div class="submitted">Submitted by Jakob Kobberholm (not verified) on Fri, 2014-10-17 22:54.</div>
    <div class="content">
     <p>Yesterday the Kubuntu backports PPA seems to have updated the libkexiv2-11 package to version 4.14.2, which causes an auto update to break the Digikam package from philip5/kubuntu-backports.<br>
Running "apt-cache policy libkexiv2-11", it showed that it was no longer running the package from philip5/kubuntu-backports.</p>
<p>The solution was to force the old 4.14.1 package to be installed by running: "sudo apt-get install libkexiv2-11=4:4.14.1-trusty~ppa1kde414".</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20918"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20918" class="active">Updated Kubuntu Backports package breaks Digikam</a></h3>    <div class="submitted">Submitted by Christian Graesser (not verified) on Sat, 2014-10-18 07:16.</div>
    <div class="content">
     <p>Great! It works!</p>
<p>Thanks a lot to Philip5 providing the ppa and Jakob for this tip!</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20916"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20916" class="active">Btw, when you use KDE 4.14.x</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Fri, 2014-10-17 22:10.</div>
    <div class="content">
     <p>Btw, when you use KDE 4.14.x you have to use my kubuntu-backports PPA also with special build of Digikam against KDE 4.14 as I have written here in other comments...</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20912"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20912" class="active">Thanks! It's works.</a></h3>    <div class="submitted">Submitted by Timophey (not verified) on Fri, 2014-10-17 18:59.</div>
    <div class="content">
     <p>Thanks! It's works.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div><a id="comment-20919"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20919" class="active">How to install Digikam 4.4.0 Kubuntu 14.04</a></h3>    <div class="submitted">Submitted by <a href="https://www.flickr.com/photos/128436756@N05/" rel="nofollow">Christian Graesser</a> (not verified) on Sat, 2014-10-18 11:22.</div>
    <div class="content">
     <p>1. To update the KDE to 4:4.14.2 I have this backport ppa enabled by copying the following line to the software sources in MUON<br>
"deb http://ppa.launchpad.net/kubuntu-ppa/backports/ubuntu trusty main"<br>
2. To update Digikam I have enabled the philip5 ppas (thanks!)<br>
"deb http://ppa.launchpad.net/philip5/kubuntu-backports/ubuntu trusty main "<br>
AND<br>
"deb http://ppa.launchpad.net/philip5/extra/ubuntu trusty main "<br>
Complete the update</p>
<p>may be restart</p>
<p>But due do some dependencies when using KDE 4:4.14.2 you NEED to do the following steps then as well:<br>
sudo apt-get install libkexiv2-11=4:4.14.1-trusty~ppa1kde414</p>
<p>further : the Database sqlite3 needs to be updated as well (Thanks to Jan).<br>
1. Download the packages sqlite3 [1] and libsqlite3-0 [2] from the upcoming new Ubuntu version 14.10, "Utopic". e.g. the 64 bit amd64<br>
1.1 If you have Skype installed you need to update also the 32 bit version (i386) of libsqlite3-0 (3.8.6-1)<br>
2. Save all *.deb packages in a new folder. e.g. /Downloads/sqlite<br>
3. Go with in the terminal to that folder e.g. cd /Downloads/sqlite<br>
3. Call in that folder: sudo dpkg -i *.deb</p>
<p>[1] http://packages.ubuntu.com/utopic/sqlite3<br>
[2] http://packages.ubuntu.com/utopic/libsqlite3-0</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20920"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20920" class="active">Great workaround guide</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Sat, 2014-10-18 18:44.</div>
    <div class="content">
     <p>Great workaround guide Christian! Also the sqlite3 update. I could include it in my "extra" PPA but I'm reluctant as it needs security monitoring as a package from my part as a security hole in that software could have grave impacts in other software than digikam. </p>
<p>I'm fixing an libkexiv2_4.14.2 update for the kubuntu-backports PPA to override the other update that should solve that little problem until next KDE update... :/</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20922"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20922" class="active">Won't this break all the</a></h3>    <div class="submitted">Submitted by Stephan (not verified) on Sun, 2014-10-19 10:48.</div>
    <div class="content">
     <p>Won't this break all the other programms using libkexiv?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20923"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20923" class="active">Where should I get</a></h3>    <div class="submitted">Submitted by Stephan (not verified) on Sun, 2014-10-19 11:12.</div>
    <div class="content">
     <p>Where should I get libexiv2-13? It's not in any of your PPAs but your version of libkexiv depends on it.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20924"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20924" class="active">libexiv2-13 is part of exiv2</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Sun, 2014-10-19 20:41.</div>
    <div class="content">
     <p>libexiv2-13 is part of exiv2 0.24 in my "extra" PPA.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div><a id="comment-20881"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20881" class="active">Now the Windows release is 2</a></h3>    <div class="submitted">Submitted by aidan (not verified) on Fri, 2014-10-10 12:49.</div>
    <div class="content">
     <p>Now the Windows release is 2 versions behind :/ this article could have been "Digikam 4.4 for Linux released". </p>
<p>Can we have a separate news announcement when the Windows one is released?<br>
As otherwise we have to keep randomly coming back and checking the download links which is mostly disheartening :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20883"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20883" class="active">eagerly waiting for 4.4</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2014-10-10 16:30.</div>
    <div class="content">
     <p>I am eagerly waiting for an upgrade as well.. 4.2 crashes for me on startup, so i'm even further behind right now :-(</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20925"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20925" class="active">Windows version, PLEASE.</a></h3>    <div class="submitted">Submitted by cc (not verified) on Mon, 2014-10-20 21:58.</div>
    <div class="content">
     <p>I would like to bring this up again. I would absolutely love to participate in the 4.4.0 party. Sadly, i'm currently left out. Please carve out some time to release a windows version, soon!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20927"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20927" class="active">added to bugtracker</a></h3>    <div class="submitted">Submitted by cc (not verified) on Fri, 2014-10-24 10:06.</div>
    <div class="content">
     <p>I added the request to the bugtracker, maybe that is a better way to make this kind of request:<br>
<a href="https://bugs.kde.org/show_bug.cgi?id=340278">https://bugs.kde.org/show_bug.cgi?id=340278</a></p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20930"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20930" class="active">Thanks!</a></h3>    <div class="submitted">Submitted by cc (not verified) on Tue, 2014-11-11 01:23.</div>
    <div class="content">
     <p>aidan, the windows release of 4.4 is now available!</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20893"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20893" class="active">Win version</a></h3>    <div class="submitted">Submitted by grix (not verified) on Sun, 2014-10-12 00:33.</div>
    <div class="content">
     <p>I use both win and linux, but for printing windows is a must because of the drivers' stability and performance, so I'm waiting the windows release of this great, great, program, collection, tool, or whatever you call it<br>
Thanks developers !!!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20894"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20894" class="active">Very stable in linux</a></h3>    <div class="submitted">Submitted by grix (not verified) on Sun, 2014-10-12 17:49.</div>
    <div class="content">
     <p>Now version 4.4 is very stable in linux (kubuntu 14.04.1)</p>
<p>Thanks</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20896"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20896" class="active">Digikam an problems with the settings</a></h3>    <div class="submitted">Submitted by Christian  (not verified) on Mon, 2014-10-13 17:38.</div>
    <div class="content">
     <p>Hi.</p>
<p>Since some versions i have problems with the setting menu: If i open the settings menu, few seconds later digikam freezes. So i have to kill the digikam process. Here, the bug is reproducible (with deleted digikamrc an new database too).</p>
<p>The problem is in version 4.4 and older. </p>
<p>My system: Opensuse 13.1 x64, KDE 4.4.</p>
<p>Christian</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20897"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20897" class="active">Hi.
Oh, bad analyses. If I</a></h3>    <div class="submitted">Submitted by Christian  (not verified) on Mon, 2014-10-13 18:11.</div>
    <div class="content">
     <p>Hi.</p>
<p>Oh, bad analyses. If I use the external db, digikam freezes after few seconds by clicking in the settings menu.</p>
<p>If I use the internal db, the navigation in the settings menu is ok, but settings in the kipi-modul (in the settings menu) is not saved after restart digikam.</p>
<p>Christian</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20899"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20899" class="active">Hi Christian, see</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2014-10-14 20:54.</div>
    <div class="content">
     <p>Hi Christian, see https://bugs.kde.org/show_bug.cgi?id=337737</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20928"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20928" class="active">reliability - ?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2014-10-26 19:27.</div>
    <div class="content">
     <p>I have been using debian for most of my work, but I wanted to run Digikam as it was highly recommended, I am now running Ubuntu 14.04 </p>
<p>But only version of Digikam that seems somewhat stable is 3.5, even that regularly crashes or hangs up trying to load pictures from my networked storage.</p>
<p>Does anyone know of a version that can be installed on Ubuntu as is stable?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20929"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20929" class="active">The version 4.4 is stable and</a></h3>    <div class="submitted">Submitted by <a href="https://www.flickr.com/photos/128436756@N05/" rel="nofollow">Christian Graesser</a> (not verified) on Fri, 2014-11-07 07:54.</div>
    <div class="content">
     <p>The version 4.4 is stable and good for production see my post little further up in the thread:<br>
"How to install Digikam 4.4.0 Kubuntu 14.04<br>
Submitted by Christian Graesser (not verified) on Sat, 2014-10-18 11:22."</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20931"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20931" class="active">digiKam won't open in windows</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2014-11-11 12:22.</div>
    <div class="content">
     <p>I am new to digiKam; just downloaded it yesterday It worked fine while i was testing it. However, today digiKam opens, but it's doesn't seem to "finish" opening. It seems to hang up. I can see the opening screen plus the Tips window opens. When I try to close the tips window, nothing happens. The little cursor circle keeps spinning and spinning. I am running windows 7 and I have now tried several versons of digiKam,  and nothing helps. Also, i noticed that the windows task manager says that digiKam is "Not Responding". Can anyone give me a clue as to what to do? Regards, Allen</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20932"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20932" class="active">Be patient, it works</a></h3>    <div class="submitted">Submitted by Grix (not verified) on Wed, 2014-11-12 01:42.</div>
    <div class="content">
     <p>Yes, usually when installing for the first time it behaves erratically:<br>
First, after install run defragmentation on disk it's installed.<br>
Then open it (sometimes takes a lot) and be sure to configure your database properly in   a location of your fastest disk. If you have a lot of pictures, try to see if you can load only  a part of them by distributing in several albums. Once you have done this, take a tour over these first albums and configure some aspects of digikam, interface, writing data, etc. No tags, no geolocation yet and no-nay-never face recognition, please. Close digikam and open it again and go foward adding more pics. If you have trouble, see if after closing are there task that still running (digikam, kdelauncher and so) and kill them, then restart digikam. Step by step you will see it'll behave better. (in windows there are things that will always crash, but other will be corrected) Yes, it's something like to let the program to adapt to your computer (or viceversa) Continue step  to step and make a database backup (only digikam.db, I mean) Start with tags (do not hurry) If you are able to, check database (I test identical copies of database file against linux and windows) Be patient. Most stable version for windows is 3.4 then, 4.2, and face recognition is a nightmare and has its own database in "C:\Documents and Settings\user\Program data\.kde\share\apps\libkface\database" It can grow very, very much; don't recommend using it until gets improved.<br>
Good luck.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20935"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/719#comment-20935" class="active">error digikam</a></h3>    <div class="submitted">Submitted by job (not verified) on Wed, 2014-11-19 15:46.</div>
    <div class="content">
     <p>Digikam "hangs"in facerecognition mode. Whilerunnig from terminal I getthe error:<br>
(digikam:3325): Gtk-CRITICAL **: IA__gtk_progress_configure: assertion 'value &gt;= min &amp;&amp; value &lt;= max' failed</p>
<p>Starting up from terminal gives the messages.<br>
Object::connect: No such signal org::freedesktop::UPower::DeviceAdded(QDBusObjectPath)<br>
Object::connect: No such signal org::freedesktop::UPower::DeviceRemoved(QDBusObjectPath)<br>
QSocketNotifier: Invalid socket 14 and type 'Read', disabling...<br>
Object::connect: No such signal org::freedesktop::UPower::DeviceAdded(QDBusObjectPath)<br>
Object::connect: No such signal org::freedesktop::UPower::DeviceRemoved(QDBusObjectPath)<br>
Object::connect: No such signal org::freedesktop::UPower::DeviceAdded(QDBusObjectPath)<br>
Object::connect: No such signal org::freedesktop::UPower::DeviceRemoved(QDBusObjectPath)<br>
Gtk-Message: (for origin information, set GTK_DEBUG): failed to retrieve property `GtkRange::activate-slider' of type `gboolean' from rc file value "((GString*) 0x8b1d310)" of type `GString'<br>
Gtk-Message: (for origin information, set GTK_DEBUG): failed to retrieve property `GtkRange::activate-slider' of type `gboolean' from rc file value "((GString*) 0x8b59f30)" of type `GString'<br>
QSocketNotifier: Invalid socket 14 and type 'Read', disabling...<br>
QSocketNotifier: Invalid socket 14 and type 'Read', disabling..<br>
Running Lubuntu 14.10 </p>
<p>Is there any solution known?</p>
<p>;</p>
         </div>
    <div class="links">» </div>
  </div>

</div>