---
date: "2012-04-18T07:18:00Z"
title: "Simulate a Faded Effect in digiKam"
author: "Dmitri Popov"
description: "Sometimes the best way to spice up a photo is to make it look faded, and digiKam makes it supremely easy to achieve this effect."
category: "news"
aliases: "/node/652"

---

<p>Sometimes the best way to spice up a photo is to make it look faded, and digiKam makes it supremely easy to achieve this effect.</p>
<p><img class="alignnone size-medium wp-image-2390" title="digikam_faded_effect" src="https://scribblesandsnaps.files.wordpress.com/2012/04/digikam_faded_effect.png?w=500" alt="" width="500" height="380"></p>
<p><a href="http://scribblesandsnaps.wordpress.com/2012/04/18/simulate-a-faded-effect-in-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>