---
date: "2008-05-13T12:35:00Z"
title: "digiKam at LGM 2008: a great event!"
author: "digiKam"
description: "Libre Graphics Meeting at Wroclaw (Poland) have been a great event. I has met several actors from open-source world working in graphics relevant projects as:"
category: "news"
aliases: "/node/315"

---

<a href="http://www.libregraphicsmeeting.org">Libre Graphics Meeting</a> at Wroclaw (Poland) have been a great event.

<br><br>
<a href="http://www.flickr.com/photos/digikam/2583231945/" title="frontpage by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3080/2583231945_311054dca4_m.jpg" width="240" height="186" alt="frontpage"></a>
<br><br>

I has met several actors from open-source world working in graphics relevant projects as:

<ul>
<li>Dave Cofin (<a href="http://www.cybercom.net/~dcoffin/dcraw">Dcraw</a>)</li>
<li>Udi Fuchs (<a href="http://ufraw.sourceforge.net">UFRaw</a>)</li>
<li>Cyril Berger and Boudewijn Rempt (<a href="http://www.koffice.org/krita">Krita</a>)</li>
<li>Pablo d'Angelo (<a href="http://hugin.sourceforge.net">Hugin</a>)</li>
<li>Cédric Gémy and Peter Linnell (<a href="http://www.scribus.net">Scribus</a>)</li>
</ul>

<p>I has presented the <a href="http://digikam3rdparty.free.fr/Documents/LGM2008/LGM2008_digiKam.pdf">current and future of digiKam</a>, especially all new search tools designed by Marcel and the <a href="http://www.digikam.org/?q=node/306">Marble integration</a> to geolocalize pictures. I has make a demo in live of 0.9.4-beta4 and current alpha version of 0.10.0 release.</p>

<p>The video recording of digiKam presentation is now online. A QuickTime file can be show at <a href="http://media.river-valley.tv/conferences/lgm2008/quicktime/0303-Gilles_Caulier.html">this url</a></p>

<p>I has started all kinds of cooperation with other photo handling applications. With Udi Fuchs, i have talk about DNG writting and a future UFRaw shared library. With Pablo d'Angelo, i have talk about the new lens auto-correction tool implemented in 0.10.0, based on <a href="http://lensfun.berlios.de">LensFun shared library</a>. Pablo has shared with me many pictures to check this new plugin, and performed a lots of inconclusive tests.</p>

<p>I have met also <a href="http://prokoudine.info/blog/?p=74">Alexandre Prokoudine</a>, a Russian photographer who has started to share a <a href="http://www.flickr.com/groups/776360@N22/pool">pool of photo</a> taken at Wroclaw during the event. He has bring me a new open-source library developped by a Russian team, dedicated to decode RAW files. This new shared library named <a href="http://www.libraw.org">libRaw</a> is based on dcraw core implementation and provide new features, all wrapped into a C++ API. It will be nice to use libRaw in the future to simplify and improve libkdcraw interface.</p>

<p>LGM 2008 has been a very constructive moment for me. I'm very impatient to go to next event of the year: <a href="http://conference2008.kde.org">akademy 2008</a> </p><p>
</p>
<div class="legacy-comments">

  <a id="comment-17552"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/315#comment-17552" class="active">LGM</a></h3>    <div class="submitted">Submitted by gerhard on Mon, 2008-05-26 21:45.</div>
    <div class="content">
     <p>It really was a great meeting</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
