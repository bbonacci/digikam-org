---
date: "2010-01-11T17:15:00Z"
title: "digiKam Users Flickr Group"
author: "Dmitri Popov"
description: "Are you on Flickr? Do you use digiKam? Then join the digiKam Users Flickr group! The group is intended to function as a hub for"
category: "news"
aliases: "/node/495"

---

<a href="http://www.flickr.com/groups/digikam-users"><img src="http://l.yimg.com/g/images/en-us/flickr-yahoo-logo.png.v2" width="180" height="30" alt="Flickr digiKam users group"></a>

<p>Are you on Flickr? Do you use digiKam? Then join the digiKam Users Flickr group! The group is intended to function as a hub for photographers who use digiKam as their tool of choice for managing and tweaking photos. Feel free to showcase your best photos and describe digiKam techniques and tools you used to achieve the final result. Head to <a href="http://www.flickr.com/groups/digikam-users/">www.flickr.com/groups/digikam-users</a> and join the group!</p>
<div class="legacy-comments">

  <a id="comment-18988"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/495#comment-18988" class="active">Great Idea!</a></h3>    <div class="submitted">Submitted by Stefan (not verified) on Mon, 2010-01-11 21:03.</div>
    <div class="content">
     <p>I love this idea! Already joined on flickr...</p>
<p>Keep on doing that great job with digiKam!!!</p>
         </div>
    <div class="links">» </div>
  </div>

</div>