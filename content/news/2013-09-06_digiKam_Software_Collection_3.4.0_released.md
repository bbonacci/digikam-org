---
date: "2013-09-06T15:45:00Z"
title: "digiKam Software Collection 3.4.0 released..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the release of digiKam Software Collection 3.4.0. This version include a new core"
category: "news"
aliases: "/node/703"

---

<a href="http://www.flickr.com/photos/digikam/9536807056/" title="DKMaintenancetool by digiKam team, on Flickr"><img src="http://farm4.staticflickr.com/3804/9536807056_8335308751_z.jpg" width="640" height="359" alt="DKMaintenancetool"></a>

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce the release of digiKam Software Collection 3.4.0.
This version include a new core implementation to manage maintenance tools, especially to support better multi-threading and multi-core CPU to speed-up processing.</p>

<p>See <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=3.4.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">the list of files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/stable/digikam/digikam-3.4.0.tar.bz2.mirrorlist">KDE repository</a></p>

<p>Next main release 3.5.0 will be a bugfix release, as we are currently busy to complete <a href="http://community.kde.org/Digikam/GSoC2013#digiKam_Google_Summer_of_Code_2013_Projects_list">all GSoC 2013 projects</a>. Look in Release plan <a href="http://community.kde.org/Digikam/GSoC2013#Roadmap_and_Releases_Plan_including_all_GSoC-2013_works">for details...</a></p>

<p>Happy digiKam</p>
<div class="legacy-comments">

  <a id="comment-20626"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/703#comment-20626" class="active">Congrats! Smugmug?</a></h3>    <div class="submitted">Submitted by Vivek (not verified) on Fri, 2013-09-06 19:10.</div>
    <div class="content">
     <p>Congratulations! Has the SmugMug kipi-plugin been updated also? Their relaunch broke kipi's upload feature (bug 323118) and http://www.dgrin.com/showthread.php?t=236580</p>
<p>Thanks!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20627"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/703#comment-20627" class="active">Just one question: When is</a></h3>    <div class="submitted">Submitted by Robin (not verified) on Fri, 2013-09-06 20:46.</div>
    <div class="content">
     <p>Just one question: When is this coming to Debian repos? We're still stuck with 3.1  ;(</p>
<p>Thanks for the great work though!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20629"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/703#comment-20629" class="active">Packages of Digikam is up to</a></h3>    <div class="submitted">Submitted by Philip5 (not verified) on Fri, 2013-09-06 23:23.</div>
    <div class="content">
     <p>Packages of Digikam is up to the maintainers for each distribution to make. Talk the Debian maintainers of Digikam at Debian for updates to latest upstream release. It's not something the Digikam team do...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20637"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/703#comment-20637" class="active">It would appear not.
SmugMug</a></h3>    <div class="submitted">Submitted by <a href="http://techbypc.com" rel="nofollow">TechByPC</a> (not verified) on Tue, 2013-09-17 02:58.</div>
    <div class="content">
     <p>It would appear not.</p>
<p>SmugMug Call Failed: invalid method</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20628"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/703#comment-20628" class="active">When?</a></h3>    <div class="submitted">Submitted by <a href="http://photographyby.woctx.com" rel="nofollow">Art &amp; Photography by Pasjr Woctx</a> (not verified) on Fri, 2013-09-06 22:30.</div>
    <div class="content">
     <p>How and when can I upgrade my digikam in Kubuntu 13.04?  I rely on digikam daily:)  Hey also I have question, on the "Lens Auto-Correction" How can I have complete freedom to choose different cameras and lens?  Say I use an Oly E-PL1 camera body but a Nikor 70-300mm Lens, currently I can't pick and choose that, I can only pick a certian lens listed under a certain camera.  Lets open that up so everyone can pick each field sepratly.  Just a thought.  Also, I'd love to see something to cut down on film grain, you have an awesome noise filter but what about film grain removal?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20630"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/703#comment-20630" class="active">The Kubuntu team package and</a></h3>    <div class="submitted">Submitted by Philip5 (not verified) on Fri, 2013-09-06 23:32.</div>
    <div class="content">
     <p>The Kubuntu team package and maintain, among other things, Digikam updates between (K)Ubuntu releases at their Launchpad PPA. Right now they have Digikam 3.3 for (K)Ubuntu 12.04, 12.10 and 13.04 but I bet they will very soon have Digikam 3.4 too. I used to make packages for (K)Ubuntu on my personal PPA but as long as the Kubuntu team do it I see no use that I also do the same packages...</p>
<p>You fund their PPA at with Digikam:<br>
https://launchpad.net/~kubuntu-ppa/+archive/backports</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20631"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/703#comment-20631" class="active">Philip5</a></h3>    <div class="submitted">Submitted by Clement M-Photographie (not verified) on Sat, 2013-09-07 12:48.</div>
    <div class="content">
     <p>Philip, I miss your packages on kubuntu raring 32bits... kUbuntu team seems to compile digikam without openmp support, so digikam from kubuntu team is very slow (at least on my 32 bits version)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20632"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/703#comment-20632" class="active">Philip5</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2013-09-08 23:33.</div>
    <div class="content">
     <p>You might consider switching to Fedora they are really fast with digicam updates; 3.3 was available within the week of the announcement on digikam.org.<br>
And it runs smooth with KDE (i'm not sure but 32bit should be no problem).</p>
<p>Always keep in mind that(k)ubuntu is a commercial product (their shop for example; they cripple the distro so you pay for functions you get for free elsewhere; not really cool)</p>
<p>just a thought though, you probably not change distro because some freak made a remark. but if you have a little hd-space (10 gig is enough for trying) you might have a look.</p>
<p>have fun<br>
O.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20633"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/703#comment-20633" class="active">WTF?</a></h3>    <div class="submitted">Submitted by Num83rGuy (not verified) on Mon, 2013-09-09 06:30.</div>
    <div class="content">
     <p><cite>Always keep in mind that(k)ubuntu is a commercial product (their shop for example; they cripple the distro so you pay for functions you get for free elsewhere; not really cool)<cite></cite></cite></p>
<p>Which orifice are you pulling this from?  They now offer commercial support but, the community support is still there.  The man pages are still there.  The online documentation is still there.  The IRC channel is still there.  KDE help center is still there.</p>
<p>Please point to this "crippling" so that I too can become enraged.  I have not seen nor heard of any such thing.</p>
<p>Please, enlighten me.  Until then you are just "some freak" who "made a remark".</p>
<p>Though I will give you credit on telling the person to multi-boot and see for him/her self.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20634"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/703#comment-20634" class="active">"the community support is</a></h3>    <div class="submitted">Submitted by Maderios (not verified) on Mon, 2013-09-09 14:08.</div>
    <div class="content">
     <p>"the community support is still there" =&gt; Ubuntu packages come from Debian...</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20635"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/703#comment-20635" class="active">Update from 3.0.0 to 3.3 with xubuntu 12.04 - OK</a></h3>    <div class="submitted">Submitted by <a href="http://www.opalesurfcasting.net" rel="nofollow">eldk</a> (not verified) on Wed, 2013-09-11 19:18.</div>
    <div class="content">
     <p>Hello,</p>
<p>I've updated from digikam 3.0.0 to 3.3.0 with NAS and a MYSQL server, on 2 xubuntu 12.04 workstations (old ones) with kubuntu backport PPA. All seems to be OK.</p>
<p>Backup then<br>
1 - add the PPA<br>
2 - update<br>
3 - upgrade</p>
<p>Greatings,</p>
<p>Eric</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20636"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/703#comment-20636" class="active">Add backport PPA :
sudo</a></h3>    <div class="submitted">Submitted by <a href="http://www.opalesurfcasting.net" rel="nofollow">eldk</a> (not verified) on Wed, 2013-09-11 20:23.</div>
    <div class="content">
     <p>Add backport PPA :</p>
<p>sudo add-apt-repository ppa:kubuntu-ppa/backports</p>
<p>PS : I'm still not writing metadata to pictures - I will try with digikam 3.4</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div>
</div>
