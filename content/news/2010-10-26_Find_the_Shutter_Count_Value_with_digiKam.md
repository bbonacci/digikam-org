---
date: "2010-10-26T17:52:00Z"
title: "Find the Shutter Count Value with digiKam"
author: "Dmitri Popov"
description: "The life expectancy of a DSLR camera is usually measured in shutter actuations. For example, Nikon D5000 is good for about 100,000 shutter clicks. That"
category: "news"
aliases: "/node/544"

---

<p>The life expectancy of a DSLR camera is usually measured in shutter actuations. For example, Nikon D5000 is good for about 100,000 shutter clicks. That doesn’t mean that your camera dies the day it exceeds its shutter actuation limit, but the shutter count provides a good indication of the camera’s condition.</p>
<p><a href="http://scribblesandsnaps.wordpress.com/2010/10/26/find-the-shutter-count-value-with-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>