---
date: "2016-01-31T16:20:00Z"
title: "digiKam 5.0.0-beta3 is released"
author: "digiKam"
description: "Dear digiKam fans and users, digiKam team is proud to announce the release of digiKam Software Collection 5.0.0 beta3. This version is new stage to"
category: "news"
aliases: "/node/752"

---

<a href="https://www.flickr.com/photos/digikam/23678728015/in/dateposted-public/" title="digiKam5.0.0-beta3-Lut3Dtool"><img src="https://farm1.staticflickr.com/682/23678728015_65cf5c10e9_c.jpg" width="800" height="450" alt="digiKam5.0.0-beta3-Lut3Dtool"></a>

<p>Dear digiKam fans and users,</p>

<p>
digiKam team is proud to announce the release of digiKam Software Collection 5.0.0 beta3. This version is new stage to the long way to stabilize code of Qt5 port of next main digiKam release. See <a href="https://www.digikam.org/node/749">previous announcement</a> about 5.0.0-beta2 release to know all details about code re-writing under progress.
</p>

<p>
As usual, with this new release, we have introduced a lots of bug fixes compared to previous one, but not only. A new <a href="https://bugs.kde.org/show_bug.cgi?id=353789">3DLut tool</a> have been included to Image Editor and Batch Queue Manager. 3D lookup tables are used to map one color space to another. With this king of tool, you will be able to apply nice color effects over images, as vintage, over satured, or high contrast photo.
</p>

<a href="https://www.flickr.com/photos/digikam/23050618884/in/dateposted-public/" title="digiKam5.0.0-beta3-Lut3Dtool-2"><img src="https://farm1.staticflickr.com/740/23050618884_936e5039f6_c.jpg" width="800" height="450" alt="digiKam5.0.0-beta3-Lut3Dtool-2"></a>

<p>For furher information, take a look into the list of <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=5.0.0&amp;product=digikam&amp;product=kipiplugins">files currently closed</a> in KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/unstable/digikam/digikam-5.0.0-beta3.tar.bz2.mirrorlist">KDE repository</a></p>

<p>This version is for testing purposes.<b> It’s not currently advised to use it in production.</b></p>

<p>Thanks in advance for your feedback.

</p><p>Happy digiKaming!</p>


<div class="legacy-comments">

  <a id="comment-21166"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/752#comment-21166" class="active">Great</a></h3>    <div class="submitted">Submitted by Geoff (not verified) on Fri, 2016-02-05 14:30.</div>
    <div class="content">
     <p>Thanks for the update and wishing you speedy bug smashing.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21167"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/752#comment-21167" class="active">Until when there will be a</a></h3>    <div class="submitted">Submitted by Wolfgang (not verified) on Mon, 2016-02-15 20:19.</div>
    <div class="content">
     <p>Until when there will be a Windows version of Digikam 5.0 ... (beta)?<br>
Runs Digikam 5.0 liquid under Windows than before?<br>
Thanks for the recent troubles.</p>
<p>wolfgang</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21172"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/752#comment-21172" class="active">i am curious as well - are</a></h3>    <div class="submitted">Submitted by Shaks (not verified) on Tue, 2016-03-15 14:02.</div>
    <div class="content">
     <p>i am curious as well - are there any plans for a windows beta binary? couldn't find anything...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21168"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/752#comment-21168" class="active">face recognition is not</a></h3>    <div class="submitted">Submitted by Andre (not verified) on Fri, 2016-02-19 16:37.</div>
    <div class="content">
     <p>face recognition is not working for me as expected. any hint how to use it?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21169"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/752#comment-21169" class="active">Just a little question - is</a></h3>    <div class="submitted">Submitted by Nadine (not verified) on Wed, 2016-02-24 16:15.</div>
    <div class="content">
     <p>Just a little question - is there a mysql support again?<br>
I updated to 4.12.0 a few months ago and where so sad that there was only SQLite.<br>
I hope so...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21170"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/752#comment-21170" class="active">Digikam 4.x have experimental</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Thu, 2016-02-25 21:32.</div>
    <div class="content">
     <p>Digikam 4.x have experimental mysql support if the packager enabled it when building it but as it's not recommended to use many disable it for distribution. If you really want it you have to build it your self, get packages that include it or ask your package maintainer to include it.</p>
<p>Digikam 5 will (hopefully) have more stable mysql support as an alternative.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21174"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/752#comment-21174" class="active">MariaDB</a></h3>    <div class="submitted">Submitted by dajomu (not verified) on Wed, 2016-03-30 11:45.</div>
    <div class="content">
     <p>Will it only support mysql or will mariaDB be supported as well?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21175"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/752#comment-21175" class="active">Since MariaDB is a fork of</a></h3>    <div class="submitted">Submitted by Bob (not verified) on Mon, 2016-04-11 04:13.</div>
    <div class="content">
     <p>Since MariaDB is a fork of MySQL, it's almost always fully compatible with apps that specify MySQL.<br>
i.e. don't worry about it.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-21171"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/752#comment-21171" class="active">Very big thx for release!</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2016-03-11 12:05.</div>
    <div class="content">
     <p>Very big thx for release!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21173"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/752#comment-21173" class="active">MySQL server working on RaspberryPi with a ubuntu15 LAN</a></h3>    <div class="submitted">Submitted by AWSBarker (not verified) on Tue, 2016-03-29 16:41.</div>
    <div class="content">
     <p>Great work team.<br>
I got 5.0beta3 working on a 3 machine LAN running Ubuntu 15.x  with MySQLserver on a RaspberryPi with  Raspbian Jessie containing &gt;50gb with ~25k images.<br>
A couple of tweaks were required on the Ubuntu clients, ie. drkonqi link to correct folder (/usr/lib/x86_64-linux-gnu/libexec/drkonqi) and use of older libraries.</p>
<p>I have a few more items to report - how can I best contribute to beta testing?<br>
AWSB</p>
         </div>
    <div class="links">» </div>
  </div>

</div>