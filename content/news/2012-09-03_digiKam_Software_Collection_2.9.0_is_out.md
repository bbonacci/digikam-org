---
date: "2012-09-03T09:28:00Z"
title: "digiKam Software Collection 2.9.0 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! After one month since 2.8.0 release, digiKam team is proud to announce the digiKam Software Collection 2.9.0, as bug-fixes"
category: "news"
aliases: "/node/664"

---

<a href="http://www.flickr.com/photos/digikam/7739392406/" title="splash-digikam by digiKam team, on Flickr"><img src="http://farm9.staticflickr.com/8428/7739392406_3516223e77.jpg" width="500" height="307" alt="splash-digikam"></a>

<p>Dear all digiKam fans and users!</p>

<p>After one month since 2.8.0 release, digiKam team is proud to announce the digiKam Software Collection 2.9.0, as bug-fixes release. This will be the last 2.x release. Next one will be 3.0.0, currently under development, following GoSC 2012 projects, <a href="http://community.kde.org/Digikam/GSoC2012">listed here</a>.</p>

<p>See <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=2.9.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">the list of files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/files">SourceForge Repository</a>, and also, through <a href="http://download.kde.org/stable/digikam/digikam-2.9.0.tar.bz2.mirrorlist">KDE repository</a></p>

<p>Happy digiKaming...</p>
<div class="legacy-comments">

  <a id="comment-20343"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/664#comment-20343" class="active">windows version</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2012-09-03 10:11.</div>
    <div class="content">
     <p>Nice to see the development goes on so quickly. But: Is the windows-version abandoned? </p>
<p>Also: The Captcha to post comments is almos impossible to read.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20398"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/664#comment-20398" class="active">Windows Package Not Abandoned</a></h3>    <div class="submitted">Submitted by Ananta Palani on Tue, 2012-10-09 23:52.</div>
    <div class="content">
     <p>The Windows package is not abandoned, just delayed relative to the release of the source tarball. The reason that KDE on Windows is unstable, and to release the latest bug fixes for Windows requires constantly rebuilding with the latest version of KDE SC on Windows. For instance, to fix the problem with digiKam not starting with USB devices attached, I had to migrate from KDE SC 4.8.3 that I used for digiKam 2.6.0 and 2.7.0 to KDE SC 4.8.5. Today I released digiKam 2.8.0 and 2.9.0 for Windows simultaneously, the latter just over a month since the source for 2.9.0 was released, and just over 2 months since 2.8.0 was released. That seems hardly like abandonment to me, and is rather quick considering this software and packaging are done for free in developers spare time. You can download digiKam 2.9.0 <a href="http://sourceforge.net/projects/digikam/files/digikam/2.9.0/digiKam-installer-2.9.0-win32.exe/download">here</a>.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20347"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/664#comment-20347" class="active">Compiling</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2012-09-04 17:59.</div>
    <div class="content">
     <p>Tried to compile 11 times. Breaks off after 28% with the same errors no matter what I do.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20348"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/664#comment-20348" class="active">Windows / Bugs</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2012-09-05 01:00.</div>
    <div class="content">
     <p>Would be nice to have a complete package to download (including ALL dependencies) for windows, instead of searching for bits here and others there.</p>
<p>Does anyone know if the USB(?) bug is fixed. That being if you have external usb devices attached Digikam crashes shortly after startup (for me usually during album scanning). This exists on both the Win and Linux versions from at least v2.0 onward. Yes, it's been reported as various bugs, but I beleive it's related to USB.</p>
<p>This is a fantastic piece of software and I congratulate the devs for its existance but many issues remain in getting it to work consistantly for many users. Otherwise keep up the good work.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20399"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/664#comment-20399" class="active">digiKam 2.9.0 for Windows released and fixes USB bug</a></h3>    <div class="submitted">Submitted by Ananta Palani on Tue, 2012-10-09 23:57.</div>
    <div class="content">
     <p>What bits and pieces do you refer to? digiKam for Windows has been released in one package on Sourceforge for quite a while! I just released digiKam 2.8.0 and 2.9.0 for Windows simultaneously. They are built against KDE SC 4.8.5 which fixes the USB related crashes at start-up. I do not know if it will fix your crash during album scanning as I have not seen this occur myself. If you have not done so already, please file a bug report. You can download digiKam 2.9.0 from <a href="http://sourceforge.net/projects/digikam/files/digikam/2.9.0/digiKam-installer-2.9.0-win32.exe/download">here</a>.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20355"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/664#comment-20355" class="active">Export Function</a></h3>    <div class="submitted">Submitted by Henning (not verified) on Mon, 2012-09-10 02:21.</div>
    <div class="content">
     <p>Good morning,</p>
<p>I was waiting for the version 2.9.0 in the hope that the bug in the export function will disappear. But no, Digikam still crashes on export to e-mail  or any other webservice (i.e. Piwigo / Facebook etc etc )</p>
<p>Too bad..........</p>
<p>Henning</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
