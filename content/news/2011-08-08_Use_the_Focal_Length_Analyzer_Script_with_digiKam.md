---
date: "2011-08-08T08:48:00Z"
title: "Use the Focal Length Analyzer Script with digiKam"
author: "Dmitri Popov"
description: "The Focal Length Analyzer is a nifty little Bash script that pulls focal length data from digiKam’s database back end and generates nice graphs based"
category: "news"
aliases: "/node/619"

---

<p>The Focal Length Analyzer is a nifty little Bash script that pulls focal length data from digiKam’s database back end and generates nice graphs based on the extracted data.</p>
<p><a href="http://scribblesandsnaps.wordpress.com/2011/08/08/use-the-focal-length-analyzer-script-with-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>