---
date: "2009-11-23T16:00:00Z"
title: "new wavelets noise reduction tool under construction for 1.0.0 release..."
author: "digiKam"
description: "I'm currently working on a new tool for Image Editor and Batch Queue Manager to perform camera sensor noise using Wavelets. The tool works into"
category: "news"
aliases: "/node/486"

---

<p>I'm currently working on a new tool for Image Editor and Batch Queue Manager to perform camera sensor noise using Wavelets. The tool works into YCrCB color space and support 16 bits color depth.</p>

<a href="http://www.flickr.com/photos/digikam/4121932336/" title="waveletsdenoise4 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2697/4121932336_8160c7919b.jpg" width="500" height="313" alt="waveletsdenoise4"></a>

<p>The tool core code is based on <a href="http://registry.gimp.org/node/4235">Gimp Wavelets De-noise tool</a>. I ported code to C++/Qt4 and fixed some memory management bugs and improved a little bit the processing loop to be be faster.</p>

<a href="http://www.flickr.com/photos/digikam/4120068613/" title="waveletsdenoise2 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2635/4120068613_73d223d6a3.jpg" width="500" height="313" alt="waveletsdenoise2"></a>

<p>Code is not yet finalized, especially in settings view. Users must be able to adjust noise removal per channel (Luminosity or Chrominance). For the moment same settings is applied to all channels at the same time. But it's just a question of time... as usual...)</p>

<a href="http://www.flickr.com/photos/digikam/4120965159/" title="waveletsdenoise3 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2698/4120965159_761fa83114.jpg" width="500" height="313" alt="waveletsdenoise3"></a>

<p>Old Noise Reduction tool is removed from digiKam now. This one based on DCamNoise2 code is really complicated to handle : too much settings to adjust. The new one is really simple and work very well. I will post a more complete entry in my blog later, when tool will be finalized...</p>


<div class="legacy-comments">

  <a id="comment-18847"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/486#comment-18847" class="active">Great work !!!</a></h3>    <div class="submitted">Submitted by Daniel (not verified) on Mon, 2009-11-23 20:03.</div>
    <div class="content">
     <p>This is my first comment on this blog and I have to say: digiKam is the best image tool I've ever seen. Thanks for the great work !!!</p>
<p>But I've also a question about these different image editor tools. There are these buttons to choose the way you want to see the differences between the original image and the edited one.<br>
Sometimes there are above the image(s) on the left side and sometimes they are below the image(s) on the right side. Is there any plan to have it always on the same location. Nothing important but it increases the usability a bit more.</p>
<p>I'm looking forward getting the final 1.0.0 release :-)</p>
<p>Daniel</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18848"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/486#comment-18848" class="active">Yes there is... :-)</a></h3>    <div class="submitted">Submitted by Andi Clemens on Mon, 2009-11-23 22:26.</div>
    <div class="content">
     <p>Yes there is... :-)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18850"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/486#comment-18850" class="active">Wow</a></h3>    <div class="submitted">Submitted by <a href="http://www.asinen.org/blog" rel="nofollow">Stuart Jarvis</a> (not verified) on Tue, 2009-11-24 12:27.</div>
    <div class="content">
     <p>That's it really - I'm going to have to go back through some of my old photos once this is out.</p>
<p>Great work, as always, and it's nice to see this being based on something from Gimp-land</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18851"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/486#comment-18851" class="active">i am happy for photo geek</a></h3>    <div class="submitted">Submitted by philippe (not verified) on Tue, 2009-11-24 12:47.</div>
    <div class="content">
     <p>i am happy for photo geek</p>
<p>but non photo geek wants only<br>
- to experiment emotion with souvenir<br>
- to live closer together with its family and friends</p>
<p>then<br>
non photo geek wants :<br>
- a good gui<br>
- a mean to organize and to find easily photos<br>
- a mean to share photos</p>
<p>non geek has low interest about fixing photos</p>
<p>non geek has hich interest in : digikam must be included seamlessly in the workflow among the other tools in particular web photo cms.<br>
For example :<br>
- not to have error in exif data about rotation<br>
- for picasaweb it must be functionaly complete then must be able to send all tags for photos and movies</p>
<p>many thanks for your works<br>
and<br>
be more Proust-ian (yes i also like madeleine)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18853"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/486#comment-18853" class="active">I'm not to keen on photo</a></h3>    <div class="submitted">Submitted by <a href="http://vw.homelinux.net" rel="nofollow">Anonymous</a> (not verified) on Tue, 2009-11-24 14:57.</div>
    <div class="content">
     <p>I'm not to keen on photo editing either.  I shoot only in RAW so I have more leeway with photos, but I hate having to spend too much time editing.  That's actually why I use Digikam.  For me Digikam does make it infinitely easier to manage my photos and do my most common edits quickly (crop, resize, sharpen).  Digikam developers are always improving existing tools or adding new ones and I've never felt like that has made using Digikam more difficult.  Selective editing is still at the top of my list of feature requests (perform an edit on a specific selected area, such as sharpen the subject in the photo only), but I love the improvements so far in 1.0.</p>
<p>Usually I don't switch from using stable versions of Digikam until the first RC, but this time I couldn't wait any longer and finally switched for good at b6.  Good work as this is probably the first time I've done more than limited testing of a Digikam beta, without problems.  Very impressive considering the number of changes.</p>
<p>Vern</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18852"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/486#comment-18852" class="active">good result !</a></h3>    <div class="submitted">Submitted by shamaz (not verified) on Tue, 2009-11-24 13:19.</div>
    <div class="content">
     <p>I didn't know there was an wavelet based algorithm to remove noise... but the result looks really good ! It manage to preserve details and it's far less destructive than an usual blurring effect.<br>
The possibility ot adjust noise removal per channel is really a must have :). Since some camera tends to do more chroma noise than luma noise...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18854"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/486#comment-18854" class="active">"Et Voilà..." It's done !!!</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2009-11-24 17:00.</div>
    <div class="content">
     <a href="http://www.flickr.com/photos/digikam/4131333810/" title="waveletsnr by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2505/4131333810_5f36c3206a.jpg" width="500" height="400" alt="waveletsnr"></a>         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18859"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/486#comment-18859" class="active">Good work</a></h3>    <div class="submitted">Submitted by Rob (not verified) on Wed, 2009-11-25 20:55.</div>
    <div class="content">
     <p>Excellent news. Thanks for adding this, the wavelet denoise in GIMP or UFraw is a tool I really appreciate and I look forward to doing it in Digikam.</p>
<p>Having selective editing to apply denoise/sharpening/exposure correction in specific areas would be my biggest ask. But if that is contray to spirit of the editor here's another thought:</p>
<p>Global denoise is great but only great if the exposure CCD noise is even. What I/we often do is lighten the shadows to recover detail but that always selectively increases the noise in those shadows. If there was a way to link the wavelet denoise to that it would be absolute magic i.e. you would apply a global denoise but when the shadows were boosted the matching denoise would be applied to those boosted values. Would that be even possible technically?</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div>
</div>
