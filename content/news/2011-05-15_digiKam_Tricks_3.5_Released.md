---
date: "2011-05-15T17:59:00Z"
title: "digiKam Tricks 3.5 Released"
author: "Dmitri Popov"
description: "Here is what's new in this release: Updated screenshots Minor tweaks and fixes Revised Appendix C Continue to read"
category: "news"
aliases: "/node/603"

---

<p>Here is what's new in this release:</p>
<ul>
<li>Updated screenshots</li>
<li>Minor tweaks and fixes</li>
<li>Revised Appendix C</li>
</ul>
<p><a href="http://scribblesandsnaps.wordpress.com/2011/05/15/digikam-tricks-3-5-released/#comment-886">Continue to read</a></p>

<div class="legacy-comments">

</div>