---
date: "2009-06-09T17:53:00Z"
title: "digiKam 1.0.0-beta1 released..."
author: "digiKam"
description: "Dear all digiKam fans and users! A new test cycle has been started to complete 1.0.0 release... digiKam tarball can be downloaded from SourceForge at"
category: "news"
aliases: "/node/458"

---

<p>Dear all digiKam fans and users!</p>

<p>A new test cycle has been started to complete 1.0.0 release...</p>

<a href="http://www.flickr.com/photos/digikam/3600708900/" title="pgfsupportindigikam by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3592/3600708900_b6734a6579.jpg" width="500" height="313" alt="pgfsupportindigikam"></a>

<p>digiKam tarball can be downloaded from SourceForge <a href="http://sourceforge.net/project/showfiles.php?group_id=42641">at this url</a></p>

<p>digiKam will be also available for Windows. Precompiled packages can be downloaded with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<p>See below the list of new features and bugs-fix coming with this release:</p>
 
<h5>NEW FEATURES (since 0.10.x series):</h5><br>

<b>General</b>        : <a href="http://www.digikam.org/node/427">Add new Batch Queue Manager</a>.<br>
<b>General</b>        : New tool bar animation banner.<br>                                
<b>General</b>        : Fix compilation under MSVC 9.<br>                                 
<b>General</b>        : <a href="http://www.digikam.org/node/442">New first run assistant</a>.<br>    
<b>General</b>        : New dialog to show Database statistics.<br>                       
<b>General</b>        : Add support of wavelets image file <a href="http://www.libpgf.org">format PGF</a>.<br><br>

<b>AlbumGui</b>       : Icon view is now based on pure Qt4 model/view.<br>
<b>AlbumGui</b>       : New overlay button over icon view items to perform lossless rotations.<br><br>

<b>ImageEditor</b>    : <a href="http://www.digikam.org/node/439">New Liquid Rescale tool.</a><br><br>

<b>CameraGUI</b>      : New history of camera actions processed.<br><br>

BUGFIXES FROM KDE BUGZILLA (alias B.K.O | http://bugs.kde.org):<br>

001 ==&gt; 091742 : Batch process auto levels.<br>
002 ==&gt; 149361 : Not all edit actions are available as batch commands (for example auto-levels and gamma correction).<br>                                                                                                     
003 ==&gt; 187523 : digiKam crashes on startup (undefined symbol).<br>                                              
004 ==&gt; 187569 : Crash on star removing when filtering by stars.<br>                                             
005 ==&gt; 186549 : digiKam crashes when hovering the bottom part of its.<br>                                       
006 ==&gt; 187641 : Error while loading shared libraries: libkdcraw.so.5.<br>                                       
007 ==&gt; 187429 : Crash on Launch after disable KIPI plugins rotate.<br>                                          
008 ==&gt; 187937 : Writing tags to files doesn't update modification timestamp.<br>                                
009 ==&gt; 184687 : Crash after deleting an empty album.<br>                                                        
010 ==&gt; 186766 : Crash after finish resize and rename of images in folder.<br>                                   
011 ==&gt; 184953 : Zooming and paning in slideshow / without toolbars.<br>                                         
012 ==&gt; 187508 : Spontaneous crash whilst doing nothing in particular.<br>                                       
013 ==&gt; 188235 : Assigning a color profile to TIFF images does not work anymore with 0.10.0.<br>                 
014 ==&gt; 187748 : Install application icons to right path for non-KDE desktops.<br>                               
015 ==&gt; 186255 : digiKam crashes when switching collection database.<br>                                         
016 ==&gt; 178292 : AspectRatioCrop is slow and jumps around in first second.<br>                                   
017 ==&gt; 181892 : AspectRatioCrop: some properties should be disabled.<br>                                        
018 ==&gt; 188573 : RW2 converter broken for low light images.<br>                                                  
019 ==&gt; 188642 : GPS data in Digikam shows the wrong places on the map.<br>                                      
020 ==&gt; 188907 : Album view should display subfolders, like tag view does.<br>                                   
021 ==&gt; 183038 : "Select All" action interrupts other widgets.<br>                                               
022 ==&gt; 189022 : Better guessing of proportions with 'inverse transformation' option.<br>                        
023 ==&gt; 189286 : EXIF info lost while converting from NEF to TIFF.<br>                                           
024 ==&gt; 189336 : Preview of RAW look different than actual image.<br>                                            
025 ==&gt; 189413 : Images without GPS-data are shown on the equator in the geolocation-view.<br>                   
026 ==&gt; 184954 : Unable to delete items from camera - canon sx10is.<br>                                          
027 ==&gt; 189168 : You do not have write access to your home directory base path.<br>                              
028 ==&gt; 185617 : Images are invalid / not found when an album is moved.<br>                                      
029 ==&gt; 187265 : Advanced search crashes if a selected album has been deleted.<br>                               
030 ==&gt; 185177 : Searches are not updated when changes are made to the resulting image set.<br>                  
031 ==&gt; 189542 : Crash on opening download dialog.<br>                                                           
032 ==&gt; 114225 : Free rotation using an horizontal line.<br>                                                     
033 ==&gt; 189843 : Crash on ubuntu jaunty beta.<br>                                                                
034 ==&gt; 167836 : Image preview is too large.<br>                                                                 
035 ==&gt; 189250 : Editing a canon camera jpeg adds broken sRGB profile.<br>                                       
036 ==&gt; 187733 : Cannot turn off sidebar after Infrared filter usage ?<br>                                        
037 ==&gt; 188755 : Strange behaviour on selection.<br>                                                             
038 ==&gt; 185884 : Empty view after clicking the left thumbnail image in Image Editor filter view.<br>             
039 ==&gt; 188062 : Synchronize Images with Database does not save Copyright, Credit, Source, By-line, By-line Title in IPTC.<br>                                                                                                
040 ==&gt; 186823 : Several bugs in lens defect auto-correction plugin.<br>                                         
041 ==&gt; 190612 : digiKam and Showfoto crash.<br>                                                                 
042 ==&gt; 191092 : No images shown in digiKam albums.<br>                                                          
043 ==&gt; 191203 : digiKam crashed on first load, after being installed on ubuntu 9.04.<br>                        
044 ==&gt; 151321 : Remaining space incorrectly calculated when using symlinks (import from camera).<br>            
045 ==&gt; 189742 : Thumbnails not saved between sessions.<br>                                                      
046 ==&gt; 180507 : Crash when dragging picture to another app with kwin_composite enabled.<br>                     
047 ==&gt; 190296 : Can not save images on Windows.<br>                                                             
048 ==&gt; 141240 : Camera download: file rename with regexps.<br>                                                  
049 ==&gt; 181951 : digiKam crashes (SIGABRT) during the importing of pictures from Kodak V530 camera.<br>          
050 ==&gt; 166392 : File-Renaming: custom sequence-number doesn't work.<br>                                         
051 ==&gt; 182332 : Can't stop generating of thumbnails when import from a card reader.<br>                         
052 ==&gt; 152382 : Unable to view photo.<br>                                                                       
053 ==&gt; 161744 : Filenames are lower case when imported via sd card reader, upper case when imported via usb cable directly from camera.<br>                                                                                  
054 ==&gt; 187560 : Iptc data for author are missing while downloading images.<br>                                  
055 ==&gt; 192294 : digiKam crash when i try to import photos from my card reader.<br>                              
056 ==&gt; 192413 : Missing items from right-click search menu.<br>                                                 
057 ==&gt; 131551 : Camera port info deleted when editing camera title.<br>                                         
058 ==&gt; 181726 : digiKam collect video, music when I connect usb key.<br>                                        
059 ==&gt; 154626 : Autodetect starting number for "appending numbers" when downloading images.<br>                 
060 ==&gt; 188316 : Missing NEF files when importing from USB disk.<br>                                             
061 ==&gt; 193163 : digiKam ignores settings on the rating stars.<br>                                               
062 ==&gt; 189168 : You do not have write access to your home directory base path.<br>                              
063 ==&gt; 193226 : Data loss on downloading photos from camera.<br>                                                
064 ==&gt; 189460 : digiKam crash when goelocating image in the marble tab.<br>                                     
065 ==&gt; 186046 : Crash after calling settings.<br>                                                               
066 ==&gt; 193348 : Can't build digiKam on kde 4.2.3.<br>                                                           
067 ==&gt; 150072 : Download with digiKam doesn't work with the first start of digiKam.<br>                         
068 ==&gt; 193738 : Have the image editor status bar show the current color depth.<br>                              
069 ==&gt; 191678 : Exif rotation for Pentax Pef not working.<br>                                                   
070 ==&gt; 146455 : Image rotation and auto-rotate don't work.<br>                                                  
071 ==&gt; 152877 : Thumbnails: URI does not follow Thumbnail Managing Standard.<br>                                
072 ==&gt; 140615 : Pre-Creating of image thumbnails in whole digiKam/all subfolders.<br>                           
073 ==&gt; 193967 : VERY fast loading of thumbnails [PATCH].<br>                                                    
074 ==&gt; 191492 : digiKam hangs on startup "reading database".<br>                                                
075 ==&gt; 171950 : Lost pictures after 'download/delete all' when target dir had same file names.<br>              
076 ==&gt; 191774 : digiKam download sorting slow, and wrong with multiple folders.<br>                             
077 ==&gt; 191842 : digiKam download forgets to download some pictures (multiple big folders).<br>                  
078 ==&gt; 187902 : Status bar gives wrong picture filename and position in list after "Save As".<br>               
079 ==&gt; 193894 : Pop up "Copy Finished!" dialogue when done downloading from camera.<br>
080 ==&gt; 193870 : digiKam freezes after second exif rotation on same file.<br>
081 ==&gt; 194342 : What is the right rename pattern to get names like 2009.02.18_09h56m30s ?<br>
082 ==&gt; 194177 : digiKam crashes always on startup and causes signal 11 (SIGSEGSV) - app not useable.<br>
083 ==&gt; 169213 : digiKam should not enter new album after creating it.<br>
084 ==&gt; 191634 : Statistics of the database, and all pictures ?<br>
085 ==&gt; 193228 : Experimental option "write metadata to RAW files" corrupts Nikon NEFs.<br>
086 ==&gt; 194116 : digiKam crashed for no obvious reason.<br>
087 ==&gt; 179766 : Auto crop does not fully remove black corners created by free rotation.<br>
088 ==&gt; 134308 : Add arrow buttons on thumbnail to perform lossless rotation.<br>
089 ==&gt; 194804 : Tag, rate, rotate and delete via slideshow.<br>
090 ==&gt; 192425 : Star rating setting under thumbnails is a nuisance.<br>
091 ==&gt; 194330 : digiKam crashed when deleting image.<br>
092 ==&gt; 188959 : On first use, digiKam should not scan for images without user confirmation.<br>
093 ==&gt; 195202 : Thumbnail rename fails (cross-device link) and so cache doesn't work.<br>
094 ==&gt; 161749 : Raw Converter needs more options.<br>
095 ==&gt; 150598 : File rename during image upload fails silently.<br>
096 ==&gt; 161865 : Image not placed in New Tag category.<br>
097 ==&gt; 136897 : THM files are not uploaded.<br>
098 ==&gt; 195494 : Deleting and cancelling a picture with the keyboard still delete the picture.<br>
099 ==&gt; 120994 : Print preview generates HUGE postscript files.<br>
100 ==&gt; 191633 : Rename a folder, press F5 and see thumbnails go away.<br>
101 ==&gt; 195565 : Loading next image while in curves tool does not confirm and loads nothing.<br>
102 ==&gt; 189046 : digiKam crashes finding duplicates in fingerprints.<br>
103 ==&gt; 185726 : SIGSERV 11 by choosing an image for fullscreen.<br>
104 ==&gt; 193616 : Location not shown digiKam right sidebar after geolocating in external program.<br>

<div class="legacy-comments">

  <a id="comment-18588"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18588" class="active">Nepomuk</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2009-06-09 18:35.</div>
    <div class="content">
     <p>First of all, congratulations for this very important release! it's nice to see that you have finally reached 1.0 status :)</p>
<p>One question: Is there any kind of Nepomuk support? Is it going to be support for it in the future?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18616"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18616" class="active">Planed...</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2009-06-18 22:09.</div>
    <div class="content">
     <p>This week end, Marcel go to this <a href="http://techbase.kde.org/Projects/Nepomuk/CodingSprint2009">coding sprint...</a> to code a Nepomuk interface for digiKam...</p>         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18623"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18623" class="active">Nepomuk interface for digiKam done...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2009-06-24 05:18.</div>
    <div class="content">
     Look <a href="http://article.gmane.org/gmane.comp.kde.digikam.devel/34153">this message</a> from Marcel...         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18589"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18589" class="active">Congratulation</a></h3>    <div class="submitted">Submitted by Romain (not verified) on Tue, 2009-06-09 19:21.</div>
    <div class="content">
     <p>Congratulations for this release, I'm always very impressed when I read the tons of bugfixes !!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18591"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18591" class="active">im using this version under</a></h3>    <div class="submitted">Submitted by el barto (not verified) on Tue, 2009-06-09 20:15.</div>
    <div class="content">
     <p>im using this version under kde4.3beta2 and take a lot of time to load the images from a album, and in the 0.10 version was really fast.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18592"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18592" class="active">solved, just deleted the old</a></h3>    <div class="submitted">Submitted by el barto (not verified) on Tue, 2009-06-09 20:48.</div>
    <div class="content">
     <p>solved, just deleted the old config and now works fast like 0.10.x</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18593"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18593" class="active">Hmm this is weird, don't see</a></h3>    <div class="submitted">Submitted by Andi Clemens on Tue, 2009-06-09 21:05.</div>
    <div class="content">
     <p>Hmm this is weird, don't see the connection here. But well if it helped :-)</p>
<p>Andi</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18624"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18624" class="active">im using this version under</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2009-06-25 16:10.</div>
    <div class="content">
     <p>im using this version under kde4.3beta2 and take a lot of time to load the images from a album, and in the 0.10 version was really fast.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18625"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18625" class="active">Gilles,
this could be related</a></h3>    <div class="submitted">Submitted by Andi Clemens on Thu, 2009-06-25 19:40.</div>
    <div class="content">
     <p>Gilles,</p>
<p>this could be related to the thumbsDB. I experience the same behavior, 1.0.0 is way slower then 0.10 as I mentioned before.</p>
<p>Andi</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18595"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18595" class="active">digiKam rocks !!!</a></h3>    <div class="submitted">Submitted by <a href="http://notoveryet.wordpress.com/" rel="nofollow">rajat</a> (not verified) on Wed, 2009-06-10 06:24.</div>
    <div class="content">
     <p>that's an impressive list.... :)<br>
Thanks a lot for your work for continuosly improving the software....</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18596"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18596" class="active">Congratulations and thank you</a></h3>    <div class="submitted">Submitted by <a href="http://www.lcgsone.com" rel="nofollow">LCG</a> (not verified) on Wed, 2009-06-10 08:22.</div>
    <div class="content">
     <p>Congratulations and thank you for this release. It looks great :)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18597"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18597" class="active">Problems compiling kipi-plugins-0.3.0 for digikam-1.0.0-beta1</a></h3>    <div class="submitted">Submitted by oswald maenner (not verified) on Wed, 2009-06-10 22:46.</div>
    <div class="content">
     <p>I have a debian/lenny system with the KDE 4.1 libraries,<br>
and have manually installed LibRaw-0.7.2 and exiv2-0.18.1 under /usr/local</p>
<p>running cmake . in the kipi-plugins-0.3.0 does not find these libraries (only the original lenny ones), however with<br>
  cmake -DKEXIV2_INCLUDE_DIR=/usr/local/include/exiv2 -DKEXIV2_LIBRARIES=/usr/local/lib \<br>
     -DKDCRAW_INCLUDE_DIR=/usr/local/include -DKDCRAW_LIBRARIES=/usr/local/lib .</p>
<p>I still get the error </p>
<p>-- Found Qt-Version 4.4.3 (using /usr/bin/qmake-qt4)<br>
-- Found X11: /usr/lib/libX11.so<br>
-- Found Automoc4: /usr/bin/automoc4<br>
-- Found Perl: /usr/bin/perl<br>
-- Found KDE 4.1 include dir: /usr/include<br>
-- Found KDE 4.1 library dir: /usr/lib<br>
-- Found KDE4 kconfig_compiler preprocessor: /usr/bin/kconfig_compiler<br>
-- Found automoc4: /usr/bin/automoc4<br>
-- Found Kexiv2 library in cache: /usr/local/lib<br>
-- Found Kdcraw library in cache: /usr/local/lib<br>
-- Check Kipi library in local sub-folder...<br>
-- Check Kipi library using pkg-config...<br>
-- Found libkipi release &lt; 0.2.0, too old<br>
CMake Error at /usr/share/kde4/apps/cmake/modules/FindKipi.cmake:85 (message):<br>
  Could NOT find libkipi header files<br>
Call Stack (most recent call first):<br>
  CMakeLists.txt:39 (FIND_PACKAGE)</p>
<p>What is strange is that cmake does not recognise that it is run from inside the kipi-plugins-0.3.0 directory and that there is a line<br>
  FIND_PACKAGE(Kipi REQUIRED)<br>
in the kipi-plugins-0.3.0/CMakeLists.txt file; when I comment out this line and rerun with<br>
  cmake -DKIPI_FOUND=1 .<br>
The generation of the build files completes.</p>
<p>However when I then run make, I get the error</p>
<p>[  0%] Generating common/libkipiplugins/svnversion.h<br>
[  0%] Built target kipiplugins-svnversion<br>
Generating batchprogressdialog.moc<br>
Generating imageslist.moc<br>
Generating imagedialog.moc<br>
[  0%] Building CXX object common/libkipiplugins/CMakeFiles/kipiplugins.dir/kipiplugins_automoc.o<br>
[  0%] Building CXX object common/libkipiplugins/CMakeFiles/kipiplugins.dir/kpaboutdata.o<br>
[  0%] Building CXX object common/libkipiplugins/CMakeFiles/kipiplugins.dir/kpwriteimage.o<br>
In file included from /home/ossi/Apps/digikam-1.0.0-beta1/LIBS/kipi-plugins-0.3.0/common/libkipiplugins/kpwriteimage.cpp:52:<br>
/home/ossi/Apps/digikam-1.0.0-beta1/LIBS/kipi-plugins-0.3.0/common/libkipiplugins/kpwriteimage.h:51:43: error: libkdcraw/rawdecodingsettings.h: No such file or directory<br>
/home/ossi/Apps/digikam-1.0.0-beta1/LIBS/kipi-plugins-0.3.0/common/libkipiplugins/kpwriteimage.h:55:30: error: libkexiv2/kexiv2.h: No such file or directory<br>
.....</p>
<p>However the files  rawdecodingsettings.h and kexiv2.h are nowhere to be found in LibRaw-0.7.2 and exiv2-0.18.1</p>
<p>At the moment I am lost as I am completely new to the cmake environment. Any ideas on how I should proceed?</p>
<p>Regards, Oswald</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18600"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18600" class="active">Installed  1.0.0-beta1 from debian/unstable</a></h3>    <div class="submitted">Submitted by oswald maenner (not verified) on Thu, 2009-06-11 12:31.</div>
    <div class="content">
     <p>After copying my root partition to another HD and - with fingers crossed - upgraded digicam from the debian/unstable repository together with a load of kde4 and other libraries.</p>
<p>Fortunately the upgrade went smoothly, and digikam 1.0.0-beta1 started without problems, sucessfully migrating my 0.9.x meta-tags to the version 4 database.</p>
<p>Now I can plug my album+database USB harddrive also into my Kubuntu laptop (currently still digikam 0.10.0) and process the fotos there.</p>
<p>Great Job!</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18598"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18598" class="active">can I have root album on</a></h3>    <div class="submitted">Submitted by <a href="http://www.new-krobuzon.cz" rel="nofollow">marek</a> (not verified) on Thu, 2009-06-11 11:18.</div>
    <div class="content">
     <p>can I have root album on FTP??</p>
<p>dolphin say: the directory is not exist, or is nor readable, but directory exist and is rewritabe, readable :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18599"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18599" class="active">or please add "export to FTP"</a></h3>    <div class="submitted">Submitted by <a href="http://www.new-krobuzon.cz" rel="nofollow">marek</a> (not verified) on Thu, 2009-06-11 11:21.</div>
    <div class="content">
     <p>or please add "export to FTP" to "export" - might be good</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18652"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18652" class="active">use the force luke</a></h3>    <div class="submitted">Submitted by <a href="http://yoho.wordpress.com" rel="nofollow">yoho</a> (not verified) on Mon, 2009-07-06 04:26.</div>
    <div class="content">
     <p>You can add a collection located on a network share : go to settings -&gt; configure digikam. I think if you manage your ftp folder this way, it's much more flexible.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18601"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18601" class="active">PPA for Kubuntu?</a></h3>    <div class="submitted">Submitted by <a href="http://www.oceanwatcher.com/" rel="nofollow">Oceanwatcher</a> (not verified) on Fri, 2009-06-12 17:12.</div>
    <div class="content">
     <p>Is there a ppa for Kubuntu somewhere that I can add so I get this release?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18602"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18602" class="active">Does it support marble 0.8svn</a></h3>    <div class="submitted">Submitted by Dany Martineau (not verified) on Sat, 2009-06-13 02:51.</div>
    <div class="content">
     <p>Does it support marble 0.8svn (from kde 4.3.0 beta2)?  I'm asking because the only way i could run digikam 1.0.0 beta 1 is when i disable marble support in compiling (-DENABLE_MARBLEWIDGET=no -DWITH_MarbleWidget=no).</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18604"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18604" class="active">Impressive Progress</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2009-06-15 11:08.</div>
    <div class="content">
     <p>I upgraded to KDE 4 last month and was dismayed at the large number<br>
of applications that have taken many steps backwards in the transition.<br>
DigiKam is one of the few that I use regularly that has maintained all<br>
of its functionality and added some nice new features. You deserve a<br>
lot of praise for not following the crowd, rewriting <em>everything</em> from<br>
scratch and leaving your users disappointed.</p>
<p>I look forward to the new releases and I'm sure that your approach of<br>
improving things incrementally will continue to delight your growing<br>
user base.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18605"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18605" class="active">Many thanks</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2009-06-15 11:20.</div>
    <div class="content">
     <p>Thanks a lots for this comment. It's really digiKam team approach : no feature regressions...</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18611"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18611" class="active">I want to support to 100% to</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2009-06-16 11:41.</div>
    <div class="content">
     <p>I want to support to 100% to that comment. Digikam did the KDE4-port really well.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18612"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18612" class="active">Congratz and Thanks</a></h3>    <div class="submitted">Submitted by Sisko (not verified) on Tue, 2009-06-16 13:22.</div>
    <div class="content">
     <p>I am really impressed about the good progress this application has made.</p>
<p>It's not only the app. There is a good documentation (for installung/compiling too), a nice website and a transparent developing process.</p>
<p>It just feels...</p>
<p>Professional</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18614"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18614" class="active">and... I like that new Batch</a></h3>    <div class="submitted">Submitted by Sisko (not verified) on Wed, 2009-06-17 08:45.</div>
    <div class="content">
     <p>and... I like that new Batch Queue Manager. Really good work.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18617"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18617" class="active">Crashes on delete and move pictures</a></h3>    <div class="submitted">Submitted by Chrisch (not verified) on Mon, 2009-06-22 00:11.</div>
    <div class="content">
     <p>I installed 1.0.0-beta1 from debian/unstable (sidux) and encounter some crashes (not always) while deleting and moving an amount of mails (30 to 100) between my albums. After starting digikam again the delete/move proceeded sometimes half and sometimes to it end.</p>
<p>I install gdb and digikam-dbg now to create some backtrace. I'm using digikam since 0.9.x and 0.10.x.</p>
<p>Great work anyway.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18620"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18620" class="active">added to bugs.kde.org</a></h3>    <div class="submitted">Submitted by Chrisch (not verified) on Tue, 2009-06-23 01:34.</div>
    <div class="content">
     <p>Here's my <a href="http://bugs.kde.org/show_bug.cgi?id=197562">bug-report</a> about this crash.</p>
<p>Good luck<br>
Chrisch</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18621"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18621" class="active">Fixed in svn</a></h3>    <div class="submitted">Submitted by Chrisch (not verified) on Wed, 2009-06-24 00:24.</div>
    <div class="content">
     <p>Fixed in svn (see duplicate in bug-report)<br>
Thanks and Regards,<br>
Chrisch</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18638"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18638" class="active">http://www.erreauk.com</a></h3>    <div class="submitted">Submitted by <a href="http://www.erreauk.com" rel="nofollow">erreauk</a> (not verified) on Fri, 2009-07-03 17:41.</div>
    <div class="content">
     <p>I installed 1.0.0-beta1 from debian/unstable (sidux) and encounter some crashes (not always) while deleting and moving an amount of mails (30 to 100) between my albums. After starting digikam again the delete/move proceeded sometimes half and sometimes to it end.</p>
<p><a href="http://www.erreauk.com">erreauk</a></p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18622"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18622" class="active">manual sorting in icon view?</a></h3>    <div class="submitted">Submitted by Etienne (not verified) on Wed, 2009-06-24 00:54.</div>
    <div class="content">
     <p>Hi, love your app!</p>
<p>However, one thing is missing in my point if view, an ability to do manual sorting.  I need that feature to sort photos from multiple sources (different cameras), before renaming them and sending them to my website for online viewing.</p>
<p>This feature is missing from all linux photo-management apps, but present in similar windows apps such as ACDSee and FastStone Image Viewer</p>
<p>Does the new icon view make this easier to implement?<br>
Thanks!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18635"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18635" class="active">Sharpen tool</a></h3>    <div class="submitted">Submitted by Luigi (not verified) on Fri, 2009-07-03 17:31.</div>
    <div class="content">
     <p>Hello,<br>
I can't find "sharpen tool" :-(( it was in menu enhance but now is there "only" brightnes/... , hotpixels, in-paiting, lens, noise reduction, restoration. I have gentoo with kipi 0.4, Digikam from SVN (revision 990984) - but I can't find sharpen tool so in some older versions. Do you know where to look for a mistake, please? in compiling (useflags) or in other packages which provides this feature...or...? Thanks a lot!</p>
<p>BTW: Digikam is GREAT! :-)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18637"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18637" class="active">broken packages.</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2009-07-03 17:35.</div>
    <div class="content">
     <p>Nothing have been removed. Sharpen tool is always at the same place.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18639"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18639" class="active">thank for super-quick</a></h3>    <div class="submitted">Submitted by Luigi (not verified) on Fri, 2009-07-03 17:48.</div>
    <div class="content">
     <p>thank for super-quick answer!<br>
I understand that you can't know all packages from all the linux distributions - but please, try advise me, what packages (projects) may be it.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18640"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18640" class="active">No idea...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2009-07-03 17:53.</div>
    <div class="content">
     <p>Well, compile yourself digiKam (:=)))...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18641"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18641" class="active">I have self-compiled digikam</a></h3>    <div class="submitted">Submitted by Luigi (not verified) on Fri, 2009-07-03 18:00.</div>
    <div class="content">
     <p>I have self-compiled digikam - without problems and warnings. I have compiled Digikam under instruction in section "download" &gt; svn (kde4)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18642"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18642" class="active">start digiKam with a new user</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2009-07-03 18:12.</div>
    <div class="content">
     <p>create new account, and start digiKam. setup all properly, and look if Sharpen tool is there...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18643"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18643" class="active">i have created new user named</a></h3>    <div class="submitted">Submitted by Luigi (not verified) on Fri, 2009-07-03 18:27.</div>
    <div class="content">
     <p>i have created new user named "test", configured startup wizzard in Digikam, but with sharpen tool no changes - missing :-(</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18644"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18644" class="active">strange...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2009-07-03 18:46.</div>
    <div class="content">
     <p>Sharpen tool come from with core editor image plugin. This one include Blur, Red Eyes, BCG, BW, HSL, Ratio crop, etc... Do you see these options in editor ?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18645"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18645" class="active">I don't see it, where exactly</a></h3>    <div class="submitted">Submitted by Luigi (not verified) on Fri, 2009-07-03 18:59.</div>
    <div class="content">
     <p>I don't see it, where exactly should be - in the menu?<br>
color:curves adj.,levels adj.,channel mixer,color effects,white balance.<br>
enhance: hot pixels, inpainting,lens,noise reduction,restoration.<br>
transform: rotate L/R,Flip H/V,Crop,Liquid rescale,free rotation,perspective adj.,shear.<br>
decorate: add border,insert text,templ.superimpose,apply texture<br>
filters: blur effects, charcoal drawing,distortion effects,emboss,add film grain,IR film,Oil paint,raindrops</p>
<p>BTW: I have a friend with other distro (Arch linux) and he has the same problem</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18646"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18646" class="active">Core pluging is not installed...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2009-07-03 20:44.</div>
    <div class="content">
     <p>It's clear : image editor core plugin is not complied and installed. I don't know why...</p>

<p>In fact <a href="http://websvn.kde.org/trunk/extragear/graphics/digikam/imageplugins/coreplugin">code is there</a> </p>

<p>When you compile digiKam, check if this dir is compiled.</p>         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18647"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18647" class="active">OK, you have right :-)</a></h3>    <div class="submitted">Submitted by Luigi (not verified) on Fri, 2009-07-03 21:28.</div>
    <div class="content">
     <p>OK, you have right :-) Tomorrow i check the compilation process, today I am tired... Thank you for your time VERY much!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18648"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18648" class="active">Hi,
i'm back :-) So i</a></h3>    <div class="submitted">Submitted by Luigi (not verified) on Sat, 2009-07-04 13:02.</div>
    <div class="content">
     <p>Hi,<br>
i'm back :-) So i recompiled digiKam - here is the <a href="http://upload.damak.cz/764372">log file</a>. Coreplugins are compiled and installed, sharpen tool too :-(</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18649"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/458#comment-18649" class="active">the log is uploaded also here</a></h3>    <div class="submitted">Submitted by Luigi (not verified) on Sat, 2009-07-04 15:02.</div>
    <div class="content">
     <p>the log is uploaded also <a href="http://rapidshare.com/files/251845721/digikam_emerge.txt.html">here</a> (Previous link is to the czech server without english interface for download - i'm sorry)</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div></div></div></div></div></div></div>
</div>
