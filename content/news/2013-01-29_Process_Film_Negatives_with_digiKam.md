---
date: "2013-01-29T09:48:00Z"
title: "Process Film Negatives with digiKam"
author: "Dmitri Popov"
description: "While digiKam is first and foremost an application for processing and organizing digital photos, it also features tools for working with film negatives. Continue to"
category: "news"
aliases: "/node/681"

---

<p>While digiKam is first and foremost an application for processing and organizing digital photos, it also features tools for working with film negatives.</p>
<p><img class="size-medium wp-image-3320  " alt="Using the Color Negative tool" src="http://scribblesandsnaps.files.wordpress.com/2013/01/digikam-colornegative.png?w=500" width="500" height="383"></p>
<p><a href="http://scribblesandsnaps.com/2013/01/29/process-film-negatives-with-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>