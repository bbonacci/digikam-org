---
date: "2006-01-06T15:56:00Z"
title: "Splashscreen contributions..."
author: "digiKam"
description: "For the next digiKam releases (0.8.1 and 0.9.0), we need photographs for digiKam and Showfoto splashscreens... SplashScreen is a simple way for users to contribute"
category: "news"
aliases: "/node/55"

---

<p>For the next digiKam releases (0.8.1 and 0.9.0), we need photographs for digiKam and Showfoto splashscreens...</p>
<p>SplashScreen is a simple way for users to contribute to digiKam project. The pictures must be correctly exposed/composed, and the subject must be choosen from a real photographer inspiration. Note that we will add a horizontal frame to the bottom of the image as in the current splashs.</p>
<p>If somebody wants to contribute with nice photographs, please contact me by email : caulier dot gilles at gmail dot com</p>
<p>Thanks in advance</p>

<div class="legacy-comments">

</div>
