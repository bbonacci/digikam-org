---
date: "2008-09-03T15:25:00Z"
title: "digiKam 0.10.0-beta3 release for KDE4"
author: "digiKam"
description: "Dear all digiKam fans and users! The digiKam development team is happy to release the 3rd beta release dedicated to KDE4. The digiKam tarball can"
category: "news"
aliases: "/node/371"

---

Dear all digiKam fans and users!<br><br>

The digiKam development team is happy to release the 3rd beta release dedicated to KDE4. The digiKam tarball can be downloaded from <a href="http://sourceforge.net/project/showfiles.php?group_id=42641">SourceForge</a>.

<br>
<a href="http://www.flickr.com/photos/digikam/2824330661/" title="Raw Import tool for KDE4 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3104/2824330661_bfffca55a1.jpg" width="500" height="400" alt="Raw Import tool for KDE4"></a>
<br>

<p>Take care, it's always a BETA code with many of new bugs not yet fixed... Do not use yet in production...</p>
 
<p>... for production, use KDE3 with 0.9.x, which is still mantained and <a href="http://www.digikam.org/drupal/node/359">0.9.4 stable release</a> have been published this summer.</p>

<p>To compile this KDE4 version, you need libkexiv2, libkdcraw, and libkipi for KDE4. These libraries are now included in KDE core. You need last libkdcraw and libkexiv2 from svn trunk to compile digiKam (will be released with KDE 4.2). To extract libraries source code from svn, look at bottom of <a href="http://www.digikam.org/drupal/download?q=download/svn">this page</a> for details</p>

<p>For others depencies, consult the README file. There are also optional depencies to enable some new features as lensfun for lens auto-correction, and marble for geolocation.</p>
 
<p>The digikam.org web site has been redesigned for the new KDE4 series. All <a href=" http://www.digikam.org/drupal/node/323">screenshots</a> have been updated and a lots of <a href="  http://www.digikam.org/drupal/tour">video demos</a> have been added to present new features included in this version.</p>
 
<h5>NEW FEATURES (since 0.9.x series):</h5><br>

<b>General</b> : Ported to CMake/Qt4/KDE4.<br>
<b>General</b> : Thumbs KIO-Slave removed. digiKam now use multi-threading to generate thumnails.<br>
<b>General</b> : Removed all X11 library dependencies. Code is now portable under MACOS-X and Win32.<br>
<b>General</b> : Support of XMP metadata (require <a href="http://www.exiv2.org">Exiv2 library</a> &gt;= 0.16).<br>
<b>General</b> : Hardware handling using KDE4 Solid interface.<br>
<b>General</b> : Preview of Video and Audio files using KDE4 Phonon interface.<br>
<b>General</b> : Database file can be stored on a customized place to support remote album library path.<br>
<b>General</b> : New database schema to host more photo and collection informations.<br>
<b>General</b> : Database interface fully re-written using Qt4 SQL plugin.<br>
<b>General</b> : Support of multiple roots album paths.<br>
<b>General</b> : Physical root albums are managed as real album.<br>
<b>General</b> : New option in Help menu to list all RAW file formats supported.<br>
<b>General</b> : Geolocation of pictures from sidebars is now delegate to KDE4 Marble widget.<br>
<b>General</b> : New option in Help menu to list all main components/libraries used by digiKam.<br>
<b>General</b> : libkdcraw dependency updated to 0.3.0.<br>
<b>General</b> : Raw metadata can be edited, changed, added to TIFF/EP like RAW file formats (require Exiv2 &gt;= 0.18). Currently DNG, NEF, and PEF raw files are supported. More will be added in the future.<br><br>
 
<b>CameraGUI</b> : New design for camera interface.<br>
<b>CameraGUI</b> : New Capture tool.<br>
<b>CameraGUI</b> : New bargraph to display camera media free-space.<br><br>
 
<b>AlbumGUI</b> : Added Thumbbar with Preview mode to easy navigate between pictures.<br>
<b>AlbumGUI</b> : Integration of Simple Text Search tool to left sidebar as Amarok.<br>
<b>AlbumGUI</b> : New advanced Search tools. Re-design of Search backend, based on XML. Re-design of search dialog for a better usability. Searches based on metadata and image properties are now possible.<br>
<b>AlbumGUI</b> : New fuzzy Search tools based on sketch drawing template. Fuzzy searches backend use an Haar wevelet interface. You simply draw a rough sketch of what you want to find and digiKam displays for you a thumbnail view of the best matches.<br>
<b>AlbumGUI</b> : New Search tools based on marble widget to find pictures over a map.<br>
<b>AlbumGUI</b> : New Search tools to find similar images against a reference image.<br>
<b>AlbumGUI</b> : New Search tools to find duplicates images around whole collections.<br><br>
 
<b>ImageEditor</b> : Added Thumbbar to easy navigate between pictures.<br>
<b>ImageEditor</b> : New plugin based on <a href="http://lensfun.berlios.de">LensFun library</a> to correct automaticaly lens aberrations.<br>
<b>ImageEditor</b> : LensDistortion and AntiVignetting are now merged with LensFun plugin.<br>
<b>ImageEditor</b> : All image plugin tool settings provide default buttons to reset values.<br>
<b>ImageEditor</b> : New Raw import tool to handle Raw pictures with customized decoding settings.<br>
<b>ImageEditor</b> : All image plugin dialogs are removed. All tools are embedded in editor window.<br>

<h5>BUGS FIXES:</h5><br>

001 ==&gt; 146864 : Lesser XMP support in digiKam.<br>
002 ==&gt; 145096 : Request: acquire mass storage from printer as from camera. Change menu "Camera" to "Acquire".<br>
003 ==&gt; 134206 : Rethink about: Iptc.Application2.Urgency digiKam Rating.<br>
004 ==&gt; 149966 : Alternative IPTC Keyword Separator (dot notation).<br>
005 ==&gt; 129437 : Album could point to network path. Now it's impossible to view photos from shared network drive.<br>
006 ==&gt; 137694 : Allow album pictures to be stored on network devices.<br>
007 ==&gt; 114682 : About library path.<br>
008 ==&gt; 122516 : Album Path cannot be on Network device (Unmounted).<br>
009 ==&gt; 107871 : Allow multiple album library path.<br>
010 ==&gt; 105645 : Impossible to not copy images in ~/Pictures.<br>
011 ==&gt; 132697 : Metadata list has no scrollbar.<br>
012 ==&gt; 148502 : Show rating in embedded preview / slideshow.<br>
013 ==&gt; 155408 : Thumbbar in the album view.<br>
014 ==&gt; 138290 : GPSSync plugin integration in the side bar.<br>
015 ==&gt; 098651 : Image Plugin filter based on clens.<br>
016 ==&gt; 147426 : Search for non-voted pics.<br>
017 ==&gt; 149555 : Always present search box instead of search by right-clicking and selecting simple or advanced search.<br>
018 ==&gt; 139283 : IPTC Caption comment in search function.<br>
019 ==&gt; 150265 : Avanced search filter is missing search in comment / description.<br>
020 ==&gt; 155735 : Make it possible to search on IPTC-text.<br>
021 ==&gt; 147636 : GUI error in advanced searches: lots of free space.<br>
022 ==&gt; 158866 : Advanced Search on Tags a mess.<br>
023 ==&gt; 149026 : Search including sub-albums.<br>
024 ==&gt; 153070 : Search for image by geo-location.<br>
025 ==&gt; 154764 : Pictures saved into root album folder are not shown.<br>
026 ==&gt; 162678 : digiKam crashed while loading.<br>
027 ==&gt; 104067 : Duplicate image finder should offer more actions on duplicate images found.<br>
028 ==&gt; 107095 : Double image removal: Use trashcan.<br>
029 ==&gt; 112473 : findimages shows only small thumbnails.<br>
030 ==&gt; 150077 : Find Duplicate Images tool quite unusable on many images (a couple of issues).<br>
031 ==&gt; 161858 : Find Duplicate Image fails with Canon Raw Files.<br>
032 ==&gt; 162152 : Batch Duplicate Image Management.<br>
033 ==&gt; 164418 : GPS window zoom possibility.<br>
034 ==&gt; 117287 : Search albums on read only album path.<br>
035 ==&gt; 164600 : No picture in view pane.<br>
036 ==&gt; 164973 : Showfoto crashed at startup.<br>
037 ==&gt; 165275 : build-failure - imageresize.cpp - 'KToolInvocation' has not been declared.<br>
038 ==&gt; 165292 : albumwidgetstack.cpp can't find Phonon/MediaObject.<br>
039 ==&gt; 165341 : Crash when changing histogram channel when welcome page is shown.<br>
040 ==&gt; 165318 : digiKam doesn't start because of SQL.<br>
041 ==&gt; 165342 : Crash when changing album sort modus.<br>
042 ==&gt; 165338 : Right sidebar initally too big.<br>
043 ==&gt; 165280 : Sqlite2 component build failure.<br>
044 ==&gt; 165769 : adjustcurves.cpp - can't find version.h.<br>
045 ==&gt; 166472 : Thumbnail bar gone in image editor when switching back from fullscreen.<br>
046 ==&gt; 166663 : Tags not showing pictures without "Include Tag Subtree".<br>
047 ==&gt; 166616 : Filmstrip mode in View.<br>
048 ==&gt; 165885 : Thumbnails and images are NOT displayed in the main view center pane.<br>
049 ==&gt; 167139 : Crash if Exif.GPSInfo.GPSAltitude is empty.<br>
050 ==&gt; 167168 : Timeline view shows redundant extra sections.<br>
051 ==&gt; 166440 : Removing images from Light Table is not working properly.<br>
052 ==&gt; 166484 : digiKam crashes when changing some settings and using "find similar" after the changes made.<br>
053 ==&gt; 167124 : Timeline view not updated when changing selected time range.<br>
054 ==&gt; 166564 : Display of *already* *created* thumbnails is slow.<br>
055 ==&gt; 166483 : Error in "not enough disk space" error message.<br>
056 ==&gt; 167379 : Image selection for export plugin.<br>
057 ==&gt; 166622 : Confusing Add Images use.<br>
058 ==&gt; 166576 : digiKam quits when running "Rebuild all Thumbnail Images" twice.<br>
059 ==&gt; 167562 : Image Editor Shortcut Keys under Edit redo/undo are missing in GUI.<br>
060 ==&gt; 167561 : Crash when moving albums.<br>
061 ==&gt; 167529 : Image Editor not working correctly after setting "use horizontal thumbbar" option.<br>
062 ==&gt; 167621 : digiKam crashes when trying to remove a tag with the context menu.<br>
063 ==&gt; 165348 : Problems when trying to import KDE3 data.<br>
064 ==&gt; 166424 : Crash when editing Caption with Digikam4 SVN.<br>
065 ==&gt; 166310 : Preview not working in image effect dialogs.<br>
066 ==&gt; 167571 : Unnatural order when removing images from light table.<br>
067 ==&gt; 167139 : Crash if Exif.GPSInfo.GPSAltitude is empty.<br>
068 ==&gt; 167778 : Assigning ratings in image editor via shortcuts or toolbar not working.<br>
069 ==&gt; 168567 : Unable to edit or remove iptc and xmp info.<br>
070 ==&gt; 168846 : crash after playing with tags (seems to be rather a crash in Qt or kdelibs).<br>
071 ==&gt; 166671 : Image filter dialog buttons are much too big.<br>
072 ==&gt; 134486 : Keywords are not written to raw files even though they do embed iptc/exif.<br>
073 ==&gt; 168839 : Digikam crashed after tagging.<br>
074 ==&gt; 167085 : Color selection in sketch search tool looks plain black.<br>
075 ==&gt; 168852 : Crash on profile application.<br>
076 ==&gt; 167867 : Album view is reset by changing settings.<br>
077 ==&gt; 168461 : Info in Properties panel not updated after moving to other image.<br>
078 ==&gt; 169704 : Crash during RAW import.<br>
079 ==&gt; 166437 : Deleting images in Image Editor not working properly.<br>
<div class="legacy-comments">

  <a id="comment-17726"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/371#comment-17726" class="active">digiKam</a></h3>    <div class="submitted">Submitted by Stefan Waidheim (not verified) on Wed, 2008-09-03 16:52.</div>
    <div class="content">
     <p>Still doesn't handle images without filename extensions. I'd hoped this bug would be gone by now.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17728"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/371#comment-17728" class="active">digiKam 0.10.0-beta3 release for KDE4</a></h3>    <div class="submitted">Submitted by Arnd (not verified) on Thu, 2008-09-04 10:50.</div>
    <div class="content">
     <p>Stefan, have you filed a wish in the bug tracker?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17731"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/371#comment-17731" class="active">Yes, I did. Comments were</a></h3>    <div class="submitted">Submitted by Stefan Waidheim (not verified) on Thu, 2008-09-04 18:07.</div>
    <div class="content">
     <p>Yes, I did. Comments were rather ... mixed. "are you too stupid to rename files?" was one of the nicer ones.<br>
I tried looking into the source but it is too difficult for me.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-17729"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/371#comment-17729" class="active">Final cmake doesn't find kipi plugins</a></h3>    <div class="submitted">Submitted by <a href="http://kubuntulover.blogspot.com" rel="nofollow">Bugsbane</a> (not verified) on Thu, 2008-09-04 12:34.</div>
    <div class="content">
     <p>Hey Giles,</p>
<p>Thanks for another impressive list of fixes / enhancements on the latest beta.</p>
<p>Following all the KDE4 instructions at http://www.digikam.org/drupal/download?q=download/svn ,the final cmake reports:</p>
<p>"--  libkipi library found.......... NO<br>
--<br>
CMake Error at digikam/CMakeLists.txt:150 (message):<br>
   digiKam needs libkipi library &gt;= 0.2.0. You need to install libkipi first"</p>
<p>All the previous steps went fine (including checking out DigiKam and kipi-plugins step). Kipi-plugins seems to exist in ../../graphics and I used all the same folder names suggested. I'm using Kubuntu Hardy Remix with KDE 4.1.1. I looked at the README without seeing anything that was obvious (although I'm fairly new to compiling).</p>
<p>Any ideas?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17732"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/371#comment-17732" class="active">Quote from the</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2008-09-04 18:15.</div>
    <div class="content">
     <p>Quote from the announcement:<br>
<cite>You need last libkdcraw and libkexiv2 from svn trunk to compile digiKam (will be released with KDE 4.2</cite><br>
In other words, it fails because you don't have svn trunk, what will become KDE 4.2 one day installed. Your libraries are from 4.1.1, which is pretty recent, but not bleeding edge.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17734"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/371#comment-17734" class="active">KDE 4.1 branch synchronized...</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2008-09-06 22:03.</div>
    <div class="content">
     <p>Today, i have synchronized KDE 4.1 branch in svn with trunk about libkexiv2 and libkdcraw. So, this will not be a problem to compile digiKam with next KDE 4.1.2 release. </p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-17735"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/371#comment-17735" class="active">Cannot "view" images</a></h3>    <div class="submitted">Submitted by pkolloch@gmx.net (not verified) on Tue, 2008-09-09 22:05.</div>
    <div class="content">
     <p>Thanks a lot for digikam!</p>
<p>This is the first kde4 application I tried to build. After some effort I succeeded, mostly.</p>
<p>Unfortunately, if I click on a thumbnail in album view to enlarge it, it does not work. Same thing with the "view" button in the toolbar. The thumbnails look fine. If I use the "edit" via the toolbar functionality, I can view the images. "Slide" also works.</p>
<p>Do you have any idea what I could check to make this work? Does the view functionality use anything special?</p>
<p>I have the same problem with the trunk version (858769) as with the source tar for this release.</p>
<p>Thanks a lot for your help!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17736"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/371#comment-17736" class="active">It's a splitter size init problem...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2008-09-09 22:12.</div>
    <div class="content">
     <p>There is something weird with the horizontal splitter size on the bottom of preview mode which separate view and thumbbar. Just resize it vertically, and all will become fine.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17739"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/371#comment-17739" class="active">??? Splitter Problem ???</a></h3>    <div class="submitted">Submitted by pkolloch@gmx.net (not verified) on Wed, 2008-09-10 21:53.</div>
    <div class="content">
     <p>Maybe I am a bit slow. I resized everything which looked resizable to me.</p>
<p>The normal album view looks like the following</p>
<p>http://eigenvalue.net/digikam/01_albumview.png</p>
<p>If I click on view or the thumbnail, it looks like </p>
<p>http://eigenvalue.net/digikam/02_clicked_on_thumbnail.png</p>
<p>And if I click on edit and open a new video it shows this window</p>
<p>http://eigenvalue.net/digikam/03_clicked_on_edit.png</p>
<p>Thanks again for any help...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17740"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/371#comment-17740" class="active">preview </a></h3>    <div class="submitted">Submitted by Fri13 (not verified) on Wed, 2008-09-10 22:04.</div>
    <div class="content">
     <p>You have same problem what I had. There is something wierd but not anything serious.<br>
When you click the photo thumbnail on mainview, you said you get this http://eigenvalue.net/digikam/02_clicked_on_thumbnail.png</p>
<p>Now look closely top of the zoom slider. There is three dots. It is not so well showed because it depends of the used theme. But move mouse over that edge and your cursor change to resize arrows. Then just pull up littebit and you get the thumbnail view up and right away your preview is shown. </p>
<p>I think the fix for this would be that thubmnail-bar would have minimum size when it is enabled.<br>
Didn't test does this bug exist when the thumbnail-bar is disabled. If so, then the the code needs littlebit more checkin what is now doing that, because it has worked in history. ;-)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17742"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/371#comment-17742" class="active">Thanks a lot...</a></h3>    <div class="submitted">Submitted by Peter (not verified) on Thu, 2008-09-11 17:23.</div>
    <div class="content">
     <p>even after you said where the dots are I needed a minute to recognize them. But now I got it. Thanks a lot to you and the rest of the team.</p>
<p>[BTW I already fearing the hordes of spam bots since I just realized that I messed up so that my email shows up here...]</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17741"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/371#comment-17741" class="active">This splitter...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2008-09-10 22:19.</div>
    <div class="content">
     <p><a href="http://digikam3rdparty.free.fr/Screenshots/temp/albumguiandhslider.png">This splitter...</a></p>

    </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-17737"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/371#comment-17737" class="active">RAW Import</a></h3>    <div class="submitted">Submitted by Stefan Taferner (not verified) on Wed, 2008-09-10 09:20.</div>
    <div class="content">
     <p>Hello!</p>
<p>Nice work, digikam is coming along great!</p>
<p>One thing I noticed is that when I load a (Canon) RAW image and save it as jpeg,<br>
the "Metadata" side-page is empty for the jpeg. The "Properties" side-page is empty (the one with the tabs EXIF, Vendor Comments, IPTC, XMP.</p>
<p>Feel free to contact me if you need more information.</p>
<p>Kind regards,<br>
Stefan</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17738"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/371#comment-17738" class="active">This is a RAWConverter kipi-plugins bug...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2008-09-10 09:29.</div>
    <div class="content">
     <p>This is a RAWConverter kipi-plugins bug fixed recently in svn. Problem do not appear in digiKam image editor.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
