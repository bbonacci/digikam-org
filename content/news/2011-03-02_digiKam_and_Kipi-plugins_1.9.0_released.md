---
date: "2011-03-02T13:32:00Z"
title: "digiKam and Kipi-plugins 1.9.0 released..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce digiKam and Kipi-plugins 1.9.0 release! digiKam tarball can be downloaded from SourceForge at"
category: "news"
aliases: "/node/581"

---

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce digiKam and Kipi-plugins 1.9.0 release!</p>

<a href="http://www.flickr.com/photos/digikam/5491131187/" title="digiKam-1.9.0 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5298/5491131187_93c1cbe0c3_z.jpg" width="640" height="360" alt="digiKam-1.9.0"></a>

<p>digiKam tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/digikam/files">at this url</a></p>

<p>kipi-plugins tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/kipi/files">at this url</a></p>

<p>See below the list of bugs-fixes coming with this release.</p>

<p>Enjoy digiKam...</p>

<h5>BUGFIXES FROM KDE BUGZILLA:</h5><br>

001 ==&gt; 264181 : png-1.5 compatibility fix.<br>
002 ==&gt; 263697 : digiKam 1.8.0 (svn) crashes on startup most times.<br>
003 ==&gt; 237492 : Grayscale TIFF display broken.<br>
004 ==&gt; 263786 : digikam 1.7.0 crashes on startup.<br>
005 ==&gt; 228531 : Inconsistant use of preview zoom-in function key-strokes.<br>
006 ==&gt; 233612 : The DocumentName tag is not added when importing.<br>
007 ==&gt; 246778 : Disable folder collapsing under certain circumstances.<br>
008 ==&gt; 264363 : Image editor always crashes when triggering Colour Auto-Correction.<br>
009 ==&gt; 234021 : digiKam crash on loading.<br>
010 ==&gt; 264999 : Exif-dates not recognices OK.<br>
011 ==&gt; 264745 : When adding new tag to multiple images, tags not in common are removed from metadata, though still present in database.<br>
012 ==&gt; 173901 : digiKam contains a convience copy of cimg code.<br>
013 ==&gt; 264208 : Underexposure indicator should always work in "pure color" mode.<br>
014 ==&gt; 264769 : digiKam crash on startup.<br>
015 ==&gt; 264830 : Raw importer broken. Not possible to revert changes.<br>
016 ==&gt; 103371 : Only one digikam instance should run on the same image library path.<br>
017 ==&gt; 265445 : digikam-1.8.0 fails to build against kde-4.6.x/gcc-4.6.<br>
018 ==&gt; 264631 : Deleteing last TAG on a picture does actually not remove the tag within the exif metadata.<br>
019 ==&gt; 195648 : Quickfilter by tags: Exclude tags.<br>
020 ==&gt; 265641 : digiKam crash at startup.<br>
021 ==&gt; 264184 : png-1.5 compatibility fix.<br>
022 ==&gt; 264655 : ksnapshot is crashing on "send to".<br>
023 ==&gt; 243908 : Coordinates do not get retrieved.<br>
024 ==&gt; 181319 : Provide a batch tool to add geolocation coordinates to a complete album.<br>
025 ==&gt; 266771 : Unable to get GPS coordinates from map with Geolocation function of digiKam.<br>
<div class="legacy-comments">

  <a id="comment-19841"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/581#comment-19841" class="active">Thanks</a></h3>    <div class="submitted">Submitted by Hermann (not verified) on Wed, 2011-03-02 14:10.</div>
    <div class="content">
     <p>Hi,<br>
first I must say thank you for this great programme. Since this is 1.9 will the next be 2.0? When will that be? I cant wait for Face recognition.</p>
<p>thnaks,<br>
Hermann</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19842"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/581#comment-19842" class="active">release plan is here...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2011-03-02 14:20.</div>
    <div class="content">
     <p>http://www.digikam.org/drupal/about/releaseplan</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19843"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/581#comment-19843" class="active">Windows build</a></h3>    <div class="submitted">Submitted by Ed (not verified) on Wed, 2011-03-02 14:33.</div>
    <div class="content">
     <p>Any idea when next stable windows build be out ?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19846"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/581#comment-19846" class="active">ask to KDE windows team...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2011-03-02 17:25.</div>
    <div class="content">
     <p>ask to KDE windows team for kde windows installer version.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19848"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/581#comment-19848" class="active">So there will be no</a></h3>    <div class="submitted">Submitted by Daniel (not verified) on Thu, 2011-03-03 08:27.</div>
    <div class="content">
     <p>So there will be no WinInstaller from your site, Gilles? I liked it very much, because it just install what is needed for digiKam. Hope the KDE win installer will be updated in the near future. It's a pity that this excellent software is not yet available on windows in it's full functionality.</p>
<p>Anyway, thank you very much for your great work. I really love the software.</p>
<p>Daniel</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19849"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/581#comment-19849" class="active">make NSIS installer take a while.</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2011-03-03 08:45.</div>
    <div class="content">
     <p>In fact i use my KDE windows installation from my laptop to make NSIS installer.</p>
<p>Currently, I updated my KDE windows to last stable, and all is broken...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div><a id="comment-19844"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/581#comment-19844" class="active">Crash on Startup</a></h3>    <div class="submitted">Submitted by DRB (not verified) on Wed, 2011-03-02 14:51.</div>
    <div class="content">
     <p>Thank you! I moved up to 1.8 early because 1.7 was crashing on start up...yeah that didn't help. I am happy to see so many crash on start up bugs fixed. Looking forward to downloading 1.9!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19845"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/581#comment-19845" class="active">Is this the last version</a></h3>    <div class="submitted">Submitted by Burke (not verified) on Wed, 2011-03-02 15:08.</div>
    <div class="content">
     <p>Is this the last version before 2.0?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19847"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/581#comment-19847" class="active">yes...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2011-03-02 17:26.</div>
    <div class="content">
     <p>... It's last 1.x version. We are focused to 2.0 now.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19851"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/581#comment-19851" class="active">Installing digiKam 1.9 In Ubuntu 10.10</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2011-03-05 08:07.</div>
    <div class="content">
     <p>Look here : http://www.mohamedmalik.com/?p=883</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
