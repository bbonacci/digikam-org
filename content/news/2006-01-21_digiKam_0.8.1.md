---
date: "2006-01-21T17:06:00Z"
title: "digiKam 0.8.1"
author: "Tom Albers"
description: "We are pleased to announce digiKam 0.8.1. This version brings raw support, new icons, image rating, printing enhancements and in the meanwhile we also fixed"
category: "news"
aliases: "/node/59"

---

We are pleased to announce digiKam 0.8.1.  This version brings raw support, 
new icons, image rating, printing enhancements and in the meanwhile we also  
fixed some bugs.

The noteworthy features in this release:
<ul>
<li> You are now able to rate your images from 1 start to 5 stars. In the search 
part of digiKam, you can search for images with a certain rating.

</li><li> A new theme has been added, the 'Dessert' theme.

</li><li> 8 bit raw support has been added, you can now view those images in the 
imageviewer. In digiKam version 0.9.0 there will be 16 bit raw support.

</li><li> copy &amp; paste support has been added, you can copy between albums and from 
konqueror to digiKam.

</li><li> dcop functions have been added to open the camera dialog at a certain path 
or start the autodetect function similar to the command line option

</li><li> a command line option has been added to open the camera dialog at a certain 
path. This feature and the previous are preparations for full media kio 
support.

</li><li> showfoto can now open a directory with images in there

</li><li> automatically adjust orientation of printing depending on the image

</li><li> new icons!

</li><li> kipi plugins now work on search or date folders
</li></ul>

Some of the bugs we squashed since 0.8.0:

<ul>

<li> the configure option --enable-nfs-hack did not work, now it does work again.

</li><li> the configure option --enable-final did not work, now it does work again.

</li><li> showfoto's open dialog now opens in the location you used the last time.

</li><li> a bunch of memory leaks are now fixed.

</li><li> printing did not really work. Now it does work again.

</li></ul>
<div class="legacy-comments">

</div>