---
date: "2018-01-14T00:00:00"
title: "digiKam 5.8.0 is released"
author: "digiKam Team"
description: "Dear digiKam fans and users, following the release 5.7.0 published in September 2017, the digiKam team is 
proud to announce the new stable release 5.8.0."
category: "news"
---

![Lac Mirroir, Le Melezet, Ceillac, French Alps](/img/news/2018-01-14-5.8.0_release_announcement/2018-01-14_intro.png)

[Following the release of 5.7.0](https://www.digikam.org/news/2017-09-12-5.7.0-release-announcement/) published in September 2017, 
the digiKam team is proud to announce the new release 5.8.0 of the digiKam Software Collection. 
In this version a lot of work has happened behind the scenes and in fixing bugs as usual, 
which does not mean there are no enhancements: a new tool to export collections to UPNP/DLNA compatible devices has been introduced.

### Bugs Triage

Following the large update of [bug categories](https://bugs.kde.org/describecomponents.cgi?product=digikam) processed before the 
previous release, we have been able to reassign entries to the right components. With the help of pre-release bundles computed each day, 
we have been able to provide a digiKam test version along with the fixes applied to close reports in bugzilla. 
The result with this release is a set of more than 200 entries officially closed.

### Application Bundle Improvements

![MacOS and Windows digiKam Installers in Action](/img/news/2018-01-14-5.8.0_release_announcement/2018-01-14_bundle_installers.png)

For each platform supported by the digiKam project, we propose a bundle package ready to use, which embed all the needs 
to have a suitable application. digiKam project confirm the support of The [AppImage](https://en.wikipedia.org/wiki/AppImage) format 
as the best Linux bundle, which has been updated to last SDK available in build virtual machine based on Linux Centos. 
This one now permits to run the bundle under [Firejail](https://firejail.wordpress.com/documentation-2/appimage-support/) sandboxing 
technology for a better security level..

![Showfoto Running Under Windows](/img/news/2018-01-14-5.8.0_release_announcement/2018-01-14_showfoto_windows.png)

The Windows installer is now built with the latest [NSIS](https://en.wikipedia.org/wiki/Nullsoft_Scriptable_Install_System) 
version to be compatible with Windows 10. The installer code has also been cleaned to remove obsolete packaging 
plugins and to prevent false virus detections while installing. Note that all digiKam Windows bundles are fully 
built under Linux with the [MXE](http://mxe.cc/) cross-compiler and there is no risk to inject Windows viruses in the application.

![Showfoto Running Under MacOS](/img/news/2018-01-14-5.8.0_release_announcement/2018-01-14_showfoto_macos.png)

All bundles, including the MacOS package build with the latest [Packages](http://s.sudre.free.fr/Software/Packages/about.html), 
now include more optional features available on digiKam and also Showfoto, 
as some new low level libraries have been ported to pure Qt5, such as digital scanner support and events file support 
with export to Calendar tool.

### Mysql Support Improvements

![Mysql Config Panel and Migration Tool](/img/news/2018-01-14-5.8.0_release_announcement/2018-01-14_mysql_and_migration.png)

A lot of work has been done around the Mysql support with the help of end users testing step by step progress and the 
relevant fixes applied daily. For these hacking stages, the pre-release bundles have really helped a lot, 
by providing a ready to use beta version of digiKam including all debug statements necessary to investigate 
the dysfunctions, especially for the database schema update performed on older collections hosted on a 
[Mysql](https://www.mysql.com/) or [Mariadb](https://mariadb.org/) server. 
The migration tools have also received many fixes, especially to move from [Sqlite](https://sqlite.org/about.html) databases, 
when collections grow substantially and over time become more and more slow. Now, the migration tool can import and export 
better from/to Mysql or Sqlite without data-loss while databases are being converted.

### Export to UPNP/DLNA devices

![New Tool to Share Collections With DLNA Devices](/img/news/2018-01-14-5.8.0_release_announcement/2018-01-14_share_with_dlna.png)

In September 2017, the digiKam team has been invited to take part in the 
[Randa Meetings](https://dot.kde.org/2017/12/11/randa-meetings-2017-accessibility-for-everyone). 
In this tiny town in the Swiss alps a lot of KDE developers work on improving their software. 
We have focused the reunion on including the new media server dedicated to sharing collection contents 
on local networks with compatible [DLNA](https://en.wikipedia.org/wiki/Digital_Living_Network_Alliance) 
devices or applications, such as tablets, cellulars, TV, etc. 
This tool is now available in all digiKam views through the main Tools menu and also in Showfoto.

![Showofoto DLNA Server Visible into VLC](/img/news/2018-01-14-5.8.0_release_announcement/2018-01-14_showfoto_dlna.png)

There are plenty of DLNA applications available in digital stores like iTunes or GooglePlay. 
As the DLNA and [UPNP](https://en.wikipedia.org/wiki/Universal_Plug_and_Play) protocols are standardized, 
we don’t need to develop dedicated applications for each client device that wants to access the digiKam/Showfoto contents.

### Port DropBox Export Tools to OAuth2

![digiKam DropBox Export Tool Running Under Windows](/img/news/2018-01-14-5.8.0_release_announcement/2018-01-14_dropbox_windows.png)

Concerning digiKam export tools to remote Internet services, the port to [OAuth](https://en.wikipedia.org/wiki/OAuth) version 2 
is advancing steadily with more work planned in the course of the year. 
After porting the [Flickr](https://www.flickr.com/) and [Imgur](https://imgur.com/) export tools with the 5.7.0 release, 
the [DropBox](https://www.dropbox.com/) export tool is now ported to the new authentication API, 
so after a short time of dysfunction the tool is back and ready for use in production. In the future other tools, 
such as Facebook and GPhoto export, will be ported to OAuth version 2. This will probably be a subject for the students 
working with us for the upcoming [Google Summer of Code 2018](https://developers.google.com/open-source/gsoc/timeline).

### Final Words

As always, but especially noteworthy this time, you can look at 
[the list of 231 resolved issues](https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&limit=0&o1=equals&order=bug_id&product=digikam&product=kipiplugins&query_format=advanced&v1=5.8.0) 
for detailed information.

digiKam 5.8.0 Software collection source code tarball, Linux 32/64 bits AppImage bundles, MacOS package, 
and Windows 32/64 bits installers can be downloaded from [this repository](http://download.kde.org/stable/digikam/).

Happy new year 2018 and happy digiKaming!
