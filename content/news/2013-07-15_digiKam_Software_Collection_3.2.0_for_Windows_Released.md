---
date: "2013-07-15T22:27:00Z"
title: "digiKam Software Collection 3.2.0 for Windows Released"
author: "Ananta Palani"
description: "I am pleased to announce the release of digiKam 3.2.0 for Windows built against KDE 4.10.2 that can be downloaded from the KDE repository. The"
category: "news"
aliases: "/node/700"

---

<p><img src="http://lh4.googleusercontent.com/-d4fIISBVVHY/UeRqhyD8EAI/AAAAAAAABdg/FvWl_BKmnDw/w950-h535-no/digikamWindows3.2.0.png" width="640" height="361"></p>
<p>I am pleased to announce the release of digiKam 3.2.0 for Windows built against KDE 4.10.2 that can be downloaded from the <a href="http://download.kde.org/stable/digikam/digiKam-installer-3.2.0-win32.exe">KDE repository</a>. The build platform is the same as for 3.1.0 and 3.0.0. If you encounter any problems please file a very detailed <a href="https://bugs.kde.org/enter_bug.cgi?product=digikam&amp;op_sys=MS%20Windows&amp;rep_platform=MS%20Windows&amp;version=3.2.0&amp;format=guided">bug report</a>.</p>
<p>Bug fixes include those completed for the digiKam 3.2.0 source release as well as:</p>
<p>1. Nothing happening when trying to edit geolocation bookmarks<br>
2. Crash/hang when using in-painting and restoration tools due to thread leak</p>
<p>Known Windows problems</p>
<p>1. After rotating an image using the rotate icons in the thumbnail view, the user may have to press F5 to see the change<br>
2. Bugs listed on <a href="https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&amp;bug_status=CONFIRMED&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED&amp;list_id=676982&amp;op_sys=MS%20Windows&amp;product=digikam&amp;product=digikamimageplugins&amp;product=showfoto&amp;query_format=advanced&amp;query_based_on=&amp;columnlist=bug_severity%2Cpriority%2Copendate%2Cbug_status%2Cresolution%2Ccomponent%2Cop_sys%2Crep_platform%2Cshort_desc">KDE Bugzilla</a></p>
<p>Enjoy!</p>

<div class="legacy-comments">

  <a id="comment-20585"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/700#comment-20585" class="active">thanks!</a></h3>    <div class="submitted">Submitted by Fabio (not verified) on Tue, 2013-07-16 17:23.</div>
    <div class="content">
     <p>thanks!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20586"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/700#comment-20586" class="active">Tanks.</a></h3>    <div class="submitted">Submitted by wQuick (not verified) on Tue, 2013-07-16 19:59.</div>
    <div class="content">
     <p>Tanks.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20587"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/700#comment-20587" class="active">New Panasonic DMC-LF1</a></h3>    <div class="submitted">Submitted by photoken (not verified) on Thu, 2013-07-18 03:50.</div>
    <div class="content">
     <p>I've just received my LF1, which is a newly released camera.  It is not recognized by DigiKam (3.2.0 on Win7 x64) as a camera, which is not surprising since the camera is so new.  I'm willing to deal with Panasonic to get the necessary information that DigiKam needs, if you can tell me what information to ask them for.</p>
<p>Ken</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20588"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/700#comment-20588" class="active">Raw support ?</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2013-07-18 09:03.</div>
    <div class="content">
     <p>If you talk about RAW support, you must take contact with libraw (http://www.libraw.org) team, and provide raw file samples to hack demosaising support</p>
<p>If you talk about import support through PTP protocol, you must take contact with Gphoto2 (http://www.gphoto2.org).</p>
<p>Both library are used in-deep by digiKam.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20589"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/700#comment-20589" class="active">Windows port still crashes</a></h3>    <div class="submitted">Submitted by Ken (not verified) on Sat, 2013-07-20 04:13.</div>
    <div class="content">
     <p>Windows port still crashes within minutes :-(</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20590"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/700#comment-20590" class="active">After it crashes it keeps</a></h3>    <div class="submitted">Submitted by Ken (not verified) on Sat, 2013-07-20 05:41.</div>
    <div class="content">
     <p>After it crashes it keeps folders locked and leaves about a dozen processes behind that have to be killed manually. I also suspect it may have corrupted a bunch of my NEF files, but can't prove that and I'm sure as heck never going to launch this again to find out. Completely unusable.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20593"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/700#comment-20593" class="active">More info would be helpful</a></h3>    <div class="submitted">Submitted by Ananta Palani on Mon, 2013-07-22 22:45.</div>
    <div class="content">
     <p>When giving feedback it would be a lot more helpful to know what digikam was doing and/or what you were doing with digikam when it crashed. I'm sorry if you feel digikam corrupted your NEF files. I would be surprised if this happened as digikam does not write to files unless told to do so explicitly. If there is any way you could try digikam on a single (copy) of one of your corrupt NEF files and see if digikam crashes (or upload the NEF file somewhere and e-mail me) so I can fix the crash, that would be great.</p>
<p>The folder locks are due to monitoring folders for changes, they would have nothing to do with corrupting files.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20591"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/700#comment-20591" class="active">It's strange... For me it's</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2013-07-20 13:41.</div>
    <div class="content">
     <p>It's strange... For me it's the first version which may be used for workflow.<br>
(Windows 8 Pro 64)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20594"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/700#comment-20594" class="active">macos</a></h3>    <div class="submitted">Submitted by sarge (not verified) on Tue, 2013-07-23 12:56.</div>
    <div class="content">
     <p>why not a version for mac os</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20598"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/700#comment-20598" class="active">No dedicated MacOS Packager</a></h3>    <div class="submitted">Submitted by Ananta Palani on Thu, 2013-08-01 10:27.</div>
    <div class="content">
     <p>At the moment we don't have a dedicated MacOS packager, so releases are infrequent. I don't have a Mac and don't know how to compile for it, so I'm sorry I can't do it. If you, or someone you know, would like to start you are more than welcome to!</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20595"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/700#comment-20595" class="active">Useful functionality removed 3.2</a></h3>    <div class="submitted">Submitted by Hasan (not verified) on Fri, 2013-07-26 07:12.</div>
    <div class="content">
     <p>Image editor used to load all the images in the folder, but now it only loads the one chosen when opening the editor window. I usually edit images in batches for my project so this functionality is essential for me and removing it is killing my workflow. I am reinstalling 3.1 for now :(</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20599"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/700#comment-20599" class="active">Workaround</a></h3>    <div class="submitted">Submitted by Ananta Palani on Thu, 2013-08-01 11:16.</div>
    <div class="content">
     <p>I think the 'functionality' you describe was actually a bug. Instead of loading the selected images into the image editor/light table it loaded all of them in the folder. Now, only the selected images are loaded.</p>
<p>If you want the old functionality, just type Ctrl-A to select all images in a folder and then proceed as before!</p>
<p>If you still want the old functionality, perhaps you could create a <a href="https://bugs.kde.org/enter_bug.cgi?product=digikam&amp;op_sys=MS%20Windows&amp;rep_platform=MS%20Windows&amp;version=3.2.0&amp;format=guided">feature request</a> that a check-box be added to load all by default or something. If you create a feature request, describe the behavior in 3.1 and say it was removed in 3.2 but that you'd like the ability to choose the old method.</p>
<p>I'm sorry the functionality is not what you were used to, but from a users perspective the new style makes more sense.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20600"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/700#comment-20600" class="active">That totally worked! Thanks</a></h3>    <div class="submitted">Submitted by Hasan (not verified) on Mon, 2013-08-05 03:15.</div>
    <div class="content">
     <p>That totally worked! Thanks so much! :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20596"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/700#comment-20596" class="active">I am missing useful tools</a></h3>    <div class="submitted">Submitted by Stefan (not verified) on Sun, 2013-07-28 21:04.</div>
    <div class="content">
     <p>I am missing useful tools like geotagging. Will the be a Windows version with full functionality?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20597"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/700#comment-20597" class="active">Geotagging is present</a></h3>    <div class="submitted">Submitted by Ananta Palani on Thu, 2013-08-01 10:26.</div>
    <div class="content">
     <p>The Windows version has almost full functionality compared to the *nix version and definitely has geotagging support. Click on an image or images and go to the 'Image' menu and click 'Geo-location'. You can view an images geotags by clicking the world icon on the right of the album/thumbnail viewer, and you can search by geotag by clicking the world icon on the left. Is this not geotagging support?</p>
<p>What else do you think is missing?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20615"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/700#comment-20615" class="active">Ok. Haven't worked with</a></h3>    <div class="submitted">Submitted by Stefan (not verified) on Tue, 2013-08-20 20:34.</div>
    <div class="content">
     <p>Ok. Haven't worked with Digikam for a while as earlier versions were always not stable for me. But found now al functions I was looking for.<br>
Thank you.<br>
Great work.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20605"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/700#comment-20605" class="active">local contrast stopps after 10%</a></h3>    <div class="submitted">Submitted by bengomin (not verified) on Tue, 2013-08-06 13:08.</div>
    <div class="content">
     <p>Thank you very much!!!<br>
one bug that i saw so far is that the calculation of the local contrast stopps (freezes) after 10% (WinXP 32bit).<br>
The task manager of Windows XP says that the digikam.exe is not demanding any CPU after the 10% are reached.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>