---
date: "2011-07-05T10:06:00Z"
title: "digiKam Tricks 3.9 Released"
author: "Dmitri Popov"
description: "This release includes the following new material: Import Photos in digiKam Process Photos with digiKam’s Batch Queue Manager and a Bash Script Continue to read"
category: "news"
aliases: "/node/611"

---

<p>This release includes the following new material:</p>
<ul>
<li>Import Photos in&nbsp;digiKam</li>
<li>Process Photos with digiKam’s Batch Queue Manager and a Bash&nbsp;Script</li>
</ul>
<p><a href="http://scribblesandsnaps.wordpress.com/digikam-tricks-book/">Continue to read</a></p>

<div class="legacy-comments">

</div>