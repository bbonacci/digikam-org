---
date: "2015-03-23T09:35:00Z"
title: "digiKam Recipes 4.5.1 Released"
author: "Dmitri Popov"
description: "A freshly-baked release of digiKam Recipes is ready for your reading pleasure. The new version features the Export and Share Photos with digiKam recipe which"
category: "news"
aliases: "/node/733"

---

<p>A freshly-baked release of&nbsp;<a href="http://scribblesandsnaps.com/digikam-recipes/">digiKam Recipes</a>&nbsp;is ready for your reading pleasure. The new version features the&nbsp;<em>Export and Share Photos with digiKam</em> recipe which offers&nbsp;a comprehensive overview of digiKam's sharing and exporting capabilities.</p>
<p><a href="http://scribblesandsnaps.com/digikam-recipes/"><img src="https://scribblesandsnaps.files.wordpress.com/2015/01/digikamrecipes-4-3-1.png" alt="digikamrecipes-4.3.1" width="375" height="500"></a></p>
<p>The new&nbsp;<em>Extract and Examine Metadata from digiKam's Database</em> recipe explains how to pull and analyze photographic metadata stored in the <em>digikam4.db</em> database. Finally, the <em>Rescale Photos with the Liquid Rescale Tool</em> recipe explains how to use the Liquid Rescale tool for intelligent rescaling. As always, the new release includes minor updates, fixes and tweaks.</p>
<p><a href="http://scribblesandsnaps.com/2015/03/23/digikam-recipes-4-5-1-released/">Continue reading</a></p>

<div class="legacy-comments">

</div>