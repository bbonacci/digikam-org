---
date: "2009-07-24T11:08:00Z"
title: "Kipi-plugins 0.5.0 for KDE4 released"
author: "digiKam"
description: "Dear all digiKam fans and users! Kipi-plugins 0.5.0 maintenance release for KDE4 is out. kipi-plugins tarball can be downloaded from SourceForge at this url Kipi-plugins"
category: "news"
aliases: "/node/469"

---

<p>Dear all digiKam fans and users!</p>

<p>Kipi-plugins 0.5.0 maintenance release for KDE4 is out.</p>

<a href="http://www.flickr.com/photos/digikam/3751243371/" title="kipi-plugins0.5.0 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2439/3751243371_dc72109725.jpg" width="500" height="400" alt="kipi-plugins0.5.0"></a>

<p>kipi-plugins tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/kipi/files">at this url</a></p>

<p>Kipi-plugins will be also available for Windows. Precompiled packages can be donwloaded with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<p>See below the list of new features and bugs-fix coming with this release:</p>

<b>PrintWizard</b>  : Added support for 2-1/3" x 3-1/3" pictures.<br>
<b>DNGConverter</b> : Update internal XMP sdk from Adobe to version 4.4.<br>
<b>PrintImages</b>  : Added multiple pages printing.<br>
<b>PrintWizard</b>  : Page layout into template.xml file, custom templates can be added now.<br><br>

001 ==&gt; PrintWizard  : 162085 : digiKam freezes while printing.<br>
002 ==&gt; HtmlExport   : 196890 : "Next" button on first wizard page is initially disabled.<br>
003 ==&gt; FlickrExport : 180055 : Can't create new photoset: Invalid primary photo id (n).<br>
004 ==&gt; FlickrExport : 192541 : Name based Flickr set selection is ambiguous [patch].<br>
005 ==&gt; TimeAdjust   : 180244 : Timeadjust is not building on mac os x leopard.<br>
006 ==&gt; SendImages   : 169952 : Pictures on nfs-drive crashes digiKam when i send picture as mail.<br>
007 ==&gt; BatchProcess : 173664 : Menu: Can't resize image(s).<br>
008 ==&gt; DngConverter : 199129 : xmp_sdk/common/LargeFileAccess.cpp: size of array ‘check_off_t_size’ is negative.<br>
009 ==&gt; picasaExport : 188850 : Does not upload to picassaweb --- just hangs for hours.<br>
010 ==&gt; ScanImages   : 147366 : SIGABORT on Second Scan from HP 5200 scanner.<br>
011 ==&gt; SendImages   : 190208 : Change image properties options should be grayed out when the check box is unchecked.<br>
012 ==&gt; SendImages   : 146487 : kipi-plugins export to email fails when email too large.<br>
013 ==&gt; SendImages   : 189984 : Impossible to email images if the path contains characters such as éèà.<br>
014 ==&gt; RawConverter : 161337 : Image rotation of converted RAW photos (portrait format) broken.<br>
015 ==&gt; Calendar     : 196888 : Create Calender: Year formatted as 2,009 on last page of wizard.<br>
016 ==&gt; Calendar     : 111697 : I wish for the option to change the first day of week (sun / mon).<br>
017 ==&gt; GPSSync      : 187517 : GPSSync when entering no map.<br>
018 ==&gt; BatchProcess : 200483 : digiKam crashes after batch image-color-improvement on single picture.<br>
019 ==&gt; AcquireImages: 200558 : Crash when saving scanned image to .tiff.<br>
020 ==&gt; BatchProcess : 200217 : Crash after batch recompress.<br>
021 ==&gt; BatchProcess : 200722 : Crashes after successfull batch and closing resize dialog.<br>
022 ==&gt; AdvSlideShow : 200647 : Not all main settings are not restored correctly.<br>
023 ==&gt; BatchProcess : 168243 : Delete photo after not found imagemagick.<br>
024 ==&gt; BatchProcess : 149488 : After batch renaming pictures, the pictures appears as unkown images, and can't be removed from album nor can be viewed or edit.<br>
025 ==&gt; SendImages   : 146828 : Sendimages opera command line.<br>
026 ==&gt; BatchProcess : 186648 : Batch rename doesn't refresh view.<br>
027 ==&gt; PrintWizard  : 188426 : Add support for different size  printwizard templates.<br>
028 ==&gt; MetadataEdit : 200960 : Image / Metadata / Remove Captions does not remove caption from Xmp.tiff.ImageDescription.<br>
029 ==&gt; AdvSlideShow : 155179 : Slideshow ignores Rating Filter.<br>
030 ==&gt; BatchProcess : 201257 : digiKam crashes when batch resizing images.<br>

<div class="legacy-comments">

  <a id="comment-18693"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/469#comment-18693" class="active">Create MPEG slideshow</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2009-08-01 09:52.</div>
    <div class="content">
     <p>Are there any plans to revive the "create mpeg slideshow" plugin in the KDE 4 set ?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18701"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/469#comment-18701" class="active">Same question here. I am</a></h3>    <div class="submitted">Submitted by Rincewind (not verified) on Mon, 2009-08-17 17:27.</div>
    <div class="content">
     <p>Same question here. I am missing this feature too :-(</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18703"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/469#comment-18703" class="active">Mpeg encoder plugin...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2009-08-18 08:08.</div>
    <div class="content">
     <p>Old MpegEncoder plugin is not ported to KDE4. This one use a bash script in background to play with ImageMagick + MJPEGTools. This way is wrong and not really portable.</p>

<p>With Angello, We will review all candidate solutions in next coding event planed in October at Genoa to replace this plugin with a better program, as <a href="http://www.kde-apps.org/content/show.php/SMILE?content=83276">SMILE</a> for example.
         </p></div>
    <div class="links">» </div>
  </div>
<a id="comment-18707"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/469#comment-18707" class="active">Same for me too - I really</a></h3>    <div class="submitted">Submitted by The Liquidator (not verified) on Mon, 2009-08-24 14:05.</div>
    <div class="content">
     <p>Same for me too - I really miss this feature</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18742"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/469#comment-18742" class="active">I really thank to one who</a></h3>    <div class="submitted">Submitted by davidd (not verified) on Sun, 2009-10-18 21:05.</div>
    <div class="content">
     <p>I really thank to one who wrote this article. I have always been reading and writing texts like this in blogs. Also, I, as a daily writer, present my respects to everyone. I just watched videos like this in <a href="http://www.programsitesi.org" title="video izle">video izle</a>. I research in all areas.</p>
<p>I think people must first research before writing...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18699"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/469#comment-18699" class="active">Choosing install dir</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2009-08-12 12:20.</div>
    <div class="content">
     <p>How can I define in which directory the files will be installed when I issue the make install command? By default it goes to /usr/local/, but I need it in /usr/. Thanks</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18700"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/469#comment-18700" class="active">README</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2009-08-12 12:47.</div>
    <div class="content">
     <p>Look in README file, all is explained in details...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18704"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/469#comment-18704" class="active">Gallery export</a></h3>    <div class="submitted">Submitted by Samuel (not verified) on Wed, 2009-08-19 21:22.</div>
    <div class="content">
     <p>With the gallery export plugin and a gallery 2 installation , even though I check "Comment sets Title" and/or "comment sets caption" in the plugin, these fields are set with the filename instead.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18709"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/469#comment-18709" class="active">Will you try to improve the</a></h3>    <div class="submitted">Submitted by JustMe (not verified) on Thu, 2009-08-27 19:38.</div>
    <div class="content">
     <p>Will you try to improve the interface soon? Right now it's too messy.. Keep up the good work anyway.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
