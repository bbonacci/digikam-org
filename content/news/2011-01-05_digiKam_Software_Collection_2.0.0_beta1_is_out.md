---
date: "2011-01-05T09:31:00Z"
title: "digiKam Software Collection 2.0.0 beta1 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! Just in time for this new year 2011, digiKam team is proud to announce the first digiKam Software Collection"
category: "news"
aliases: "/node/561"

---

<p>Dear all digiKam fans and users!</p>

<p>Just in time for this new year 2011, digiKam team is proud to announce the first digiKam Software Collection 2.0.0 beta release!</p>

<a href="http://www.flickr.com/photos/digikam/5323487542/" title="digikam2.0.0-19 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5008/5323487542_fdea5ef240.jpg" width="500" height="400" alt="digikam2.0.0-19"></a>

<p>With 2.x, digiKam include now all shared libraries maintained by digiKam team plus kipi-plugins source code, as a software collection. The goal is to simplify packaging to reduce external dependencies.</p>

<a href="http://www.flickr.com/photos/digikam/5319746295/" title="digikam2.0.0-05 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5130/5319746295_df7bba3672_z.jpg" width="640" height="256" alt="digikam2.0.0-05"></a>

<p>digiKam software collection 2.0.0 include all Google Summer of Code 2010 projects, as <b>XMP sidecar support</b>, <b>Face Recognition</b>, <b>Image Versioning</b>, and <b>Reverse Geocoding</b>. All this works have been processed during <a href="http://techbase.kde.org/Projects/Digikam/CodingSprint2010">Coding Sprint 2010</a>. You can find a resume of this event <a href="http://www.digikam.org/drupal/node/538">at this page</a>.</p>

<a href="http://www.flickr.com/photos/digikam/5319746291/" title="digikam2.0.0-04 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5249/5319746291_60bcdc0b28_z.jpg" width="640" height="256" alt="digikam2.0.0-04"></a>

<p>This beta release is not stable. Do not use yet in production. Please report all bugs to KDE bugzilla following indications <a href="http://www.digikam.org/drupal/support">from this page</a>. The release plan can be seen <a href="http://www.digikam.org/drupal/about/releaseplan">at this url</a>.</p>

<p>digiKam software collection tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/digikam/files">at this url</a></p>

<p>We hope to all digiKam users an happy new year 2011...</p>
<div class="legacy-comments">

  <a id="comment-19726"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19726" class="active">Thanks a lot, this looks like</a></h3>    <div class="submitted">Submitted by gandalf (not verified) on Wed, 2011-01-05 10:23.</div>
    <div class="content">
     <p>Thanks a lot, this looks like 2.0 is going to be an awesome digikam with all the new features! Since this version is probably under heavy development, is there a special svn or git repository available? As a first impression, versioning looks great, and I also love the much improved map view. On the other hand, I have some questions how to use face recognition (clicking "add new face tag" seems to have no effect) or reverse geo-coding (where do I see the tags digikam would assign to my pictures) - will there be extended documentation about these new features somewhere?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19727"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19727" class="active">code repository...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2011-01-05 10:32.</div>
    <div class="content">
     <p>digiKam Software Collection 2.0.0 source code is not yet migrated into KDE Git repository. This will be done in near future by Marcel.<br>
All code still maintened at KDE subversion, into a dedicated branch:</p>
<p>http://websvn.kde.org/branches/extragear/graphics/digikam</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19744"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19744" class="active">Hi,
If you want to try</a></h3>    <div class="submitted">Submitted by Gabriel (not verified) on Thu, 2011-01-06 08:05.</div>
    <div class="content">
     <p>Hi,</p>
<p>If you want to try Reverse Geocoding,here [0] you have a small tutorial. Please tell me if this is not enough, to make it more complete.</p>
<p>[0] http://community.kde.org/Digikam/GSoC2010/ReverseGeocoding</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19728"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19728" class="active">Is there some linux package</a></h3>    <div class="submitted">Submitted by Awikatchikaen (not verified) on Wed, 2011-01-05 11:59.</div>
    <div class="content">
     <p>Is there some linux package scheduled for the betas ? Or someone able to do it ?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19732"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19732" class="active">I don't know which</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2011-01-05 14:45.</div>
    <div class="content">
     <p>I don't know which distribution you use. Some distributions offer unstable repositories that contain beta software for testing or dedicated repositories for certain programs so that users can get the newest features. You should ask your distribution, though Google might be help enough. Alternatively, of course, you can build from sources.</p>
<p>But beware: this is beta and may destroy data.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19729"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19729" class="active">Very exciting news -- I am</a></h3>    <div class="submitted">Submitted by DRB (not verified) on Wed, 2011-01-05 14:14.</div>
    <div class="content">
     <p>Very exciting news -- I am looking forward to face recognition. I hope the software collection approach will help with ease of installation for Windows users as well -- I have been telling everyone about digiKam for years but very few people are ready to switch to Linux for one program.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19730"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19730" class="active">Awesome, but</a></h3>    <div class="submitted">Submitted by tdg (not verified) on Wed, 2011-01-05 14:25.</div>
    <div class="content">
     <p>Hey this is really awesome, but there are some problems:<br>
Face recognition doesn't work? There are buttons and the face-scanner but nothing apart from that nothing shows.</p>
<p>Right now to get the most recent version<br>
<code><br>
svn co svn://anonsvn.kde.org/home/kde/branches/extragear/graphics/digikam<br>
cd digikam<br>
mkdir build<br>
cd build<br>
cmake -DCMAKE_BUILD_TYPE=debugfull -DCMAKE_INSTALL_PREFIX=`kde4-config --prefix` ..<br>
make<br>
sudo make install<br>
</code><br>
but to compile digikam, before I had to build (the same way)<br>
<code><br>
svn co svn://anonsvn.kde.org/home/kde/trunk/KDE/kdegraphics/libs/libkexiv2<br>
</code></p>
<p>how do you get Face detection/recognition to work?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19731"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19731" class="active">look this bugzilla entry...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2011-01-05 14:28.</div>
    <div class="content">
     <p>https://bugs.kde.org/show_bug.cgi?id=262074</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19735"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19735" class="active">So I installed the</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2011-01-05 15:48.</div>
    <div class="content">
     <p>So I installed the opencv-files, they are present in /usr/share/opencv - and still no success - anything else needing to be done?<br>
nb: installed from opensuse factory-tested - might be the problem, maybe It's a packaging bug?</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19738"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19738" class="active">facedetection don't work</a></h3>    <div class="submitted">Submitted by vince06fr (not verified) on Wed, 2011-01-05 19:36.</div>
    <div class="content">
     <p>I also have the same worries, the face-detection does not work, the scanner seems to work, but no face is detected,<br>
 th buttons are present but it is not possible to add a "face-tag ".<br>
Is this a bug or have I forgotten to install something?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19739"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19739" class="active">same here, the scanner works</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2011-01-05 19:52.</div>
    <div class="content">
     <p>same here, the scanner works and I can manually correct its results afterwards, but the "add face tag" icon doesn't do anything</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19740"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19740" class="active">look in this new file...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2011-01-05 20:08.</div>
    <div class="content">
     <p>https://bugs.kde.org/show_bug.cgi?id=262212</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-19733"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19733" class="active">Embedded code copies</a></h3>    <div class="submitted">Submitted by ScottK (not verified) on Wed, 2011-01-05 14:56.</div>
    <div class="content">
     <p>"With 2.x, digiKam include now all shared libraries maintained by digiKam team plus kipi-plugins source code, as a software collection. The goal is to simplify packaging to reduce external dependencies."</p>
<p>Sigh.  This does the opposite of simplify things for packagers.  Shipping multiple copies of embedded libraries bloats distros and complicates maintenance and support.  If it's your own embedded copy, it's not really a shared library is it?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19734"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19734" class="active">yes, it is...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2011-01-05 15:04.</div>
    <div class="content">
     <p>It's shared libs with API/ABI increased. There is no conflict with kdegraphics/libs copy.</p>
<p>The advantage is to be able to manage last code in SC due to release plan completely different between KDE and digiKam.</p>
<p>In fact, when code is judged stable and suitable it's copied in Kdegraphics/libs has well, and SC ABI/API is increased again.</p>
<p>You have always the choose to use external libs instead...</p>
<p>This problem have been indeep reported by digiKam users in the past.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19743"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19743" class="active">Well, I second what ScottK wrote!</a></h3>    <div class="submitted">Submitted by <a href="http://www.tigen.org/kevin.kofler/" rel="nofollow">Kevin Kofler</a> (not verified) on Thu, 2011-01-06 00:32.</div>
    <div class="content">
     <p>Sorry, but for us packagers, embedded libraries are just junk we need to rm -rf when building the software. They're not helpful to us.</p>
<p>When there are ABI compatibility issues, well, either we keep an older Digikam which is compatible with the shared libraries we ship, or we backport the current shared libraries to our kdegraphics package. (We've done the latter on more than one occasion in Fedora.)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19751"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19751" class="active">So when packagers from</a></h3>    <div class="submitted">Submitted by ScottK (not verified) on Fri, 2011-01-07 06:47.</div>
    <div class="content">
     <p>So when packagers from multiple distributions tell you this will make our work harder, will you believe we know about what we do or that you know better about packaging?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19754"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19754" class="active">The subject still open...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2011-01-07 10:35.</div>
    <div class="content">
     <p>Please look this file :</p>
<p>https://bugs.kde.org/show_bug.cgi?id=252964</p>
<p>To improve 2.0.0 packaging behavior please comment to this file or create a new one. We are always open to discussion.</p>
<p>Also, Git migration still in the way and must be done before 2.0.0 final release planed around May. This will have an effect on packaging provided by digiKam team (tarball)</p>
<p>For the moment, during whole beta stage and until Git migration is not yet complete, we will use the current packaging schema.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19759"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19759" class="active">I'm not sure what I should</a></h3>    <div class="submitted">Submitted by ScottK (not verified) on Fri, 2011-01-07 17:20.</div>
    <div class="content">
     <p>I'm not sure what I should comment?</p>
<p>There are dependencies on the kdegraphics libraries in KDE SC, so separating them from KDE SC is unlikely and certainly not going to happen before your next release.</p>
<p>Bundled code copies cause problems for distributions.  It's extremely unlikely we would ship a Digikam version that required a newer kdegraphics than was in the version of the KDE SC we were shipping.  We would most likely wait to ship it until the next release and then we'd have to configure Digikam to use the system libraries.  This isn't just something that's up to the Kubuntu developers.  In our distro context we'd need buy in from the security team as it increases the code base they have to support and we'd need to investigate if we had room for the additional libraries (even our DVD image is reasonably full).</p>
<p>Whatever you think, it's not easier. Period.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19764"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19764" class="active">Sigh...</a></h3>    <div class="submitted">Submitted by Andreas K. Hüttel (not verified) on Sun, 2011-01-09 21:37.</div>
    <div class="content">
     <p>Well, speaking as the current Gentoo maintainer of Digikam, I can also confirm that every single bundled library makes things just messier and more work... We also have the clear policy of unbundling.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19766"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19766" class="active">why copying in other</a></h3>    <div class="submitted">Submitted by Thomas (not verified) on Thu, 2011-01-13 08:01.</div>
    <div class="content">
     <p>why copying in other packages? It's, as ScottK and others allready said, exactly the opposite of making things easier. If nothing uses the now included packages, than its fine. But there are dependencies to outside of digikam. AFAIK does kdeplasma-addons use the kipiplugins. If so, that libraries should not be included somewhere else. This is the same (I'll call it) mistake as KDE4 has made - it is a flood of forks which will get unmaintainable long term.<br>
Just my 2ct.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19807"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19807" class="active">Re: why copying in other</a></h3>    <div class="submitted">Submitted by <a href="http://www.csamuel.org/" rel="nofollow">Chris Samuel</a> (not verified) on Sun, 2011-01-30 08:05.</div>
    <div class="content">
     <p>Probably because of distros like Ubuntu which have been shipping versions of Exiv2 which are 100 times slower (my measurement, the related Debian bug measured 250x) at handling Nikon images than the previous version for the last 6 months (and will be almost a year by the time that 11.04 is released with the fixed 0.20 version).</p>
<p><a href="https://launchpad.net/~kubuntu-ppa/+archive/backports">https://bugs.launchpad.net/ubuntu/+source/exiv2/+bug/596327</a></p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19741"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19741" class="active">A little OT question: I'm</a></h3>    <div class="submitted">Submitted by redm (not verified) on Wed, 2011-01-05 20:57.</div>
    <div class="content">
     <p>A little OT question: I'm using digikam only once in a while, so I'm not too familiar with it. But when I was trying to sort some photos today, I noticed that I can't edit the images when opening them in the editor (besides simple things like rotate). E.g. manipulating the histogram, it's shown, but I can't change anything. The view buttons in the statusbar are all grayed out. It all seems to be kind of read only but I couldn't find how to enable editing. All I know is that it worked in the past. Any clues?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19745"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19745" class="active">Slow digikam</a></h3>    <div class="submitted">Submitted by dajomu (not verified) on Thu, 2011-01-06 14:25.</div>
    <div class="content">
     <p>I wonder if the speed of loading thumbnails has improved or will be improved with this release. In folders with &gt;60 photos, digikam is waaay slower than picasa which has virtually no lag at all on my asrock ion 330ht.<br>
How come picasa is so much faster? Does it preload in a different manner or maybe digikam does not preload? Is the picasa database just that much faster?<br>
Anyone knows what I am talking about and experince the same thing?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19746"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19746" class="active">Thanks a lot for this great</a></h3>    <div class="submitted">Submitted by <a href="http://ubuntuku.org" rel="nofollow">ubuntuku</a> (not verified) on Thu, 2011-01-06 15:19.</div>
    <div class="content">
     <p>Thanks a lot for this great app! Everything seems to be working, still testing though. Here's how I compiled it in Kubuntu Maverick: http://ubuntuku.org/22/how-to-install-compile-digikam-2-0-0-beta-from-source/</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19748"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19748" class="active">Face recognition or detection</a></h3>    <div class="submitted">Submitted by Stephen S (not verified) on Thu, 2011-01-06 17:16.</div>
    <div class="content">
     <p>Out of curiosity, is the new feature going to be face detection or face recognition?</p>
<p>By that, I mean that face detection would be locating a face object on a photo. And face recognition would be face detection plus being able to recognize and tag the 'owner' of the face it detected. I know that Apple's iPhoto will learn a person's face and automatically guess who a face is after you tell it a few examples.</p>
<p>Face detection will be cool, but face recognition would be even cooler!</p>
<p>Also on my wishlist is exporting photos to Facebook with the faces already detected, linking them up to the facebook friends lists. Proly won't happen, but I can dream...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19756"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19756" class="active">It is face detection that is</a></h3>    <div class="submitted">Submitted by Marcel Wiesweg (not verified) on Fri, 2011-01-07 13:49.</div>
    <div class="content">
     <p>It is face detection that is implemented and works pretty well - I cannot cite papers with state of the art comparison, but I believe really a lot of people rely on the haar cascades shipped with OpenCV.</p>
<p>The detection part is much harder - both in the sense of availability of free code, and in the sense that the problem of recognition in an uncontrolled environment (differing lightning and pose) is solved. Dont expect to get usable result from this yet. Maybe in the future.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19765"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19765" class="active">Usefulness of Face Detection</a></h3>    <div class="submitted">Submitted by Jeff (not verified) on Mon, 2011-01-10 17:26.</div>
    <div class="content">
     <p>I know that face detection can be useful for machine vision-related tasks, such as autofocus, but what exactly does it add to DigiKam that makes it any better than just using my own eyes to detect the faces?</p>
<p>Is there any example (e.g., a screen cast or some screen shots) that shows how it can be used in DigiKam to improve productivity?</p>
<p>Thanks,<br>
J.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19750"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19750" class="active">This is it...</a></h3>    <div class="submitted">Submitted by <a href="http://krita" rel="nofollow">Bugsbane</a> (not verified) on Fri, 2011-01-07 01:37.</div>
    <div class="content">
     <p>The big one. The one I've been waiting for! Digikam with the three big features I've been longing for: support for faces, reverse geotagging and nondestructive image editing! Woohoo!</p>
<p>Great job to all involved. 2.0.0 is going to be one kick booty release! Now to get compiling and bugtest the heck out of this sucker. :)</p>
<p>&lt;3 Bugsbane</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19752"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19752" class="active">"With 2.x, digiKam include</a></h3>    <div class="submitted">Submitted by I (not verified) on Fri, 2011-01-07 10:06.</div>
    <div class="content">
     <p>"With 2.x, digiKam include now all shared libraries maintained by digiKam team plus kipi-plugins source code, as a software collection. The goal is to simplify packaging to reduce external dependencies."</p>
<p>I compiled digiKam on Debian Sid. Face recognition does not work.</p>
<p>--  digiKam 2.0.0-beta1 dependencies results<br>
--<br>
--  Qt4 SQL module found..................... YES<br>
--  Qt4 SCRIPT module found.................. YES<br>
--  Qt4 SCRIPTTOOLS module found............. YES<br>
--  MySQL Server found....................... YES<br>
--  MySQL install_db tool found.............. YES<br>
--  libtiff library found.................... YES<br>
--  libpng library found..................... YES<br>
--  libjasper library found.................. YES<br>
--  liblcms library found.................... YES<br>
--  Boost Graph library found................ YES<br>
--  libkipi library found.................... YES<br>
--  libkexiv2 library found.................. YES<br>
--  libkdcraw library found.................. YES<br>
--  libkface library found................... YES<br>
--  libkmap library found.................... YES<br>
--  libpgf library found..................... NO  (optional - internal version used instead)<br>
--  libclapack library found................. NO  (optional - internal version used instead)<br>
--  libgphoto2 library found................. YES (optional)<br>
--  libkdepimlibs library found.............. YES (optional)<br>
--  Nepomuk libraries found.................. YES (optional)<br>
--  libglib2 library found................... YES (optional)<br>
--  liblqr-1 library found................... YES (optional)<br>
--  liblensfun library found................. YES (optional)<br>
--  Doxygen found............................ YES (optional)<br>
--  digiKam can be compiled.................. YES<br>
-</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19753"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19753" class="active">Look this file...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2011-01-07 10:30.</div>
    <div class="content">
     <p>https://bugs.kde.org/show_bug.cgi?id=262212</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19758"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19758" class="active">Clone tool news?</a></h3>    <div class="submitted">Submitted by <a href="http://temporaryland.wordpress.com" rel="nofollow">rm42</a> (not verified) on Fri, 2011-01-07 16:39.</div>
    <div class="content">
     <p>The feature I am eagerly waiting for is a proper clone tool.  And I know I am not alone in this.  It was acknowledged as a bug some time back.  Has there been any progress in this respect?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19762"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19762" class="active">not yet...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2011-01-07 19:33.</div>
    <div class="content">
     <p>...but this feature is the next on the TODO list, by priority...</p>
<p>I hope to fink a contributor about ths subject at nex Google Summer Of Code 2011...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19763"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19763" class="active">local editing</a></h3>    <div class="submitted">Submitted by JulienN (not verified) on Sat, 2011-01-08 19:06.</div>
    <div class="content">
     <p>It would be nice to have "local editing" as a google summer of code project.</p>
<p>Julien</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19792"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19792" class="active">Epicness :D
Hope that some</a></h3>    <div class="submitted">Submitted by Alex Kuster (not verified) on Wed, 2011-01-26 21:36.</div>
    <div class="content">
     <p>Epicness :D<br>
Hope that some day PostgreSQL can be used as a storage backend (instead of MySQL or Sqlite)<br>
I've definitely have to try this version :D</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19802"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/561#comment-19802" class="active">I agree with you.</a></h3>    <div class="submitted">Submitted by <a href="http://www.clless.com" rel="nofollow">janeyquiel</a> (not verified) on Fri, 2011-01-28 13:50.</div>
    <div class="content">
     <p>I agree with your assumption.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
