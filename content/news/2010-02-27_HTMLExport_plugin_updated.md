---
date: "2010-02-27T20:10:00Z"
title: "HTMLExport plugin updated"
author: "Anonymous"
description: "Dear digiKam users Few months earlier I started to work on HTMLExport plugin and first two updates I made are mentioned in the TODO file"
category: "news"
aliases: "/node/504"

---

<p>Dear digiKam users</p>
<p>Few months earlier I started to work on HTMLExport plugin and first two updates I made are mentioned in the TODO file or request on bugzilla.<br>
The first one is showing template preview alongside theme description as shown in the next screenshot:</p>
<p><a href="http://www.flickr.com/photos/34965557@N04/4392072101/" title="screenshot1 by giasoneregna, on Flickr"><img src="http://farm5.static.flickr.com/4024/4392072101_9238762f74.jpg" width="500" height="333" alt="screenshot1"></a></p>
<p>The second one is getting metadata from image files during the album creation, using XSLT for the theming mechanism is really powerful, but in the last version the amount of information included in gallery.xml is very limited - now you can access much more information, look at THEME_HOWTO file for a detailed list. There is also a new detailed theme to test this featured, an example on the next screenshot.</p>
<p><a href="http://www.flickr.com/photos/34965557@N04/4392072391/" title="screenshot2 by giasoneregna, on Flickr"><img src="http://farm5.static.flickr.com/4047/4392072391_03b7413043.jpg" width="500" height="349" alt="screenshot2"></a> </p>
<p>Stay tuned on next features, please check out current svn and tell any bugs at bugs.kde.org.  </p>
<p><a href="http://giasone.wordpress.com/2010/02/27/htmlexport-plugin-updated/" target="_blank">Versione Italiana </a></p>

<div class="legacy-comments">

  <a id="comment-19064"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/504#comment-19064" class="active">you're my hero! I used to use</a></h3>    <div class="submitted">Submitted by <a href="http://nowwhatthe.blogspot.com" rel="nofollow">jospoortvliet</a> (not verified) on Sun, 2010-02-28 14:56.</div>
    <div class="content">
     <p>you're my hero! I used to use this functionality in ye olde days but haven't for a while. This might change that ;-)</p>
         </div>
    <div class="links">» </div>
  </div>

</div>