---
date: "2018-10-15T00:00:00"
title: "Use digiKam with a NAS and MariaDB"
author: "Dmitri Popov"
description: "Use digiKam with a NAS and MariaDB"
category: "news"
---

Got a NAS? Still store your photo library and digiKam databases on a local machine? It's time to take your digiKam setup to the next level by moving your photo library to the NAS and switching to the MariaDB (or MySQL) database backend. This allows you to access your photo library from any machine on the same network as well as keep your photo library safe thanks to the fact that storage on most NAS appliances is usually configured as RAID. [Continue reading](https://scribblesandsnaps.com/2018/10/19/use-digikam-with-a-nas-and-mariadb/)...
