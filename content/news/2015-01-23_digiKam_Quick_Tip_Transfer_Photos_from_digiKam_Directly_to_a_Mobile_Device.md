---
date: "2015-01-23T09:51:00Z"
title: "digiKam Quick Tip: Transfer Photos from digiKam Directly to a Mobile Device"
author: "Dmitri Popov"
description: "Provided that you use KDE, you can push photos from digiKam directly to a mobile device that supports the MTP protocol (e.g., most Android devices)"
category: "news"
aliases: "/node/729"

---

<p>Provided that you use KDE, you can&nbsp;push photos from digiKam directly to a mobile device that supports the MTP protocol (e.g., most Android devices) using the KioImportExport&nbsp;Kipi plugin.</p>
<p><img src="https://scribblesandsnaps.files.wordpress.com/2015/01/mtp_export.png" alt="mtp_export" width="473" height="376"></p>
<p><a href="http://scribblesandsnaps.com/2015/01/23/digikam-quick-tip-transfer-photos-from-digikam-directly-to-a-mobile-device/">Continue to read</a></p>

<div class="legacy-comments">

</div>