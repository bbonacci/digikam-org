---
date: "2011-11-07T10:56:00Z"
title: "digiKam Software Collection 2.3.0 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! After one month of development, digiKam team is proud to announce the digiKam Software Collection 2.3.0, as bug-fixes release,"
category: "news"
aliases: "/node/632"

---

<a href="http://www.flickr.com/photos/digikam/6321935618/" title="digiKam2.3.0-01 by digiKam team, on Flickr"><img src="http://farm7.static.flickr.com/6042/6321935618_c464c4700d_z.jpg" width="640" height="256" alt="digiKam2.3.0-01"></a>

<p>Dear all digiKam fans and users!</p>

<p>After one month of development, digiKam team is proud to announce the digiKam Software Collection 2.3.0, as bug-fixes release, including a total of <a href="https://projects.kde.org/projects/extragear/graphics/digikam/repository/revisions/master/entry/project/NEWS.2.3.0">43 fixed bugs</a>. In this release, layout of Captions/Tags tab from right sidebar have been improved for usability issue.</p>

<p>Close companion Kipi-plugins is released along with digiKam 2.3.0. This release features a lots of improvement into FlashExport plugin through <a href="http://community.kde.org/GSoC/2011/Ideas#digiKam">Season of KDE 2011 (SoK)</a> project to support new <a href="http://www.simpleviewer.net/products/">SimpleViewer tools</a>. This relase include also <a href="https://projects.kde.org/projects/extragear/graphics/kipi-plugins/repository/revisions/master/entry/project/NEWS.2.3.0">16 bug-fixes</a>.</p>

<a href="http://www.flickr.com/photos/digikam/6321935696/" title="digiKam2.3.0-02 by digiKam team, on Flickr"><img src="http://farm7.static.flickr.com/6019/6321935696_ca7ebf9f6a_z.jpg" width="640" height="256" alt="digiKam2.3.0-02"></a>

<p>As usual, digiKam software collection tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/files">SourceForge Repository</a>. Stand alone <a href="http://sourceforge.net/projects/digikam/files/digikam/2.3.0/digiKam-installer-2.3.0-win32.exe/download">Windows installer</a> have been also updated.</p>

<p>Happy digiKam...</p>
<div class="legacy-comments">

  <a id="comment-20089"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20089" class="active">Tagging Tab FTW</a></h3>    <div class="submitted">Submitted by DRB (not verified) on Mon, 2011-11-07 16:15.</div>
    <div class="content">
     <p>I am very excited about tagging being moved to it's own (sub)tab. On my netbook (1366x768) I could only see three tags at a time since version 2.1.0 making tagging nearly impossible.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20090"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20090" class="active">Libkface</a></h3>    <div class="submitted">Submitted by frattale (not verified) on Tue, 2011-11-08 09:20.</div>
    <div class="content">
     <p>Is there a hope that libkface will work properly and perform face recognition? I have found that it still doesn't work in version 2.2 and I had to name all people by my self.</p>
<p>Anyway, each new digikam release is a great emotion: thanks for this wonderful software!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20091"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20091" class="active">Debian</a></h3>    <div class="submitted">Submitted by <a href="http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=628019" rel="nofollow">Malte</a> (not verified) on Tue, 2011-11-08 18:54.</div>
    <div class="content">
     <p>Any news on the Debian lib problem? I have been waiting for Digikam 2 since it came out:</p>
<p>http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=628019</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20092"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20092" class="active">The bug you have linked to is</a></h3>    <div class="submitted">Submitted by zevans23 (not verified) on Tue, 2011-11-08 21:06.</div>
    <div class="content">
     <p>The bug you have linked to is reported fixed, as it's now in debian-experimental - so I am not sure what you're asking for... do you still have issues? If so suggest reporting them there!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20094"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20094" class="active">There was no bug</a></h3>    <div class="submitted">Submitted by Malte (not verified) on Wed, 2011-11-09 00:14.</div>
    <div class="content">
     <p>I called it a problem, not a bug. I quote from here:</p>
<p>http://lists.debian.org/debian-experimental-changes/2011/10/msg00021.html</p>
<p>* digikam 2.0.0 uses features from kdegraphics 4.7 &amp; ships a private version of the kdegraphics libs - this is not the Debian way :-(<br>
* Unsatisfactory Conflicts: libkexiv2-dev kipi-plugins-common libkdcraw-dev libkipi-dev kdegraphics-libs-data &amp; libkipi8</p>
<p>Then there was the problem with the right version of KDE not having reached Debian Sid yet. </p>
<p>But I am not following all those things close enough to know exactly what I am talking about. Yes, Digikam was uploaded to Experimental. But AFAIK the maintainer still has a lot of reservations about it. I thought an innocent question in a blog would gain some insight.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20109"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20109" class="active">Oh I see... I think these two</a></h3>    <div class="submitted">Submitted by zevans23 (not verified) on Sat, 2011-11-12 23:32.</div>
    <div class="content">
     <p>Oh I see... I think these two issues are related. I guess the latest Digikam relies on features that are only in KDE 4.7, so the choices are...</p>
<p>- take those features out of a Digikam version for KDE 4.6<br>
or<br>
- add the right libs to the backports in mainstream Debian, but I don't even know how to start that conversation...</p>
<p>I'm using the philip5 repo on Ubuntu so I get a consistent set of libraries. Except face recognition has never ever worked on any of the digikam versions.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20111"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20111" class="active">Face recognition doesn't work</a></h3>    <div class="submitted">Submitted by Philip Johnsson (not verified) on Sun, 2011-11-13 14:52.</div>
    <div class="content">
     <p>Face recognition doesn't work as it's not really implemented yet even though you see the feature in the GUI. So far only face recognition works. It's not a build, package or dist issue but it's yet to be fully developed and integrated.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20125"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20125" class="active">digikam Debian packages now up in experimental</a></h3>    <div class="submitted">Submitted by <a href="http://packages.debian.org/digikam" rel="nofollow">Mark Purcell</a> (not verified) on Sat, 2011-11-19 23:36.</div>
    <div class="content">
     <p>The digikam packages in experimental are quite usable.</p>
<p>http://packages.debian.org/experimental/digikam/</p>
<p>Mark</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20127"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20127" class="active">Thank You</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2011-11-21 19:44.</div>
    <div class="content">
     <p>It really seems to be quite stable.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div><a id="comment-20093"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20093" class="active">I try digikam 2.3 on windows</a></h3>    <div class="submitted">Submitted by vincent (not verified) on Tue, 2011-11-08 22:22.</div>
    <div class="content">
     <p>I try digikam 2.3 on windows XP and Vista (on two different PC). I have a problem on these two systems: I can't remove pictures. Digikam shows an error box.....</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20097"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20097" class="active">What is the error message?
If</a></h3>    <div class="submitted">Submitted by Ananta Palani (not verified) on Thu, 2011-11-10 15:40.</div>
    <div class="content">
     <p>What is the error message?</p>
<p>If you are trying to delete the picture, then that is currently not supported (see the readme file that was displayed after installing digiKam). The problem is not with digiKam, but with KDE itself. You can permanently delete files, however, by pressing shift-delete.</p>
<p>Here is the relevant bug report for digiKam:</p>
<p>   https://bugs.kde.org/show_bug.cgi?id=229465</p>
<p>and here is the actual cause of the problem, which is several years old:</p>
<p>   https://bugs.kde.org/show_bug.cgi?id=166280</p>
<p>We are working on a solution, however.</p>
<p>-Ananta</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20096"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20096" class="active">Sidebar defaults to geotagging tab after edit</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2011-11-10 15:06.</div>
    <div class="content">
     <p>This bug is stated as resolved some years back but it's still hanging on! Problem occurs in fullscreen mode after any edit - out pops the geotagging side bar. No matter what I've done I can't tame it! Any suggestions?</p>
<p>Great work on all the new features.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20100"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20100" class="active">More info needed...</a></h3>    <div class="submitted">Submitted by Mike (not verified) on Fri, 2011-11-11 09:16.</div>
    <div class="content">
     <p>Hi,</p>
<p>could you please report this problem directly on the bugreport or open a new bug on bugs.kde.org with a more detailed descriptiont? Otherwise it will be hard for us to figure out what the problem is...</p>
<p>Mike</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20101"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20101" class="active">Reproduce:
* Open picture in</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2011-11-11 13:25.</div>
    <div class="content">
     <p>Reproduce:<br>
* Open picture in Image Editor<br>
* Hide sidebar, if visible<br>
* Switch to full screen<br>
* Apply any effect (like Curves Adjust), press Apply</p>
<p>Now the (geotag) sidebar shows up, even though it has been hidden previously.</p>
<p>Bug report i was referring to in post above is https://bugs.kde.org/show_bug.cgi?id=187733. The same source problem i believe caused people to report these bugs http://bugs.kde.org/show_bug.cgi?id=220739 and http://bugs.kde.org/show_bug.cgi?id=207921.</p>
<p>So I'll comment on the open bug report to keep it going. Otherwise was wondering if other people have workarounds until it's fixed?</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20098"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20098" class="active">Face tagging</a></h3>    <div class="submitted">Submitted by River (not verified) on Fri, 2011-11-11 03:43.</div>
    <div class="content">
     <p>Installing this version for Windows has been much more fluid than anything I've tried in the past except the menu icons referred to:<br>
C:/Program Files/DigiKam/kde4/bin/*<br>
but DigiKam installed itself to:<br>
C:/Program Files/DigiKam/bin/*</p>
<p>I corrected the icons but could this also be the cause of the issue with face detection? It takes no time and claims all 20,000 photos are done but does nothing and finds no faces. This was also the issue with I had with 2.0.0 (I tried a number of versions and finally got the face detection tag to work - it also did nothing) and version 1.7 just didn't cut it.</p>
<p>Any suggestions as to a solution? I know there were some pretty involved ones for 2.0.0 that I have now forgotten (and I hoped would no longer be relevant).</p>
<p>DigiKam is really not much use to me without this functionality as I already have a work flow without DigiKam and if it can't do this my partner and I are not willing to shift and learn all the changes to use DigiKam instead for little benefit.</p>
<p>One final question is about transferring data from Fotobounce, Anyone got any ideas who I might transfer the tagging data (any of it, even just the photo comments) to DigiKam? (I'm willing to shift but you can see that the re-tagging of all faces is causing me issues too, but Fotobounce just isn't handling the load at all (it hasn't been since we hit about 8,000 photos).</p>
<p>River</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20099"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20099" class="active">An update.</a></h3>    <div class="submitted">Submitted by River (not verified) on Fri, 2011-11-11 07:03.</div>
    <div class="content">
     <p>First Fotobounce: I've found the option and it works pretty well as far as I can tell.</p>
<p>DigiKam. It now lives in the folder:<br>
C:/Programs Files/digiKam/kde4/*<br>
because I started to get the idea that might be the problem. This did not help.</p>
<p>I worked out how to get the fingerprints done and did them (took a while); this did not help.</p>
<p>Face detection still does not detect any faces.</p>
<p>I added some manually. Did not help.</p>
<p>I added some of my original high resolution images to the library. Did not help.</p>
<p>I imported some images into a new folder. Did not help.</p>
<p>I added some exported images from Fotobounce. The faces marked by Fotobounce were added to the list of "People" tags in the Face detection tab. Did not help detect new faces.</p>
<p>In each case and every time I have tried this I tried a completely new scan and a scan and merge. I also tried a Benchmark and (if I had not added faces) the clear whole database and start again.</p>
<p>I should also mention that each time I restarted digiKam I have also been killing kde4d, klauncher and dbud-daemon before restarting digiKam.</p>
<p>Since one of the old problems was lack of the right libraries here is my list as reported by digiKam (it all seems correct to me, but I don't know):<br>
digiKam version 2.3.0<br>
Exiv2 can write to Jp2: Yes<br>
Exiv2 can write to Jpeg: Yes<br>
Exiv2 can write to Pgf: Yes<br>
Exiv2 can write to Png: Yes<br>
Exiv2 can write to Tiff: Yes<br>
Exiv2 supports XMP metadata: Yes<br>
LibCImg: 130<br>
LibClapack: internal library<br>
LibExiv2: 0.21<br>
LibJPEG: 80<br>
LibJasper: 1.900.1<br>
LibKDE: 4.7.2 (4.7.2)<br>
LibKExiv2: 2.0.0<br>
LibKGeoMap: 2.0.0<br>
LibKdcraw: 2.0.0<br>
LibLCMS: 119<br>
LibPGF: 6.11.42 - internal library<br>
LibPNG: 1.4.4<br>
LibQt: 4.7.4<br>
LibRaw: 0.13.8<br>
LibTIFF: LIBTIFF, Version 3.9.2 Copyright (c) 1988-1996 Sam Leffler Copyright (c) 1991-1996 Silicon Graphics, Inc.<br>
Marble Widget: 0.12.2 (stable release)<br>
Parallelized demosaicing: Yes<br>
Database backend: QSQLITE<br>
LibKface: 2.0.0<br>
LibKipi: 1.2.0<br>
LibOpenCV: 2.3.0<br>
Libface: 0.2</p>
<p>The version of the Handbook seems to be Revision 1.2 (2010-02-20) - and refers to digiKam 1.1.0 on the second page - there is no information that I can find at all referring to face tagging. This does not help.</p>
<p>The database statistics are:<br>
digiKam version 2.3.0<br>
Images:<br>
GIF: 1<br>
JPG: 19570<br>
PNG: 111<br>
total: 19682<br>
:<br>
:<br>
Total Items: 19682<br>
Albums: 629<br>
Tags: 46<br>
Database backend: QSQLITE</p>
<p>The online digiKam handbook is no more up to date.</p>
<p>The FAQ does not have a solution to this problem.</p>
<p>There are no threads on the digiKam mailing list or the digiKam developers mailing list that refer to this type of problem for 2.3.0 (though there are some very old ones for version 2.0.0 and earlier which didn't work then (or weren't applicable) and don't work now (or aren't applicable)).</p>
<p>On the KDE bug tracker my problem is not listed (as far as I can tell) on any of these results:<br>
https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;product=digikam&amp;op_sys=MS+Windows&amp;order=bugs.bug_id</p>
<p>or these results:<br>
https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;product=digikam&amp;component=Face+Detection&amp;order=bugs.bug_id</p>
<p>and the following gives no results at all:<br>
https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;product=digikam&amp;component=Face+Detection&amp;op_sys=MS+Windows&amp;order=bugs.bug_id</p>
<p>any more specific searches mostly for MS Windows or version 2.3.0 (I was confused by 2.4.0 and 2.5.0 being listed) return no results. I checked all the most recent bugs thoroughly and scanned the ones older than version 2.2.0 release date on both of the above searches that gave results.</p>
<p>I have not reported this in the KDE bugzilla because I suspect that it is potentially a Windows installer issue rather than a digiKam bug (but I don't know).</p>
<p>I am running a Japanese language version of Windows 7 Starter 32-bit.</p>
<p>If there are any more details, logs or any programs I can run to try and trace the problem I am happy to help. If there is somewhere better for me to be writing this, please tell me where (though if I have to create a username then I will likely ave to create many since I am great at forgetting them (and passwords) and lose pieces of paper all the time... I have to use accounts daily to avoid this and even then... *sigh*).</p>
<p>Thank you for your time, help and what looks to be a better and better piece of software that I'm more keen than ever to use!</p>
<p>River</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20102"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20102" class="active">Apologies</a></h3>    <div class="submitted">Submitted by River (not verified) on Fri, 2011-11-11 14:47.</div>
    <div class="content">
     <p>Apologies for the bluntness of my message, I have spent too long in front of my computer failing to get digiKam to work as advertised despite clearly being very, very close! :) It is very frustrating.</p>
<p>River</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20115"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20115" class="active">Bug Report</a></h3>    <div class="submitted">Submitted by Ananta Palani (not verified) on Mon, 2011-11-14 22:03.</div>
    <div class="content">
     <p>Thanks for filing the bug report here:</p>
<p>   https://bugs.kde.org/show_bug.cgi?id=286463</p>
<p>Sorry for the delay in responding, our available time is very limited! I have responded to your question on the bug report.</p>
<p>Best,<br>
-Ananta</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20116"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20116" class="active">Thank you</a></h3>    <div class="submitted">Submitted by River (not verified) on Tue, 2011-11-15 02:21.</div>
    <div class="content">
     <p>If anyone else has this problem the solution was rather simple but rather non-intuitive (and still indicates a bug I'm afraid).</p>
<p>Where the choice of albums is the default given (which is your album root directory) does not work. deselect it and it will scan your entire collection.</p>
<p>Ananta - I'll fill in the bug report with the results, it's a minor bug but probably an easy to fix one... no? Just don't have the root directory selected as the default album to scan... though there is clearly another bug there that yo can't select the root album/directory to scan everything (this might come back to haunt if there is ever the option to have multiple root albums...</p>
<p>Thank you for your time,</p>
<p>River</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div><a id="comment-20106"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20106" class="active">After the upgrade to digikam</a></h3>    <div class="submitted">Submitted by Karol (not verified) on Fri, 2011-11-11 19:28.</div>
    <div class="content">
     <p>After the upgrade to digikam 2.3 (in Ubuntu 11.10) do not play avi files. What's the matter?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20107"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20107" class="active">check mplayerthumbs package</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2011-11-11 19:30.</div>
    <div class="content">
     <p>there is a special KDE plugin to render video thumbnails. Video preview are delegate to KDE. Check mplayerthumb package...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20121"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20121" class="active">Package mplayerthums is</a></h3>    <div class="submitted">Submitted by kadkarol (not verified) on Tue, 2011-11-15 20:47.</div>
    <div class="content">
     <p>Package mplayerthums is installed. I see the thumbnails, but are not play video files</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20112"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20112" class="active">Export to Remote Computer</a></h3>    <div class="submitted">Submitted by id4alexey (not verified) on Sun, 2011-11-13 14:53.</div>
    <div class="content">
     <p>Hi! I have problem with "Export to Remote Computer" plugin in Windows version: The "Select target location ..." window is unreadable.<br>
P.S. The plugin didn't work in previous versions also.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20113"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20113" class="active">Confirm this error</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2011-11-13 15:24.</div>
    <div class="content">
     <p>I can confirm this error. For me it looks like render issue. On top of the opening dialog some very, very smal icons are visible.<br>
Does this plugin work under Linux?</p>
<p>Daniel</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20114"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20114" class="active">Bug Report</a></h3>    <div class="submitted">Submitted by Ananta Palani (not verified) on Mon, 2011-11-14 19:26.</div>
    <div class="content">
     <p>Can one of you open a bug report for this issue here:</p>
<p>   https://bugs.kde.org/enter_bug.cgi?product=kipiplugins&amp;format=guided</p>
<p>The component is 'KIOImportExport'. Also attach a screenshot if you could.</p>
<p>The issue may be related to KIO (a KDE component):</p>
<p>   https://bugs.kde.org/show_bug.cgi?id=225777</p>
<p>Thanks!<br>
-Ananta</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20117"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20117" class="active">Created bug report</a></h3>    <div class="submitted">Submitted by Daniel (not verified) on Tue, 2011-11-15 10:07.</div>
    <div class="content">
     <p>I've created the bug report 286661.</p>
<p>Best regards,<br>
 Daniel</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20122"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20122" class="active">Great ... much beter then ver</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2011-11-16 10:40.</div>
    <div class="content">
     <p>Great ... much beter then ver 2.2 ...</p>
<p>only what i got a little problem when i setup full scin support under Ubuntu 11.04. </p>
<p>I think its time to donate some money for bear or juice ...   </p>
<p>keep doing great job ... :D ... :D ...</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20123"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20123" class="active">Features or Bugs?</a></h3>    <div class="submitted">Submitted by Michael Molthagen (not verified) on Wed, 2011-11-16 11:15.</div>
    <div class="content">
     <p>Hi, I'm a great fan of DigiKam and use it on a daily base, but there are two topics confusing me...</p>
<p>1. If I have a tag "/Camera/Canon EF 50 F1.4 USM", after editing it will turn into "/Canon EF 50 F1/4 USM"</p>
<p>2. If I want to sharpen an image, i cannot change the view size (eg. "fit to window size" or "original size"), I have to select the view size prior to open the sharpening dialog</p>
<p>If I remember correctly, DigiKam 1.x didn't work those ways. </p>
<p>Kind regards from Germany's wild south,</p>
<p>Michael</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20134"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20134" class="active">point 2 fixed for 2.4.0</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2011-11-24 12:28.</div>
    <div class="content">
     <p>i just commited fixes for 2/</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20124"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/632#comment-20124" class="active">THANK YOU</a></h3>    <div class="submitted">Submitted by KS (not verified) on Thu, 2011-11-17 21:46.</div>
    <div class="content">
     <p>THANK YOU THANK YOU THANK YOU THANK YOU<br>
I honestly switched to Gnome to run Shotwell because the right-side tags thing was angering me so thoroughly.  Need not be angry anymore!</p>
<p>I really do LOVE Digikam, but there are a lot of features of it I just don't use.  These crowd and clutter the interface.  Any chance these could be disabled in the preferences?  Maybe make them plugins?  An example for me would be geolocation.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
