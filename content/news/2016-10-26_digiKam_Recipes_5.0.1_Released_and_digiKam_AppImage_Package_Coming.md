---
date: "2016-10-26T20:29:00Z"
title: "digiKam Recipes 5.0.1 Released and digiKam AppImage Package Coming"
author: "Dmitri Popov"
description: "Hot on the heels of the major digiKam 5.x.x release comes a thoroughly revised version of the digiKam Recipes book. Although it doesn’t include any"
category: "news"
aliases: "/node/760"

---

<p>Hot on the heels of the major digiKam 5.x.x release comes a thoroughly revised version of the digiKam Recipes book. Although it doesn’t include any new material, the entire content and all screenshots have been revised and updated to reflect changes in the latest version of digiKam. In addition to that, some obsolete content has been removed. But a shiny new book cover featuring catchy colors makes up for that.</p>
<p><img src="http://i.imgur.com/5hsUjKS.jpg"></p>
<p>Speaking of digiKam releases, the upcoming version 5.3.0 will be available as an AppImage 64-bit package. <a href="https://medium.com/@dmpop/digikam-recipes-5-0-1-released-and-digikam-appimage-package-coming-d803d00c4079">Continue reading</a></p>

<div class="legacy-comments">

</div>