---
date: "2007-07-26T17:04:00Z"
title: "digiKam splashscreens : call to photographers..."
author: "digiKam"
description: "With the next releases of the digiKam, the time has come to find the ideal splash-screens to go with it. Now is your chance to"
category: "news"
aliases: "/node/249"

---

With the next releases of the digiKam, the time has come to find the ideal splash-screens to go with it. Now is your chance to join the ranks of the precious few who have had their artwork associated with a release of digiKam!
<br><br>
SplashScreen is a simple way for users to contribute to digiKam project. The pictures must be correctly exposed and composed, and the subject must be choosen using a real photographer inspiration.
<br><br>
The next stable release 0.9.3 of digiKam is planed to september. This version will includes few bug fix for KDE3 envirronement. For this release, we need 2 new splash-screens dedicaced to digiKam and Showfoto startup.
<br><br>
Note, than it will also the same about digiKam and Showfoto for KDE4 envirronement planed around december.
<br><br>
To resume, we need 4 color or black and white pictures, taken horizontally. We need only photos, no need to put few marks about program name and version. We have a template to do it later. See below the official 0.9.2 splash-screen as exemple:

<br><br><a href="http://www.digikam.org/?q=system/files&amp;file=images/digikam-splash.png"><img src="http://www.digikam.org/?q=system/files&amp;file=images/digikam-splash_0.png" width="503" height="330">
</a><br><br>

If you is interressed, you can send me your photo to my gmail box. Look <a href="http://www.digikam.org/?q=contact">contact web project page</a> for details.
<br><br>
Thanks in advance for your contributions...

<div class="legacy-comments">

</div>