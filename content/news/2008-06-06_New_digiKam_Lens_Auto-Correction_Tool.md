---
date: "2008-06-06T14:08:00Z"
title: "New digiKam Lens Auto-Correction Tool"
author: "digiKam"
description: "digiKam for KDE4 will become with another important tool for photograph: lens distorsions auto-correction. The plugin have been created by Adrian Schroeter from OpenSuse team."
category: "news"
aliases: "/node/319"

---

<p>digiKam for KDE4 will become with another important tool for photograph: lens distorsions auto-correction. The plugin have been created by <b>Adrian Schroeter</b> from OpenSuse team.</p>

<p>This tool have been implemented as a new image editor plugin. It use <a href="http://lensfun.berlios.de/">LensFun</a> shared library witch provide a database of lens settings to fix optical distorsions automatically. The tool use picture metadata to find the right settings to apply (Exif and Makernotes). You can adjust yourself all parameters if you need.</p> 

<p>A video of this tool in action is available below. The test image used in this video come from <b>Pablo d'Angelo</b> collection (<a href="http://hugin.sourceforge.net">Hugin project</a>).</p>

<object width="500" height="404"><param name="movie" value="http://www.youtube.com/v/f6LkEGKTfAE&amp;hl=fr"><embed src="http://www.youtube.com/v/f6LkEGKTfAE&amp;hl=fr" type="application/x-shockwave-flash" width="500" height="404"></object>


<p>I have take also some screenshots of this tool with a better resolution:</p>

<a href="http://www.flickr.com/photos/digikam/2583988298/" title="lensautocorrection3 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3046/2583988298_1b9c5b1dbc_m.jpg" width="240" height="148" alt="lensautocorrection3"></a>

<br>

<a href="http://www.flickr.com/photos/digikam/2583988290/" title="lensautocorrection2 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3268/2583988290_cb9dc6585b_m.jpg" width="240" height="192" alt="lensautocorrection2"></a>

<br>

<a href="http://www.flickr.com/photos/digikam/2583157751/" title="lensautocorrection1 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3118/2583157751_0acc15baa1_m.jpg" width="240" height="192" alt="lensautocorrection1"></a>

<div class="legacy-comments">

  <a id="comment-17582"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/319#comment-17582" class="active">How can I add this plugin to</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2008-06-12 11:43.</div>
    <div class="content">
     <p>How can I add this plugin to digikam in kde3?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17583"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/319#comment-17583" class="active">This plugin is only available</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2008-06-12 13:48.</div>
    <div class="content">
     <p>This plugin is only available in KDE4 version of digiKam. there is no plant to backport it to KDE3</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18052"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/319#comment-18052" class="active">Thanks for resource.</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2008-12-17 13:17.</div>
    <div class="content">
     <p>Thanks for resource.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18234"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/319#comment-18234" class="active">lensfun</a></h3>    <div class="submitted">Submitted by john (not verified) on Tue, 2009-02-10 19:09.</div>
    <div class="content">
     <p>LensFun sounds like it is going to be a great tool for fixing the images. I never like seeing photos that are blurry and have problems with the pixel ratio. Hopefully this tool will be able to fix distorted images and make them look better.</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
