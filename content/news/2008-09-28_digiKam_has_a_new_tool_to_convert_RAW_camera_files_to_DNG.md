---
date: "2008-09-28T20:12:00Z"
title: "digiKam has a new tool to convert RAW camera files to DNG..."
author: "digiKam"
description: "Yes! a DNG converter for Linux... For a long time, many people asked me to implement a RAW camera image converter to Digital NeGative (DNG)."
category: "news"
aliases: "/node/373"

---

<a href="http://upload.wikimedia.org/wikipedia/en/thumb/7/72/DNG_tm.svg/120px-DNG_tm.svg.png"><img src="http://upload.wikimedia.org/wikipedia/en/thumb/7/72/DNG_tm.svg/120px-DNG_tm.svg.png" width="120" height="80"></a>

<p>Yes! a DNG converter for Linux...</p>

<p>For a long time, many people asked me to implement a RAW camera image converter to <a href="http://en.wikipedia.org/wiki/Digital_Negative_(file_format)">Digital NeGative (DNG)</a>. Why should one support DNG under Linux? To improve interoperability with other photo-management programs such as Adobe tools, and to improve <a href="http://en.wikipedia.org/wiki/Digital_Asset_Management">Digital Asset Management (DAM)</a>, as <b>Gerhard Kulzer</b> discusses in <a href="http://www.gerhard.fr/DAM">this nice tutorial</a> and in particular in <a href="http://www.gerhard.fr/DAM/part2.html">Part 2</a>.</p>

<p>I have already talked about DNG in another <a href="http://www.digikam.org/drupal/node/368">ticket</a>. My first viewpoint about DNG SDK from Adobe has been wrong: missing tutorials, weird API documentation, wrong source code licence, etc... But after having found this <a href="http://www.barrypearson.co.uk/articles/dng">nice web site</a> from <b>Barry Pearson</b> which provides a huge collection of papers about DNG, I had a second look at the DNG SDK.</p>

<p>My first problem with DNG is to be able to extract un-demosaiced RAW image data to pass to DNG sdk. Here, dcraw cannot help really. By chance, after the port of libkdcraw to <a href="http://www.libraw.org">LibRaw</a>, I have found an easy way to extract these data, plus  lots of relevant meta-information, fully suitable for the DNG SDK. I would like to <b>Alex Tutubalin</b> from LibRaw for pointing this out.</p>

<p>After few weeks of studying the DNG SDK code to learn the API and the code from this little application dedicated to <a href="http://dng4ps2.chat.ru/index_en.html">convert Canon Powershoot RAW files to DNG</a>, I have finally found the way to write the first RAW to DNG converter for Linux, which support a lots of major RAW camera formats, as NEF, CR2, MRW, PAF, etc...</p>

<p>A DNG codec has been written around the DNG SDK and a command line tool to perform unit-tests. After a lots of conversions tests using official Adobe DNG converter and my program, a new kipi-plugin is born... See below for a screenshot of this tool in action...</p>

<a href="http://www.flickr.com/photos/digikam/3106260869/" title="dngconverterkde4 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3248/3106260869_c8f9186199.jpg" width="500" height="313" alt="dngconverterkde4"></a>

<p>The tool transfers RAW data to DNG, creates a JPEG preview and a thumbnail, sets a lots of TIFF-EP/DNG tags with original raw files metadata, does a backup of all Exif metadata and makernotes, and creates a set of XMP tags, accordingly. The generated is a valid DNG file, which is checked by Adobe DNGValidate tool...</p>

<p>The plan for the future is to improve the DNG tags creation following special makernotes entries, to support new RAW file formats, and to add a new option to backup the original RAW file within the DNG.</p>

<p>To conclude: DNG SDK is not perfect, but not too bad. I hope that Adobe will publish a future version using a real open source license such as BSD (ie, in the same way as the  XMP SDK is published).</p>

<div class="legacy-comments">

  <a id="comment-17819"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-17819" class="active">digiKam description extended on my website</a></h3>    <div class="submitted">Submitted by <a href="http://www.barrypearson.co.uk/articles/dng/" rel="nofollow">Barry Pearson</a> (not verified) on Mon, 2008-09-29 08:21.</div>
    <div class="content">
     <p>I've added digiKam to the list of DNG Converters, and added it to the History page as the first Linux DNG Converter.<br>
http://www.barrypearson.co.uk/articles/dng/products.htm#converters<br>
http://www.barrypearson.co.uk/articles/dng/history.htm#year5</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17820"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-17820" class="active">Thanks Barry...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2008-09-29 08:35.</div>
    <div class="content">
     <p>Thanks Barry...</p>
<p>Note that the digiKam DNG converter compile fine also under Windows and MacOS-X, as it use Qt4 which is multi-platform API.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17828"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-17828" class="active">I've updated the page again</a></h3>    <div class="submitted">Submitted by <a href="http://www.barrypearson.co.uk/articles/dng/" rel="nofollow">Barry Pearson</a> (not verified) on Mon, 2008-09-29 15:59.</div>
    <div class="content">
     <p>I've changed the page to make it clear that is supports Windows and MacOS-X.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-17821"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-17821" class="active">some hints</a></h3>    <div class="submitted">Submitted by <a href="http://www.bergphoto.org" rel="nofollow">bergi</a> (not verified) on Mon, 2008-09-29 09:21.</div>
    <div class="content">
     <p>Thanks for the first open DNG converter!</p>
<p>I'm working on my own raw converter NOT based on dcraw. At the moment my converter supports only Canon CR2 files, but I think I have already some hints for you after a quick look at the source code:</p>
<p>- color matrix: you are using the color matrix from dcraw, dcraw is using only one color matrix and this matrix is a copy of one of the matrices of the Adobe DNG Converter. As described in the chapter "Mapping Camera Color Space to CIE XYZ Space" of the DNG specification two matrices could be used and as you are already using the Adobe matrices why not using them completely?</p>
<p>- camera calibration: Adobe calculates the color matrices with a reference cam. Every camera has a different calibration. The calibration matrix can be calculated by comparing the 5200K neutral white balance values from the reference cam against your 5200K values (the same values for both calibration matrices). (Only tested with Canon cams!)</p>
<p>bergi</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17822"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-17822" class="active">color matrix and calibration</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2008-09-29 09:48.</div>
    <div class="content">
     <p>&gt; - color matrix: you are using the color matrix from dcraw, dcraw is using only one color &gt;matrix and this matrix is a copy of one of the matrices of the Adobe DNG Converter. As &gt;described in the chapter "Mapping Camera Color Space to CIE XYZ Space" of the DNG &gt;specification two matrices could be used and as you are already using the Adobe matrices &gt;why not using them completely?</p>
<p>Sure, but how to get this values from Adobe. There are harcoded in DNG converter binary program...</p>
<p>The only way that i can see is to run Adobe DNG convert over a huge RAW file collection and to extract color matrix as well using Exiftool for ex. I suspect that color matrix from dcraw.c have been generated like this, or i miss something ?</p>
<p>&gt; - camera calibration</p>
<p>I have not yet deal with this values. It's on my todo list...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17823"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-17823" class="active">&gt; Sure, but how to get this</a></h3>    <div class="submitted">Submitted by <a href="http://www.bergphoto.org" rel="nofollow">bergi</a> (not verified) on Mon, 2008-09-29 10:42.</div>
    <div class="content">
     <p>&gt; Sure, but how to get this values from Adobe. There are harcoded in DNG converter binary program...</p>
<p>The only way that i can see is to run Adobe DNG convert over a huge RAW file collection and to extract color matrix as well using Exiftool for ex. I suspect that color matrix from dcraw.c have been generated like this, or i miss something ?</p>
<p>I have done it this way and I also think the dcraw matrices were generated this way. I have done this already for some Canon cams. You can find them in nice readable xml/rdf format in the camdefinitions folder of my raw converter. Would be cool to agree to a common format.</p>
<p>Perhaps you already know this page, but there are some more raw examples: <a href="http://www.rawsamples.ch/">http://www.rawsamples.ch/</a></p>
<p>&gt; I have not yet deal with this values. It's on my todo list...<br>
Feel free to ask when you have reached this item on your todo list.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17824"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-17824" class="active">ok. i can see it.</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2008-09-29 11:07.</div>
    <div class="content">
     <p>&gt;I have done it this way and I also think the dcraw matrices were generated this way. I have &gt;done this already for some Canon cams. You can find them in nice readable xml/rdf format in &gt;the camdefinitions folder of my raw converter. Would be cool to agree to a common format.</p>
<p>ok. i can see it. xml is the right way to store these values. It will be fine to use the same file format. But where to store this file ? LibRaw ?</p>
<p>&gt;Perhaps you already know this page, but there are some more raw examples: &gt;http://www.rawsamples.ch/</p>
<p>yes i know. there are also others RAW repositories on the web.</p>
<p>Gilles</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17827"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-17827" class="active">&gt; ok. i can see it. xml is</a></h3>    <div class="submitted">Submitted by <a href="http://www.bergphoto.org" rel="nofollow">bergi</a> (not verified) on Mon, 2008-09-29 15:40.</div>
    <div class="content">
     <p>&gt; ok. i can see it. xml is the right way to store these values. It will be fine to use the same file format. But where to store this file ? LibRaw ?</p>
<p>Yes, LibRaw is in my opinion the right place, but this will be a bigger change. Until now everything is in the source code.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17829"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-17829" class="active">&gt;ok. i can see it. xml is the</a></h3>    <div class="submitted">Submitted by <a href="http://www.libraw.org" rel="nofollow">Alex Tutubalin</a> (not verified) on Tue, 2008-09-30 09:36.</div>
    <div class="content">
     <p>&gt;ok. i can see it. xml is the right way to store these values. It will be fine to use the same file format. But where to store this file ? LibRaw ?</p>
<p>LibRaw will not store _additional_ DNG-related data due to three reasons:</p>
<p>  * Copyright issues. LibRaw already have one set of DNG ColorMatrix tables derived from dcraw code. In this case we rely on Coffin's copyright clearance and free status of dcraw itself.<br>
  * the only programs that needs/uses two color matrices in DNG file are Adobe programs (Lightroom and Camera Raw). If you already use Adobe programs, you can also use free Adobe DNG converter.<br>
  * The 'DNG processing model' (quoting from Adobe staff member in dpreview forums) introduced in DNG 1.2 is much more complex, than in DNG 1.0/1.1. Should LibRaw store all (possibly copyrighted) Adobe color data in source code? I don't think so.</p>
<p>We (LibRaw team) dislike 'DNG processing model' because of very poor image quality produced by Adobe products (try compare with Raw Photo Processor): high noise levels (produced by mixing low noise Green with high noise Red channels), false colors (produced by using ProPhoto as internal space) and poor quality of color profiles (produced by small gamut of color targed used).</p>
<p>In my thoughts, the only place to store DNG-related color data is in DNG converter itself. I've added cam_xyz[] field to LibRaw interface only because it was a very small step (2 lines of code and 5 lines in documentation).</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17830"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-17830" class="active">&gt; * the only programs that</a></h3>    <div class="submitted">Submitted by <a href="http://www.bergphoto.org" rel="nofollow">bergi</a> (not verified) on Tue, 2008-09-30 15:12.</div>
    <div class="content">
     <p>&gt; * the only programs that needs/uses two color matrices in DNG file are Adobe programs (Lightroom and Camera Raw). If you already use Adobe programs, you can also use free Adobe DNG converter.<br>
You can only know about open source raw converters. Until now all open source raw converters are based on dcraw and I think it's wrong to use only one matrix just because dcraw doesn't implement the full DNG standard.</p>
<p>&gt; * The 'DNG processing model' (quoting from Adobe staff member in dpreview forums) introduced in DNG 1.2 is much more complex, than in DNG 1.0/1.1. Should LibRaw store all (possibly copyrighted) Adobe color data in source code? I don't think so.<br>
My extracted color matrices are equal in every digit to the dcraw matrices. I don't know much about the copyright in this situation, but the matrices are extracted from a DNG file (the owner of this DNG file is not Adobe!) and not from the binary. My idea was also not to store it in the source, I'm storing it in a xml/rdf file. This makes it also easier to add your own matrices.</p>
<p>&gt; We (LibRaw team) dislike 'DNG processing model' because of very poor image quality produced by Adobe products (try compare with Raw Photo Processor): high noise levels (produced by mixing low noise Green with high noise Red channels), false colors (produced by using ProPhoto as internal space) and poor quality of color profiles (produced by small gamut of color targed used).<br>
I implemented my own raw converter just because of the nice colors I got from Lightroom. For example dcraw completely ignores the camera calibration. With the DNG model two different cameras from the same model will produce the same colors. I think the noise you are talking about is a result of a bad demosaic filter, dcraw has already a better one, so you don't have to care about it. Small gamut? You know this page? <a href="http://www.brucelindbloom.com/index.html?WorkingSpaceInfo.html">http://www.brucelindbloom.com/index.html?WorkingSpaceInfo.html</a></p>
<p>&gt; In my thoughts, the only place to store DNG-related color data is in DNG converter itself. I've added cam_xyz[] field to LibRaw interface only because it was a very small step (2 lines of code and 5 lines in documentation).<br>
If they are there just for information you are right, but it would be nice if LibRaw would also use them.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17831"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-17831" class="active">&gt; I don't know much about the</a></h3>    <div class="submitted">Submitted by Iliah Borg (not verified) on Tue, 2008-09-30 17:07.</div>
    <div class="content">
     <p>&gt; I don't know much about the copyright in this situation, but the matrices are extracted from a DNG file (the owner of this DNG file is not Adobe!)</p>
<p>Adobe refused to use Nikon white balance extracted and decrypted from NEF files (those also do not belong to Nikon) saying they do not want to violate DMCA.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17836"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-17836" class="active">&gt; Adobe refused to use Nikon</a></h3>    <div class="submitted">Submitted by <a href="http://www.bergphoto.org" rel="nofollow">bergi</a> (not verified) on Wed, 2008-10-01 09:37.</div>
    <div class="content">
     <p>&gt; Adobe refused to use Nikon white balance extracted and decrypted from NEF files (those also do not belong to Nikon) saying they do not want to violate DMCA.<br>
That's a different thing. To extract the white balance you have to decrypt them, the algorithm is unknown, so you have to decompile Nikons binaries.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17840"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-17840" class="active">&gt; To extract the white</a></h3>    <div class="submitted">Submitted by Iliah Borg (not verified) on Thu, 2008-10-02 14:45.</div>
    <div class="content">
     <p>&gt; To extract the white balance you have to decrypt them, the algorithm is unknown, so you have to decompile Nikons binaries.<br>
Well, not exactly. One can simply use dcraw or make a set of planned experiments (like some actually did) to get the way to decipher WB.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-17832"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-17832" class="active">&gt;My idea was also not to</a></h3>    <div class="submitted">Submitted by <a href="http://www.libraw.org" rel="nofollow">Alex Tutubalin</a> (not verified) on Tue, 2008-09-30 19:13.</div>
    <div class="content">
     <p>&gt;My idea was also not to store it in the source, I'm storing it in a xml/rdf file. This makes it also easier to add your own matrices.<br>
External database is a very good idea. </p>
<p>&gt;For example dcraw completely ignores the camera calibration.<br>
You can use camera ICC profile with dcraw.</p>
<p>&gt;Small gamut? You know this page? http://www.brucelindbloom.com/index.html?WorkingSpaceInfo.html<br>
ProPhoto working space has very (very-very) large gamut. But 24-square ColorChecker (used for profiling; also CC24-profiling built in DNG Profile editor) has very small gamut. So, you need to extrapolate your picture data into wide-gamut space using low-gamut profile data. Result is far from perfect.</p>
<p>&gt;If they are there just for information you are right, but it would be nice if LibRaw would also use them.<br>
LibRaw is not for color processing. LibRaw is for RAW data extraction (very small and limited). Of course, without interpolation-postprocessing code RAW data is not useful, so we've included dcraw postprocessing code into LibRaw (as quick and durty solution). This code is 'good enough' (look around: 90% of raw-enabled software uses either unmodified dcraw code or external dcraw binary and users are happy /enough/), we do not plan to extend this code.</p>
<p>The next steps with LibRaw will be<br>
  - get maximum from raw data (masked pixels, de-banding) - work in progress now;<br>
  - get some from exif data - metadata required for raw processing (tone curves, wb settings and so).</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17837"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-17837" class="active">&gt; External database is a very</a></h3>    <div class="submitted">Submitted by <a href="http://www.bergphoto.org" rel="nofollow">bergi</a> (not verified) on Wed, 2008-10-01 10:07.</div>
    <div class="content">
     <p>&gt; External database is a very good idea.<br>
Than we should agree to a common format. My proposal is xml/rdf, you can look into my source for examples.</p>
<p>&gt; ProPhoto working space has very (very-very) large gamut. But 24-square ColorChecker (used for profiling; also CC24-profiling built in DNG Profile editor) has very small gamut. So, you need to extrapolate your picture data into wide-gamut space using low-gamut profile data. Result is far from perfect.<br>
OK, we can do it better if we use our own matrices created with a better color checker. But at the moment dcraw isn't better in this topic, because it's using Adobes matrices.</p>
<p>&gt; LibRaw is not for color processing. LibRaw is for RAW data extraction (very small and limited). Of course, without interpolation-postprocessing code RAW data is not useful, so we've included dcraw postprocessing code into LibRaw (as quick and durty solution). This code is 'good enough' (look around: 90% of raw-enabled software uses either unmodified dcraw code or external dcraw binary and users are happy /enough/), we do not plan to extend this code.<br>
You just don't want to extend the dcraw code or you really don't want to include more postprocessing code? If you would include it you can take my code, you will have to change it in some places because it's based on my metadata model, but it's GPL.</p>
<p>&gt; - get some from exif data - metadata required for raw processing (tone curves, wb settings and so).<br>
Tone curves would be cool. I think Canon has one in the makernotes, but some preprocessing is required to use it with linear gamma.<br>
White balance belongs to the postprocessing, you should take a look at my code.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17838"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-17838" class="active">&gt;Than we should agree to a</a></h3>    <div class="submitted">Submitted by <a href="http://www.libraw.org" rel="nofollow">Alex Tutubalin</a> (not verified) on Wed, 2008-10-01 19:53.</div>
    <div class="content">
     <p>&gt;Than we should agree to a common format. My proposal is xml/rdf, you can look into my source for examples.<br>
In general case, camera color may (and should?) be described in terms of ICC profile. With high quality<br>
profiles you don't need to have two rgb-xyz matrices for different color temperatures and interpolation between these matrices (as in DNG processing model). Camera does not change with light source.</p>
<p>For DNG case you need to store two 3x3 or 3x4 matrices for DNG 1.1 and some another data for DNG 1.2. These data is identified by make/model pair. So, any documented (and extensible) format is good enough. I do not like XML much due to (de)serialization overhead on large data amounts, but for several hundreds of camera models it does not make sense.</p>
<p>&gt;You just don't want to extend the dcraw code or you really don't want to include more postprocessing code?</p>
<p>LibRaw goal is to read and decode RAW files (image data + metadata need for postprocessing phase). dcraw_* postprocessing calls added only for possible 'immediate use' in 3rd-party apps such as digiKam :).</p>
<p>The next phase will be RAW analysis tools (like Rawnalyze)</p>
<p>Full RAW converter (with focus on quality, so with FP-math, accurate ICC code and so on) is not small task. We definitely will do either (extensible) postprocessing library or (extensible) GUI converter or both. Definitely, not in this year.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17839"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-17839" class="active">&gt; In general case, camera</a></h3>    <div class="submitted">Submitted by <a href="http://www.bergphoto.org" rel="nofollow">bergi</a> (not verified) on Thu, 2008-10-02 08:51.</div>
    <div class="content">
     <p>&gt; In general case, camera color may (and should?) be described in terms of ICC profile. With high quality<br>
profiles you don't need to have two rgb-xyz matrices for different color temperatures and interpolation between these matrices (as in DNG processing model). Camera does not change with light source.</p>
<p>I agree, but we should support both because building a big ICC profile database will take some time.</p>
<p>&gt; For DNG case you need to store two 3x3 or 3x4 matrices for DNG 1.1 and some another data for DNG 1.2. These data is identified by make/model pair. So, any documented (and extensible) format is good enough. I do not like XML much due to (de)serialization overhead on large data amounts, but for several hundreds of camera models it does not make sense.</p>
<p>What it is your proposal? What I don't want is a text format like the channels.conf from VDR. There are many different extension, but everyone incompatible to the other. Is a database (SQLite) an option for you?</p>
<p>&gt; LibRaw goal is to read and decode RAW files (image data + metadata need for postprocessing phase). dcraw_* postprocessing calls added only for possible 'immediate use' in 3rd-party apps such as digiKam :).</p>
<p>The next phase will be RAW analysis tools (like Rawnalyze)</p>
<p>Full RAW converter (with focus on quality, so with FP-math, accurate ICC code and so on) is not small task. We definitely will do either (extensible) postprocessing library or (extensible) GUI converter or both. Definitely, not in this year.</p>
<p>I'm working already on a raw converter, but not based on dcraw because I want full control. I also have already tested camera ICC profiles, but the Canon profiles are only lookup tables with a gamma curve applied. I will investigate later more in this topic, but first I want to make my converter more usable. Definitely we should stay in contact.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div></div></div></div></div><a id="comment-17833"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-17833" class="active">Are you sure that no metadata</a></h3>    <div class="submitted">Submitted by Jack (not verified) on Tue, 2008-09-30 20:06.</div>
    <div class="content">
     <p>Are you sure that no metadata is lost in your converter? How did you verify this?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17834"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-17834" class="active">Exiftool and Exiv2...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2008-10-01 07:52.</div>
    <div class="content">
     <p>I check metadata with Exiftool and Exiv2. </p>
<p>Of course not all metadata are not yet backported properlly. But at least 80-90% of original are saved. I use Exiv2 to read original and DNG sdk api to write to DNG. In the future i will use Exiv2 to write in DNG, because Adobe sdk is limited.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17835"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-17835" class="active">My reason to not use DNG is</a></h3>    <div class="submitted">Submitted by Jack (not verified) on Wed, 2008-10-01 09:17.</div>
    <div class="content">
     <p>My reason to not use DNG is just this. Its never sure that all meta is ported correctly, even with adobe converter.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17882"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-17882" class="active">Hello. I work on the Adobe</a></h3>    <div class="submitted">Submitted by Eric Chan (not verified) on Wed, 2008-10-22 23:35.</div>
    <div class="content">
     <p>Hello. I work on the Adobe Camera Raw and Lightroom teams as a computer scientist / engineer. I also work on the DNG SDK, DNG Profile Editor, DNG processing model, DNG spec, etc.</p>
<p>The DNG Converter copies over ALL the tags from the original file (CR2, NEF, ARW, etc.) into the DNG, even the ones it does not understand. This includes private MakerNote information for which Adobe generally has very little information. By copying it over directly, bit for bit, we do not throw out proprietary data.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-17908"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-17908" class="active">thank you   sohbet</a></h3>    <div class="submitted">Submitted by red (not verified) on Sat, 2008-11-01 10:57.</div>
    <div class="content">
     <p>thank you </p>
<p>sohbet</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18045"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-18045" class="active">you are using the color</a></h3>    <div class="submitted">Submitted by Ceejay2005 (not verified) on Tue, 2008-12-16 12:02.</div>
    <div class="content">
     <p>you are using the color matrix from dcraw, dcraw is using only one color matrix and this matrix is a copy of one of the matrices of the Adobe DNG Converter. The calibration matrix can be calculated by comparing the 5200K neutral white balance values from the reference cam against your 5200K values (the same values for both calibration matrices).</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18046"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/373#comment-18046" class="active">Well, I have done it this way</a></h3>    <div class="submitted">Submitted by NJ (not verified) on Tue, 2008-12-16 17:51.</div>
    <div class="content">
     <p>Well, I have done it this way and I also think that the dcraw matrices were generated this way.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
