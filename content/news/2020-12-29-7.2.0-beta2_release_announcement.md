---
date: "2020-12-29T00:00:00"
title: "digiKam 7.2.0-beta2 is released"
author: "digiKam Team"
description: "Dear digiKam fans and users, we are proud to announce the second beta of digiKam 7.2.0."
category: "news"
---

[![](https://i.imgur.com/gh4crgXh.png "digikam 7.2.0-beta2 running unde Apple MacOS BigSur")](https://imgur.com/gh4crgX)

Dear digiKam fans and users,

Just a few words to inform the community that 7.2.0-beta2 is out and ready to test four month late
the [7.2.0 beta1 release](https://www.digikam.org/news/2020-10-29-7.2.0-beta1_release_announcement/).

After integrating the student codes working on faces management while this summer, we have worked to stabilize code and respond
to many user feedbacks about the usability and the performances improvements of faces tagging, faces detection, and faces recognition,
already presented in July with [7.0.0 release announcement](https://www.digikam.org/news/2020-07-19-7.0.0_release_announcement/).

One very important point introduced with this release is the separation of the huge data model files used with face detection and
recognition which are now downloaded on demand at run-time if necessary. This reduces a lot the size of digiKam bundles files published at release times..

Also, we don't forget the Apple users to port the application under last MacOS BigSur, check the compatibility with Roseta2 emulutator
to allow end-users to run digiKam with Silicon based devices, and make the MacOS bundle fully relocatable and more Mac-compliant.

We also fight against plenty of bugs, and after a second long triaging stage, this new version comes with
[more than 225 bug-fixes since last stable release 7.1.0](https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&limit=0&o1=equals&order=bug_id&product=digikam&v1=7.2.0).
We are now close to the final digiKam release planned officially in January or February 2021. See the release plan for more details.

Thanks to all users for [your support and donations](https://www.digikam.org/donate/),
and to all contributors, students, testers who allowed us to improve this release.

digiKam 7.2.0-beta2 source code tarball, Linux 32/64 bits AppImage bundles, MacOS Intel package, and Windows 32/64 bits installers
can be downloaded from [this repository](https://download.kde.org/unstable/digikam/).
Don't forget to not use this beta in production yet and thanks in advance for your feedback in Bugzilla.

Rendez-vous in a few weeks for the final stage and we hope for a happy new year 2021 using digiKam.
