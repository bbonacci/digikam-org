---
date: "2010-03-22T21:00:00Z"
title: "digiKam with another database backend than SQLite"
author: "Holger Foerster"
description: "Last summer I saw bug #127321 an thought, this can be done by me (partly at least). After some (successful) prototypic implementation, most of the"
category: "news"
aliases: "/node/509"

---

<p>Last summer I saw <a href="https://bugs.kde.org/show_bug.cgi?id=127321">bug #127321</a> an thought, this can be done by me (partly at least).<br>
After some (successful) prototypic implementation, most of the time where spent into smooth integration in the existing<br>
project:</p>
<ul>
<li>adding a solution for migrating from SQLite to MySQL and vice versa</li>
<li>add possibiliy to start an own database server</li>
<li>and bug fixing...</li>
</ul>
<p>Because only a few persons trying out this work (which resides atm. in a dedicated branch), the most bugs are possible undiscovered.</p>
<p>So if you are brave enough to crash your whole existing database structure (images are not touched), have a little amount of free time or you would like<br>
to have the future right now and you are able to compile your version by yourself: You are the right person :)</p>
<p>(For the last point: If you are not yet familiar on how to test current development code, this page gives you a short introduction how to get and compile digiKam from source.<br>
For reporting bugs please use the normal KDE bug tracker.</p>
<p>The dedicated branch is located at <a href="http://websvn.kde.org/branches/extragear/graphics/digikam/1.0-databaseext/">this url from KDE svn repository</a></p>
<p>Assume you compiled your version and would like to test digikam:<br>
After the first start, your existing database settings will be converted to an extended format.<br>
After that, your are using digiKam in SQLite mode, it should work like before (from users point of view).</p>
<p>To switch to MySQL mode:</p>
<ul>
<li>go to the database settings (Settings -&gt; Configure Digikam -&gt; Database)
<p><a href="http://www.flickr.com/photos/48638687@N08/4450628678/" title="Database Settings von Hamsi2k bei Flickr"><img src="http://farm5.static.flickr.com/4057/4450628678_b1ff6d4a87.jpg" width="500" height="422" alt="Database Settings"></a></p>
</li><li>at the database type combobox, choose QMYSQL as backend.</li>
<li>now your have 2 options:</li>
<p>    -&gt; use an existing database management server where you have created a dedicated account<br>
	- fill in all needed data for that<br>
    -&gt; use an internal database management server<br>
	- this mode works like the internal akonadi database server mode (thanks goes to the akonadi people - great work!), but has a little bit improved<br>
	- only check the "Internal Server" checkbox</p>
<li>press "Check connections" - A dialog should show you, that the connection could be established with the current settings.</li>
<li>press "OK"</li>
<p>  The new settings should applied. You are ready to test =).
</p></ul>
<p>To copy your SQLite data to your MySQL database do the following:</p>
<ul>
<li>go to the migration dialog (Tools -&gt; Database Migration)
<p><a href="http://www.flickr.com/photos/48638687@N08/4454457569/" title="Database Migration Dialog von Hamsi2k bei Flickr"><img src="http://farm5.static.flickr.com/4069/4454457569_003468a126.jpg" width="500" height="445" alt="Database Migration Dialog"></a></p></li>
<li>As source (left side) select your SQLite database</li>
<li>As target (right side) type in your MySQL database connection database</li>
<li>Press the "Migrate -&gt;" button and enjoy the progress on the bottom of the dialog</li>
</ul>
<p>Notes:<br>
1) [General] If the connection is down, digiKam stops its database actions, and tries to reconnect.</p>
<p><a href="http://www.flickr.com/photos/48638687@N08/4450628776/" title="Information about connection errors von Hamsi2k bei Flickr"><img src="http://farm5.static.flickr.com/4016/4450628776_d50d07d7b6.jpg" width="496" height="304" alt="Information about connection errors"></a></p>
<p>   If the database link appears again, digiKam continues it's work.<br>
   You can cancel this waiting behaviour but then you have only limitied access (almost nothing) to the digikm application.<br>
   (But you can change the database settings, in case that the host has changed or something else.)<br>
2) [Internal Database Server] The digiKam database server is started only once per system. It registers on D-Bus and watches all digiKam<br>
   instances of the same user. It stops, if these instances are closed.</p>
<p>Background information:<br>
It's easy to add a new database backend like PostgreSQL to digiKam, because most of the sql statements are moved out from code to a configuration file<br>
(which can be extended on demand). It exists only specific code for the database server and configuration dialog.</p>
<p>Finally: Thanks goes to the great digiKam team and Marcel in particular, which gaves many ideas, hints and coding examples.</p>

<div class="legacy-comments">

  <a id="comment-19077"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/509#comment-19077" class="active">Advantages?</a></h3>    <div class="submitted">Submitted by <a href="http://flickr.com/photos/paurullan" rel="nofollow">paurullan</a> (not verified) on Mon, 2010-03-22 22:46.</div>
    <div class="content">
     <p>What uses does having a MySQL backend? It is faster or more scalable?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19078"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/509#comment-19078" class="active">Re: Advantages?</a></h3>    <div class="submitted">Submitted by Elv13 (not verified) on Mon, 2010-03-22 23:52.</div>
    <div class="content">
     <p>Well, if you have one running, why wasting ressource. On top of that, SQLite have huge regressions with recent kernel according to phoronix (I think it is one of the only test that can be trusted out of the whole PTS test suite).</p>
<p>The reason I always use Amarok MySQL capabilities is because it allow me to have multiple account connected simultaneously to the same DB and work. Of course, it is not really the case with Digikam, as the DB was stored in the picture folder. But some people would be able to delete it because it does not look like a picture (and it is not one) so they might wonder what that huge file is doing there and delete it.</p>
<p>But MySQL is faster and can unload CPU+I/O from my desktop and use my external DB server, so I will test, use and debug this feature when I will have some time.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19153"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/509#comment-19153" class="active">CPU+I/O?</a></h3>    <div class="submitted">Submitted by <a href="http://www.ds-cartes.com" rel="nofollow">r4 ds</a> (not verified) on Wed, 2010-04-21 18:01.</div>
    <div class="content">
     <p>mysql is running?</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19084"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/509#comment-19084" class="active">In my case it's the</a></h3>    <div class="submitted">Submitted by Holger Foerster on Tue, 2010-03-23 19:16.</div>
    <div class="content">
     <p>In my case it's the flexibility you get when using an "real" dbms. You can place pictures and database on a central media pc (NAS). All computers in your home network can connect to the database and share its metadata.<br>
Also you can access your data with third-party software.<br>
Maybe you want extract some metadata from it, or just want to perform custom queries.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19079"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/509#comment-19079" class="active">href?</a></h3>    <div class="submitted">Submitted by Francesco R. (not verified) on Tue, 2010-03-23 03:12.</div>
    <div class="content">
     <p>location of the branch "database-ext"?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19082"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/509#comment-19082" class="active">is there...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2010-03-23 09:13.</div>
    <div class="content">
     <p>http://websvn.kde.org/branches/extragear/graphics/digikam/1.0-databaseext/</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19097"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/509#comment-19097" class="active">svn</a></h3>    <div class="submitted">Submitted by maderios (not verified) on Sun, 2010-03-28 11:43.</div>
    <div class="content">
     <p>Hi<br>
I tried this and it doesnt work :<br>
svn co http://websvn.kde.org/branches/extragear/graphics/digikam/1.0-databaseext/digikam/</p>
<p>Repository moved permanently to '/branches/extragear/graphics/digikam/1.0-databaseext/digikam/'; please relocate</p>
<p>Thanks for any help<br>
M</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19098"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/509#comment-19098" class="active">Oh, I forgot to link the</a></h3>    <div class="submitted">Submitted by Holger Foerster on Mon, 2010-03-29 06:30.</div>
    <div class="content">
     <p>Oh, I forgot to link the Howto compile page: </p>
<p><a href="http://www.digikam.org/drupal/download/KDE4">Howto</a></p>
<p>As svn checkout just use the following command:<br>
svn co svn://anonsvn.kde.org/home/kde/branches/extragear/graphics/digikam/1.0-databaseext/</p>
<p>Hope, this helps.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19110"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/509#comment-19110" class="active">svn and future release</a></h3>    <div class="submitted">Submitted by maderios (not verified) on Tue, 2010-03-30 14:27.</div>
    <div class="content">
     <p>Thanks, it works, with some bugs, but it works. I think mysql db is useful for huge amount of pictures. I'm interested. Have you an idea about a release date ?<br>
M</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19112"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/509#comment-19112" class="active">end of may 2010</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2010-03-30 14:48.</div>
    <div class="content">
     <p>digiKam 1.3.0, including MySQL support is planed for 30 may 2010.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div><a id="comment-19119"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/509#comment-19119" class="active">now, code has been migrated to trunk</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2010-03-31 12:58.</div>
    <div class="content">
     <p>Mysql support code is now merged to KDE svn trunk. No need to use dedicated branche.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19080"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/509#comment-19080" class="active">Now someone please stand up</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2010-03-23 07:24.</div>
    <div class="content">
     <p>Now someone please stand up and centralise all db stuff to only one backend. Right now we have one for akonadi, one for nepomuk/strigi, one for amarok, and now there is one for digikam. I know i can configure them to use the same mysql server, but why not do that out of the box? It's some coordination issue between the teams?</p>
<p>(p.s. this captcha sucks big time)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19081"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/509#comment-19081" class="active">Exactly!</a></h3>    <div class="submitted">Submitted by Andre (not verified) on Tue, 2010-03-23 08:58.</div>
    <div class="content">
     <p>Exactly!</p>
<p>My first thought was: why would you create a dedicated MySql &lt;-&gt; SQLite converter program inside digicam? Isn't something usable already out there? More and more applications interface with databases, and it seems that each of them is writing their own abstraction layer or special casing to work with them. The Qt drivers are not enough, as they don't support database structure manipulation in a database agnostic way. I know, because I am writing my own set of Qt database drivers to support this as well...</p>
<p>Perhaps it is time to bundle these efforts in KDE, and create:<br>
1) A set of extended database drivers that allow database creation and database structure manipulation (create and modify tables and views) in a database-independent way.<br>
2) A common database connection/selection widget<br>
3) A program that can move application databases between database backends, perhaps based on some application-defined script, and perhaps<br>
4) A more friendly API to create database-independent queries on top of the current Qt API, which is a little inflexible and not very nice to use.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19085"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/509#comment-19085" class="active">&gt;My first thought was: why</a></h3>    <div class="submitted">Submitted by Holger Foerster on Tue, 2010-03-23 19:35.</div>
    <div class="content">
     <p>&gt;My first thought was: why would you create a dedicated MySql &lt;-&gt; SQLite converter program inside digicam? Isn't something usable already out there?</p>
<p>Yes and no. There are good database convertion tools around, think on   <a href="http://www.dbunit.org/intro.html">DBUnit</a> which is at first a tool for testing, but does a good job to transfer data from one database to another (of another vendor).<br>
I first also thought it where easy to switch the database backends. But since there are features in every dbms which are very useful (both in convenience and performance) but not SQL standard, I had to find a solution, which results in two similar but not exactly the schemas and statements for the database storage. With the current solution, like I wrote in my blog entry, It is possible to add another SQL backend where the effort is lower because it is designed for that.</p>
<p>&gt;[...]Perhaps it is time to bundle these efforts in KDE, and create: Point 1-4[...]<br>
I agree, such thing would be useful!! I would like such an set of tools.<br>
But call me a narrow-minded person, I think that we should do that in small steps.<br>
The feature request where opened in 2006. It's 2010 and we have basic independed database access.<br>
Now we can collect expirience and based on that, sometime and someone (person or team) will create a nice tool/library collection.<br>
Maybe you can open a feature request for this interesting issue on <a href="https://bugs.kde.org/">B.K.O.</a> then it will be kept in mind.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19099"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/509#comment-19099" class="active">For me it is incredibly slow,</a></h3>    <div class="submitted">Submitted by Andi Clemens on Mon, 2010-03-29 06:46.</div>
    <div class="content">
     <p>For me it is incredibly slow, much much slower than SQLite with EXT4 and barriers turned on (the bottleneck on my system when it comes to digiKam).<br>
Somehow some threads are always blocked and therefore digiKam (or someone else) always waits 10 seconds before it continues to work.<br>
After some operations, something is locked again and digiKam waits another 10 seconds (you can see this information in the console window). I mainly tested "Scan for new images", because I added a new directory for testing...<br>
I used the internal MySQL-Server for testing...</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19141"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/509#comment-19141" class="active">This is the best news this</a></h3>    <div class="submitted">Submitted by Frank (not verified) on Sat, 2010-04-10 11:26.</div>
    <div class="content">
     <p>This is the best news this year!!<br>
I've been waiting for this for more then 2 years now since I want to share the metadata with my girlfriend which is so annoying right now (like rereading metadata every now and then...).</p>
<p>P.S.: The CAPTCHA are really difficult =)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19149"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/509#comment-19149" class="active">IMHO better to use BercleyDB</a></h3>    <div class="submitted">Submitted by Olleg (not verified) on Mon, 2010-04-19 09:39.</div>
    <div class="content">
     <p>IMHO better to use BercleyDB or other file DB, rather then SQL server for this purpose.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19152"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/509#comment-19152" class="active">Images in DB?</a></h3>    <div class="submitted">Submitted by PJ (not verified) on Wed, 2010-04-21 15:35.</div>
    <div class="content">
     <p>What about putting the images and thumbnails in the database, too?</p>
<p>Does this work?</p>
<p>It would be nice to be able tu use these easily for creating one's own web applications or special scriptings.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>