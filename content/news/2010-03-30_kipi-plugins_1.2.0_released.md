---
date: "2010-03-30T12:24:00Z"
title: "kipi-plugins 1.2.0 released..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce Kipi-plugins 1.2.0 ! kipi-plugins tarball can be downloaded from SourceForge at this url"
category: "news"
aliases: "/node/512"

---

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce Kipi-plugins 1.2.0 !</p>

<a href="http://www.flickr.com/photos/47251968@N04/4338420625/" title="pw upload3 by tschensinger, on Flickr"><img src="http://farm5.static.flickr.com/4049/4338420625_bd8e612b4b.jpg" width="500" height="397" alt="pw upload3"></a>

<p>kipi-plugins tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/kipi/files">at this url</a></p>

<p>Kipi-plugins will be also available for Windows. Precompiled packages can be donwloaded with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<p>See below the list of new features and bugs-fix coming with this release:</p>

<p><b>NEW FEATURES:</b></p>

PicasaWebExport : Tool dialog redesigned with same layout than Flickr, Facebook, etc.<br>
HTMLExport      : Show screenshot alongside theme description.<br>
PiwigoExport    : New tool to export images to Piwigo server (aka PhpWebGallery).<br>
HTMLExport      : Get metadata from image files and new detailed theme.<br><br>

<a href="http://www.flickr.com/photos/34965557@N04/4392072101/" title="screenshot1 by giasoneregna, on Flickr"><img src="http://farm5.static.flickr.com/4024/4392072101_9238762f74.jpg" width="500" height="333" alt="screenshot1"></a>

<a href="http://www.flickr.com/photos/47673652@N02/4385033317/" title="Piwigo plugin in Digikam by fcoiffie, on Flickr"><img src="http://farm5.static.flickr.com/4062/4385033317_d57a6c86b9.jpg" width="500" height="410" alt="Piwigo plugin in Digikam"></a>

<p><b>BUGFIXES FROM KDE BUGZILLA:</b></p>

001 ==&gt; 225001 : GPSSync         : Google map is empty.<br>
002 ==&gt; 225162 : PicasaWebExport : Picasaweb export no progress bar.<br>
003 ==&gt; 225164 : PicasaWebExport : Picasaweb export waiting mouse cursor without ending.<br>
004 ==&gt; 203541 : PicasaWebExport : Select images to upload.<br>
005 ==&gt; 225316 : General         : ExpoBlend won't compile even with dependencies all right.<br>
006 ==&gt; 225396 : SendImages      : "Mozilla" MUA option is broken.<br>
007 ==&gt; 223492 : HtmlExport      : Show screenshot alongside theme description.<br>
008 ==&gt; 225407 : DNGConverter    : Dngconverter fix endian detection [patch].<br>
009 ==&gt; 225161 : PicasaWebExport : Picasaweb export does not export photo geotag.<br>
010 ==&gt; 225665 : PicasaWebExport : digiKam crashed when cancelling transfer to Picasa.<br>
011 ==&gt; 225646 : PicasaWebExport : Picasaweb does not export movie.<br>
012 ==&gt; 196593 : PicasaWebExport : EXIF Geotags not recognised by Picasa.<br>
013 ==&gt; 226799 : Libkdcraw       : Libkdcraw should use all CPU cores when converting images.<br>
014 ==&gt; 224872 : PiwigoExport    : Export plugin for Piwigo.<br>
015 ==&gt; 226916 : HTMLExport      : Get metadata from image files and new detailed theme.<br>
016 ==&gt; 217539 : PicasaWebExport : picasaweb ability to choose add or update photo.<br>
017 ==&gt; 227920 : RemoveRedEyes   : Cannot compile: opencv/highgui.h missing for removeredeyes plugin.<br>
018 ==&gt; 219603 : SendImages      : Renaming images to send.<br>
019 ==&gt; 229068 : PicasaWebExport : digiKam repeatedly crashes when uploading several pictures to Picasaweb.<br>
020 ==&gt; 229826 : PicasaWebExport : PicasaWeb Export won't login with certain chars in password.<br>
021 ==&gt; 196236 : FaceBookExport  : Facebook upload unreliable, and fails in unsafe fashion.<br>
022 ==&gt; 229169 : PrintAssistant  : Print Assistant on the page preview shows screen corruption.<br>
023 ==&gt; 221298 : FlickrExport    : digiKam segfaults on Flickr export.<br>
024 ==&gt; 213921 : DngConverter    : DNG Converter lost exif info about ISO.<br>
025 ==&gt; 228524 : JPEGLossLess    : Failed to rotate image [patch].<br>
026 ==&gt; 232339 : SimpleViewer    : Wrong flash export url in digiKam.<br>



<div class="legacy-comments">

  <a id="comment-19111"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/512#comment-19111" class="active">DngConverter</a></h3>    <div class="submitted">Submitted by Yngo (not verified) on Tue, 2010-03-30 14:43.</div>
    <div class="content">
     <p>Has the wb problem with Canon dslr been fixed? My Xti (500D) rawfiles result in dng with no useable camera wb when opened with ShowFoto or ufraw.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19113"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/512#comment-19113" class="active">it must be...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2010-03-30 14:49.</div>
    <div class="content">
     <p>following this report : 213921 : DngConverter : DNG Converter lost exif info about ISO.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19114"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/512#comment-19114" class="active">But ....</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2010-03-30 17:20.</div>
    <div class="content">
     <p>But that's about ISO information, isn't it?. The question is about white balance, as per this bug:<br>
Bug 191907 -  DNG Converter damages white balance metadata</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19123"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/512#comment-19123" class="active">yes, i see this bug report...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2010-04-02 10:41.</div>
    <div class="content">
     <p>Please checkout current code from svn trunk (future kipi-plugins 1.3.0), where Jens has updated Adobe DNG sdk from 1.1.0 to 1.3.0. Perhaps it solve the problem. Thanks in advance...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19124"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/512#comment-19124" class="active">Looks like it's fixed!</a></h3>    <div class="submitted">Submitted by john_st (not verified) on Fri, 2010-04-02 23:50.</div>
    <div class="content">
     <p>Just compiled the SVN version as you suggest, and the camera white balance is now correct. Thanks for that.</p>
<p>Regards, John</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div><a id="comment-19117"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/512#comment-19117" class="active">ensure digikam uses latest kipi plugins</a></h3>    <div class="submitted">Submitted by Andreas Bank (not verified) on Wed, 2010-03-31 12:35.</div>
    <div class="content">
     <p>Hi Gilles,</p>
<p>first I want to thank you for your great work! THANKS A LOT!<br>
Now my question:<br>
How can I assure that digikam uses the lates kipi plugins?<br>
Digikam-&gt;Help-&gt;Komponent information shows libkipi Version = 0.4.0<br>
Is there a way to update libkipi?<br>
Adept tells me that "Kipi Plugins 1.0.0-1ubuntu1~keramic1" and "libkipi6 4:4.3.5-0ubuntu1~keramic1" are installed.</p>
<p>All the best!</p>
<p>Andi</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19118"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/512#comment-19118" class="active">libkipi != kiipi-plugins</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2010-03-31 12:57.</div>
    <div class="content">
     <p>libkipi is a shared lib to use plugins<br>
kipi-plugins is tools package.</p>
<p>There is no way for the moment to get kipi-plugins version to host application.</p>
<p>Solution : open a tool as DNG converter and go to Help/About dialog.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19121"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/512#comment-19121" class="active">problem with make</a></h3>    <div class="submitted">Submitted by devil77 (not verified) on Fri, 2010-04-02 10:25.</div>
    <div class="content">
     <p>Hello,</p>
<p>i want to install kipi-plugins. cmake show no error but make bring me the following message.</p>
<p><code><br>
In file included from /home/Downloads/kipi/advancedslideshow/commoncontainer.cpp:25:<br>
/home/Downloads/kipi/advancedslideshow/commoncontainer.h:39:31: error: Phonon/VolumeSlider: No such file or directory<br>
In file included from /home/Downloads/kipi/advancedslideshow/commoncontainer.cpp:25:<br>
/home/Downloads/kipi/advancedslideshow/commoncontainer.h:127: error: ‘Phonon’ has not been declared<br>
/home/Downloads/kipi/advancedslideshow/commoncontainer.h:127: error: expected ‘{’ before ‘VolumeSlider’<br>
/home/Downloads/kipi/advancedslideshow/commoncontainer.h:128: error: invalid type in declaration before ‘{’ token<br>
/home/Downloads/kipi/advancedslideshow/commoncontainer.h:128: warning: extended initializer lists only available with -std=c++0x or -std=gnu++0x<br>
/home/Downloads/kipi/advancedslideshow/commoncontainer.h:130: error: expected primary-expression before ‘public’<br>
/home/Downloads/kipi/advancedslideshow/commoncontainer.h:130: error: expected ‘}’ before ‘public’<br>
/home/Downloads/kipi/advancedslideshow/commoncontainer.h:130: error: expected ‘,’ or ‘;’ before ‘public’<br>
/home/Downloads/kipi/advancedslideshow/commoncontainer.h:135: error: expected declaration before ‘}’ token<br>
make[2]: *** [advancedslideshow/CMakeFiles/kipiplugin_advancedslideshow.dir/commoncontainer.o] Fehler 1<br>
make[1]: *** [advancedslideshow/CMakeFiles/kipiplugin_advancedslideshow.dir/all] Fehler 2<br>
make: *** [all] Fehler 2<br>
</code></p>
<p>can anybody help me to solve this problem for installation?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19122"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/512#comment-19122" class="active">Look this bugzilla entry...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2010-04-02 10:37.</div>
    <div class="content">
     <p>https://bugs.kde.org/show_bug.cgi?id=224379</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-19125"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/512#comment-19125" class="active">Kipi-plugin PPA?</a></h3>    <div class="submitted">Submitted by <a href="http://rlog.rgtti.com/" rel="nofollow">Romano</a> (not verified) on Sat, 2010-04-03 12:53.</div>
    <div class="content">
     <p>Great news. Thanks a lot. </p>
<p>Short and easy question: I have installed digiKam 1.2 via PPA on my Ubuntu Karmic... is there a PPA for Kipi-plugins? </p>
<p>Thanks again!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19136"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/512#comment-19136" class="active">Export from ShowFoto</a></h3>    <div class="submitted">Submitted by Yngo (not verified) on Thu, 2010-04-08 00:36.</div>
    <div class="content">
     <p>I dunno if this is covered from the kipi-plugins side but this is the question: Is it possible to enable exporting images from ShowFoto directly to Flickr/23 instead of using DigiKam? With ShowFoto I could just select a bunch of images in Dolphin, open them with ShowFoto and export them, with DigiKam I have to move them to a folder covered in DigiKam or include my temp folder to the database and remove them afterwards cause I only need them for upload and delete then later. In a quick way tags could be added just for upload if there are no before without going to the full DigiKam database.<br>
On another node, if I upload png images they get converted to jpg. I dunno if this is a bug or feature but is there a way to switch them off?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19143"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/512#comment-19143" class="active">Use Gwenview in this case</a></h3>    <div class="submitted">Submitted by Buchan Milne (not verified) on Mon, 2010-04-12 13:19.</div>
    <div class="content">
     <p>Browse the directory of photos with Gwenview, select the photos you want to upload, and use Plugins-&gt;Export-&gt;Export to Facebook</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19145"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/512#comment-19145" class="active">Pixelpipe?</a></h3>    <div class="submitted">Submitted by <a href="http://eothred.wordpress.com" rel="nofollow">Yngve I. Levinsen</a> (not verified) on Thu, 2010-04-15 06:52.</div>
    <div class="content">
     <p>Dear beloved developers,</p>
<p>Would it be possible to add an export plugin to the pixelpipe service? Pixelpipe currently is a "middle layer" to export to more than 100 different services (of some you already support with dedicated plugins of course). This way you would need to write only one plugin in order to support all these services to some extent (for upload at least, don't think you can download but would be great anyway). See pixelpipe.com for more information.</p>
<p>If this plugin becomes available I promise to make a small donation to the digikam project! :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19146"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/512#comment-19146" class="active">interesting...</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2010-04-15 07:44.</div>
    <div class="content">
     <p>Please fill a new file in KDE bugzilla about Pixelpipe, into kipiplugins component. It sound very interesting to support. If API are available, please join urls for developers. Thanks in advance.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19148"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/512#comment-19148" class="active">done :)</a></h3>    <div class="submitted">Submitted by <a href="http://eothred.wordpress.com" rel="nofollow">Yngve I. Levinsen</a> (not verified) on Sun, 2010-04-18 16:59.</div>
    <div class="content">
     <p>https://bugs.kde.org/show_bug.cgi?id=234726</p>
<p>I submitted it as a wishlist item but then somehow it changed to a normal bug and now I don't figure out how to change it back.. Anyway, hope it wont be too much work to write a plugin... :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div>
</div>