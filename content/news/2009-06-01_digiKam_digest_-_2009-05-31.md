---
date: "2009-06-01T12:57:00Z"
title: "digiKam digest - 2009-05-31"
author: "Anonymous"
description: "roadmap to next release, finally 1.0! usability fixes attempt to speed up loading of thumbnails by compromise on quality, after tests idea was mostly rejected;"
category: "news"
aliases: "/node/452"

---

<style>
tr.bugs { margin: 5px; border: solid white 1px; text-align: center }
td { margin: 3px; padding: 3px;}
</style><p>
<a href="http://www.flickr.com/photos/digikam/3584842114/" title="rotateoverlay by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2430/3584842114_c64e42cfbb_m.jpg" width="240" height="150" alt="rotateoverlay"></a></p>
<ul>
<li>roadmap to next release,  <a href="http://www.digikam.org/drupal/about/releaseplan">finally 1.0</a>!</li>
<li>usability fixes</li>
<li>attempt to speed up loading of thumbnails by compromise on quality,</li>
<li>after tests idea was mostly rejected; implemented "cheat scaling" for</li>
<li>generation of thumbnails</li>
<li>instead beginning of thumbnails cache separate from .thumbnails standard:</li>
<ul>
<li>should give faster loading</li>
<li>thumbnails for off-line storage</li>
</ul>

<li>new statistics tool</li>
<li>new tool for logs of camera operations</li>
<li>many bug and regression fixes in new Qt4 list view model</li>
<li>usability and interface fixes in Batch Queue Manager</li>
<li>preparation for overlays in Albums GUI</li>
<li>fixed auto-crop issues in Free Rotation plugin</li>
</ul>
<p><a href="http://www.flickr.com/photos/digikam/3572850378/" title="bdstatdlg by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3615/3572850378_b31eed9f6d_m.jpg" width="240" height="192" alt="bdstatdlg"></a></p>
<p><code><br>
------------------------------------------------------------------------</code></p>
<p>Bug/wish count</p>
<table>
<tbody><tr class="bugs">
<td></td>
<td>Opened bugs</td>
<td>Opened last week</td>
<td>Closed last week</td>
<td>Change</td>
<td>Opened wishes</td>
<td>Opened last week</td>
<td>Closed last week</td>
<td>Change</td>
</tr>
<tr class="bugs">
<td>digikam</td>
<td>226</td>
<td>+8</td>
<td>-10</td>
<td>-2</td>
<td>269</td>
<td>+6</td>
<td>-5</td>
<td>1</td>
</tr>
<tr class="bugs">
<td>kipiplugins</td>
<td>108</td>
<td>+5</td>
<td>-0</td>
<td>5</td>
<td>139</td>
<td>+2</td>
<td>-1</td>
<td>1</td>
</tr>
</tbody></table>
<p>Full tables:<br>
<a href="https://bugs.kde.org/component-report.cgi?product=digikam">digiKam</a><br>
<a href="https://bugs.kde.org/component-report.cgi?product=kipiplugins">KIPI-plugins</a></p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 972588 by aclemens:</p>
<p>Add example to the tooltip, too, to make it more clear how to use the<br>
'#' character for example.</p>
<p> M  +9 -7      manualrenameinput.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 972480 by cgilles:</p>
<p>optimize QImage scaled 2nd pass to use fast transform instead smooth<br>
transform. Quality still here. [MM: later reverted]<br>
BUG: 193967</p>
<p> M  +7 -8      thumbnailcreator.cpp<br>
 M  +3 -0      thumbnailcreator_p.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 972580 by cgilles:</p>
<p>Fix important bug in Camera gui:</p>
<p>- Race condition: Only show delete camera item dialog when all items are<br>
  really downloaded, to ask confirmation<br>
- Factorize items deletion code to common method to avoid redondant<br>
  implementation.<br>
- Only delete items which have been downloaded and not only current<br>
  selection used to perform download.<br>
- If items are skipped during download, take a care about and do not<br>
  process deletion of.</p>
<p>BUG: 171950</p>
<p> M  +65 -81    cameraui.cpp<br>
 M  +3 -0      cameraui.h<br>
 M  +2 -0      cameraui_p.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973443 by cgilles:</p>
<p>new generic history view widget</p>
<p> AM            dhistoryview.cpp   [License: GPL (v2+)]<br>
 AM            dhistoryview.h   [License: GPL (v2+)]</p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973434 by cgilles:</p>
<p>as Camera GUI, pop-up windows over taskbar bar when Batch Queue Manager<br>
is complete</p>
<p> M  +3 -0      queuemgrwindow.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973428 by cgilles:</p>
<p>added history time column.<br>
added pop-up menu to copy and paste history contents to clipboard</p>
<p> M  +46 -4     logview.cpp<br>
 M  +2 -0      logview.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973326 by mwiesweg:</p>
<p>Add support for an incremental refresh.<br>
When the album remains the same but some images are added or removed we<br>
shall not reset the model. Instead changes are registered and proper<br>
rowsInserted() / rowsRemoved() signals are emitted.</p>
<p> M  +135 -5    imagemodel.cpp<br>
 M  +5 -0      imagemodel.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973119 by cgilles:</p>
<p>pop-up passive windows under taskbar to bring user when all downloading<br>
operations are done.<br>
BUGS: 193894</p>
<p> M  +10 -1     cameraui.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973071 by cgilles:</p>
<p>change cursor over history item if link to camera icon view is valid</p>
<p> M  +15 -0     logview.cpp<br>
 M  +4 -0      logview.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973039 by cgilles:</p>
<p>register folder and file info from controller to history view<br>
click on history entry move focus to right item from camera icon view</p>
<p> M  +16 -16    cameracontroller.cpp<br>
 M  +4 -4      cameracontroller.h<br>
 M  +16 -8     cameraui.cpp<br>
 M  +3 -2      cameraui.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973032 by cgilles:</p>
<p>Camera Gui : new download history log view to simplify camera operations<br>
analysis.<br>
TODO : click on history entry must move camera icon view to right item.<br>
BUG: 158374</p>
<p> M  +37 -7     cameraui.cpp<br>
 M  +2 -0      cameraui.h<br>
 M  +2 -1      cameraui.rc<br>
 M  +5 -0      cameraui_p.h<br>
 AM            logview.cpp   [License: GPL (v2+)]<br>
 AM            logview.h   [License: GPL (v2+)]</p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 972967 by cgilles:</p>
<p>Batch Queue Manager : improve busy state notification on all view</p>
<p> M  +9 -2      assignedlist.cpp<br>
 M  +1 -0      assignedlist.h<br>
 M  +2 -0      queuelist.cpp<br>
 M  +6 -5      queuemgrwindow.cpp<br>
 M  +9 -0      queuepool.cpp<br>
 M  +2 -0      queuepool.h<br>
 M  +6 -0      queuesettingsview.cpp<br>
 M  +2 -0      queuesettingsview.h<br>
 M  +6 -0      toolsettingsview.cpp<br>
 M  +2 -0      toolsettingsview.h<br>
 M  +2 -1      toolslistview.cpp<br>
 M  +9 -0      toolsview.cpp<br>
 M  +2 -0      toolsview.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973809 by mwiesweg:</p>
<p>Move functionality from ItemViewHoverButton's virtual methods to<br>
AbstractWidgetDelegateOverlay, which uses an event filter to be able to<br>
use non-subclassed widgets.<br>
Rename HoverWidgetDelegateOverlay to HoverButtonDelegateOverlay, it<br>
takes a button.</p>
<p> M  +62 -8     digikam/imagedelegateoverlay.cpp<br>
 M  +6 -2      digikam/imagedelegateoverlay.h<br>
 M  +3 -49     libs/widgets/common/itemviewhoverbutton.cpp<br>
 M  +0 -4      libs/widgets/common/itemviewhoverbutton.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973812 by mwiesweg:</p>
<p>Bringing back the rate-on-hover widget in icon view.<br>
Currently still disabled, because some bits are missing</p>
<p> M  +1 -0      CMakeLists.txt<br>
 A             digikam/imageratingoverlay.cpp   [License: GPL (v2+)]<br>
 A             digikam/imageratingoverlay.h   [License: GPL (v2+)]</p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973688 by mwiesweg:</p>
<p>Split the base class for showing buttons as overlay on the icon view to<br>
have a more generic base class not confined to buttons (in preparation<br>
for bringing the live rating widget back)</p>
<p> M  +62 -26    imagedelegateoverlay.cpp<br>
 M  +35 -8     imagedelegateoverlay.h<br>
 M  +7 -7      imageselectionoverlay.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973689 by mwiesweg:</p>
<p>Add a method to retrieve statistics about image formats in db.<br>
Returns a map format -&gt; count.<br>
"Format" is as specified in the DB schema docs. Example:</p>
<p>QMap(("JPG", 25184)("MOV", 2)("MPEG", 2)("PNG", 95)("PPM", 2)("RAW-ARW",<br>
2) ("RAW-CR2", 7)("RAW-CRW",7)("RAW-DCR", 1)("RAW-DNG", 2)("RAW-MRW",<br>
8)("RAW-NEF", 6) ("RAW-ORF", 1)("RAW-PEF", 2)("RAW-RAF", 4)("RAW-RW2",<br>
2)("RAW-X3F", 1)("TIFF", 26))</p>
<p> M  +18 -0     albumdb.cpp<br>
 M  +6 -0      albumdb.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973571 by cgilles:</p>
<p>separate progress bar and count of tasks/items</p>
<p> M  +10 -3     queuemgrwindow.cpp<br>
 M  +3 -0      queuemgrwindow_p.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973496 by cgilles:</p>
<p>when user click on Batch Queue Manager History entry, Queue list and<br>
processed item relevant are selected.</p>
<p> M  +4 -4      queuelist.cpp<br>
 M  +2 -1      queuelist.h<br>
 M  +21 -0     queuemgrwindow.cpp<br>
 M  +1 -0      queuemgrwindow.h<br>
 M  +14 -0     toolsview.cpp<br>
 M  +5 -0      toolsview.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973486 by cgilles:</p>
<p>Add history view to Batch Queue Manager</p>
<p> M  +32 -3     queuemgrwindow.cpp<br>
 M  +2 -0      queuemgrwindow.h<br>
 M  +10 -4     queuepool.cpp<br>
 M  +2 -0      queuepool.h<br>
 M  +4 -3      queuesettingsview.cpp<br>
 M  +32 -3     toolsview.cpp<br>
 M  +7 -0      toolsview.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973464 by cgilles:</p>
<p>when BQM is busy, as we use current selected tab from Queue pool to<br>
identify which queue is processed, we need to disable tab to prevent<br>
problem if user clik on tab item.</p>
<p> M  +6 -0      queuepool.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973975 by aclemens:</p>
<p>Add a spreadsheet document to have a better overview of the modelview<br>
porting process. It contains all the issues reported on the mailinglist.</p>
<p>Please update the list if issues are fixed that I might have forgotten.<br>
I would suggest to add all newly found problems in here, too.</p>
<p>The mailinglist become too unreadable.</p>
<p>Andi</p>
<p>CCMAIL:digikam-devel@kde.org</p>
<p> A             porting2ModelView.ods  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973970 by cgilles:</p>
<p>try cheat scale. It's really faster now ???<br>
CCBUGS: 193967</p>
<p> M  +8 -2      thumbnailcreator.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973959 by aclemens:</p>
<p>New general info list dialog. This class will be used as a base class<br>
for ComponentsInfoDlg and DBStatsDlg.</p>
<p> M  +1 -0      CMakeLists.txt<br>
 A             libs/dialogs/infodlg.cpp   [License: GPL (v2+)]<br>
 A             libs/dialogs/infodlg.h   [License: GPL (v2+)]</p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973947 by gateau:</p>
<p>Check if there is an album at startup and also init m_urlList.</p>
<p>Makes plugin work with Gwenview.</p>
<p> M  +13 -0     plugin_advancedslideshow.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973987 by cgilles:</p>
<p>bump digiKam version to 1.0.0-beta1<br>
CCMAIL: digikam-devel@kde.org</p>
<p> M  +4 -4      CMakeLists.txt<br>
 M  +1 -1      NEWS<br>
 M  +1 -1      digikam.lsm.cmake  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973908 by cgilles:</p>
<p>New Database statistic dialog to show count of items by type mime.<br>
screenshot : http://farm4.static.flickr.com/3615/3572850378_535a270800_o.png<br>
BUG: 191634</p>
<p> M  +1 -0      CMakeLists.txt<br>
 M  +7 -0      digikam/componentsinfo.h<br>
 M  +12 -0     digikam/digikamapp.cpp<br>
 M  +1 -0      digikam/digikamapp.h<br>
 M  +2 -0      digikam/digikamapp_p.h<br>
 M  +2 -1      digikam/digikamui.rc<br>
 AM            libs/database/dbstatdlg.cpp   [License: GPL (v2+)]<br>
 AM            libs/database/dbstatdlg.h   [License: GPL (v2+)]</p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 973904 by aclemens:</p>
<p>When a new album is created, don't enter it immediately.</p>
<p>BUG:169213</p>
<p> M  +0 -1      albumfolderview.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 974006 by aclemens:</p>
<p>Add a link to the Qt documentation for date format settings.<br>
There are too much variables that the can be listed here, and they might<br>
change as well in the future.<br>
Adding this link is a good solution I guess.</p>
<p> M  +5 -1      manualrenameinput.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 974005 by aclemens:</p>
<p>Don't hide the tooltip for the manualrename widget when it is clicked,<br>
this was just some workaround in the past because the label was not<br>
hidden when the window lost focus.</p>
<p>I will add a clickable link to the tooltip so closing it will prevent<br>
the link from getting called.</p>
<p> M  +0 -6      dcursortracker.cpp<br>
 M  +0 -8      dcursortracker.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 974389 by mwiesweg:</p>
<p>Initial revisions for an sqlite based thumbnail database.<br>
Not functional as of yet.</p>
<p> M  +9 -6      CMakeLists.txt<br>
 M  +15 -0     libs/database/databasebackend.cpp<br>
 M  +2 -0      libs/database/databasebackend.h<br>
 A             libs/database/thumbnaildatabaseaccess.cpp   [License: GPL (v2+)]<br>
 A             libs/database/thumbnaildatabaseaccess.h   [License: GPL (v2+)]<br>
 A             libs/database/thumbnaildb.cpp   [License: GPL (v2+)]<br>
 A             libs/database/thumbnaildb.h   [License: GPL (v2+)]<br>
 A             libs/database/thumbnailschemaupdater.cpp   [License: GPL (v2+)]<br>
 A             libs/database/thumbnailschemaupdater.h   [License: GPL (v2+)]</p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 974453 by aclemens:</p>
<p>Add more debugging info when the database could not be found. Normally<br>
digiKam should not work correctly if this script fails, so the user<br>
should be already aware of that.</p>
<p> MM +10 -5     cleanup_digikamdb  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 974368 by mwiesweg:</p>
<p>Crash: ThumbBarView::removeItem will delete the given item, so we must not<br>
access it afterwards to get its ImageInfo</p>
<p> M  +13 -11    lighttablebar.cpp<br>
 M  +1 -1      lighttablebar.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 974475 by cgilles:</p>
<p>add libpgf 5.0.0 code. Not yet used. todo : make a QImage &lt;=&gt; pgf<br>
interface using memory stream to pass image data</p>
<p> M  +9 -0      CMakeLists.txt<br>
 M  +7 -0      digikam/CMakeLists.txt<br>
 A             libs/database/libpgf (directory)<br>
 AM            libs/database/libpgf/BitStream.h   [License: BSD]<br>
 AM            libs/database/libpgf/Decoder.cpp   [License: BSD]<br>
 AM            libs/database/libpgf/Decoder.h   [License: BSD]<br>
 AM            libs/database/libpgf/Encoder.cpp   [License: BSD]<br>
 AM            libs/database/libpgf/Encoder.h   [License: BSD]<br>
 AM            libs/database/libpgf/PGFimage.cpp   [License: BSD]<br>
 AM            libs/database/libpgf/PGFimage.h   [License: BSD]<br>
 AM            libs/database/libpgf/PGFplatform.h   [License: BSD]<br>
 AM            libs/database/libpgf/PGFtypes.h   [License: BSD]<br>
 AM            libs/database/libpgf/Stream.cpp   [License: BSD]<br>
 AM            libs/database/libpgf/Stream.h   [License: BSD]<br>
 AM            libs/database/libpgf/Subband.cpp   [License: BSD]<br>
 AM            libs/database/libpgf/Subband.h   [License: BSD]<br>
 AM            libs/database/libpgf/WaveletTransform.cpp   [License: BSD]<br>
 AM            libs/database/libpgf/WaveletTransform.h   [License: BSD]</p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 974491 by mwiesweg:</p>
<p>In DigikamView, emit signalSelectionChanged(), now with the number of selected indexes,<br>
directly, not after compressing with a timer.<br>
This means actions can be enabled directly the correct context menu be shown.</p>
<p> M  +17 -19    digikam/digikamapp.cpp<br>
 M  +1 -0      digikam/digikamapp.h<br>
 M  +1 -2      digikam/digikamview.cpp<br>
 M  +1 -1      digikam/digikamview.h<br>
 M  +5 -0      digikam/imagecategorizedview.cpp<br>
 M  +1 -0      digikam/imagecategorizedview.h<br>
 M  +4 -4      utilities/kipiiface/kipiinterface.cpp<br>
 M  +1 -1      utilities/kipiiface/kipiinterface.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 974513 by mwiesweg:</p>
<p>Clear hashes here to re-count when filter changed</p>
<p> M  +2 -0      imagefiltermodel.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 974519 by cgilles:</p>
<p>Added 2 methods to read/write PGF image from to QImage using QByteArray as data stream<br>
Marcel, code is untested. Take a care...<br>
CCMAIL: marcel.wiesweg@gmx.de</p>
<p> M  +83 -0     thumbnaildb.cpp<br>
 M  +5 -0      thumbnaildb.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 974514 by mwiesweg:</p>
<p>Call databaseForThread before touching the transaction count.<br>
If the very first thing that a thread did in its life was creating a transaction,<br>
the database connection would be opened just after incrementTransactionCount(),<br>
and open() would reset the transaction counter from 1 to 0, so that all subsequent<br>
transaction would not be committed.<br>
(This was the case first time with the MetadataManager worker threads assigning rating,<br>
which seemed to work because the ImageInfo was updated, but not the database)</p>
<p> M  +3 -1      databasebackend.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 975402 by cgilles:</p>
<p>move qt pgf code in a dedicated class</p>
<p> M  +1 -16     test/CMakeLists.txt<br>
 M  +7 -83     test/qtpgftest.cpp<br>
 M  +0 -83     thumbnaildb.cpp<br>
 M  +1 -6      thumbnaildb.h<br>
 AM            thumbnailpgf.cpp   [License: GPL (v2+)]<br>
 AM            thumbnailpgf.h   [License: GPL (v2+)]</p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 975405 by cgilles:</p>
<p>more test code. save pgf file to disk</p>
<p> M  +20 -1     qtpgftest.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 975398 by cgilles:</p>
<p>new test program to check Qt PGF interface</p>
<p> M  +2 -3      CMakeLists.txt<br>
 A             test (directory)<br>
 AM            test/CMakeLists.txt<br>
 AM            test/qtpgftest.cpp   [License: GPL (v2+)]<br>
 M  +4 -5      thumbnaildb.cpp<br>
 M  +2 -2      thumbnaildb.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 975636 by cgilles:</p>
<p>quality 3 sound like the best compression ratio to have the same quality than JPEG</p>
<p> M  +1 -1      qtpgftest.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 975580 by aclemens:</p>
<p>First attempt to fix auto-crop: I played around with the algorithm a<br>
little and it seems to work quite well.<br>
Can you confirm this?</p>
<p>Andi</p>
<p>CCBUG:179766</p>
<p> M  +50 -28    freerotation.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 975533 by mwiesweg:</p>
<p>Be more paranoid about using m_index (only guaranteed to be valid if<br>
widget is currently shown)</p>
<p> M  +2 -2      imageratingoverlay.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 975618 by cgilles:</p>
<p>PGF Encoding work. Decoding still with Red and Blue inverted</p>
<p> M  +14 -7     thumbnailpgf.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 975625 by cgilles:</p>
<p>now all work fine : PGF interface is ready to use</p>
<p> M  +3 -3      test/qtpgftest.cpp<br>
 M  +5 -7      thumbnailpgf.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 975519 by mwiesweg:</p>
<p>Add method to clean cached thumbnails</p>
<p> M  +7 -0      loadingcacheinterface.cpp<br>
 M  +6 -0      loadingcacheinterface.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 975515 by mwiesweg:</p>
<p>Hover rating in icon view is back!<br>
Add the necessary connection to delegate (to not draw rating starts<br>
beneath transparent rating box widget) and to MetadataManager via<br>
DigikamImageView.<br>
Structure code in ImageRatingOverlay. Listen to dataChanged() for the<br>
case that rating changes while hovering.</p>
<p> M  +12 -0     digikamimageview.cpp<br>
 M  +2 -0      digikamimageview.h<br>
 M  +60 -12    imageratingoverlay.cpp<br>
 M  +13 -3     imageratingoverlay.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 975733 by mwiesweg:</p>
<p>Retrieve category count from view. This is more reliable than from model<br>
and should work in all cases now.</p>
<p> M  +10 -6     imagecategorydrawer.cpp<br>
 M  +2 -1      imagecategorydrawer.h<br>
 M  +3 -2      imagedelegate.cpp<br>
 M  +1 -2      imagedelegate.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 976018 by aclemens:</p>
<p>do not try to crop when angle is 0.00°</p>
<p> M  +6 -0      freerotation.cpp  </p>
<p>------------------------------------------------------------------------<br>
 </p>

<div class="legacy-comments">

  <a id="comment-18569"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/452#comment-18569" class="active">Versioning</a></h3>    <div class="submitted">Submitted by Martin (not verified) on Mon, 2009-06-01 15:10.</div>
    <div class="content">
     <p>Congratulations! Will 1.0 have <a href="http://bugs.kde.org/show_bug.cgi?id=142056">image versioning</a>, a feature that was <a href="http://mail.kde.org/pipermail/digikam-devel/2007-June/013008.html">suggested</a> by the team to be one of the criteria for bumping the version number past 1.0?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18575"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/452#comment-18575" class="active">Features list isn't frozen.</a></h3>    <div class="submitted">Submitted by mikolajmachowski (not verified) on Tue, 2009-06-02 12:32.</div>
    <div class="content">
     <p>Features list isn't frozen.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18603"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/452#comment-18603" class="active">I’ll be coming back to read</a></h3>    <div class="submitted">Submitted by lord (not verified) on Mon, 2009-06-15 09:04.</div>
    <div class="content">
     <p>I’ll be coming back to read any of your future articles..<br>
Thank you for this</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18576"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/452#comment-18576" class="active">PPA testing repository for Ubuntu</a></h3>    <div class="submitted">Submitted by VangelistX (not verified) on Fri, 2009-06-05 17:01.</div>
    <div class="content">
     <p>I wonder if there is a possibility to push the new test builds of DigiKam to a PPA repository on Launchpad for all Ubuntu based distros users?</p>
<p>PPA repositories on Launchpad are very popular hence IMHO your program could be better tested in various environments before the final release.</p>
<p>You could create Your own PPA repo and push daily or weekly snapshots on it, or use one of the existing PPA. </p>
<p>Today I made a search on Launchpad for DigiKam packages:<br>
https://launchpad.net/+search?field.text=digiKam+PPA</p>
<p>And found the DigiKam Experimental repo:<br>
https://launchpad.net/~digikam-experimental/+archive/ppa</p>
<p>What do You think?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18585"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/452#comment-18585" class="active">Ubuntu repository</a></h3>    <div class="submitted">Submitted by Fri13 on Tue, 2009-06-09 11:03.</div>
    <div class="content">
     <p>In my humble opinion is that I believe to strict upstream-downstream development. It is distributors job to compile packages for their distribution so their users get easier install. If distributors do not have time or man power to compile packages, then there is volunteers among it's users who can do it for others.</p>
<p>And I do not like much about Ubuntu's launchpad because someway it just seems to disturb upstream development because of Ubuntu fans. But that is too just my personal opinion. </p>
<p>I believe that there is one digiKam developer who use Ubuntu so if the "official" deb packages are needed, then I suggest to turn to Canonical side.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18579"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/452#comment-18579" class="active">dngconverter plugin</a></h3>    <div class="submitted">Submitted by aj (not verified) on Sun, 2009-06-07 11:47.</div>
    <div class="content">
     <p>I'm using the dngconverter plugin, when opening the resulting dng files with ufraw it tells me there is no camera whitebalance and it's using fallback to auto. Is this a restriction of the dng format or a feature not yet implemented in the converter? I'm converting from canon cr2 raw format using the standalone plugin, no jpeg thumbnail and lossless compression enabled. Starting the plugin from within digikam I get the same results. I'd like to use dng for all my photos as the space savings are quite significant if you sum it up. I use openSUSE 11.1 64bit and packages from KDE:KDE4:Factory:Desktop repository, kipi-plugins version is 0.3-14.4</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
