---
date: "2015-04-07T22:20:00Z"
title: "digiKam Software Collection 4.9.0 released..."
author: "digiKam"
description: "Dear digiKam fans and users, The digiKam Team is proud to announce the release of digiKam Software Collection 4.9.0. This release includes a new sets"
category: "news"
aliases: "/node/735"

---

<a href="https://www.flickr.com/photos/digikam/16443031063"><img src="https://farm9.staticflickr.com/8808/16443031063_485814ca52_c.jpg" width="800" height="225"></a>

<p>Dear digiKam fans and users,</p>

<p>
The digiKam Team is proud to announce the release of digiKam Software Collection 4.9.0. This release includes a new sets of bugs fixes from <a href="https://plus.google.com/u/0/107171232114475191915/about">Maik Qualmann</a> who maintain KDE4 version while <a href="https://techbase.kde.org/Projects/Digikam/CodingSprint2014#KDE_Framework_Port">KF5 port is under progress</a>.
</p>

<p>
See the new list of the <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=4.9.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">issues closed</a> in digiKam 4.9.0 available through the KDE Bugs-tracking System.
</p>

<p>The digiKam software collection tarball and Windows installer can be downloaded from the <a href="http://download.kde.org/stable/digikam/">KDE repository</a>.
</p>

<p>Have fun to use this new digiKam release.</p>

<p>digiKam Team...</p>
<div class="legacy-comments">

  <a id="comment-20997"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-20997" class="active">Thanks for releasing a</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2015-04-07 22:51.</div>
    <div class="content">
     <p>Thanks for releasing a Windows installer at the same time (it has been several versions behind previously). Appreciated!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21007"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21007" class="active">Hooray!</a></h3>    <div class="submitted">Submitted by cc (not verified) on Fri, 2015-04-10 22:34.</div>
    <div class="content">
     <p>I was just about to say the same thing. Thanks a lot!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21044"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21044" class="active">Överdosera inte alltför - en</a></h3>    <div class="submitted">Submitted by <a href="http://www.sverigeapoteketonline.net/viagra.html" rel="nofollow">Viagra</a> (not verified) on Mon, 2015-06-08 05:56.</div>
    <div class="content">
     <p>Överdosera inte alltför - en riktig dos kommer att ordineras till dig av din läkare, men huvudregeln är enkel: ta inte mer än 100 mg Viagra Sildenafil inom 24 timmar - <a href="http://www.apotekgothenburg.com/viagra.html">Viagra säljes</a>. Detta kommer att ge dina chanser att drabbas av biverkningar ner mycket låg.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20998"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-20998" class="active">Ubuntu</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2015-04-08 03:23.</div>
    <div class="content">
     <p>Hi,<br>
The current version in Ubuntu repositories in 4.7.0. Is there a ppa for installing the latest one?</p>
<p>Thanks,<br>
Ken</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20999"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-20999" class="active">I can recommend Philip</a></h3>    <div class="submitted">Submitted by qpal (not verified) on Wed, 2015-04-08 07:27.</div>
    <div class="content">
     <p>I can recommend Philip repositories on https://launchpad.net/~philip5.<br>
There is small delay between new versions announced and available in Philip repositories, but it is the best source of digiKam I know of. My personal thanks to Philip for his work!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21002"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21002" class="active">Out of date</a></h3>    <div class="submitted">Submitted by <a href="http://www.ndauthorservices" rel="nofollow">NDAS</a> (not verified) on Wed, 2015-04-08 13:10.</div>
    <div class="content">
     <p>Last known update for digikam through this ppa was 4:3.1.0-precise~ppa1kde410. That is too out of date to bother with, and newer versions are available directly in most repositories.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21003"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21003" class="active">I don't support other</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Wed, 2015-04-08 18:54.</div>
    <div class="content">
     <p>I don't support other versions of ubuntu than 14.04 och 14.10 for the moment on my PPA and that old digikam 3.5 version you are refereeing to is for Ubuntu Precise (12.04) and should be removed but just been left there. Digikam 4.9 is available as soon as Launchpad servers have built the packages now.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21010"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21010" class="active">Thanks for all the work</a></h3>    <div class="submitted">Submitted by Rob (not verified) on Sun, 2015-04-26 03:17.</div>
    <div class="content">
     <p>Thanks for all the work you've done in keeping the ppa updated.  Any chance you'll support 15.04 soon?  A dependency in kipi-plugins for libqtgstreamer fails in 15.04.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21012"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21012" class="active">Please remove mysql dependency!</a></h3>    <div class="submitted">Submitted by Karma Kolabor (not verified) on Tue, 2015-04-28 12:23.</div>
    <div class="content">
     <p>could you please remove the mysql dependency from this package? Form what i see Digikam does not need mysql to run!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21018"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21018" class="active">mysql shoulden't be a forced</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Tue, 2015-04-28 21:03.</div>
    <div class="content">
     <p>mysql shoulden't be a forced dependency with my digikam packages on my PPA but libqt4-sql-mysql is a suggested dependency for users that want the option to test/use the mysql as experimental database.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-21014"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21014" class="active">WHICH REPO?</a></h3>    <div class="submitted">Submitted by Karma Kolabor (not verified) on Tue, 2015-04-28 12:31.</div>
    <div class="content">
     <p>Hi, there are two different repositories, which one should one use for 14.04 ? I do not understand the difference and why there are two repos with the same digikam, please elaborate on that, thank you!</p>
<p>https://launchpad.net/~philip5/+archive/ubuntu/extra</p>
<p>https://launchpad.net/~philip5/+archive/ubuntu/kubuntu-backports</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21016"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21016" class="active">And what about the msylwester repo</a></h3>    <div class="submitted">Submitted by Karma Kolabor (not verified) on Tue, 2015-04-28 13:02.</div>
    <div class="content">
     <p>Also I have one old repo here for digikam:<br>
http://ppa.launchpad.net/msylwester/digikam/ubuntu</p>
<p>what is going on with this? Isn't this the repo-de-jour anymore? </p>
<p>Philipp, how long will you support your repo? At which point will you decide to not support it anymore because it is too much work? Please do not misunderstand me, I totally respect the work you do, but this has to be an official position so it can be a reliable solution for a long time. </p>
<p>This whole situation is really annoying - an official repo for digikam is urgently needed, supported by the authors with a commitment to support at least the LTS releases.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21020"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21020" class="active">I think msylwester wanted to</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Tue, 2015-04-28 21:19.</div>
    <div class="content">
     <p>I think msylwester wanted to pick only packages from my PPA that have to do with Digikam and not provide the other programs and updates that can be found there at times. Not sure if he maintain his PPA selection. You have to ask him.</p>
<p>I can't really say how long I support a Ubuntu version. Usually as long as there is demand for packages and/or if it's realistic or possible to backport or provide what's needed on an older Ubuntu version to build and run the latest Digikam version at the time.</p>
<p>Upstream authors/developers are never responsible for providing and maintaining packages of software for different distros. That's what distro maintainers are for. Different distros have different rules on how they update and upgrade software so it's question that should be taken back to distro maintainers.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21019"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21019" class="active">Ubuntu 14.04 came with KDE</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Tue, 2015-04-28 21:09.</div>
    <div class="content">
     <p>Ubuntu 14.04 came with KDE 4.13 but later got KDE 4.14 as backport/updates if you choose it. My "extra" PPA is built against the standard KDE 4.13 and if that's what you use then only use my "extra" PPA. If you have enabled sources that provide Ubuntu 14.04 with KDE 4.14 then you ALSO need special packages built against the updated and newer version of KDE to work. Even with KDE 4.14 you will need packages from my "extra" so it's never wither PPA but can be both that is needed.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21017"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21017" class="active">trying to install from that</a></h3>    <div class="submitted">Submitted by Karma Kolabor (not verified) on Tue, 2015-04-28 13:18.</div>
    <div class="content">
     <p>trying to install from that repo gives me this error: </p>
<p>The following packages have unmet dependencies:<br>
 kipi-plugins : Depends: libqtsolutions-soap-head1 which is a virtual package.<br>
 libkgeomap2 : Depends: libmarblewidget19 (&gt;= 4:4.14.2) which is a virtual package.<br>
 libkexiv2-11 : Depends: libexiv2-13 which is a virtual package.<br>
The following actions will resolve these dependencies:</p>
<p>nothing was installed. Is there some more information that I need to install from this repo?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21023"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21023" class="active">Great</a></h3>    <div class="submitted">Submitted by Pepou (not verified) on Wed, 2015-04-29 09:17.</div>
    <div class="content">
     <p>Digikam 4.9 is huge !!!! thanks to all dev. And thanks philip5 to keep your ppa up to date !!!</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-21000"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21000" class="active">database - mysql</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2015-04-08 12:02.</div>
    <div class="content">
     <p>Hi,</p>
<p>1. Any plans to support mysql?<br>
2. I assume using SQLLite is the reason for that digikam is slow. At least much slower than Picasa<br>
3. I am sure many does have their photos on their local computer, but for those of us that store them on a NAS will digikam support working directly on a NAS storage?<br>
4. Why do I have to import photos when I already have them organized? Is this they way you really want this software to work?</p>
<p>Thanks for creating great software like this. To bad non-NAS support is a deal breaker.</p>
<p>Dajomu</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21001"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21001" class="active">...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2015-04-08 12:12.</div>
    <div class="content">
     <p>1/ Mysql support still experimental. We need maintainer and developer to work on it.<br>
2/ It's not slow if Db file is stored on fast device, as SSD for ex.<br>
3/ Already implemented. Use "remote collection" type in setup.<br>
4/ No need. See 3/</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21005"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21005" class="active">slow</a></h3>    <div class="submitted">Submitted by dave (not verified) on Thu, 2015-04-09 11:26.</div>
    <div class="content">
     <p>Hi,</p>
<p>I do not run a NAS with SSD and do not plan to do so either. Too costly.<br>
So why is Picasa so much faster? No lag at all no matter what computer I use, old or new, SSD or spinning disks.</p>
<p>Dajomu</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21011"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21011" class="active">MySQL</a></h3>    <div class="submitted">Submitted by cpriisholm (not verified) on Sun, 2015-04-26 19:47.</div>
    <div class="content">
     <p>First a big thanks for 4.9 as it fixes some issues related to RAW images.<br>
Any suggestions as how to migrate from MySQL to sqlite?<br>
With the upgrade to kubuntu 15.04 digikam upgrades past versions with mysql support. And the currently 4.2 on 14.10 suffers from the bug where it goes into a loop when accessing certain menus (thus making it impossible to run the database migration option).</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21021"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21021" class="active">If you use my packages of</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Tue, 2015-04-28 21:24.</div>
    <div class="content">
     <p>If you use my packages of Digikam for Kubuntu I will hopefully provide packages of digikam 4.9 specially built for (K)ubuntu 15.04 this coming weekend. Maybe that will solve some of the problem that might occur after an distro upgrade.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21024"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21024" class="active">Thank you!  I'm very happy to</a></h3>    <div class="submitted">Submitted by Rob (not verified) on Wed, 2015-04-29 15:14.</div>
    <div class="content">
     <p>Thank you!  I'm very happy to see the news that you'll be adding 15.04 support.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-21015"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21015" class="active">beware of mysql</a></h3>    <div class="submitted">Submitted by Karma Kolabor (not verified) on Tue, 2015-04-28 12:48.</div>
    <div class="content">
     <p>From years of doing software support for different products using mysql I can only warn all you about using mysql with your precious photo collection. It is a very bad software, please just remove it from Digikam. I guarantee you will have lots of trouble with mysql and I do not recommend to install this crap software on a serious photo production workstation or make your photo production software or workflow depend on this software in any way. </p>
<p>If you absolutely need a database, then postgresql is the much better way to go. But sqlite is absolutely good enough for a single user workstation. You can find lots of posts about mysql trouble everywhere on the net, do a research for yourself. It would be a much better idea not to integrate this into a software that touches your private picture collection, be warned!</p>
<p>No, please do not answer with anecdotal "I had never problems with mysql" postings, it adds nothing of value to the discussion. Every admin or developer with some years of experience (and who has worked with other databases too!) will give you the same hint: avoid it, if possible!</p>
<p>This is a serious warning, do not use mysql if you like your photos.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21025"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21025" class="active">anecdotal advice</a></h3>    <div class="submitted">Submitted by cc (not verified) on Tue, 2015-05-05 23:42.</div>
    <div class="content">
     <p>Not to be rude, but your advice is purely anecdotal, as well. Even if it comes from "years of experience".</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-21004"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21004" class="active">libkgeomap translations missing?</a></h3>    <div class="submitted">Submitted by Nico (not verified) on Thu, 2015-04-09 00:47.</div>
    <div class="content">
     <p>looks like the libkgeomap translations are missing in the 4.9.0 tarball<br>
-&gt; I guess I'll have to work with the 4.8.0 ones for the openSUSE packages...</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21006"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21006" class="active">Thanks</a></h3>    <div class="submitted">Submitted by <a href="http://roehs.de" rel="nofollow">Malte</a> (not verified) on Thu, 2015-04-09 12:36.</div>
    <div class="content">
     <p>Thanks for your great work!</p>
<p>Especially about the "Manual adding of faces made easier" patch I am really happy.</p>
<p>Well done!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21008"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21008" class="active">Thanks for patching face recognition</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2015-04-13 09:13.</div>
    <div class="content">
     <p>Thanks a lot for patching face recognition and for the advice on KDE bugtracker that the old database may be corrupted and database rebuild is needed!<br>
https://bugs.kde.org/show_bug.cgi?id=323888#c112<br>
-----<br>
Quote:<br>
"It's also possible that your face database file is corrupted due to previous dysfunctions.<br>
The idea is to rename file as .old and to re-try to scan face with a fresh DB file.<br>
file is located in your home dir at ~/.kde4/share/apps/libkface/database/recognition.db"<br>
----</p>
<p>This worked for me, now face tagging runs smoothly again :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21009"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21009" class="active">Recommended distribution and version?</a></h3>    <div class="submitted">Submitted by linus (not verified) on Mon, 2015-04-13 13:28.</div>
    <div class="content">
     <p>Hello,</p>
<p>ive been trying to install the latest digikam version in a virtualbox<br>
on<br>
a fresh ubuntu 14.10<br>
or<br>
a fresh kubuntu 14.4</p>
<p>and earlier at<br>
kubuntu 14.4 which has been updated from earlier versions</p>
<p>With the last one i compiled 5 or 6 Versions of digikam myself which nearly never worked as expected. But it worked instead for the last try (4.6.0 or so) which i cant get to work.</p>
<p>Now i decided to use the packages and run into any other trouble which has been documented. </p>
<p>But yesterday i found some new: 404 Error on accessing the backports repo, which brings me to kububtu 14.4 (kde 14.3) where no backports should be needed. </p>
<p>But today the fresh kubuntu 14.4 leads after a good start to the cryptic missing symbol again...</p>
<p>So my question is:<br>
Which distribution and version is recommened?</p>
<p>Where all dependencies are fullfilled,<br>
Where no funny workarounds are needed<br>
Where the apt-get brings me the program and it works without silently dying at start or complaining about missing symbols, ibusses...</p>
<p>Thanks in advance<br>
Linus</p>
<p>PS: Ive also tried the 4.9.0 windows exe, which started easy and nicely but was not able to rescan some new folders in my collection or, to be precise, it rescans but doesnt find any new items.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21013"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21013" class="active">Where is the OFFICIAL Digikam</a></h3>    <div class="submitted">Submitted by Karma Kolabor (not verified) on Tue, 2015-04-28 12:25.</div>
    <div class="content">
     <p>Where is the OFFICIAL Digikam ppa repository for Ubuntu 14.04? I would like to install this new version from a ppa that is officially supported by the authors.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21022"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21022" class="active">There is no other official</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Tue, 2015-04-28 21:28.</div>
    <div class="content">
     <p>There is no other official versions of Digikam from others than the maintainers of your linux distro of choice. The authors/developers doesn’t officially support any packages only the upstream source code.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21026"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/735#comment-21026" class="active">Thanks</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2015-05-06 02:11.</div>
    <div class="content">
     <p>Congratulations to Digikam Team on 4.9 release and thanks for the Windows package.<br>
JL</p>
         </div>
    <div class="links">» </div>
  </div>

</div>