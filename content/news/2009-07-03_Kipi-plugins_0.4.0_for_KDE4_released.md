---
date: "2009-07-03T11:49:00Z"
title: "Kipi-plugins 0.4.0 for KDE4 released"
author: "digiKam"
description: "Dear all digiKam fans and users! Kipi-plugins 0.4.0 maintenance release for KDE4 is out. kipi-plugins 0.4.0 tarball can be downloaded from SourceForge at this url"
category: "news"
aliases: "/node/463"

---

<p>Dear all digiKam fans and users!</p>

<p>Kipi-plugins 0.4.0 maintenance release for KDE4 is out.</p>

<a href="http://www.flickr.com/photos/digikam/3684299388/" title="kipi-plugins0.4.0 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2587/3684299388_28a9c58e66.jpg" width="500" height="400" alt="kipi-plugins0.4.0"></a>

<p>kipi-plugins 0.4.0 tarball can be downloaded from SourceForge <a href="http://sourceforge.net/project/showfiles.php?group_id=149779&amp;package_id=165761">at this url</a></p>

<p>Kipi-plugins will be also available for Windows. Precompiled packages can be donwloaded with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<p>See below the list of new features and bugs-fix coming with this release:</p>

<b>GalleryExport</b> : Raw images can be uploaded directly to remote server.<br>
<b>DNGConverter</b>  : Fix compilation under MacOS-X.<br><br>

001 ==&gt; DNGConverter       : 192289 : Using dots (.) as separators confuses the rename system.<br>
002 ==&gt; FlashExport        : 192165 : digiKam flash export plugin does not auto-rotate portrait images.<br>
003 ==&gt; BatchProcessImages : 192469 : Repeatedly crashes when doing batch resize or rename.<br>
004 ==&gt; BatchProcessImages : 192675 : digiKam Crashes after re-compressing images.<br>
005 ==&gt; BatchProcessImages : 190724 : digiKam crashes after resizing several pictures in a batch.<br>
006 ==&gt; BatchProcessImages : 191749 : digiKam crashes when closing the picture resize box.<br>
007 ==&gt; BatchProcessImages : 190273 : Crash after hitting ok after batch convert.<br>
008 ==&gt; BatchProcessImages : 189786 : Batch Menu Border Images.<br>
009 ==&gt; BatchProcessImages : 191507 : Converting RAW (Canon) to JPG crash.<br>
010 ==&gt; BatchProcessImages : 189798 : digiKam crashes with SIGSEGV when converting .CR2 image to .jpg.<br>
011 ==&gt; RAWConverter       : 192787 : RawConverter: Image rotated but EXIF tag not cleared.<br>
012 ==&gt; FlickrExport       : 183639 : Can't upload photo without a PhotoSet.<br>
013 ==&gt; FlickrExport       : 192862 : Spaces break tag into bits.<br>
014 ==&gt; GPSSync            : 191914 : Copy and paste of GPS-data in the correlator and the track list editor.<br>
015 ==&gt; PrintImage         : 192982 : Fit image to paper on printing.<br>
016 ==&gt; RAWConverter       : 193433 : digiKam crashes when converting RAW (Canon cr2) to JPEG.<br>
017 ==&gt; BatchProcessImages : 195182 : Batch processing - Resize Images.<br>
018 ==&gt; AdvancedSlideshow  : 187228 : Use the correct form for plurals in kipi advancedslideshow plugin.<br>
019 ==&gt; GalleryExport      : 194245 : Exif information lost when exporting to Gallery.<br>
020 ==&gt; BatchProcessImages : 196166 : Crash after batch resize (klick on close).<br>
021 ==&gt; Libkdcraw          : 196271 : libraw_alloc.h: error: The function "bzero" must have a prototype.<br>
022 ==&gt; PrintImage         : 137687 : Let the user decide the position where to print a photo.<br>
023 ==&gt; MetaDataEdit       : 197218 : Plugin corrupts CRC information in PNGs.<br>
024 ==&gt; AquireImages       : 194341 : digiKam crashed with seg fault.<br>
025 ==&gt; BatchProcessImages : 198039 : digiKam crashed after resizing several photos.<br>
026 ==&gt; BatchProcessImages : 197764 : Batch resize crashes digiKam upon clicking the "close" option once resizing is done.<br>
027 ==&gt; BatchProcessImages : 193488 : Abort after batch processing.<br>
028 ==&gt; BatchProcessImages : 191955 : Turn off when resizing.<br>
029 ==&gt; GalleryExport      : 198058 : EXIF information is lost on gallery export (only when resizing).<br>
030 ==&gt; MetadataEdit       : 198407 : Xmp comment language in afrikaans instead of French.<br>


<div class="legacy-comments">

  <a id="comment-18636"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/463#comment-18636" class="active">Looking sexier all the time!</a></h3>    <div class="submitted">Submitted by <a href="http://kubuntulover.blogspot.com" rel="nofollow">Bugsbane</a> (not verified) on Fri, 2009-07-03 17:33.</div>
    <div class="content">
     <p>Looking sexier all the time! That's a lot of crashes that will never happen again. :)</p>
<p>Nice photos by the way...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18651"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/463#comment-18651" class="active">Happy</a></h3>    <div class="submitted">Submitted by <a href="http://www.panopixel.org/" rel="nofollow">DrSlony</a> (not verified) on Mon, 2009-07-06 01:32.</div>
    <div class="content">
     <p>I'm happy to see all these crash fixes, they were really plaguing me. Thank you all :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18656"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/463#comment-18656" class="active">The url for the screenshot is</a></h3>    <div class="submitted">Submitted by Jan (not verified) on Mon, 2009-07-06 21:45.</div>
    <div class="content">
     <p>The url for the screenshot is broken.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18657"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/463#comment-18657" class="active">Fixed...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2009-07-06 21:49.</div>
    <div class="content">
     <p>Done</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
