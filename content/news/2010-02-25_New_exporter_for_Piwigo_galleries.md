---
date: "2010-02-25T11:33:00Z"
title: "New exporter for Piwigo galleries"
author: "Frederic Coiffier"
description: "Dear digiKam users, A new export plugin has been added to the Kipi-plugins development trunk: an export to Piwigo plugin! Some of you may not"
category: "news"
aliases: "/node/503"

---

<p>Dear digiKam users,</p>
<p>A new export plugin has been added to the Kipi-plugins development<br>
trunk: an export to <a href="http://piwigo.org">Piwigo</a> plugin!</p>
<p><a href="http://www.flickr.com/photos/47673652@N02/4385033317/" title="Piwigo plugin in Digikam de fcoiffie, sur Flickr"><img src="http://farm5.static.flickr.com/4062/4385033317_d57a6c86b9.jpg" width="500" height="410" alt="Piwigo plugin in Digikam"></a></p>
<p>Some of you may not know Piwigo yet: it's a powerful web gallery based<br>
on usual PHP/MySQL technologies. It's a good solution if you want to<br>
self-host your photos and avoid third-parties services like Picasa or<br>
Flickr. It can be compared to Menalto Gallery, which is already<br>
supported by Kipi-plugins, even if I find Piwigo lighter and more<br>
complete when it comes to browsing capabilities (hierarchical<br>
categories, tags or chronology).</p>
<p>Moreover, Piwigo is built by a dynamic and welcoming community.</p>
<p>Regarding the plugin itself, it currently:</p>
<ol>
<li>connects to your website,</li>
<li>lists your category tree,</li>
<li>uploads selected photos.</li>
</ol>

<p>and the result:</p>
<p><a href="http://www.flickr.com/photos/47673652@N02/4385795902/" title="Piwigo gallery de fcoiffie, sur Flickr"><img src="http://farm3.static.flickr.com/2709/4385795902_1a9e8535a4.jpg" width="500" height="411" alt="Piwigo gallery"></a></p>
<p>I hope you'll find it useful.</p>

<div class="legacy-comments">

  <a id="comment-19062"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/503#comment-19062" class="active">Whaooo !!!</a></h3>    <div class="submitted">Submitted by Sylvain (not verified) on Thu, 2010-02-25 13:47.</div>
    <div class="content">
     <p>Great !!! I use piwigo and Digikam and it's a very good new</p>
<p>it's fantastic...</p>
<p>Congratulations to the Digikam team and all contributors</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19063"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/503#comment-19063" class="active">A very expected feature</a></h3>    <div class="submitted">Submitted by <a href="http://piwigo.org" rel="nofollow">Pierrick Le Gall</a> (not verified) on Thu, 2010-02-25 20:16.</div>
    <div class="content">
     <p>Hi Frederic and congratulations!</p>
<p>I'm Piwigo founder and I would like to let you know (Digikam fans) that many Piwigo users were waiting for this export plugin :-)</p>
<p>Thanks a lot. Frederic knows he can count on me for testing and improving Piwigo communication API.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>