---
date: "2017-03-21"
title: "Contribute"
author: "digiKam Team"
description: "Contribute to the digiKam project"
category: "contrib"
aliases: "/contrib"
menu: "navbar"
---

The easiest way to contribute is to spread the word about digiKam.
We also encourage you to test digiKam, report bugs, and submit feature requests.
You might also want to subscribe to the [digikam-users](https://mail.kde.org/mailman/listinfo/digikam-users) mailing list
and help other digiKam users with their questions and problems.

## Reporting Bugs and Submitting Feature Requests

Use the bug tracking system for all bug reports and new feature requests.
Take a look at the [support page](/support) for further information.

## Freezes and Other Run-Time Issues

### Linux host

Just run digiKam from the terminal command line to capture the text traces generated by the application.
Note that you need to turn on before all debug traces from digiKam with QT_LOGGING_RULES environment variable.

```
    export QT_LOGGING_RULES="digikam*=true"
    digikam
```

### Windows Host

On Windows, application text output is not sent to the terminal as under Linux.
You need to install an extra application named [DebugView tool](https://learn.microsoft.com/sysinternals/downloads/debugview) to capture text traces
generated by digiKam.

If digiKam start properly, go to **Settings/Configure digiKam/Miscellaneous/System** dialog page and turn on the option **Enable internal debug logging** to generate debug traces.

If digiKam do not start, go to your Windows System Information panel and add a new user variable with these criteria:

```
    name: "QT_LOGGING_RULES"
    value: "digikam*=true"
```

Run DebugView and later digiKam. The digiKam debug traces must appears in central view of DebugView.

### macOS Host

As under Linux, run the application from a terminal.
digiKam executable is installed in `/Applications/digiKam.org/digikam.app/Contents/MacOS/digikam`.
Just start it from the command line to view text traces.
Note that you need to turn on before all debug traces from digiKam with QT_LOGGING_RULES environment variable.

```
    export QT_LOGGING_RULES="digikam*=true"
    /Applications/digiKam.org/digikam.app/Contents/MacOS/digikam
```

## Dealing with Crashes in digiKam

### Linux host

In case digiKam crashes, you can provide a backtrace using GDB debugger.
digiKam needs to be compiled with all debug info; otherwise the backtrace will be useless.
If you installed digiKam using the packages provided by your distribution, make sure to install
the corresponding debug package. [The debugging guide](https://community.kde.org/Guidelines_and_HOWTOs/Debugging)
provides further information on debugging.

If you use a regular binary, i.e. no AppImage, use the following command to get into GDB and start digiKam:

```
    gdb digikam
    (gdb) catch throw
    (gdb) run
```

If you use an AppImage bundle, replace the name of the AppImage with the one you have and run:

```
    ./digikam-8.0.0-x86-64-debug.appimage debug
```

Note: uses the AppImage bundle file with the `-debug` suffix. It's more heavy because it includes debug symbols in binary files.
These symbols will be used by the debugger to localize the dysfunction in source code.

Instructions how to get into GDB are below. When you are in debugger and digiKam crashes, you will get to the
GDB command prompt. If digiKam "just" freezes, press ctrl-c to get to the GDB prompt.
Once you are at the prompt, use the "bt" command, copy the backtrace and exit GDB as shown here:

```
    (gdb) bt
    (gdb) _the backtrace is here_
    (gdb) quit
```

### Windows Host

#### DrMinGW Crash Course

The binary installers including `-debug` suffix in file name comes with debug symbols, and a [DrMinGw crash course handler](https://vroby.ddns.net/Public/sdlBasic/MinGW/doc/drmingw/drmingw.html)
will generate a file backtrace in you home directory when a dysfunction happen:

```
    C:\Users\_user_name_\AppData\Local\digikam_crash.log
```

Just replace "_user_name_" with your Windows login account.

#### Running in QtCreator

If no crash course file is generated with DrMinGW, alternative is to run **digikam.exe** program in debugger. We recommand [to install QtCreator](https://www.qt.io/product/development-tools), the open source IDE provided by Qt Prohect which propose a simple option to run an external application in debugger.
After installing QtCreator, run it and go to the **Debug/Start Debuging/Start and Debug External Application** menu entry. A dialog appears where you will just fill the **Local Executable** setting with the path to digikam.exe (usualy **C:\Program Files\digiKam\digikam.exe**).
Press **Ok** button, this will run digiKam in debugger. Use the application to reproduce the dysfunction, and when it crash, the backtrace is located on the bottom [in the Stack View](https://doc.qt.io/qtcreator/creator-stack-view.html).

### macOS Host

Mac users need to install [Apple XCode development tool](https://en.wikipedia.org/wiki/Xcode) which integrate **lldb**
debugger. Run it as follows to get a crash backtrace from the debugger:

```
    lldb /Applications/digiKam.org/digikam.app/Contents/MacOS/digikam
    ...
    (gdb) r
    ...
    macOS will ask here for admin right to run digiKam in debugger
    ...
    run digiKam to reproduce the crash condition
    ...
    (gdb) bt
    (gdb) _the backtrace is here_
    (gdb) quit

```

Note: uses the PKG bundle file with the `-debug` suffix. It's more heavy because it includes debug symbols in binary files.
These symbols will be used by the debugger to localize the dysfunction in source code.

## Checking for Memory Leaks

To check for memory leaks in digiKam under Linux, use the [valgrind](http://valgrind.org) tool.
Run the command below, and report the trace to developers:

```
    valgrind --tool=memcheck --leak-check=full --error-limit=no digikam
```


## Submitting Patches

To fix digiKam source code, you need to checkout current implementation from git following [intructions here](/download/git).
Before you submit patches, please read the [HACKING section](https://www.digikam.org/api) from online API documentation.
Send patches against the current version of the code (latest git/master revision) and not the stable release or
an old beta version. Patches can be created using the following command:

```
    git diff HEAD > mydiff.patch
```

The patches must be attached to a new entry in [Bugzilla](https://bugs.kde.org/describecomponents.cgi?product=digikam).
Please do not use mailing lists or private mail.

Alternative: you can make a Pull Resquest (PR) on gitlab using a fork of [the project](https://invent.kde.org/graphics/digikam) with your modifications.

## Application Translations

If you want to contribute to the digiKam internationalization effort, contact the [translation teams](http://l10n.kde.org/).
Also, please read the [Translation HOWTO](http://l10n.kde.org/docs/translation-howto/) to know the workflow used by translators.

Application is localized in more than 50 languages. To switch to your preferred language, go to Settings/Configure Languages
menu entry.

[![](https://i.imgur.com/4ywUf0d.png "digiKam Setup Languages Dialog")](https://imgur.com/4ywUf0d)

## Writing and Translating the Documentation

Help to write or translate the digiKam documentation is always welcome.
For all documentation-related matters, write to [digiKam-devel mailing list](https://mail.kde.org/mailman/listinfo/digikam-devel).

Our onine [documentation](https://docs.digikam.org/en/index.html) is based on the
[Sphinx](https://www.sphinx-doc.org) framework and [ReStructuredText](https://docutils.sourceforge.io/rst.html) format.

The [README](https://invent.kde.org/documentation/digikam-doc/-/blob/master/README.md) file explains in details how to be involved with this documentation.

## Pictures Samples

We need RAW, TIFF, and JPEG files from different manufacturers (Canon, Nikon, Sony, Olympus, Sigma, etc.).
We use these files to analyze embedded metadata to improve camera support in digiKam.
We also need sample files produced by different applications (including other platforms and proprietary
software like Adobe Photoshop) that include IPTC/XMP metadata. This can help us to improve compatibility
with other tools and implement automatic import of these data into the digiKam database.

## Splash Screens and Background Photo

As a photographer, you can submit your best photos for use as digiKam splash-screens and background photo.
Take a look [at this page](http://www.digikam.org/splashcreens) for further information.
