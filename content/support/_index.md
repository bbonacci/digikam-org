---
date: "2017-03-21"
title: "Support"
author: "digiKam Team"
description: "Support resources for digiKam"
category: "support"
aliases: "/support"
menu: "navbar"
---

### Getting Help

New to digikam ? Needs some help ?

**First**, read carefully the [digikam documentation](/documentation/).
\
 It's actively maintained and you should find most of what you need
there.

**Then**, if you still have questions, you should have a look in the
[FAQ (Frequently Asked Questions)](/documentation/faq/).

If you can't find an answer to your question, feel free to
[subscribe](/support/#mailinglists) to the digikam mailing list to get
support. \
 If you think it's a bug, you can report it to bugzilla, see below.

### Reporting bugs and wishes

Please use the bug tracking system for all bug-reports and new feature
wishlists. Take a care to use the right component to post a new file at
the right place. You can checkout the current bug-reports and wishlists
at these urls:

-   [digiKam bugs and
    wishes](https://bugs.kde.org/enter_bug.cgi?format=guided&product=digikam)

digiKam use external libraries to manage metadata
([Exiv2](http://www.exiv2.org)) and digital camera
([GPhoto](http://www.gphoto.org)). Please use links listed below to
reports specifics bugs relevant of these libraries:

-   [Exiv2 library bugs and wishes](https://github.com/Exiv2/exiv2/issues)
-   [QtAV library issues and wishes](https://github.com/wang-bin/QtAV/issues)
-   [Libraw library issues and wishes](https://github.com/LibRaw/LibRaw/issues)
-   [Lensfun library issues and wishes](https://github.com/lensfun/lensfun/issues)
-   [GPhoto library bugs and wishes](http://gphoto.org/bugs)

### Mailing list Subscription

-   digikam-users\
     [Mailing list
    subscription](https://mail.kde.org/mailman/listinfo/digikam-users)
    for all questions, comments, suggestions, and requests by the
    **digiKam users**. \
    [Browse mailing list archive](http://mail.kde.org/pipermail/digikam-users/)

    To search a topic with your best web engine, enter these keywords in search text field:
    ```
    site:mail.kde.org inurl:digikam-users _my_topic_
    ```

-   digikam-devel\
     [Mailing list
    subscription](https://mail.kde.org/mailman/listinfo/digikam-devel)
    for **digiKam developers** threads. \
    [Browse mailing list archive](http://mail.kde.org/pipermail/digikam-devel/)

    To search a topic with your best web engine, enter these keywords in search text field:
    ```
    site:mail.kde.org inurl:digikam-devel _my_topic_
    ```

### External Links and Resouces

-   [Script to Migrate From Windows Photo Gallery to digiKam](https://github.com/farblos/wpg2dk)
-   [Lightroom Shortcuts Scheme for digiKam](https://github.com/polandy/lightroomshortcuts4digikam)
-   [Notes on Interoperability Between digiKam and Synology Photo Station](https://github.com/roleohibachi/digikam-synology-config)
-   [Script to Migrate Picasa Metadata to digiKam](https://github.com/Philipp91/picasa2digikam)
-   [Script to Batch Export Images From digiKam Image Database With Various Settings.](https://github.com/jensb/digikam-select)
-   [Tutorial to use Multiple Photo Libraries With digiKam](https://unix.cafe/wp/en/2020/08/how-to-use-multiple-photo-libraries-with-digikam/)
-   [Collection of Scripts for digiKam](https://github.com/teuben/DigikamScripts)
-   [Script to Symlink Image Files Based on Their Rating](https://github.com/euphi/digikamLinker)
-   [Portable Version of digiKam](https://github.com/truefriend-cz/Portable-digiKam)
-   [Script to Query the digiKam Database](https://github.com/looran/digikuery)
-   [Script to Show Statistics From the digikam Database](https://github.com/silviuvulcan/digikam-stats)
-   [Script to Repair Corrupted digiKam Database](https://github.com/bbqbailey/repairDigikam)
-   [SQL Scripts for Analysis the digiKam Database](https://github.com/rzsykzmxpxzasqxtyxlveeanfu/digikam-mysql)
-   [Collection of Scripts for Manipulating the digiKam Database](https://github.com/mcdamo/digikam-scripts)
-   [Scripts to Manipulate Face Management Properties](https://github.com/Tan4ek/digikam-utils)
-   [Tutorial to Migrate from Lightroom to digiKam](https://ocroquette.wordpress.com/migrating-from-lightroom-6-to-digikam-7/)
