---
date: "2017-03-21"
title: "Documentation"
author: "digiKam Team"
description: "digiKam Documentation and Resources"
category: "documentation"
aliases: "/docs"
menu: "navbar"
---

### Documentation and Useful Resources

![Online Handbook](/img/content/documentation/handbook_HTML.webp)

The digiKam documentation can be [read online here](https://docs.digikam.org/en/index.html) or downloaded in [EPUB format here](https://docs.digikam.org/en/epub/DigikamManual.epub). 

[digiKam tutorials](https://userbase.kde.org/Digikam/Tutorials) wiki offers tutorials that cover various digiKam tools and functionality.

### API and Database Schema

If you plan contribute as a developer to digiKam, you might find the following links useful:

* [digiKam API](https://www.digikam.org/api/)
* [Database schema](https://invent.kde.org/graphics/digikam/-/raw/master/project/documents/DBSCHEMA.ODS)

Use [SQliteBrowser](https://sqlitebrowser.org/) to work with SQlite3 database files.

### Contribute

We encourage you to be involved in the existing documentation by submitting comments, corrections, and patches. 
If you'd like to help to the documentation, have a look at the [Contribute](/contribute#documentation) page.
