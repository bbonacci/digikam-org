---
date: "2018-03-25"
title: "Release Plan"
author: "digiKam Team"
description: "The release plan for the digiKam software."
category: "releases"
aliases: "/about/releaseplan"
---

### digiKam 8.x Release Plan

|      Date     |       Version          |                                                          Comments                                                                                                                                        |
|      ---      |         ---            |                                                            ---                                                                                                                                           |
|   2023.06.XX  |        8.1.0           | Huge bugs triaging release.                                                                                                                                                                              |
|   2023.04.16  |        8.0.0           | Qt6 and KF6 frameworks port with Qt5 backward compatibility. New Image Quality Sorting based on deep-learning engine.                                                                                    |

### digiKam 7.x Release Plan

|      Date     |       Version          |                                                          Comments                                                                                                                                        |
|      ---      |         ---            |                                                            ---                                                                                                                                           |
|   2023.03.12  |        7.10.0          |                                                                                                                                                                                                          |
|   2022.12.05  |        7.9.0           |                                                                                                                                                                                                          |
|   2022.09.04  |        7.8.0           |                                                                                                                                                                                                          |
|   2022.06.24  |        7.7.0           |                                                                                                                                                                                                          |
|   2022.03.06  |        7.6.0           | Qt5 LTS maintenance release.                                                                                                                                                                             |
|   2022.01.16  |        7.5.0           | Contextual Open-With support under macOS. RTL support improvements. GUI internationalization and localization updates with more than 50 languages. Add Platform Input Context Support in AppImage bundle.|
|   2021.12.18  |        7.4.0           | Showfoto folder-view and stack-view. MJPEG server plugin. Separate local thumbnails Sqlite database with remote Mysql database. Image Quality Sorter improvements.                                       |
|   2021.07.12  |        7.3.0           | ExifTool metadata viewer, iNaturalist export tool. Base Media Format metadata support. Better support of FITS astro-photo. Huge improvements of DNG Converter. Batch RAW to DNG converter in Showfoto.   |
|   2021.03.22  |        7.2.0           |                                                                                                                                                                                                          |
|   2021.02.07  |        7.2.0-rc        | Including Online Version Downloader.                                                                                                                                                                     |
|   2020.12.27  |        7.2.0-beta2     | macOS bundle relocatable and compatible with BigSur. Face engine data models downloadable in demand.                                                                                                     |
|   2020.10.30  |        7.2.0-beta1     | Include faces workflow and face engine improvements from GSoC 2020 projects.                                                                                                                             |
|   2020.09.06  |        7.1.0           | Summer maintenance release. Add UTF-8 support for IPTC metadata.                                                                                                                                         |
|   2020.07.19  |        7.0.0           |                                                                                                                                                                                                          |
|   2020.06.07  |        7.0.0-rc        | Add Qt 5.15 support.                                                                                                                                                                                     |
|   2020.04.24  |        7.0.0-beta3     |                                                                                                                                                                                                          |
|   2020.02.02  |        7.0.0-beta2     | Add Qt 5.14 support. Switch AppImage and macOS package from QtWebkit to QtWebEngine. Face Scan UI improvements and simplications.                                                                        |
|   2019.12.22  |        7.0.0-beta1     | Re-written Face detection and recognition based on OpenCV deep learning engine (GoSC 2019 project). LibRaw 0.20 support.                                                                                 |

[![](https://i.imgur.com/BEIAMAsh.png "digiKam 7.0.0")](https://imgur.com/BEIAMAs)

### digiKam 6.x Release Plan

|      Date     |       Version          |                                                          Comments                                                                                                                                        |
|      ---      |         ---            |                                                            ---                                                                                                                                           |
|   2019.11.09  |        6.4.0           | New Clone tool for Image Editor. New DPlugins interface for native image loaders and Raw Import tools.                                                                                                   |
|   2019.09.08  |        6.3.0           | Libraw 0.19.5 support. First version of exported DPlugin API. GMic-Qt ported as external plugin for Image Editor.                                                                                        |
|   2019.08.04  |        6.2.0           | Exiv2 0.27.2 and Libraw 0.19.3 support.                                                                                                                                                                  |
|   2019.04.14  |        6.1.0           | New plugins interface named "DPlugins" for Generic, Editor, and BQM tools.                                                                                                                               |
|   2019.02.10  |        6.0.0           |                                                                                                                                                                                                          |
|   2018.12.10  |        6.0.0-beta3     | Libraw 0.19 support. New grouping items options.                                                                                                                                                         |
|   2018.10.31  |        6.0.0-beta2     | Exiv2 0.27 support. Time-Adjust tool. DrMinGw support under Windows. New separate album contents options.                                                                                                |
|   2018.08.19  |        6.0.0-beta1     | Refactored version with less dependencies and with video management support. This version will include also GoSC 2017/2018 projects.                                                                     |

[![](https://c2.staticflickr.com/2/1843/44139156541_8ebdb4c93f_c.jpg "digiKam 6.0.0 - Image Editor Raw Import Tools Using Last Libraw 0.19")](https://www.flickr.com/photos/digikam/44139156541)


### digiKam 5.x Release Plan

|      Date     |       Version          |                                                          Comments                                                                                                                                        |
|      ---      |         ---            |                                                            ---                                                                                                                                           |
|   2018.03.25  |        5.9.0           |                                                                                                                                                                                                          |
|   2018.01.12  |        5.8.0           | Including new UPNP/DLNA export tool.                                                                                                                                                                     |
|   2017.08.27  |        5.7.0           | Including new Print Creator, Send by Email tool, many bugs fixed and triaged.                                                                                                                            |
|   2017.06.11  |        5.6.0           | Including new HTML Gallery tool, new Video SlideShow tool, Items Grouping improvements, Mysql garbage collector, GPS Bookmarks improvements.                                                             |
|   2017.03.12  |        5.5.0           | Including database garbage collector.                                                                                                                                                                    |
|   2017.01.08  |        5.4.0           | Ported to QtAv for Video Files support.                                                                                                                                                                  |
|   2016.11.06  |        5.3.0           |                                                                                                                                                                                                          |
|   2016.09.18  |        5.2.0           |                                                                                                                                                                                                          |
|   2016.08.07  |        5.1.0           |                                                                                                                                                                                                          |
|   2016.07.03  |        5.0.0           |                                                                                                                                                                                                          |
|   2016.05.29  |        5.0.0-beta7     |                                                                                                                                                                                                          |
|   2016.04.24  |        5.0.0-beta6     |                                                                                                                                                                                                          |
|   2016.03.27  |        5.0.0-beta5     |                                                                                                                                                                                                          |
|   2016.02.28  |        5.0.0-beta4     |                                                                                                                                                                                                          |
|   2016.01.17  |        5.0.0-beta3     |                                                                                                                                                                                                          |
|   2015.11.29  |        5.0.0-beta2     |                                                                                                                                                                                                          |
|   2015.10.18  |        5.0.0-beta1     |                                                                                                                                                                                                          |

[![](https://c1.staticflickr.com/5/4785/40984193011_e899c28e21_c.jpg "digikam 5.9.0 - Time search and Lens correction tool")](https://www.flickr.com/photos/digikam/40984193011)
[![](https://c1.staticflickr.com/5/4783/40984191931_ec5bc20553_c.jpg "digikam 5.9.0 - Albums View and Light Table")](https://www.flickr.com/photos/digikam/40984191931)

### digiKam 4.x Release Plan

|      Date     |       Version          |                                                          Comments                                                                                                                                        |
|      ---      |         ---            |                                                            ---                                                                                                                                           |
|   2015.10.11  |        4.14.0          |                                                                                                                                                                                                          |
|   2015.08.30  |        4.13.0          |                                                                                                                                                                                                          |
|   2015.07.26  |        4.12.0          |                                                                                                                                                                                                          |
|   2015.06.14  |        4.11.0          |                                                                                                                                                                                                          |
|   2015.05.10  |        4.10.0          |                                                                                                                                                                                                          |
|   2015.04.05  |        4.9.0           |                                                                                                                                                                                                          |
|   2015.02.22  |        4.8.0           |                                                                                                                                                                                                          |
|   2015.01.25  |        4.7.0           |                                                                                                                                                                                                          |
|   2014.12.14  |        4.6.0           |                                                                                                                                                                                                          |
|   2014.11.09  |        4.5.0           |                                                                                                                                                                                                          |
|   2014.10.06  |        4.4.0           |                                                                                                                                                                                                          |
|   2014.09.08  |        4.3.0           |                                                                                                                                                                                                          |
|   2014.08.03  |        4.2.0           |                                                                                                                                                                                                          |
|   2014.06.22  |        4.1.0           |                                                                                                                                                                                                          |
|   2014.05.11  |        4.0.0           | Version including all GoSC 2013 and 2014 projects.                                                                                                                                                       |
|   2014.04.13  |        4.0.0-rc        |                                                                                                                                                                                                          |
|   2014.03.30  |        4.0.0-beta4     |                                                                                                                                                                                                          |
|   2014.02.23  |        4.0.0-beta3     |                                                                                                                                                                                                          |
|   2014.01.12  |        4.0.0-beta2     |                                                                                                                                                                                                          |
|   2013.12.01  |        4.0.0-beta1     |                                                                                                                                                                                                          |

[![](https://c2.staticflickr.com/4/3929/15428982716_f238a6b06f_c.jpg "digiKam 4.x")](https://www.flickr.com/photos/digikam/15428982716/)
[![](https://c2.staticflickr.com/8/7441/11964874854_c3214b0895_c.jpg "digiKam 4.x")](https://www.flickr.com/photos/digikam/11964874854/)

### digiKam 3.x Release Plan

|      Date     |       Version          |                                                          Comments                                                                                                                                        |
|      ---      |         ---            |                                                            ---                                                                                                                                           |
|   2013.09.29  |        3.5.0           |                                                                                                                                                                                                          |
|   2013.09.01  |        3.4.0           |                                                                                                                                                                                                          |
|   2013.08.04  |        3.3.0           | Including [Face Recognition feature](http://youtu.be/iaFGy0n0R-g)                                                                                                                                        |
|   2013.07.03  |        3.3.0-beta3     |                                                                                                                                                                                                          |
|   2013.06.16  |        3.3.0-beta2     |                                                                                                                                                                                                          |
|   2013.06.02  |        3.3.0-beta1     |                                                                                                                                                                                                          |
|   2013.05.12  |        3.2.0           |                                                                                                                                                                                                          |
|   2013.04.28  |        3.2.0-beta2     |                                                                                                                                                                                                          |
|   2013.04.07  |        3.2.0-beta1     | Including new [Table-View Mode](http://www.flickr.com/photos/digikam/8632965113/sizes/l/in/photostream/)                                                                                                 |
|   2013.03.10  |        3.1.0           | Including bugs-fixes detected by [Coverity SCAN](http://scan.coverity.com/)                                                                                                                              |
|   2013.02.06  |        3.0.0           | Version including all GoSC 2012 projects.                                                                                                                                                                |

[![](https://c1.staticflickr.com/9/8462/7919791574_1cdbff1b6d_c.jpg "digiKam 3.x")](https://www.flickr.com/photos/digikam/7919791574/)
[![](https://c1.staticflickr.com/9/8310/8025492511_a2a17d02ac_c.jpg "digiKam 3.x")](https://www.flickr.com/photos/digikam/8025492511/i)

### digiKam 2.x Release Plan

|      Date     |       Version          |                                                          Comments                                                                                                                                        |
|      ---      |         ---            |                                                            ---                                                                                                                                           |
|   2012.09.02  |        2.9.0           |                                                                                                                                                                                                          |
|   2012.08.05  |        2.8.0           |                                                                                                                                                                                                          |
|   2012.07.08  |        2.7.0           |                                                                                                                                                                                                          |
|   2012.05.06  |        2.6.0           | Version including Color and Pick labels, Tags keyboard shortcuts, Group of items.                                                                                                                        |
|   2012.01.08  |        2.5.0           | Version including XMP Sidecar support, Face Recognition, Image Versioning, and Reverse Geo-location.                                                                                                     |
|   2011.12.04  |        2.4.0           |                                                                                                                                                                                                          |
|   2011.11.06  |        2.3.0           |                                                                                                                                                                                                          |
|   2011.10.02  |        2.2.0           |                                                                                                                                                                                                          |
|   2011.09.04  |        2.1.0           |                                                                                                                                                                                                          |
|   2011.07.24  |        2.0.0           |                                                                                                                                                                                                          |

[![](https://c1.staticflickr.com/7/6161/6210520128_8d6548d9c8_z.jpg "digiKam 2.x")](http://www.flickr.com/photos/digikam/6210520128/)

### digiKam 1.x Release Plan

|      Date     |       Version          |                                                          Comments                                                                                                                                        |
|      ---      |         ---            |                                                            ---                                                                                                                                           |
|   2011.02.19  |        1.9.0           |                                                                                                                                                                                                          |
|   2011.01.23  |        1.8.0           |                                                                                                                                                                                                          |
|   2010.12.19  |        1.7.0           |                                                                                                                                                                                                          |
|   2010.11.22  |        1.6.0           |                                                                                                                                                                                                          |
|   2010.10.10  |        1.5.0           |                                                                                                                                                                                                          |
|   2010.08.22  |        1.4.0           |                                                                                                                                                                                                          |
|   2010.06.07  |        1.3.0           |                                                                                                                                                                                                          |
|   2010.03.28  |        1.2.0           |                                                                                                                                                                                                          |
|   2010.01.31  |        1.1.0           |                                                                                                                                                                                                          |
|   2009.12.20  |        1.0.0           |                                                                                                                                                                                                          |

### digiKam 0.x Release Plan

|      Date     |       Version          |                                                          Comments                                                                                                                                        |
|      ---      |         ---            |                                                            ---                                                                                                                                           |
|   2009.05.30  |        0.10.0          |                                                                                                                                                                                                          |
|   2009.03.15  |        0.9.5           |                                                                                                                                                                                                          |
|   2008.07.16  |        0.9.4           |                                                                                                                                                                                                          |
|   2007.12.23  |        0.9.3           |                                                                                                                                                                                                          |
|   2007.06.13  |        0.9.2           |                                                                                                                                                                                                          |
|   2007.03.04  |        0.9.1           |                                                                                                                                                                                                          |
|   2006.12.18  |        0.9.0           |                                                                                                                                                                                                          |
|   2006.07.10  |        0.8.0           |                                                                                                                                                                                                          |
|   2005.05.21  |        0.7.0           |                                                                                                                                                                                                          |
|   2003.12.08  |        0.6.0           |                                                                                                                                                                                                          |
|   2002.10.03  |        0.5.0           |                                                                                                                                                                                                          |
|   2002.05.10  |        0.4.0           |                                                                                                                                                                                                          |
|   2002.06.07  |        0.3.0           |                                                                                                                                                                                                          |
|   2002.03.03  |        0.2.0           |                                                                                                                                                                                                          |
|   2001.12.24  |        0.1.0           |                                                                                                                                                                                                          |
