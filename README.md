![](logo.png) This repository hosts the digiKam website based on [Hugo](https://en.wikipedia.org/wiki/Hugo_(software)) framework.

# Changes Workflow

A git "dev" branch was create to check changes online with https://staging.digikam.org before to push new revision officially in https://www.digikam.org
To process with git, follow step below:

- From up to date git master branch, create a new local "dev" branch:

```
git checkout -b dev remotes/origin/dev
```

- Optionaly, synchronize this branch with master branch, in case of:

```
git merge master && git push
```

- Made changes in dev branch and check the result to [staging.digikam.org](http://staging.digikam.org).
  This url is updated [few minutes after a commit](https://binary-factory.kde.org/view/Websites/job/Website_staging-digikam-org/) to dev branch.

- When all is fine, backport your changes done in dev branch to master branch:

```
git checkout master && git merge dev && git push
```

- All your last changes must be visible to [www.digikam.org](https://www.digikam.org).
  This url is updated [few minutes later](https://binary-factory.kde.org/view/Websites/job/Website_digikam-org/) automatically.

# Front Page Carousel Content Update

The carousel from front page is based on [Foundation Orbit](https://foundation.zurb.com/sites/docs/v/5.5.3/components/orbit.html)

The images and caption text are included in this index.html file:

/themes/hugo-theme-digikam/layouts/index.html

Follow the same convention to add another .orbit-slide 'li' element
and the associated 'figcaption' text.

Don't forget to add another 'button' tag below it, increment the
'data-slide="X"' value and add appropriate text for screen readers.

The carousel PNG image (1024x768) are located at /static/img/carousel/

# New Release metadata Update

When a new release is ready, the /data/release.yml just needs to be updated.
Update the 'version' value, and each binary file name.

b64 stands for 64-bit releases.
